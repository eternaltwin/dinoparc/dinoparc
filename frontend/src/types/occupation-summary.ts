export interface OccupationData {
    location: string;
    faction: string;
    percentage: string;
}

export interface OccupationSummary {
    nextRefresh: number;
    occupationData: OccupationData[];
    alivePVEWarEnnemies: [];
}