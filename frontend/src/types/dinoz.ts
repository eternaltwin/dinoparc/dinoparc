import { PlaceNumber } from "../containers/utils/LocationImageByPlaceNumber";
import { ElementsValues } from "./elements-values";

export interface Dinoz {
  id: string;
  name: string;
  level: number;
  appearanceCode: string;
  skillsMap: { [key: string]: null | number };
  elementsValues: ElementsValues;
  danger: number;
  inTourney: boolean;
  actionsMap: { [key: string]: any };
  placeNumber: PlaceNumber;
  passiveList: Record<string, unknown>;
  malusList: string[];
  life: number;
  experience: number;
  race: string;
  cherryEffectMinutesLeft: number;
  bossInfos?: null | BossInfo;
  maxLife?: null | number;
  taggedAsClanEnnemy: boolean;
  dinozIsActive: boolean;
}

interface BossInfo {
  bossId: string;
  bossCategory: string;
  armyDinoz: null | ArmyDinoz;
}

interface ArmyDinoz {
  armyDinozId: string;
  bossId: string;
  bossLifeLost: 0;
  dinozId: string;
  masterId: string;
  maxNbAttacks: number;
  nbAttacks: number;
  rank: number;
}

export interface ArmyOverview {
  armyDinozs: ArmyOverviewDinoz[];
  boss: Dinoz;
}

interface ArmyOverviewDinoz {
  armyDinozId: string;
  dinozName: string;
  masterName: string;
  bossLifeLost: 0;
  maxNbAttacks: number;
  nbAttacks: number;
  rank: number;
}
