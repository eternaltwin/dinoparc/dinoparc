export type HistoryType =
    | "fight"
    | "buyDinoz"
    | "buyObject"
    | "death"
    | "newDay"
    | "buy_irma"
    | "buy_pruniac"
    | "buyObjectCrater"
    | "buyObjectAnomaly"
    | "buyObjectPlains"
    | "buyObjectSecretLair"
    | "level_up"
    | "fled"
    | "fusion"
    | "sellToJean"
    | "sacrifice"
    | "steal_home"
    | "steal_victim"
    | "buy_bons"
    | "sell_bons"
    | "bazarListing"
    | "poison_regular"
    | "poison_piqure"
    | "sellIngredients";

export interface History {
  buyAmount?: number | null;
  expGained?: number | null;
  fromDinozName?: string | null;
  fromUserName?: string | null;
  hasWon: boolean;
  icon: string;
  lifeLost?: number | null;
  numericalDate: string;
  object?: string | null;
  playerId: string;
  seen: boolean;
  toDinozName?: string | null;
  toUserName?: string | null;
  type: HistoryType;
}
