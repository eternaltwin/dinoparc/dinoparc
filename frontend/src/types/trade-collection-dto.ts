export interface TradeCollectionResponseDto {
    nbOfGiftTickets: number;
    nbOfGiftMagikTickets: number;
    success: boolean;
}
