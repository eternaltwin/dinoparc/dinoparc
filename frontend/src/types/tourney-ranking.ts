export interface TourneyRanking {
  position: number;
  dinozName: string;
  masterName: string;
  nbPoints: number;
  level: number;
  alive: boolean;
}
