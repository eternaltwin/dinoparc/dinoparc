export interface BazarItem {
  active: boolean;
  dinozId: string;
  endDateInEpochSeconds: number;
  id: string;
  initialDurationInDays: number;
  lastBiderId: string | null;
  lastBiderName: string | null;
  lastPrice: number;
  objects: { [key: string]: number };
  sellerId: string;
  sellerName: string;
}
