export interface ArenaDto {
  levelGapIsValid: unknown;
  fighterIsValid: unknown;
  displayingReward: unknown;
  isDisplayingReward: unknown;
  actualStage: unknown;
  actualFighterId: unknown;
  actualWins: unknown;
  neededWins: unknown;
  ennemyNameFr: unknown;
  ennemyNameEs: unknown;
  ennemyNameEn: unknown;
  actualFighterName: unknown;
  appearanceCode: string;
  ennemyLevel: unknown;
  elementMajeur: unknown;
  rewards: unknown[];
}
