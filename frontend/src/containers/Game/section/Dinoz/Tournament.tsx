import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import { Dinoz } from "../../../../types/dinoz";
import Message from "../../../../components/Message";
import Progress from "../../../../components/Progress";
import GameSection from "./shared/GameSection";
import getLocationImageByNumber, {
  PlaceNumber,
} from "../../../utils/LocationImageByPlaceNumber";
import ContentWithImage, {
  ImageWrap,
} from "../../../../components/ContentWithImage";
import Action from "../../../../components/Action";
import { useUserData } from "../../../../context/userData";
import { Tourney } from "../../../../types/tourney";
import { t } from "i18next";
import getActionImageFromActionString from "../../../utils/ActionImageFromString";
import getCollectionImageByString from "../../../utils/CollectionImageByInteger";
import Tooltip from "../../../../components/Tooltip";
import { Loader } from "../../../../components/Loader";
import { ProcessAction } from ".";
import { FightDataPayload } from "../../../../types/fight-data-payload";
import AsyncSection from "../../../../components/AsyncSection";
import {
  getTournament,
  getTournamentAccess,
  getTournamentFight,
  getTournamentQuit,
} from "../../../../services/tournament.api";

import "./Tournament.scss";

type Props = {
  dinoz: Dinoz;
  processAction: ProcessAction;
  returnToDinoz: () => void;
  goToFight: (data: FightDataPayload) => void;
  refreshHistory: () => void;
  reloadMyDinoz: () => void;
};

export default function TournamentSection(props: Props) {
  const { t } = useTranslation();
  const { accountId } = useUserData();
  const [isLoading, setIsLoading] = useState(false);
  const [tournoi, setTournoi] = useState<Tourney | false>(false);

  const [hasAccessToTournament, setHasAccessToTournament] = useState(true);
  const [isLoadingTourneyFight, setIsLoadingTourneyFight] = useState(false);
  const [errorTourney, setErrorTourney] = useState(false);
  const [dinozInTourney, setDinozInTourney] = useState(props.dinoz.inTourney);

  const dinozId = props.dinoz.id;
  if (props.dinoz.placeNumber in TournamentNameByPlaceNumber === false) {
    throw new Error("Place number not valid");
  }
  const dinozPlaceNumber = props.dinoz.placeNumber as AvailablePlaceNumber;
  const tournament = getTournamentStringByDinozPlaceNumber(dinozPlaceNumber);

  useEffect(() => {
    setIsLoading(true);

    if (!dinozInTourney) {
      getTournamentAccess({ accountId, dinozId, tournament }).then(
        ({ data }) => {
          setHasAccessToTournament(data);
          if (!data) {
            setDinozInTourney(false);
          }
          setIsLoading(false);
        }
      );
      return;
    }

    getTournament({ accountId, dinozId, tournament }).then(({ data }) => {
      setTournoi(data);
      setIsLoading(false);
    });
  }, [accountId, dinozId, tournament, dinozInTourney]);

  function activateTournamentView() {
    setDinozInTourney(true);
  }

  function makeTournamentFight() {
    setIsLoadingTourneyFight(true);
    getTournamentFight({ accountId, dinozId, tournament }).then(({ data }) => {
      if (!data) {
        setErrorTourney(true);
      } else {
        props.goToFight(data);
        props.refreshHistory();
      }

      setIsLoadingTourneyFight(false);
    });
  }

  function quitTournament() {
    getTournamentQuit({ accountId, dinozId, tournament }).then(({ data }) => {
      if (data === true) {
        setDinozInTourney(false);
        props.reloadMyDinoz();
        props.returnToDinoz();
      }
    });
  }

  return (
    <GameSection
      title={t(getTournamentStringByDinozPlaceNumber(dinozPlaceNumber))}
      returnToDinoz={props.returnToDinoz}
    >
      <TournamentIntro placeNumber={dinozPlaceNumber} />

      <AsyncSection loading={isLoading}>
        {hasAccessToTournament === false ? (
          <Message type="danger">
            {dinozPlaceNumber === PlaceNumber.Dinoville
              ? t("noAccessDinoville")
              : dinozPlaceNumber === PlaceNumber.temple
              ? t("noAccessZen")
              : "ERROR"}
          </Message>
        ) : dinozInTourney && tournoi !== false ? (
          <TournamentDetail
            tournoi={tournoi}
            errorTourney={errorTourney}
            actions={
              <TournamentActions
                dinoz={props.dinoz}
                isLoadingTourneyFight={isLoadingTourneyFight}
                makeTournamentFight={makeTournamentFight}
                processAction={props.processAction}
                quitTournament={quitTournament}
                reloadMyDinoz={props.reloadMyDinoz}
              />
            }
          />
        ) : (
          <div>
            <Action
              imgSrc={getActionImageFromActionString("Tournament")}
              onClick={() => activateTournamentView()}
            >
              {t("participate")}
            </Action>
          </div>
        )}
      </AsyncSection>
    </GameSection>
  );
}

const TournamentNameByPlaceNumber = {
  [PlaceNumber.Dinoplage]: "TournoideDinoplage",
  [PlaceNumber.montdino]: "TournoiduMontDino",
  [PlaceNumber.temple]: "TournoiduTempleBouddhiste",
  [PlaceNumber.mayinca]: "TournoidesRuinesMayincas",
  [PlaceNumber.Dinoville]: "TournoideDinoville",
} as const;

type AvailablePlaceNumber = keyof typeof TournamentNameByPlaceNumber;

function getTournamentStringByDinozPlaceNumber(
  placeNumber: AvailablePlaceNumber
) {
  return TournamentNameByPlaceNumber[placeNumber];
}

const tournamenetContentKey = {
  [PlaceNumber.Dinoplage]: "textTournoiDinoplage",
  [PlaceNumber.montdino]: "textTournoiMontDino",
  [PlaceNumber.temple]: "textTournoiZen",
  [PlaceNumber.mayinca]: "textTournoiRuines",
  [PlaceNumber.Dinoville]: "textTournoiDinoville",
} as const;

const TournamentIntro = ({
  placeNumber,
}: {
  placeNumber: AvailablePlaceNumber;
}) => {
  return (
    <ContentWithImage className="mb-3">
      <ImageWrap>
        <img
          alt=""
          className="imgIrma"
          src={getLocationImageByNumber(placeNumber)}
        />
      </ImageWrap>
      <p>{t(tournamenetContentKey[placeNumber])}</p>
    </ContentWithImage>
  );
};

const TournamentDetail = ({
  tournoi,
  errorTourney,
  actions,
}: {
  tournoi: Tourney;
  errorTourney?: boolean;
  actions: React.ReactNode;
}) => {
  return (
    <div>
      {errorTourney === true && (
        <Message className="mb-3" type="danger">
          {" "}
          {t("noContestantsError")}
        </Message>
      )}

      <div className="tournamentSectionTitle">{t("objectifs")}</div>
      <div className="tournamentGrid">
        <RewardBox type="bronze" actualPoints={tournoi.actualPoints} />
        <RewardBox type="silver" actualPoints={tournoi.actualPoints} />
        <RewardBox type="gold" actualPoints={tournoi.actualPoints} />
      </div>

      <div className="tournamentSectionTitle mt-4">{t("situation")}</div>
      <div className="tournamentGrid">
        <div className="tournamentBox tournamentResume">
          <div>
            <strong>{t("actualPoints")}</strong> {" : "} {tournoi.actualPoints}
          </div>
          <div>
            <strong>{t("position")}</strong> {" : "} {tournoi.position}
          </div>
          <div>
            <strong>{t("inscrits")}</strong> {" : "} {tournoi.nbInscrits}{" "}
            {t("joueurs")}
          </div>
        </div>

        <div className="tournamentBox tournamentUserResume">
          <ul className="listStatsTor">
            <li>
              <strong>
                {t("victoires")} {" : "} {tournoi.currentWins}
              </strong>
            </li>
            <li>
              <strong>
                {t("defaites")} {" : "} {tournoi.currentLosses}
              </strong>
            </li>
            <li>
              {t("victoiresTotal")} {" : "} {tournoi.totalWins}
            </li>
            <li>
              {t("defaitesTotal")} {" : "} {tournoi.totalLosses}
            </li>
            <li>
              {t("recordPts")} {" : "} {tournoi.record}
            </li>
          </ul>
        </div>

        <div className="tournamentBox tournamentActions">{actions}</div>
      </div>

      <TournamentRankingTable rankings={tournoi.rankings} />
    </div>
  );
};

const RewardBox = ({
  type,
  actualPoints,
}: {
  type: "bronze" | "silver" | "gold";
  actualPoints;
}) => {
  const imageSrc = getCollectionImageByString(
    type === "bronze" ? "3" : type === "silver" ? "2" : "1"
  );
  const target = type === "bronze" ? 5 : type === "silver" ? 10 : 15;

  return (
    <div className="rewardsTourneys tournamentBox">
      <div className="medals">{t(`medal_${type}`)}</div>
      <img alt="" src={imageSrc} />
      <div className="target">
        {target} {t("points")}
      </div>
      <Progress value={actualPoints} max={target} />
    </div>
  );
};

const TournamentActions = ({
  dinoz,
  isLoadingTourneyFight,
  makeTournamentFight,
  processAction,
  reloadMyDinoz,
  quitTournament,
}: {
  dinoz: Dinoz;
  isLoadingTourneyFight?: boolean;
  makeTournamentFight: (dinoz: Dinoz) => void;
  processAction: ProcessAction;
  reloadMyDinoz: () => void;
  quitTournament: () => void;
}) => {
  const dinozCanFight = dinoz.actionsMap["Combat"] === true;

  return (
    <div className="tournamentActionsList">
      {dinozCanFight === true ? (
        isLoadingTourneyFight !== true ? (
          <Action
            imgSrc={getActionImageFromActionString("Combat")}
            onClick={() => {
              makeTournamentFight(dinoz);
            }}
            data-for="tournamentTooltip"
            data-place="right"
            data-tip={t("tooltip.fightTor")}
          >
            {t("actionTor1")}
          </Action>
        ) : (
          <Loader size="small" />
        )
      ) : (
        <Action
          imgSrc={getActionImageFromActionString("Potion Irma")}
          onClick={(e) => {
            processAction(dinoz.id, "Potion Irma", e);
          }}
          data-for="tournamentTooltip"
          data-place="right"
          data-tip={t("tooltip.Potion Irma")}
        >
          {t("Potion Irma")}
        </Action>
      )}

      <Action
        imgSrc={getActionImageFromActionString("Here")}
        onClick={() => {
          reloadMyDinoz();
        }}
        data-for="tournamentTooltip"
        data-place="right"
        data-tip={t("tooltip.returnTor")}
      >
        {t("actionTor2")}
      </Action>

      <Action
        imgSrc={getActionImageFromActionString("Cancel")}
        onClick={() => {
          quitTournament();
        }}
        data-for="tournamentTooltip"
        data-place="right"
        data-tip={t("tooltip.cancelTor")}
      >
        {t("actionTor3")}
      </Action>

      <Tooltip id="tournamentTooltip" />
    </div>
  );
};

const TournamentRankingTable = ({
  rankings,
}: {
  rankings: Tourney["rankings"];
}) => {
  return (
    <table className="tournamentTable mt-4 has-text-centered">
      <colgroup>
        <col/>
        <col width="180"/>
        <col/>
        <col width="180"/>
        <col/>
      </colgroup>
      <thead>
        <tr>
          <th colSpan={5} className="py-2">
            {t("rankingsLabel")}
          </th>
        </tr>
        <tr>
          <th>{t("position")}</th>
          <th>Dinoz</th>
          <th>{t("niveau")}</th>
          <th>{t("maitre")}</th>
          <th>{t("classement.nbPoints")}</th>
        </tr>
      </thead>
      <tbody>
      {rankings.map((ranking, i) => {
        return (
            <>
              {ranking.alive && <tr className="dinozAliveInTourney" key={JSON.stringify(ranking)}>
                <td>{ranking.position}</td>
                <td>{ranking.dinozName}</td>
                <td>{ranking.level}</td>
                <td>{ranking.masterName}</td>
                <td>{ranking.nbPoints}</td>
              </tr>
              }

              {!ranking.alive && <tr className="dinozDeadInTourney" key={JSON.stringify(ranking)}>
                <td>{ranking.position}</td>
                <td>{ranking.dinozName}</td>
                <td>{ranking.level}</td>
                <td>{ranking.masterName}</td>
                <td>{ranking.nbPoints}</td>
              </tr>
              }
            </>
        );
      })}
      </tbody>
    </table>
  );
};
