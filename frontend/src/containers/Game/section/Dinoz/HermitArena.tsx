import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useState } from "react";
import { MoonLoader } from "react-spinners";
import axios from "axios";
import { apiUrl } from "../../../../index";
import ReactSWF from "react-swf";
import getElementImageByString from "../../../utils/ElementImageByString";
import guards from "../../../../media/game/arena_guards.png";
import hermit from "../../../../media/game/hermit.png";
import getHistoryImageFromKey from "../../../utils/HistoryImageFromKey";
import tinycoins from "../../../../media/minis/tiny_coin.gif";
import fightSwf from "../../../../media/swf/fight.swf";
import wbf from "../../../../media/game/winBannerFight.png";
import lbf from "../../../../media/game/lossBannerFight.png";
import getConsumableByKey from "../../../utils/ConsumableByKey";
import ReactTooltip from "react-tooltip";
import getActionImageFromActionString from "../../../utils/ActionImageFromString";
import { ArenaDto } from "../../../../types/arena-dto";
import { FightDataPayload } from "../../../../types/fight-data-payload";
import emptyDinoz from "../../../../media/dinoz/emptyDinoz.png";
import DinozRenderTile from "../../shared/DinozRenderTile";
import urlJoin from "url-join";
import emptyFight from "../../../../media/dinoz/fight/emptyFight.png";
import lostElement from "../../../../media/dinoz/fight/lostElement.png";

export default function HermitArena(props) {
  const { t, i18n } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(true);
  let [arenaDto, setArenaDto] = useState<Partial<ArenaDto>>({});
  let [somethingAintRight, setSomethingAintRight] = useState(false);
  let [needPotion, setNeedPotion] = useState(false);
  let [reload, setReload] = useState(false);
  let [dinoz, setDinoz] = useState({});
  let [irmaSuccess, setIrmaSuccess] = useState(0);
  let [sectionCurrentlyActive, setSectionCurrentlyActive] = useState(1);
  let [isLoadingFight, setIsLoadingFight] = useState(false);
  let [fightDataPayload, setFightDataPayload] = useState<
    Partial<FightDataPayload>
  >({});
  let [fightStep, setFightStep] = useState(1);

  useEffect(() => {
    setSectionCurrentlyActive(1);
    setIsLoading(true);
    axios
      .get(urlJoin(apiUrl, "account", store.getAccountId(), "arena", props.dinozId))
      .then(({ data }) => {
        arenaDto = data;
        setArenaDto(arenaDto);
        setNeedPotion(false);
        setSomethingAintRight(false);

        axios.get(urlJoin(apiUrl, "account", store.getAccountId(), props.dinozId))
          .then(({ data }) => {
            dinoz = data;
            setDinoz(dinoz);
            validateInformations(dinoz);
            setIsLoading(false);
          });
      });
  }, [props.dinozId, reload]);

  function validateInformations(dinoz) {
    let somethingAintRight = false;

    if (!arenaDto.levelGapIsValid) {
      somethingAintRight = true;
      setSomethingAintRight(somethingAintRight);
    }

    if (!arenaDto.fighterIsValid) {
      somethingAintRight = true;
      setSomethingAintRight(somethingAintRight);
    }

    if (arenaDto.displayingReward) {
      somethingAintRight = true;
      setSomethingAintRight(somethingAintRight);
    }

    if (dinoz.actionsMap["Combat"] === false) {
      needPotion = true;
      setNeedPotion(needPotion);
    }

    if (dinoz.life < 1) {
      somethingAintRight = true;
      setSomethingAintRight(somethingAintRight);
    }

    if (arenaDto.actualStage > 500) {
      setSectionCurrentlyActive(4);
    }
  }

  function takePotion() {
    axios.put(urlJoin(apiUrl, "account", store.getAccountId(), "irma", props.dinozId))
      .then(({ data }) => {
        if (data.noStockError === true) {
          irmaSuccess = 2;
          setIrmaSuccess(irmaSuccess);
        } else {
          irmaSuccess = 1;
          setIrmaSuccess(irmaSuccess);
          reloadMyDinoz();
        }
      });
  }

  function processFight() {
    window.scrollTo(0, 0);
    setSectionCurrentlyActive(2);
    setIsLoadingFight(true);

    axios.get(urlJoin(apiUrl, "account", store.getAccountId(), props.dinozId, "fightInArena"))
      .then(({ data }) => {
        let newFightId = Math.floor(Math.random() * 1000000);
        store.setOngoingFightId(newFightId);
        fightStep = 1;
        setFightStep(fightStep);

        fightDataPayload = data;
        setFightDataPayload(fightDataPayload);
        props.refreshLocations();
        props.refreshHistory();
        setIsLoadingFight(false);
        setIrmaSuccess(0);

        setTimeout(() => {
          moveToNextFightStep(newFightId);
        }, 500);
      });
  }

  function acceptRewards() {
    axios.put(urlJoin(apiUrl, "account", store.getAccountId(), props.dinozId, "acceptRewards"))
      .then(() => {
        props.refreshCash();
        reloadMyDinoz();
      });
  }

  function loadSummary() {
    props.refreshCash();
    setSectionCurrentlyActive(3);
  }

  function getPhraseFromWinOrLoss(condition) {
    if (condition === true) {
      return t("phrase.win");
    } else {
      return t("phrase.loss");
    }
  }

  function getBannerFromFightResult(fightResult) {
    if (fightResult === true) {
      return wbf;
    } else {
      return lbf;
    }
  }

  function getCorrectEffectLang(fightDataPayload) {
    if (i18n.language === "fr") {
      return fightDataPayload.skillsEffectsFr;
    } else if (i18n.language === "es") {
      return fightDataPayload.skillsEffectsEs;
    } else if (i18n.language === "en") {
      return fightDataPayload.skillsEffectsEn;
    }
  }

  function getCorrectNameForLang(fightDataPayload) {
    if (i18n.language === "fr") {
      return fightDataPayload.rightDinozName.split("-")[0];
    } else if (i18n.language === "es") {
      return fightDataPayload.rightDinozName.split("-")[1];
    } else if (i18n.language === "en") {
      return fightDataPayload.rightDinozName.split("-")[2];
    }
  }

  function reloadMyDinoz() {
    reload = !reload;
    setReload(reload);
  }

  function getCorrectCharmLogLineLang(fightDataPayload) {
    if (i18n.language === "fr") {
      return fightDataPayload.charmsEffectsFr;
    } else if (i18n.language === "es") {
      return fightDataPayload.charmsEffectsEs;
    } else if (i18n.language === "en") {
      return fightDataPayload.charmsEffectsEn;
    }
  }

  function getElementFightImage(dinozElement, marginLeft) {
    return (
        <>
          <img className="fightElementImg" alt="" src={getElementImageByString(dinozElement)}
               style={{marginLeft: marginLeft + 'px'}}/>
        </>
    );
  }

  function moveToNextFightStep(newFightId) {
    // Step 2 (2s after step 1), start first element animation
    // Step 3 (2s after step 2), show first element score
    // Step 4 (2s after step 3), show global score, start second element animation
    // Step 5 (2s after step 4), show second element score
    // Step 6 (2s after step 5), show global score, start third element animation
    // Step 7 (2s after step 6), show third element score
    // Step 8 (2s after step 7), show global score
    if (store.getOngoingFightId() === newFightId && fightStep < 8) {
      setTimeout(() => {
        fightStep = fightStep + 1;
        setFightStep(fightStep);
        moveToNextFightStep(newFightId);
      }, 2000);
    }
  }

  function getScorePercentage(nbElement) {
    let scoreFrom = 0;
    let absScoreFrom = 0;
    if (nbElement === 1) {
      scoreFrom = fightDataPayload.roundOneScore as number;
    } else if (nbElement === 2) {
      scoreFrom = fightDataPayload.roundTwoScore as number;
    } else {
      scoreFrom = fightDataPayload.roundThreeScore as number;
    }

    absScoreFrom = scoreFrom > 0 ? scoreFrom : -scoreFrom;
    let percentToAdd = 0;

    if (absScoreFrom > 10000) {
      percentToAdd = 50;
    } else if (absScoreFrom > 1000) {
      percentToAdd = 40 + (absScoreFrom - 1000) / 900;
    } else if (absScoreFrom > 300) {
      percentToAdd = 30 + (absScoreFrom - 300) / 70;
    } else if (absScoreFrom > 100) {
      percentToAdd = 20 + (absScoreFrom - 100) / 20;
    } else if (absScoreFrom > 20) {
      percentToAdd = 10 + (absScoreFrom - 20) / 8;
    } else {
      percentToAdd = absScoreFrom / 2;
    }

    return (scoreFrom > 0 ? 50 + percentToAdd : 50-percentToAdd) + 'px';
  }

  function getLifeHeight(left) {
    let life = left ? fightDataPayload.initialHpLeft as number : fightDataPayload.initialHpRight as number;

    if (fightStep > 2) {
      if ((left && fightDataPayload.roundOneScore as number < 0) || (!left && fightDataPayload.roundOneScore as number > 0)) {
        life -= fightDataPayload.lifeLossFirstRoundByLoser as number;
      }
    }
    if (fightStep > 4) {
      if ((left && fightDataPayload.roundTwoScore as number < 0) || (!left && fightDataPayload.roundTwoScore as number > 0)) {
        life -= fightDataPayload.lifeLossSecondRoundByLoser as number;
      }
    }
    if (fightStep > 6) {
      if ((left && fightDataPayload.roundThreeScore as number < 0) || (!left && fightDataPayload.roundThreeScore as number > 0)) {
        life -= fightDataPayload.lifeLossThirdRoundByLoser as number;
      }
    }
    if (life < 0) {
      life = 0;
    }
    return (life as number / 150) * 130;
  }

  function getFightLeftScore() {
    if (fightStep <= 2) {
      return 0;
    }
    let scoreFirst = fightDataPayload.roundOneScore as number > 0 ? fightDataPayload.roundOneScore as number : 0;
    if (fightStep <= 4) {
      return scoreFirst;
    }

    let scoreSecond = fightDataPayload.roundTwoScore as number > 0 ? fightDataPayload.roundTwoScore as number : 0;
    if (fightStep === 5) {
      return scoreSecond;
    }
    if (fightStep === 6) {
      return scoreFirst + scoreSecond as number;
    }

    let scoreThird = fightDataPayload.roundThreeScore as number > 0 ? fightDataPayload.roundThreeScore as number : 0;
    if (fightStep === 7) {
      return scoreThird;
    }
    return scoreFirst + scoreSecond + scoreThird;
  }

  function getFightRightScore() {
    if (fightStep <= 2) {
      return 0;
    }
    let scoreFirst = fightDataPayload.roundOneScore as number < 0 ? -(fightDataPayload.roundOneScore as number) : 0;
    if (fightStep <= 4) {
      return scoreFirst;
    }

    let scoreSecond = fightDataPayload.roundTwoScore as number < 0 ? -(fightDataPayload.roundTwoScore as number) : 0;
    if (fightStep === 5) {
      return scoreSecond;
    }
    if (fightStep === 6) {
      return scoreFirst + scoreSecond as number;
    }

    let scoreThird = fightDataPayload.roundThreeScore as number < 0 ? -(fightDataPayload.roundThreeScore as number) : 0;
    if (fightStep === 7) {
      return scoreThird;
    }
    return scoreFirst + scoreSecond + scoreThird;
  }

  return (
    <div>
      {sectionCurrentlyActive === 4 && (
        <div>
          <header className="pageCategoryHeader">{t("TheArena")}</header>
          <div className="flexDivsIrma">
            <img alt="" className="imgIrma" src={guards} />
            <span className="textIrma">{t("TheArena.text")}</span>
          </div>
          <h2 className="miniHeaders2">{t("TheArena.rules")}</h2>
          <ol className="textDots">
            <li className="textElementDotted">{t("rule1")}</li>
            <li className="textElementDotted">{t("rule2")}</li>
            <li className="textElementDotted">{t("rule3")}</li>
            <li className="textElementDotted">{t("rule4")}</li>
            <li className="textElementDotted">{t("rule5")}</li>
            <li className="textElementDotted">{t("rule6")}</li>
            <li className="textElementDotted">{t("rule7")}</li>
          </ol>
          {isLoading === true && (
            <MoonLoader color="#c37253" css="margin-left : 240px;" />
          )}
          {isLoading === false && (
            <div>
              <h2 className="miniHeaders2">{t("TheArena.ermite")}</h2>
              <br />
              <div className="flexDivsIrma">
                <img alt="" className="imgIrma" src={hermit} />
                <span className="textIrma">{t("TheArena.hermitTalk")}</span>
              </div>
            </div>
          )}
        </div>
      )}

      {sectionCurrentlyActive === 3 && (
        <div>
          {isLoadingFight === true && (
            <MoonLoader color="#c37253" css="margin-left : 240px;" />
          )}
          {isLoadingFight === false && (
            <div className="battle">
              <header className="pageCategoryHeader">{t("bilan")}</header>
              <div className="result">
                {t("VotreDinoz")}{" "}
                {" " +
                  fightDataPayload.leftDinozName +
                  getPhraseFromWinOrLoss(fightDataPayload.haveWon)}
                {" (" +
                  fightDataPayload.scoreFinalHome +
                  " : " +
                  +fightDataPayload.scoreFinalEnnemy +
                  ")"}
              </div>
              <table className="layout">
                <tbody>
                  <div className="flexDivs">
                    <img
                      alt=""
                      src={getBannerFromFightResult(fightDataPayload.haveWon)}
                      className="columnWinner"
                    />
                    <div className="rewardsOffset">
                      <div className="layoutRewards">
                        <img
                          alt=""
                          className="imgSummary"
                          src={getHistoryImageFromKey("hist_fight.gif")}
                        />
                        <div className="overflowHidden">
                          <span className="textSummary">
                            <span>{t("vieperdue")}</span>
                            <br />
                            <span className="colorBlack">
                              {"-"}
                              {fightDataPayload.finalLifeLoss as number}
                              {" %"}
                            </span>
                            <br />
                            <span className="colorBeige">
                              {t("dinozadverse")}
                            </span>
                            <br />
                            <span className="colorBlack">
                              {getCorrectNameForLang(fightDataPayload) +
                                " (" +
                                t("controlepar")}
                            </span>
                            <span className="masterName">
                              {t("TheArena.ermite")}
                            </span>
                            <span className="colorBlackNP">{")"}</span>
                          </span>
                        </div>
                      </div>

                      <div className="layoutRewards">
                        <img
                          alt=""
                          className="imgSummary"
                          src={getHistoryImageFromKey("hist_xp.gif")}
                        />
                        <div className="overflowHidden">
                          <span className="textSummary">
                            <span>{t("expgagnée")}</span>
                            <br />
                            <span className="colorBlack">
                              {"+"}
                              {fightDataPayload.experienceWon as number}
                              {" %"}
                            </span>
                          </span>
                        </div>
                      </div>

                      <div className="layoutRewards">
                        <img
                          alt=""
                          className="imgSummary"
                          src={getHistoryImageFromKey("hist_error.gif")}
                        />
                        <div className="overflowHidden">
                          <span className="textSummary">
                            <span>{t("dangerAcc")}</span>
                            <br />
                            <span className="colorBlack">
                              {"+"}
                              {fightDataPayload.dangerAccumulated} {t("points")}
                            </span>
                          </span>
                        </div>
                      </div>

                      <div className="layoutRewards">
                        <img
                          alt=""
                          className="imgSummary"
                          src={getHistoryImageFromKey("hist_buy.gif")}
                        />
                        <div className="overflowHidden">
                          <span className="textSummary">
                            <span>{t("potrouvées")}</span>
                            <br></br>
                            <span className="colorBlack">{"+"}</span>
                            <span className="colorBlackPO">
                              {fightDataPayload.moneyWon as number}
                              <img alt="" className="coin" src={tinycoins} />
                            </span>
                          </span>
                        </div>
                      </div>

                      {fightDataPayload.stolenObject != null && (
                        <div className="layoutRewards">
                          <img
                            alt=""
                            className="imgSummary"
                            src={getConsumableByKey(
                              fightDataPayload.stolenObject.toString()
                            )}
                          />
                          <div className="overflowHidden">
                            <span className="textSummary">
                              <span>{t("objetVole")}</span>
                              <br />
                              <span className="colorBlack">
                                {"+1 "}
                                {t(
                                  "boutique." +
                                    fightDataPayload.stolenObject.toString()
                                )}
                              </span>
                            </span>
                          </div>
                        </div>
                      )}

                      {fightDataPayload.charmsEffectsFr.length > 0 && (
                        <div className="layoutRewards">
                          <img
                            alt=""
                            className="imgSummary"
                            src={getHistoryImageFromKey("hist_charm.gif")}
                          />
                          <div className="overflowHidden">
                            <span className="textSummary">
                              <span>{t("charmsUsed")}</span>
                              <br></br>
                              {getCorrectCharmLogLineLang(fightDataPayload).map(
                                function (effect, idx) {
                                  return (
                                    <ul className="marginList">
                                      <li
                                        className="listBlack"
                                        dangerouslySetInnerHTML={{
                                          __html: effect,
                                        }}
                                      />
                                    </ul>
                                  );
                                }
                              )}
                            </span>
                          </div>
                        </div>
                      )}

                      <div className="layoutRewards">
                        <img
                          alt=""
                          className="imgSummary"
                          src={getHistoryImageFromKey("hist_report.gif")}
                        />
                        <div className="overflowHidden">
                          {fightDataPayload.skillsEffectsFr.length > 0 && (
                            <span className="textSummary">
                              <span>{t("rapportcombat")}</span>
                              <br />
                              {getCorrectEffectLang(fightDataPayload).map(
                                function (effect, idx) {
                                  return (
                                    <ul className="marginList">
                                      <li
                                        className="listBlack"
                                        dangerouslySetInnerHTML={{
                                          __html: effect,
                                        }}
                                      />
                                    </ul>
                                  );
                                }
                              )}
                            </span>
                          )}
                          {fightDataPayload.skillsEffectsFr.length === 0 && (
                            <span className="textSummary">
                              <span>{t("rapportcombat")}</span>
                              <br />
                              <ul>
                                <li className="listBlack">
                                  {t("nothingToSignale")}
                                </li>
                              </ul>
                            </span>
                          )}
                        </div>
                      </div>

                      <div
                        className="returnFiche"
                        onClick={(e) => {
                          reloadMyDinoz();
                        }}
                      >
                        <p>{t("returnInArena")}</p>
                      </div>
                    </div>
                  </div>
                </tbody>
              </table>
            </div>
          )}
        </div>
      )}

      {sectionCurrentlyActive === 2 && (
        <div>
          {isLoadingFight === true && (
            <MoonLoader color="#c37253" css="margin-left : 240px;" />
          )}
          {isLoadingFight === false && (
            <div>
              <header className="pageCategoryHeaderHermit">
                {t("Combat") +
                  ": " +
                  fightDataPayload.leftDinozName +
                  " vs " +
                  getCorrectNameForLang(fightDataPayload)}
              </header>
              {store.getImgMode() === "RUFFLE" && (
                <ReactSWF
                  className="swf"
                  src={fightSwf}
                  width="580"
                  height="363"
                  flashVars={
                    "swf_url=/fight.swf" +
                    "&r=" +
                    fightDataPayload.leftDinozAppCode +
                    ":" +
                    fightDataPayload.rightDinozAppCode +
                    ":" +
                    fightDataPayload.leftDinozName +
                    ":" +
                    getCorrectNameForLang(fightDataPayload) +
                    ":0:0" +
                    ":" +
                    fightDataPayload.initialHpLeft +
                    ":" +
                    fightDataPayload.initialHpRight +
                    ":" +
                    fightDataPayload.firstElementLeft +
                    ":" +
                    fightDataPayload.firstElementRight +
                    ":" +
                    fightDataPayload.roundOneScore +
                    ":" +
                    fightDataPayload.charmFirstRoundForLoser +
                    ":" +
                    fightDataPayload.lifeLossFirstRoundByLoser +
                    ":" +
                    fightDataPayload.secondElementLeft +
                    ":" +
                    fightDataPayload.secondElementRight +
                    ":" +
                    fightDataPayload.roundTwoScore +
                    ":" +
                    fightDataPayload.charmSecondRoundForLoser +
                    ":" +
                    fightDataPayload.lifeLossSecondRoundByLoser +
                    ":" +
                    fightDataPayload.thirdElementLeft +
                    ":" +
                    fightDataPayload.thirdElementRight +
                    ":" +
                    fightDataPayload.roundThreeScore +
                    ":" +
                    fightDataPayload.charmThirdRoundForLoser +
                    ":" +
                    fightDataPayload.lifeLossThirdRoundByLoser +
                    ":" +
                    fightDataPayload.leftDinozBeginMessage +
                    ":" +
                    fightDataPayload.rightDinozBeginMessage +
                    ":" +
                    fightDataPayload.leftDinozEndMessage +
                    ":" +
                    fightDataPayload.rightDinozEndMessage +
                    "&dino_url=dinoz.swf"
                  }
                  allowScriptAccess="never"
                />
              )}

              {(store.getImgMode() === "BASE_64" || store.getImgMode() === "HYBRID") && (
                  <div style={{height: "363px", width: "580px"}}>
                    <div style={{position: "absolute"}}>
                      <img alt="" src={emptyFight} style={{position: "absolute", zIndex: 1}} height="363" width="580" />

                      {/* Left dinoz : life, img, fight drops / circles */}
                      <div className="fightLifeContainer" style={{marginLeft: "23px"}}>
                        <div className="fightLifeContent" style={{height: getLifeHeight(true) + 'px', marginTop: 130 - getLifeHeight(true) + 'px'}}></div>
                      </div>
                      <div style={{position: "absolute", marginTop: "35px", marginLeft: "38px"}}>
                        <DinozRenderTile
                            appCode={fightDataPayload.leftDinozAppCode as string}
                            size={140} />
                      </div>
                      {((fightStep === 3 && fightDataPayload.roundOneScore as number < 0 && fightDataPayload.lifeLossFirstRoundByLoser as number > 0)
                              || (fightStep === 5 && fightDataPayload.roundTwoScore as number < 0 && fightDataPayload.lifeLossSecondRoundByLoser as number > 0)
                              || (fightStep === 7 && fightDataPayload.roundThreeScore as number < 0 && fightDataPayload.lifeLossThirdRoundByLoser as number > 0)) &&
                          <div style={{zIndex: 1, position: "absolute", marginTop: "70px", marginLeft: "15px"}}>
                            <div className="fightDrops">
                              <div className="fightDrop" style={{marginTop: "17px", marginLeft: "10px"}}></div>
                              <div className="fightDrop" style={{marginTop: "0px", marginLeft: "17px"}}></div>
                              <div className="fightDrop" style={{marginTop: "34px", marginLeft: "17px"}}></div>
                              <div className="fightDrop" style={{marginTop: "17px", marginLeft: "25px"}}></div>
                              <div className="fightDrop" style={{marginTop: "0px", marginLeft: "33px"}}></div>
                              <div className="fightDrop" style={{marginTop: "34px", marginLeft: "33px"}}></div>
                              <div className="fightDrop" style={{marginTop: "17px", marginLeft: "41px"}}></div>
                              <div className="fightDrop" style={{marginTop: "0px", marginLeft: "49px"}}></div>
                              <div className="fightDrop" style={{marginTop: "34px", marginLeft: "49px"}}></div>
                              <div className="fightDrop" style={{marginTop: "17px", marginLeft: "57px"}}></div>
                            </div>
                          </div>}
                      {((fightStep === 3 && fightDataPayload.roundOneScore as number < 0 && fightDataPayload.charmFirstRoundForLoser as boolean)
                              || (fightStep === 5 && fightDataPayload.roundTwoScore as number < 0 && fightDataPayload.charmSecondRoundForLoser as boolean)
                              || (fightStep === 7 && fightDataPayload.roundThreeScore as number < 0 && fightDataPayload.charmThirdRoundForLoser as boolean)) &&
                          <div style={{zIndex: 1, position: "absolute", marginLeft: "35px", marginTop: "35px"}}>
                            <div className="fightCircles"><div className="fightCircle1"></div><div className="fightCircle2"></div><div className="fightCircles3"></div></div>
                          </div>}


                      {/* Elements fights */}
                      <div style={{position: "absolute", marginTop: "35px", marginLeft: "210px"}}>
                        <div className="fightElementBackground"></div>
                        {(fightStep <= 2 || (fightStep > 2 && fightDataPayload.roundOneScore as number >= 0)) && getElementFightImage(fightDataPayload.firstElementLeft, '3')}
                        {(fightStep > 2 && fightDataPayload.roundOneScore as number < 0) &&
                            <img className="fightElementImg" alt="" src={lostElement} style={{marginLeft: "3px"}}/>}
                        <div className="fightProgressBarContainer">
                          {(fightStep === 2) && <div className="fightProgressBar" style={{width: getScorePercentage(1)}}></div>}
                          {(fightStep > 2) && <div className="fightProgressBarCompleted" style={{width: getScorePercentage(1)}}></div>}
                        </div>
                        <div className="fightElementBackground" style={{marginLeft: "136px"}}></div>
                        {(fightStep <= 2 || (fightStep > 2 && fightDataPayload.roundOneScore as number < 0)) && getElementFightImage(fightDataPayload.firstElementRight, '138')}
                        {(fightStep > 2 && fightDataPayload.roundOneScore as number >= 0) &&
                            <img className="fightElementImg" alt="" src={lostElement} style={{marginLeft: "138px"}}/>}
                      </div>

                      <div style={{position: "absolute", marginTop: "70px", marginLeft: "210px"}}>
                        <div className="fightElementBackground"></div>
                        {(fightStep <= 4 || (fightStep > 4 && fightDataPayload.roundTwoScore as number >= 0)) && getElementFightImage(fightDataPayload.secondElementLeft, '3')}
                        {(fightStep > 4 && fightDataPayload.roundTwoScore as number < 0) &&
                            <img className="fightElementImg" alt="" src={lostElement} style={{marginLeft: "3px"}}/>}
                        <div className="fightProgressBarContainer">
                          {(fightStep === 4) && <div className="fightProgressBar" style={{width: getScorePercentage(2)}}></div>}
                          {(fightStep > 4) && <div className="fightProgressBarCompleted" style={{width: getScorePercentage(2)}}></div>}
                        </div>
                        <div className="fightElementBackground" style={{marginLeft: "136px"}}></div>
                        {(fightStep <= 4 || (fightStep > 4 && fightDataPayload.roundTwoScore as number < 0)) && getElementFightImage(fightDataPayload.secondElementRight, '138')}
                        {(fightStep > 4 && fightDataPayload.roundTwoScore as number >= 0) &&
                            <img className="fightElementImg" alt="" src={lostElement} style={{marginLeft: "138px"}}/>}
                      </div>

                      <div style={{position: "absolute", marginTop: "105px", marginLeft: "210px"}}>
                        <div className="fightElementBackground"></div>
                        {(fightStep <= 6 || (fightStep > 6 && fightDataPayload.roundThreeScore as number >= 0)) && getElementFightImage(fightDataPayload.thirdElementLeft, '3')}
                        {(fightStep > 6 && fightDataPayload.roundThreeScore as number < 0) &&
                            <img className="fightElementImg" alt="" src={lostElement} style={{marginLeft: "3px"}}/>}
                        <div className="fightProgressBarContainer">
                          {(fightStep === 6) && <div className="fightProgressBar" style={{width: getScorePercentage(3)}}></div>}
                          {(fightStep > 6) && <div className="fightProgressBarCompleted" style={{width: getScorePercentage(3)}}></div>}
                        </div>
                        <div className="fightElementBackground" style={{marginLeft: "136px"}}></div>
                        {(fightStep <= 6 || (fightStep > 6 && fightDataPayload.roundThreeScore as number < 0)) && getElementFightImage(fightDataPayload.thirdElementRight, '138')}
                        {(fightStep > 6 && fightDataPayload.roundThreeScore as number >= 0) &&
                            <img className="fightElementImg" alt="" src={lostElement} style={{marginLeft: "138px"}}/>}
                      </div>

                      {/* Right dinoz : img, fight drops / circles, life */}
                      <div style={{position: "absolute", marginTop: "35px", marginLeft: "400px"}}>
                        <DinozRenderTile
                            appCode={fightDataPayload.rightDinozAppCode as string}
                            size={140}
                            transformScaleX={-1} />
                      </div>
                      {((fightStep === 3 && fightDataPayload.roundOneScore as number > 0 && fightDataPayload.lifeLossFirstRoundByLoser as number > 0)
                              || (fightStep === 5 && fightDataPayload.roundTwoScore as number > 0 && fightDataPayload.lifeLossSecondRoundByLoser as number > 0)
                              || (fightStep === 7 && fightDataPayload.roundThreeScore as number > 0 && fightDataPayload.lifeLossThirdRoundByLoser as number > 0)) &&
                          <div style={{position: "absolute", marginTop: "70px", marginLeft: "377px"}}>
                            <div className="fightDrops">
                              <div className="fightDrop" style={{marginTop: "17px", marginLeft: "10px"}}></div>
                              <div className="fightDrop" style={{marginTop: "0px", marginLeft: "17px"}}></div>
                              <div className="fightDrop" style={{marginTop: "34px", marginLeft: "17px"}}></div>
                              <div className="fightDrop" style={{marginTop: "17px", marginLeft: "25px"}}></div>
                              <div className="fightDrop" style={{marginTop: "0px", marginLeft: "33px"}}></div>
                              <div className="fightDrop" style={{marginTop: "34px", marginLeft: "33px"}}></div>
                              <div className="fightDrop" style={{marginTop: "17px", marginLeft: "41px"}}></div>
                              <div className="fightDrop" style={{marginTop: "0px", marginLeft: "49px"}}></div>
                              <div className="fightDrop" style={{marginTop: "34px", marginLeft: "49px"}}></div>
                              <div className="fightDrop" style={{marginTop: "17px", marginLeft: "57px"}}></div>
                            </div>
                          </div>}

                      {((fightStep === 3 && fightDataPayload.roundOneScore as number > 0 && fightDataPayload.charmFirstRoundForLoser as boolean)
                              || (fightStep === 5 && fightDataPayload.roundTwoScore as number > 0 && fightDataPayload.charmSecondRoundForLoser as boolean)
                              || (fightStep === 7 && fightDataPayload.roundThreeScore as number > 0 && fightDataPayload.charmThirdRoundForLoser as boolean)) &&
                          <div style={{position: "absolute", marginLeft: "395px", marginTop: "35px"}}>
                            <div className="fightCircles">
                              <div className="fightCircle1"></div>
                              <div className="fightCircle2"></div>
                              <div className="fightCircles3"></div>
                            </div>
                          </div>}
                      <div className="fightLifeContainer" style={{marginLeft: "549px"}}>
                        <div className="fightLifeContent" style={{height: getLifeHeight(false) + 'px', marginTop: 130 - getLifeHeight(false) + 'px'}}></div>
                      </div>

                      {/* Fight scores */}
                      {(fightStep === 3 || fightStep === 5 || fightStep === 7) &&
                          <div className="fightScoreLoader" style={{position: "absolute", marginTop: "222px", marginLeft: "130px"}}></div>}
                      <div className="fightScore" style={{marginLeft: "130px", fontFamily: "Impact"}}>
                        {getFightLeftScore()}
                      </div>

                      {(fightStep === 3 || fightStep === 5 || fightStep === 7) &&
                          <div className="fightScoreLoader" style={{position: "absolute", marginTop: "222px", marginLeft: "333px"}}></div>}
                      <div className="fightScore" style={{marginLeft: "333px", fontFamily: "Impact"}}>
                        {getFightRightScore()}
                      </div>
                    </div>
                  </div>
              )}

              <button className="continueAfterFight" onClick={(e) => {loadSummary();}}>
                <p className="textContinueFight">{t("continueBilan")}</p>
              </button>
            </div>
          )}
        </div>
      )}

      {sectionCurrentlyActive == 1 && (
        <div>
          <header className="pageCategoryHeader">{t("TheArena")}</header>
          <div className="flexDivsIrma">
            <img alt="" className="imgIrma" src={guards} />
            <span className="textIrma">{t("TheArena.text")}</span>
          </div>
          <h2 className="miniHeaders2">{t("TheArena.rules")}</h2>
          <ol className="textDots">
            <li className="textElementDotted">{t("rule1")}</li>
            <li className="textElementDotted">{t("rule2")}</li>
            <li className="textElementDotted">{t("rule3")}</li>
            <li className="textElementDotted">{t("rule4")}</li>
            <li className="textElementDotted">{t("rule5")}</li>
            <li className="textElementDotted">{t("rule6")}</li>
            <li className="textElementDotted">{t("rule7")}</li>
          </ol>
          {isLoading === true && (
            <MoonLoader color="#c37253" css="margin-left : 240px;" />
          )}
          {isLoading === false && (
            <div>
              <h2 className="miniHeaders2">{t("TheArena.summary")}</h2>
              {!arenaDto.isDisplayingReward && (
                <div className="arenaSummary">
                  {!arenaDto.levelGapIsValid && (
                    <div className="errorResultFuzPrix">
                      {t("TheArena.error.levelGap")}
                    </div>
                  )}

                  {!arenaDto.fighterIsValid && (
                    <div className="errorResultFuzPrix">
                      {t("TheArena.error.selected")}
                    </div>
                  )}

                  {irmaSuccess == 1 && (
                    <div className="fusionMessage">{t("irma-success")}</div>
                  )}

                  {irmaSuccess == 2 && (
                    <div className="fusionMessage">{t("noStockError")}</div>
                  )}

                  <table className="tableArenaOne">
                    <tr>
                      <td className="padNull">
                        {t("etage")} : #{arenaDto.actualStage as number}
                      </td>
                      <td className="padNull">
                        {t("victoires")} : {arenaDto.actualWins as number}/
                        {arenaDto.neededWins as number}
                      </td>
                      {arenaDto.actualFighterId == null && (
                        <td className="padNull">
                          {t("fightingDinoz")} : {"?"}
                        </td>
                      )}
                      {arenaDto.actualFighterId != null && (
                        <td className="padNull">
                          {t("fightingDinoz")} :{" "}
                          {arenaDto.actualFighterName as string}
                        </td>
                      )}
                    </tr>
                  </table>
                  <div className="tableArenaOne">
                    {i18n.language.includes("fr") && (
                      <a>
                        {t("TheArena.adversary")} :{" "}
                        {arenaDto.ennemyNameFr as string}
                      </a>
                    )}
                    {i18n.language.includes("es") && (
                      <a>
                        {t("TheArena.adversary")} :{" "}
                        {arenaDto.ennemyNameEs as string}
                      </a>
                    )}
                    {i18n.language.includes("en") && (
                      <a>
                        {t("TheArena.adversary")} :{" "}
                        {arenaDto.ennemyNameEn as string}
                      </a>
                    )}
                  </div>
                  {!arenaDto.displayingReward && (
                    <div className="chapter">
                      <div className="borderDinozFightHermit">
                        <div className="tbodyFight">
                          <td className="picBoxFight">
                            <div className="dinoSheet">
                              <div className="dinoSheetWrapFight">
                                <DinozRenderTile
                                  appCode={arenaDto.appearanceCode}
                                  size={100}
                                />
                              </div>
                            </div>
                          </td>
                          <td className="infoBoxFight">
                            <div className="fiche">{t("fiche")}</div>
                            <table className="ficheTableFight">
                              <tbody>
                                <tr>
                                  <th>{t("niveau")}</th>
                                  <th className="menuDinozFicheNiveau">
                                    <a className="levelColor">
                                      {arenaDto.ennemyLevel as number}
                                    </a>
                                  </th>
                                </tr>
                                <tr>
                                  <th>{t("maitre")}</th>
                                  <th className="playerLink">
                                    {t("TheArena.ermite")}
                                  </th>
                                </tr>
                              </tbody>
                            </table>

                            {!needPotion && !somethingAintRight && (
                              <div id="attackBtn" className="attackHermit">
                                <div
                                  className="lancercombat"
                                  onClick={(e) => {
                                    processFight();
                                  }}
                                >
                                  {t("lancercombat")}
                                </div>
                              </div>
                            )}

                            {needPotion && !somethingAintRight && (
                              <div className="attackHermit">
                                <div
                                  className="lancercombat"
                                  onClick={(e) => {
                                    takePotion();
                                  }}
                                >
                                  {t("boutique.Potion Irma")}
                                </div>
                              </div>
                            )}
                          </td>
                          <td className="infoBoxRight">
                            <div className="ficheInfosComp">
                              {t("infoComp")}
                            </div>
                            <table className="ficheTableFight">
                              <tbody>
                                <tr>
                                  <th className="majorElement">
                                    {t("elementMajeur")}
                                  </th>
                                  <img
                                    alt=""
                                    src={getElementImageByString(
                                      arenaDto.elementMajeur
                                    )}
                                  />
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              )}

              {arenaDto.displayingReward && (
                <div>
                  <br />
                  <br />
                  <p className="rewardsArena">
                    {t("vousAvezTermine") + arenaDto.actualStage + "!"}
                  </p>
                  <p className="rewardsArena">{t("youHaveGot")}</p>
                  <table className="tableRewardHermit">
                    {Object.keys(arenaDto.rewards).map(function (key) {
                      return (
                        <tr>
                          <td className="icon">
                            <img alt="" src={getConsumableByKey(key)} />
                          </td>
                          <td className="name">
                            <p className="titreObjetHermit">
                              <a className="marginRewardHermit">
                                {"x"}
                                {arenaDto.rewards[key]}
                              </a>
                              <a className="espaceNom">
                                {t("boutique." + key)}
                              </a>
                              <span
                                className="imageSpan"
                                lang={i18n.language}
                                data-place="right"
                                data-tip={t("boutique.tooltip." + key)}
                              />
                            </p>
                          </td>
                        </tr>
                      );
                    })}
                    <ReactTooltip
                      className="largetooltip"
                      html={true}
                      backgroundColor="transparent"
                    />
                  </table>
                  {isLoading === false && (
                    <button
                      className="buttonRewardsArena"
                      onClick={(e) => {
                        acceptRewards();
                      }}
                    >
                      {t("TheArena.claim")}
                    </button>
                  )}
                </div>
              )}
            </div>
          )}
        </div>
      )}
      <button className="hoverReturn" onClick={(e) => {props.returnToDinoz();}}>
        <img alt="" src={getActionImageFromActionString("🡸")} />
      </button>
    </div>
  );
}
