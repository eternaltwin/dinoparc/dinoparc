import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useState } from "react";
import { MoonLoader } from "react-spinners";
import ReactTooltip from "react-tooltip";
import ReactSWF from "react-swf";
import source from "../../../../media/lieux/zone19.gif";
import axios from "axios";
import { apiUrl } from "../../../../index";
import DinozRenderTile from "../../shared/DinozRenderTile";
import { confirmAlert } from "react-confirm-alert";
import urlJoin from "url-join";
import loaderSwf from "../../../../media/swf/loader.swf";
import fusionSwf from "../../../../media/swf/fusion.swf";
import dinoSwf from "../../../../media/swf/dinoz.swf";

export default function EggHatcher(props) {
  const store = Store.getInstance();
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState(false);
  const [nbGluon, setNbGluon] = useState(0);
  const [nbSantaz, setNbSantaz] = useState(0);
  const [nbSerpantin, setNbSerpantin] = useState(0);
  const [nbFeross, setNbFeross] = useState(0);
  const [nbCobalt, setNbCobalt] = useState(0);
  const [cantHatchError, setCantHatchError] = useState(false);
  const [stepNumber, setStepNumber] = useState(1);
  const [resultingAppCode, setResultingAppCode] = useState("");

  useEffect(() => {
    axios
      .get(urlJoin(apiUrl, "account", store.getAccountId(), "eggs"))
      .then(({ data }) => {
        setNbGluon(data[0]);
        setNbSantaz(data[1]);
        setNbSerpantin(data[2]);
        setNbFeross(data[3]);
        setNbCobalt(data[4]);
        setIsLoading(false);
      });
  }, [props.dinozId]);

  function NameDinoz(eggType) {
    confirmAlert({
      customUI: ({ onClose }) => {
        let nameInput = "";
        return (
          <div className="neoparcConfirmOverlay">
            <div className="promptName">
              <p>{t("giveNameToDinoz")}</p>
              <input
                id="nameChange"
                type="text"
                onChange={(e) => {
                  nameInput = e.target.value;
                }}
              />
              <button
                className="btnOkPrompt"
                onClick={() => {
                  confirm(eggType, nameInput.substring(0, 15));
                  onClose();
                }}
              >
                Ok!
              </button>
            </div>
          </div>
        );
      },
      closeOnEscape: true,
    });
  }

  async function confirm(eggType, name) {
    setIsLoading(true);
    axios.post(urlJoin(apiUrl, "account", store.getAccountId(), "hatch", eggType, name))
      .then(({ data }) => {
        setStepNumber(2);
        if (data !== "") {
          setCantHatchError(false);
          setResultingAppCode(data);
        } else {
          setCantHatchError(true);
        }
        setIsLoading(false);
      });
  }

  function goToHatchedDinozSummary() {
    props.refresh();
  }

  return (
    <div>
      <div>
        <header className="pageCategoryHeader">{t("EggHatcher")}</header>
        <div className="displayFusions">
          <img alt="" className="imgIrma" src={source} />
          <span className="textIrma">{t("location.source.details")}</span>
        </div>
        {isLoading === true && (
          <MoonLoader color="#c37253" css="margin-left : 240px;" />
        )}

        {stepNumber === 1 && (
          <div>
            {isLoading === false && nbGluon > 0 && (
              <button
                id="btnGive"
                className="buttonGiveaway"
                onClick={(e) => {
                  NameDinoz("EGG_11");
                }}
              >
                {t("EggHatcher") + " (Goupignon)"}
                <br />
              </button>
            )}

            {isLoading === false && nbSantaz > 0 && (
              <button
                id="btnGive"
                className="buttonGiveaway"
                onClick={(e) => {
                  NameDinoz("EGG_14");
                }}
              >
                {t("EggHatcher") + " (Santaz)"}
                <br />
              </button>
            )}

            {isLoading === false && nbSerpantin > 0 && (
              <button
                id="btnGive"
                className="buttonGiveaway"
                onClick={(e) => {
                  NameDinoz("EGG_18");
                }}
              >
                {t("EggHatcher") + " (Serpantin)"}
                <br />
              </button>
            )}

            {isLoading === false && nbFeross > 0 && (
                <button
                    id="btnGive"
                    className="buttonGiveaway"
                    onClick={(e) => {
                      NameDinoz("EGG_20");
                    }}
                >
                  {t("EggHatcher") + " (Feross)"}
                  <br />
                </button>
            )}

            {isLoading === false && nbCobalt > 0 && (
              <button
                id="btnGive"
                className="buttonGiveaway"
                onClick={(e) => {
                  NameDinoz("EGG_X");
                }}
              >
                {t("EggHatcher") + " (Cobalt)"}
                <br />
              </button>
            )}
          </div>
        )}

        {stepNumber === 2 && cantHatchError && (
          <div className="errorResultFuzPrix">{t("hatchError")}</div>
        )}
        {stepNumber === 2 && !cantHatchError && (
          <div key={resultingAppCode}>
            <div className="fusionMessage">{t("hatchSuccess")}</div>
            <div className="center">
                <div className="fusionWrapper">
                    <ReactSWF
                        src={loaderSwf}
                        flashVars={"swf_url=" + fusionSwf + "?$version=2&infos=" + "M" + ":" + "N" + ":" + resultingAppCode + "&dinozUrl=" + dinoSwf}
                        width="700"
                        height="300"
                    />
                </div>

              <p className="clickableText" onClick={(e) => {goToHatchedDinozSummary();}}>{t("continue")}</p>
            </div>
          </div>
        )}
        <ReactTooltip
          className="largetooltip"
          html={true}
          backgroundColor="transparent"
        />
      </div>
    </div>
  );
}
