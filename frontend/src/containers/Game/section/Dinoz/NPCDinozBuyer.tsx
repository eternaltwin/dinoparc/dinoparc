import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useState } from "react";
import { MoonLoader } from "react-spinners";
import ReactTooltip from "react-tooltip";
import jeanbambois from "../../../../media/pnj/forestier.png";
import tinycoins from "../../../../media/minis/tiny_coin.gif";
import axios from "axios";
import { apiUrl } from "../../../../index";
import urlJoin from "url-join";

export default function NPCDinozBuyer(props) {
  const { t } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(false);
  }, [props.dinozId]);

  function getPrice(level) {
    return 2955 + level * 631;
  }

  function confirmGiveaway(dinozId) {
    setIsLoading(true);
    axios.put(urlJoin(apiUrl, "account", store.getAccountId(), props.dinozId, "giveaway", "jeanbambois"))
      .then(({ data }) => {
        if (data) {
          setIsLoading(false);
          props.refresh();
        }
      });
  }

  return (
    <div>
      <div>
        <header className="pageCategoryHeader">{"Jean Bambois"}</header>
        <div className="displayFusions">
          <img alt="" className="imgRaymond" src={jeanbambois} />
          <span className="textFusions">{t("NPCDinozBuyer.text")}</span>
        </div>
        {isLoading === true && (
          <MoonLoader color="#c37253" css="margin-left : 240px;" />
        )}
        {isLoading === false && (
          <button
            id="btnGive"
            className="buttonGiveaway"
            onClick={(e) => {
              if (window.confirm(t("confirm"))) {
                confirmGiveaway(props.dinozId);
              }
            }}
          >
            {t("confirmGiveaway") + props.dinozName}
            <br />
            {getPrice(props.dinozLevel)}
            <img alt="" className="coin" src={tinycoins} />
          </button>
        )}
        <ReactTooltip
          className="largetooltip"
          html={true}
          backgroundColor="transparent"
        />
      </div>
    </div>
  );
}
