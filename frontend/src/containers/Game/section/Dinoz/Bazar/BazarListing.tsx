import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import Countdown from "react-countdown";
import axios from "axios";
import { apiUrl } from "../../../../../index";
import getElementImageByString from "../../../../utils/ElementImageByString";
import getElementPositionByCompetence from "../../../../utils/ElementPositionByCompetence";
import getConsumableByKey from "../../../../utils/ConsumableByKey";
import tinybill from "../../../../../media/minis/tiny_bill.gif";
import ReactTooltip from "react-tooltip";
import Store from "../../../../../utils/Store";
import { Dinoz } from "../../../../../types/dinoz";
import DinozRenderTile from "../../../shared/DinozRenderTile";
import { BazarItem } from "../../../../../types/bazar";
import urlJoin from "url-join";

type Props = {
  data: BazarItem;
  srcSection: number;
  refresh: () => void;
};

export default function BazarListing(props: Props) {
  const { t, i18n } = useTranslation();
  const store = Store.getInstance();
  const [endDate, setEndDate] = useState<Date>();
  const [dinozPresent, setDinozPresent] = useState(false);
  let [dinoz, setDinoz] = useState<Partial<Dinoz>>({});
  let [objectsPresent, setObjectsPresent] = useState(false);
  let [bonsNumberOnBid, setBonsNumberOnBid] = useState(0);
  let [isErrorAfterBid, setIsErrorAfterBid] = useState<boolean>();
  let [isErrorAfterBidNotEnoughBonsFree, setIsErrorAfterBidNotEnoughBonsFree] =
    useState<boolean>();
  let [isErrorDinozLimitOnBid, setIsErrorDinozLimitOnBid] = useState<boolean>();

  useEffect(() => {
    setIsErrorAfterBid(undefined);
    let dinozId = props.data.dinozId;
    if (dinozId !== null && dinozId !== "") {
      axios
        .get(urlJoin(apiUrl, "utils", "bazar", dinozId.toString()))
        .then(({ data }) => {
          if (data !== null && data !== undefined) {
            dinoz = data;
            setDinoz(dinoz);
            setDinozPresent(true);
          } else {
            setDinozPresent(false);
          }
        });
    }

    Object.keys(props.data.objects).forEach(function (key) {
      if (props.data.objects[key] > 0) {
        objectsPresent = true;
        setObjectsPresent(objectsPresent);
      }
    });

    const newEndDate = new Date(0);
    newEndDate.setUTCSeconds(props.data.endDateInEpochSeconds);
    setEndDate(newEndDate);
    ReactTooltip.rebuild();
  }, [props.data]);

  function bonsBidChange(e) {
    bonsNumberOnBid = e.target.value;
    setBonsNumberOnBid(bonsNumberOnBid);
  }

  function sendBidToServer() {
    axios.put(urlJoin(apiUrl, "account", store.getAccountId(), "bazar", "bid", props.data.id, String(bonsNumberOnBid)))
      .then(({ data }) => {
        isErrorAfterBid = !data[0];
        setIsErrorAfterBid(isErrorAfterBid);
        isErrorAfterBidNotEnoughBonsFree = !data[1];
        setIsErrorAfterBidNotEnoughBonsFree(isErrorAfterBidNotEnoughBonsFree);
        isErrorDinozLimitOnBid = !data[2];
        setIsErrorDinozLimitOnBid(isErrorDinozLimitOnBid);
        if (data[0] && data[1] && data[2]) {
          props.refresh();
        }
      });
  }

  return (
    <div>
      {isErrorAfterBid !== undefined && isErrorAfterBid && (
        <div className="errorResultFuzPrix">{t("bidFailure")}</div>
      )}
      {isErrorAfterBidNotEnoughBonsFree !== undefined &&
        isErrorAfterBidNotEnoughBonsFree && (
          <div className="errorResultFuzPrix">{t("bidFailureFreeBons")}</div>
        )}
      {isErrorDinozLimitOnBid !== undefined && isErrorDinozLimitOnBid && (
        <div className="errorResultFuzPrix">{t("bidFailureDinozLimit")}</div>
      )}
      <div className="bazarBorderListing">
        {dinozPresent && (
          <div className="dinoBazar">
            {dinoz.appearanceCode == null ? (
              <p>{t("dinozGone")}</p>
            ) : (
              <DinozRenderTile
                className="dinoBazar dinoPic"
                appCode={dinoz.appearanceCode}
                size={100}
              />
            )}

            {props.srcSection !== 4 && (
              <div className="dinoBazar">
                <div className="levelBazarDisplay">
                  <>{dinoz.race + " ("}</>
                  {t("niveau")}
                  {" " + dinoz.level + ")"}
                </div>
                {dinoz.skillsMap !== undefined && (
                  <select className="levelBazarDropdownDisplay">
                    <option>{t("competences")}</option>
                    {Object.keys(dinoz.skillsMap)
                      .sort((a, b) =>
                        getElementPositionByCompetence(a).localeCompare(
                          getElementPositionByCompetence(b)
                        )
                      )
                      .map(function (key) {
                        return (
                          <>
                            {dinoz.skillsMap[key] > 0 && (
                              <option disabled={true}>
                                {t(key.toString())}
                                {" : "}
                                {dinoz.skillsMap[key]}
                                {"/5"}
                              </option>
                            )}
                          </>
                        );
                      })}
                  </select>
                )}
                {dinoz.elementsValues !== null &&
                  dinoz.elementsValues !== undefined && (
                    <div className="elementsDisplay">
                      <div className="elementsLiFiche">
                        <img alt="" src={getElementImageByString("Feu")} />{" "}
                        {dinoz.elementsValues.Feu as number}
                      </div>
                      <div className="elementsLiFiche">
                        <img alt="" src={getElementImageByString("Terre")} />{" "}
                        {dinoz.elementsValues.Terre as number}
                      </div>
                      <div className="elementsLiFiche">
                        <img alt="" src={getElementImageByString("Eau")} />{" "}
                        {dinoz.elementsValues.Eau as number}
                      </div>
                      <div className="elementsLiFiche">
                        <img alt="" src={getElementImageByString("Foudre")} />{" "}
                        {dinoz.elementsValues.Foudre as number}
                      </div>
                      <div className="elementsLiFiche">
                        <img alt="" src={getElementImageByString("Air")} />{" "}
                        {dinoz.elementsValues.Air as number}
                      </div>
                    </div>
                  )}
              </div>
            )}
          </div>
        )}

        {!dinozPresent && <div className="dinoBazarFiller" />}

        <div className="dinoBazar">
          <div className="verticalBar" />
        </div>

        <div className="dinoBazarListObjectsLeft">
          <div className="dinoBazarObjectsGrid9x9">
            {Object.keys(props.data.objects).map(function (object) {
              return (
                <>
                  {props.data.objects[object] > 0 && (
                    <div className="imgObjBazar">
                      <img alt="" src={getConsumableByKey(object)} lang={i18n.language} data-place="right" data-tip={t("boutique.tooltip." + object)}/>{" x"}{props.data.objects[object]}{" "}
                    </div>
                  )}
                </>
              );
            })}
            <ReactTooltip
              className="largetooltip"
              html={true}
              backgroundColor="transparent"
            />
          </div>
        </div>

        <div className="dinoBazarHBar" />
        <div className="dinoBazar">
          <a className="actualSeller" style={{ color: "purple" }}>
            {t("Vendeur")}
            {": "}
            {props.data.sellerName}
          </a>
        </div>

        {props.srcSection !== 4 && (
          <div className="dinoBazar">
            <a style={{ color: "green" }}>
              {t("PrixActuel")}
              {": "}
              {props.data.lastPrice}
              <img alt="" className="coin" src={tinybill} />
            </a>
          </div>
        )}

        {props.srcSection === 4 && (
          <div className="dinoBazar">
            <a style={{ color: "green" }}>
              {props.data.lastPrice}
              <img alt="" className="coin" src={tinybill} />
              {" ->"}
            </a>
          </div>
        )}

        {props.srcSection !== 4 && (
          <div className="dinoBazar">
            {props.data.lastBiderName === null && (
              <a style={{ color: "green" }}>
                {t("par")}
                {": "}
                {props.data.sellerName}
              </a>
            )}
            {props.data.lastBiderName !== null && (
              <a style={{ color: "green" }}>
                {t("par")}
                {": "}
                {props.data.lastBiderName}
              </a>
            )}
          </div>
        )}

        {props.srcSection === 4 && (
          <div className="dinoBazar">
            {props.data.lastBiderName === null && (
              <a style={{ color: "green" }}>
                {t("remporte")}
                {": "}
                {props.data.sellerName}
              </a>
            )}
            {props.data.lastBiderName !== null && (
              <a style={{ color: "green" }}>
                {t("remporte")}
                {": "}
                {props.data.lastBiderName}
              </a>
            )}
          </div>
        )}

        {props.srcSection !== 4 && (
          <div className="dinoBazar">
            <a className="actualSeller" style={{ color: "red" }}>
              {t("timeLeft")}
              {": "}
              {endDate !== undefined && <Countdown date={endDate} />}
            </a>
          </div>
        )}

        {props.data.sellerId !== store.getAccountId() &&
          props.srcSection !== 4 && (
            <div className="dinoBazar">
              <a style={{ color: "green" }}>
                {t("encherir")}
                {": "}
                <input
                  className="numberFieldBazarBid"
                  type="number"
                  min={props.data.lastPrice + 1}
                  onInput={(e) => {
                    bonsBidChange(e);
                  }}
                />
                <img alt="" className="coin" src={tinybill} />
                <button
                  className="buttonBidBazar"
                  onClick={(e) => {
                    if (window.confirm(t("confirm"))) {
                      sendBidToServer();
                    }
                  }}
                >
                  {t("encherir")}
                </button>
              </a>
            </div>
          )}
      </div>
      <ReactTooltip
        className="largetooltip"
        html={true}
        backgroundColor="transparent"
      />
    </div>
  );
}
