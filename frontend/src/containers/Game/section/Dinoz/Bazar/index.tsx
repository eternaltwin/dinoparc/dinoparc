import React, { useEffect, useRef, useState } from "react";
import bazar from "../../../../../media/lieux/zone15.gif";
import { useTranslation } from "react-i18next";
import axios from "axios";
import { apiUrl } from "../../../../../index";
import { MoonLoader } from "react-spinners";
import Store from "../../../../../utils/Store";
import tinybill from "../../../../../media/minis/tiny_bill.gif";
import moveEast from "../../../../../media/actions/act_move_e.gif";
import moveWest from "../../../../../media/actions/act_move_w.gif";
import getConsumableByKey from "../../../../utils/ConsumableByKey";
import BazarListing from "./BazarListing";
import { Dinoz } from "../../../../../types/dinoz";
import DinozRenderTile from "../../../shared/DinozRenderTile";
import { BazarItem } from "../../../../../types/bazar";
import urlJoin from "url-join";

export default function Bazar(props) {
  const { t } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingFilter, setIsLoadingFilter] = useState(false);
  const [sectionCurrentlyActive, setSectionCurrentlyActive] = useState(0);
  const [sellDuration, setSellDuration] = useState(3);
  const [isErrorListing, setIsErrorListing] = useState(false);
  const [bidSuccess, setBidSuccess] = useState(false);
  const durationRef = useRef<HTMLSelectElement>();
  const sellDinozRef = useRef<HTMLInputElement>();
  const saleStartingPriceRef = useRef<HTMLInputElement>();
  const selectFilterRef = useRef<HTMLSelectElement>();
  const pageSize = 20;
  let [dinoz, setDinoz] = useState<Partial<Dinoz>>({});
  let [bonsDispos, setBonsDispos] = useState<unknown>(0);
  let [bonsEngaged, setBonsEngaged] = useState<unknown>(0);
  let [sellablesDinoz, setSellablesDinoz] = useState({});
  let [sellablesObjectsMap, setSellablesObjectsMap] = useState({});
  let [dinozIdToSell, setDinozIdToSell] = useState("");
  let [allListings, setAllListings] = useState<BazarItem[]>([]);
  let [sort, setSort] = useState("default");
  let [currentPage, setCurrentPage] = useState(0);
  let [totalPageNb, setTotalPageNb] = useState(0);

  //To add a new sellable item in the bazar, declare the hooks and set them correctly at the offer creation.
  //Hint : Declare the new objects here :
  const [numberOfPrismatikToSell, setNumberOfPrismatikToSell] = useState(0);
  const [numberOfChampifuzzToSell, setNumberOfChampifuzzToSell] = useState(0);
  const [numberOfDarkPotionToSell, setNumberOfDarkPotionToSell] = useState(0);
  const [numberOfEgg11ToSell, setNumberOfEgg11ToSell] = useState(0);
  const [numberOfEgg14ToSell, setNumberOfEgg14ToSell] = useState(0);
  const [numberOfEgg18ToSell, setNumberOfEgg18ToSell] = useState(0);
  const [numberOfEgg20ToSell, setNumberOfEgg20ToSell] = useState(0);
  const [numberOfCherriesToSell, setNumberOfCherriesToSell] = useState(0);
  const [numberOfXmasTicketsToSell, setNumberOfXmasTicketsToSell] = useState(0);
  const [numberOfEternityPillToSell, setNumberOfEternityPillToSell] = useState(0);
  const [numberOfPruniacToSell, setNumberOfPruniacToSell] = useState(0);
  const [numberOfTisaneToSell, setNumberOfTisaneToSell] = useState(0);
  const [numberOfBeerToSell, setNumberOfBeerToSell] = useState(0);
  const [numberOfEggXToSell, setNumberOfEggXToSell] = useState(0);
  const [numberOfSpecialChampiToSell, setNumberOfSpecialChampiToSell] = useState(0);
  const [numberOfEggChocolateToSell, setNumberOfEggChocolateToSell] = useState(0);
  const [numberOfMonoChampiToSell, setNumberOfMonoChampiToSell] = useState(0);
  const [numberOfMagikChampiToSell, setNumberOfMagikChampiToSell] = useState(0);
  const [numberOfBronzeCasinoTokenToSell, setNumberOfBronzeCasinoTokenToSell] = useState(0);
  const [numberOfSilverCasinoTokenToSell, setNumberOfSilverCasinoTokenToSell] = useState(0);
  const [numberOfGoldCasinoTokenToSell, setNumberOfGoldCasinoTokenToSell] = useState(0);

  useEffect(() => {
    axios
      .get(urlJoin(apiUrl, "account", store.getAccountId(), props.dinozId))
      .then(({ data }) => {
        dinoz = data;
        setDinoz(dinoz);
      });
  }, [props.dinozId]);

  function toggleBazarMode(section) {
    allListings = [];
    setAllListings(allListings);
    setIsLoadingFilter(true);
    setSectionCurrentlyActive(section);
    if (section === 1) {
      setSort("default");
      setCurrentPage(0);
      refreshSection1Page(0);
    } else if (section === 2) {
      setBidSuccess(false);
      axios.get(urlJoin(apiUrl,"account", store.getAccountId(), props.dinozId, "bazar", "sellables"))
        .then(({ data }) => {
          sellablesDinoz = data.sellableDinoz;
          setSellablesDinoz(sellablesDinoz);
          sellablesObjectsMap = data.sellableObjects;
          setSellablesObjectsMap(sellablesObjectsMap);
          setIsLoading(false);
        });
    } else if (section === 3) {
      setIsLoading(true);
      setBidSuccess(false);
      axios.get<BazarItem[]>(urlJoin(apiUrl, "utils", "all-bazar-listings", store.getAccountId(), "own"))
        .then(({ data }) => {
          allListings = data;
          setAllListings(allListings);
          axios.get(urlJoin(apiUrl, "account", store.getAccountId(), "bonsStocks"))
            .then(({ data }) => {
              bonsDispos = data[0];
              setBonsDispos(bonsDispos);
              bonsEngaged = data[1];
              setBonsEngaged(bonsEngaged);
              setIsLoading(false);
            });
        });
    } else if (section === 4) {
      setBidSuccess(false);
      axios.get<BazarItem[]>(urlJoin(apiUrl, "utils", "all-bazar-listings", store.getAccountId(), "/opt_his"))
        .then(({ data }) => {
          allListings = data;
          setAllListings([]);
          setAllListings(allListings);
          axios.get(urlJoin(apiUrl, "account", store.getAccountId(), "bonsStocks"))
            .then(({ data }) => {
              bonsDispos = data[0];
              setBonsDispos(bonsDispos);
              setIsLoading(false);
              setIsLoadingFilter(false);
            });
        });
    }
  }

  function bazarPreviousPage() {
    if (currentPage > 0) {
      const newPageNb = currentPage - 1;
      setCurrentPage(newPageNb);
      refreshSection1Page(newPageNb);
    }
  }

  function bazarNextPage() {
    if (currentPage < totalPageNb - 1) {
      const newPageNb = currentPage + 1;
      setCurrentPage(newPageNb);
      refreshSection1Page(newPageNb);
    }
  }

  function refreshSection1Page(newPageNb) {
    axios.get<BazarItem[]>(urlJoin(apiUrl, "bazar", store.getAccountId(), "?active=true&limit=" + pageSize, "&offset=" + (newPageNb * pageSize), "&category=" + sort))
        .then((response) => {
          const headers = response.headers;
          setTotalPageNb(Math.trunc((Number(headers['x-total-count']) - 1) / pageSize) + 1);
          const data = response.data;
          allListings = data;
          setAllListings(allListings);
          axios
            .get(urlJoin(apiUrl, "account", store.getAccountId(), "bonsStocks"))
            .then(({ data }) => {
              bonsDispos = data[0];
              setBonsDispos(bonsDispos);
              setIsLoading(false);
              setIsLoadingFilter(false);
            });
        });
  }

  function setSaleDuration() {
    let bufferDuration = durationRef.current.value;
    if (bufferDuration === "3d") {
      setSellDuration(3);
    } else if (bufferDuration === "5d") {
      setSellDuration(5);
    } else if (bufferDuration === "7d") {
      setSellDuration(7);
    }
  }

  //Hint : Declare the new objects here :
  function sellQtyChange(e, key) {
    if (key === "bonus_prismatik") {
      setNumberOfPrismatikToSell(e.target.value);
    } else if (key === "Ticket Champifuz") {
      setNumberOfChampifuzzToSell(e.target.value);
    } else if (key === "Potion Sombre") {
      setNumberOfDarkPotionToSell(e.target.value);
    } else if (key === "EGG_11") {
      setNumberOfEgg11ToSell(e.target.value);
    } else if (key === "EGG_14") {
      setNumberOfEgg14ToSell(e.target.value);
    } else if (key === "EGG_18") {
      setNumberOfEgg18ToSell(e.target.value);
    } else if (key === "EGG_20") {
      setNumberOfEgg20ToSell(e.target.value);
    } else if (key === "Coulis Cerise") {
      setNumberOfCherriesToSell(e.target.value);
    } else if (key === "Ticket Cadeau") {
      setNumberOfXmasTicketsToSell(e.target.value);
    } else if (key === "Eternity Pill") {
      setNumberOfEternityPillToSell(e.target.value);
    } else if (key === "Pruniac") {
      setNumberOfPruniacToSell(e.target.value);
    } else if (key === "Tisane des bois") {
      setNumberOfTisaneToSell(e.target.value);
    } else if (key === "Bière de Dinojak") {
      setNumberOfBeerToSell(e.target.value);
    } else if (key === "EGG_X") {
      setNumberOfEggXToSell(e.target.value);
    } else if (key === "Ticket Champifuz Special") {
      setNumberOfSpecialChampiToSell(e.target.value);
    } else if (key === "EGG_CHOC") {
      setNumberOfEggChocolateToSell(e.target.value);
    } else if (key === "Ticket Monochrome") {
      setNumberOfMonoChampiToSell(e.target.value);
    } else if (key === "Ticket Magik") {
      setNumberOfMagikChampiToSell(e.target.value);
    } else if (key === "JetonCasinoBronze") {
      setNumberOfBronzeCasinoTokenToSell(e.target.value);
    } else if (key === "JetonCasinoArgent") {
      setNumberOfSilverCasinoTokenToSell(e.target.value);
    } else if (key === "JetonCasinoOr") {
      setNumberOfGoldCasinoTokenToSell(e.target.value);
    }
  }

  function confirmSaleAndSendToServer() {
    setIsErrorListing(false);
    setIsLoading(true);

    if (sellDinozRef.current.checked) {
      dinozIdToSell = props.dinozId;
      setDinozIdToSell(dinozIdToSell);
    }

    //Hint : Declare the new objects here :
    let objects = {};
    objects["bonus_prismatik"] = numberOfPrismatikToSell;
    objects["Ticket Champifuz"] = numberOfChampifuzzToSell;
    objects["Potion Sombre"] = numberOfDarkPotionToSell;
    objects["EGG_11"] = numberOfEgg11ToSell;
    objects["EGG_14"] = numberOfEgg14ToSell;
    objects["EGG_18"] = numberOfEgg18ToSell;
    objects["EGG_20"] = numberOfEgg20ToSell;
    objects["Coulis Cerise"] = numberOfCherriesToSell;
    objects["Ticket Cadeau"] = numberOfXmasTicketsToSell;
    objects["Eternity Pill"] = numberOfEternityPillToSell;
    objects["Pruniac"] = numberOfPruniacToSell;
    objects["Tisane des bois"] = numberOfTisaneToSell;
    objects["Bière de Dinojak"] = numberOfBeerToSell;
    objects["EGG_X"] = numberOfEggXToSell;
    objects["Ticket Champifuz Special"] = numberOfSpecialChampiToSell;
    objects["EGG_CHOC"] = numberOfEggChocolateToSell;
    objects["Ticket Monochrome"] = numberOfMonoChampiToSell;
    objects["Ticket Magik"] = numberOfMagikChampiToSell;
    objects["JetonCasinoBronze"] = numberOfBronzeCasinoTokenToSell;
    objects["JetonCasinoArgent"] = numberOfSilverCasinoTokenToSell;
    objects["JetonCasinoOr"] = numberOfGoldCasinoTokenToSell;

    let startingPrice = saleStartingPriceRef.current.value;
    if (startingPrice == "") {
      startingPrice = "1";
    }

    if (window.confirm(t("confirmSellBazar"))) {
      axios.post(urlJoin(apiUrl, "account", store.getAccountId(), props.dinozId, "postUpForSale"),
              {
                dinozId: dinozIdToSell,
                objects: objects,
                initialDurationInDays: sellDuration,
                lastPrice: startingPrice,
              }
          )
          .then(({ data }) => {
            if (data === true) {
              setIsLoading(false);
              setSectionCurrentlyActive(0);
              props.refreshToHistory();
            } else {
              setIsErrorListing(true);
              refreshOffer();
              setIsLoading(false);
            }
          });
    } else {
      refreshOffer();
      setIsLoading(false);
    }
  }

  function refreshOffer() {
    //Hint : Reset the new objects here :
    setDinozIdToSell("");
    setNumberOfPrismatikToSell(0);
    setNumberOfChampifuzzToSell(0);
    setNumberOfDarkPotionToSell(0);
    setNumberOfEgg11ToSell(0);
    setNumberOfEgg14ToSell(0);
    setNumberOfEgg18ToSell(0);
    setNumberOfEgg20ToSell(0);
    setNumberOfCherriesToSell(0);
    setNumberOfXmasTicketsToSell(0);
    setNumberOfEternityPillToSell(0);
    setNumberOfPruniacToSell(0);
    setNumberOfTisaneToSell(0);
    setNumberOfBeerToSell(0);
    setNumberOfEggXToSell(0);
    setNumberOfSpecialChampiToSell(0);
    setNumberOfEggChocolateToSell(0);
    setNumberOfMonoChampiToSell(0);
    setNumberOfMagikChampiToSell(0);
    setNumberOfBronzeCasinoTokenToSell(0);
    setNumberOfSilverCasinoTokenToSell(0);
    setNumberOfGoldCasinoTokenToSell(0);
  }

  function refreshAllListings() {
    allListings = [];
    setAllListings(allListings);
    setBidSuccess(true);
    sort = selectFilterRef.current.value;
    setSort(sort);
    window.scrollTo(0, 0);
    toggleBazarMode(1);
  }

  function changeFilter() {
    setCurrentPage(0);
    sort = selectFilterRef.current.value;
    setSort(sort);
    setBidSuccess(false);
    setCurrentPage(0);
    refreshSection1Page(0);
  }

  return (
    <div>
      <div>
        <header className="pageCategoryHeader">{t("JazzBazar")}</header>
        <div className="displayFusions">
          <img alt="" className="imgIrma" src={bazar} />
          <span className="textIrma">{t("JazzBazarIntro")}</span>
        </div>
        <button
          className="buttonChoice"
          onClick={(e) => {
            toggleBazarMode(1);
          }}
        >
          {t("JazzBazatOpt1")}
        </button>
        <button
          className="buttonChoice"
          onClick={(e) => {
            toggleBazarMode(2);
          }}
        >
          {t("JazzBazatOpt2")}
        </button>
        <button
          className="buttonChoice"
          onClick={(e) => {
            toggleBazarMode(3);
          }}
        >
          {t("JazzBazatOpt3")}
        </button>
        <button
          className="buttonChoice"
          onClick={(e) => {
            toggleBazarMode(4);
          }}
        >
          {t("results")}
        </button>

        {sectionCurrentlyActive == 1 && (
          <div className="bazarSubComponent">
            <h2 className="miniHeaders2">{t("JazzBazatOpt1")}</h2>
            {isLoading === true && (
              <MoonLoader
                color="#c37253"
                css="margin-left : 255px; margin-top : 20px;"
              />
            )}
            {isLoading === false && (
              <div>
                <div className="textBazar">
                  {t("JazzBazarIntroListings")}
                  {t("JazzBazarRuleTime")}
                </div>

                <div className="filters">{t("filters") + ": "}</div>

                {bonsDispos !== "" && (
                  <div className="youCurrentlyHave">
                    {t("youHave")}
                    {bonsDispos as string}
                    <img alt="" className="coin" src={tinybill} />
                  </div>
                )}

                {bonsDispos === "" && (
                  <div className="youCurrentlyHave">
                    {t("youHave")}
                    {0}
                    <img alt="" className="coin" src={tinybill} />
                  </div>
                )}

                <select
                  className="choiceFilterBazar"
                  id="selectFilter"
                  ref={selectFilterRef}
                  onChange={(e) => {
                    changeFilter();
                  }}
                >
                  <option value="opt_all">{t("seeAll")}</option>
                  <option value="opt_oo">{t("objectsOnly")}</option>
                  <option value="opt_50">{t("niveau") + " 50+"}</option>
                  <option value="opt_100">{t("niveau") + " 100+"}</option>
                  <option value="opt_dark">{t("sombre")}</option>
                  <option value="opt_cherry">{t("cherry-filter")}</option>
                  <option value="opt_0">{"Moueffe"}</option>
                  <option value="opt_1">{"Picori"}</option>
                  <option value="opt_2">{"Castivore"}</option>
                  <option value="opt_3">{"Sirain"}</option>
                  <option value="opt_4">{"Winks"}</option>
                  <option value="opt_5">{"Gorilloz"}</option>
                  <option value="opt_6">{"Cargou"}</option>
                  <option value="opt_7">{"Hippoclamp"}</option>
                  <option value="opt_8">{"Rokky"}</option>
                  <option value="opt_9">{"Pigmou"}</option>
                  <option value="opt_A">{"Wanwan"}</option>
                  <option value="opt_B">{"Goupignon"}</option>
                  <option value="opt_C">{"Kump"}</option>
                  <option value="opt_D">{"Pteroz"}</option>
                  <option value="opt_E">{"Santaz"}</option>
                  <option value="opt_F">{"Ouistiti"}</option>
                  <option value="opt_G">{"Korgon"}</option>
                  <option value="opt_H">{"Kabuki"}</option>
                  <option value="opt_I">{"Serpantin"}</option>
                  <option value="opt_J">{"Soufflet"}</option>
                  <option value="opt_K">{"Feross"}</option>
                </select>

                <br />
                {bidSuccess && (
                  <div className="merchantMessage">{t("bidSuccess")}</div>
                )}
                {allListings.map((listing, i) => {
                  return (
                    <BazarListing
                      key={listing.id}
                      data={listing}
                      srcSection={1}
                      refresh={refreshAllListings}
                    />
                  );
                })}
                {allListings.length > 0 && (
                  <div className="rankingsBottom">
                  <img
                    alt=""
                    onClick={(e) => {
                      bazarPreviousPage();
                    }}
                    className="returnArrowLeft"
                    src={moveWest}
                  />
                  <span className="spanPagination">{(currentPage + 1)} / {totalPageNb}</span>
                  <img
                    alt=""
                    onClick={(e) => {
                      bazarNextPage();
                    }}
                    className="returnArrowRight"
                    src={moveEast}
                  />
                </div>
                )}
                {allListings.length === 0 && !isLoadingFilter && (
                  <div className="textBazar">{t("JazzBazarNoListings")}</div>
                )}
                {allListings.length === 0 && isLoadingFilter && (
                  <MoonLoader
                    color="#c37253"
                    css="margin-left : 255px; margin-top : 20px;"
                  />
                )}
              </div>
            )}
          </div>
        )}

        {sectionCurrentlyActive == 2 && (
          <div className="bazarSubComponent">
            <h2 className="miniHeaders2">{t("JazzBazatOpt2")}</h2>
            {isLoading === true && (
              <MoonLoader
                color="#c37253"
                css="margin-left : 255px; margin-top : 20px;"
              />
            )}
            {isLoading === false && (
              <div>
                <div className="textBazar">{t("JazzBazarIntroSell")}</div>
                {isErrorListing === true && (
                  <div className="errorResultFuzPrix">
                    {t("errorListingInvalid")}
                  </div>
                )}
                <div className="block_container">
                  <div id="bloc1">
                    {"Dinoz"}
                    {" : "}
                  </div>
                  <div id="bloc2">
                    {t("sellYourDinoz")} <strong>{dinoz.name as string}</strong>
                    <input type="checkbox" id="sellDinoz" ref={sellDinozRef} />
                  </div>
                  <div id="bloc3">
                    <DinozRenderTile appCode={dinoz.appearanceCode} size={65} />
                  </div>
                </div>

                <div className="block_container">
                  <div id="bloc1" className="padBot">
                    {t("objets")}
                    {" : "}
                  </div>
                  <div id="bloc2">
                    {Object.keys(sellablesObjectsMap).map(function (key) {
                      return (
                        <div className="blockBonusBazar">
                          {sellablesObjectsMap[key] > 0 && (
                            <div>
                              <img alt="" src={getConsumableByKey(key)} />
                              <input
                                className="numberFieldBazarBis"
                                type="number"
                                min="0"
                                max={sellablesObjectsMap[key]}
                                placeholder={"0"}
                                onInput={(e) => {
                                  sellQtyChange(e, key);
                                }}
                              />
                            </div>
                          )}
                        </div>
                      );
                    })}
                  </div>
                </div>

                <div className="block_container">
                  <div id="bloc1">
                    {t("duree")}
                    {" : "}
                  </div>
                  <div id="bloc2">
                    <select
                      className="choiceInventory"
                      id={"duration"}
                      ref={durationRef}
                      onChange={(e) => {
                        setSaleDuration();
                      }}
                    >
                      <option value={"3d"}>
                        {"3"}
                        {t("days")}
                      </option>
                      <option value={"5d"}>
                        {"5"}
                        {t("days")}
                      </option>
                      <option value={"7d"}>
                        {"7"}
                        {t("days")}
                      </option>
                    </select>
                  </div>
                </div>

                <div className="block_container">
                  <div id="bloc1">
                    {t("prixDeDepart")}
                    {" : "}
                  </div>
                  <div id="bloc2">
                    <input
                      className="numberFieldBazar"
                      type="number"
                      min="1"
                      placeholder={"1"}
                      id="saleStartingPrice"
                      ref={saleStartingPriceRef}
                    />
                    <img alt="" className="coin" src={tinybill} />
                  </div>
                </div>

                <button id="btnGive" className="buttonRewardsArena"
                  onClick={(e) => {
                    confirmSaleAndSendToServer();
                  }}
                >
                  {t("preview")}
                </button>
              </div>
            )}
          </div>
        )}

        {sectionCurrentlyActive == 3 && (
          <div className="bazarSubComponent">
            <h2 className="miniHeaders2">{t("JazzBazatOpt3")}</h2>
            {isLoading === true && (
              <MoonLoader
                color="#c37253"
                css="margin-left : 255px; margin-top : 20px;"
              />
            )}
            {isLoading === false && (
              <div>
                <div className="textBazar">{t("MyOffers")}</div>
                <br />
                {bonsDispos !== "" && (
                  <div className="youCurrentlyHaveS3">
                    {t("youHave")}
                    {bonsDispos as string}
                    <img alt="" className="coin" src={tinybill} />
                    <br />
                    {t("totalEngaged")}
                    {bonsEngaged as string}
                    <img alt="" className="coin" src={tinybill} />
                  </div>
                )}
                <br />
                {allListings.map((listing, i) => {
                  return (
                    <BazarListing
                      key={listing.id}
                      data={listing}
                      srcSection={3}
                      refresh={refreshAllListings}
                    />
                  );
                })}
                {allListings.length === 0 && (
                  <div className="textBazar">{t("JazzBazarNoListings")}</div>
                )}
              </div>
            )}
          </div>
        )}

        {sectionCurrentlyActive == 4 && (
          <div className="bazarSubComponent">
            <h2 className="miniHeaders2">{t("results")}</h2>
            {isLoading === true && (
              <MoonLoader
                color="#c37253"
                css="margin-left : 255px; margin-top : 20px;"
              />
            )}
            {isLoading === false && (
              <div>
                <div className="textBazar">{t("JazzBazarResults")}</div>
                <br />
                {allListings.map((listing, i) => {
                  return (
                    <BazarListing
                      key={listing.id}
                      data={listing}
                      srcSection={4}
                      refresh={refreshAllListings}
                    />
                  );
                })}
                {allListings.length === 0 && !isLoadingFilter && (
                  <div className="textBazar">{t("JazzBazarNoListings")}</div>
                )}
                {allListings.length === 0 && isLoadingFilter && (
                  <MoonLoader
                    color="#c37253"
                    css="margin-left : 255px; margin-top : 20px;"
                  />
                )}
              </div>
            )}
          </div>
        )}
      </div>
      <br />
    </div>
  );
}
