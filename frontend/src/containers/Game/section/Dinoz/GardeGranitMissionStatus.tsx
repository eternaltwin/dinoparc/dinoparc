import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useState } from "react";
import ReactTooltip from "react-tooltip";
import axios from "axios";
import urlJoin from "url-join";
import {apiUrl} from "../../../../index";
import {MoonLoader} from "react-spinners";
import {GranitMissionData} from "../../../../types/granit-mission";
import gardien from "../../../../media/pnj/garde_granit.png";
import Dialog from "./Dialog";
import ButtonWithIcon from "../../../../components/ButtonWithIcon";
import getActionImageFromActionString from "../../../utils/ActionImageFromString";
import i18n from "i18next";
import getCollectionEpicsImageByString from "../../../utils/CollectionEpicsImageByInteger";

export default function GardeGranitMissionStatus(props) {
    const { t } = useTranslation();
    const store = Store.getInstance();
    const [isLoading, setIsLoading] = useState(true);
    let [missionData, setMissionData] = useState<Partial<GranitMissionData>>({});

    useEffect(() => {
        axios.get(urlJoin(apiUrl, "account", store.getAccountId(), "granit-mission"))
            .then(({ data }) => {
                missionData = data;
                setMissionData(missionData);
                setIsLoading(false);
            });
    }, [props.dinozId]);

    function isRaceDone(race: number, counterTarget: number) {
        if (race >= counterTarget) {
            return "✅";
        }
        return "❌";
    }

    function getPctOfProgression() {
        let partialTotal = (
            getCounter(missionData.moueffe, 10)
            + getCounter(missionData.picori, 10)
            + getCounter(missionData.castivore, 10)
            + getCounter(missionData.sirain, 10)
            + getCounter(missionData.winks, 10)
            + getCounter(missionData.gorilloz, 10)
            + getCounter(missionData.cargou, 10)
            + getCounter(missionData.hippoclamp, 10)
            + getCounter(missionData.rokky, 10)
            + getCounter(missionData.pigmou, 10)
            + getCounter(missionData.wanwan, 10)
            + getCounter(missionData.goupignon, 3)
            + getCounter(missionData.kump, 10)
            + getCounter(missionData.pteroz, 10)
            + getCounter(missionData.santaz, 3)
            + getCounter(missionData.korgon, 10)
            + getCounter(missionData.kabuki, 5)
            + getCounter(missionData.serpantin, 3)
            + getCounter(missionData.soufflet, 2)
            + getCounter(missionData.feross, 1)
        );
        return ((partialTotal / 157) * 100).toFixed(0);
    }

    function getCounter(value: number, limit: number) {
        if (value >= limit) {
            return limit;
        }
        return value;
    }

    function claimRewards(dinozId) {
        axios.put(urlJoin(apiUrl, "account", store.getAccountId(), "granit-mission-claim"))
            .then(({ data }) => {
                props.returnToDinoz();
            });
    }

    return (
        <div>
            {isLoading === true && (<MoonLoader color="#c37253" css="margin-left : 240px;" />)}
            {(isLoading === false && !missionData.complete) && (
                <div>
                    <header className="pageCategoryHeader">{t("GardeGranitMissionStatus")}</header>
                    {!missionData.complete && <div>
                        <div className="displayFusions">
                            <img alt="" className="imgIrma" src={gardien}/>
                            <span className="textCerbere">{t("GardeGranitMissionStatus.textIntro")}</span>
                        </div>
                        <p>{t('event_progression')} {" : "} {getPctOfProgression()} {"%"}</p>

                        <table className="tableMissionPostForGranitMission">
                            <thead>
                            <tr className="trRankings">
                                <th className="has-text-centered">{t('Dinoz')}</th>
                                <th className="has-text-centered">{t('counter')}</th>
                                <th className="has-text-centered">{t('cible')}</th>
                                <th className="has-text-centered">{t('completed')}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr className="trRankings">
                                <td className="has-text-centered">Moueffe Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.moueffe, 10)}</td>
                                <td className="has-text-centered">10</td>
                                <td className="has-text-centered">{isRaceDone(missionData.moueffe, 10)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Picori Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.picori, 10)}</td>
                                <td className="has-text-centered">10</td>
                                <td className="has-text-centered">{isRaceDone(missionData.picori, 10)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Castivore Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.castivore, 10)}</td>
                                <td className="has-text-centered">10</td>
                                <td className="has-text-centered">{isRaceDone(missionData.castivore, 10)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Sirain Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.sirain, 10)}</td>
                                <td className="has-text-centered">10</td>
                                <td className="has-text-centered">{isRaceDone(missionData.sirain, 10)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Winks Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.winks, 10)}</td>
                                <td className="has-text-centered">10</td>
                                <td className="has-text-centered">{isRaceDone(missionData.winks, 10)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Gorilloz Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.gorilloz, 10)}</td>
                                <td className="has-text-centered">10</td>
                                <td className="has-text-centered">{isRaceDone(missionData.gorilloz, 10)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Cargou Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.cargou, 10)}</td>
                                <td className="has-text-centered">10</td>
                                <td className="has-text-centered">{isRaceDone(missionData.cargou, 10)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Hippoclamp Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.hippoclamp, 10)}</td>
                                <td className="has-text-centered">10</td>
                                <td className="has-text-centered">{isRaceDone(missionData.hippoclamp, 10)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Rokky Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.rokky, 10)}</td>
                                <td className="has-text-centered">10</td>
                                <td className="has-text-centered">{isRaceDone(missionData.rokky, 10)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Pigmou Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.pigmou, 10)}</td>
                                <td className="has-text-centered">10</td>
                                <td className="has-text-centered">{isRaceDone(missionData.pigmou, 10)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Wanwan Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.wanwan, 10)}</td>
                                <td className="has-text-centered">10</td>
                                <td className="has-text-centered">{isRaceDone(missionData.wanwan, 10)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Goupignon Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.goupignon, 3)}</td>
                                <td className="has-text-centered">3</td>
                                <td className="has-text-centered">{isRaceDone(missionData.goupignon, 3)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Kump Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.kump, 10)}</td>
                                <td className="has-text-centered">10</td>
                                <td className="has-text-centered">{isRaceDone(missionData.kump, 10)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Pteroz Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.pteroz, 10)}</td>
                                <td className="has-text-centered">10</td>
                                <td className="has-text-centered">{isRaceDone(missionData.pteroz, 10)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Santaz Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.santaz, 3)}</td>
                                <td className="has-text-centered">3</td>
                                <td className="has-text-centered">{isRaceDone(missionData.santaz, 3)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Korgon Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.korgon, 10)}</td>
                                <td className="has-text-centered">10</td>
                                <td className="has-text-centered">{isRaceDone(missionData.korgon, 10)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Kabuki Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.kabuki, 5)}</td>
                                <td className="has-text-centered">5</td>
                                <td className="has-text-centered">{isRaceDone(missionData.kabuki, 5)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Serpantin Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.serpantin, 3)}</td>
                                <td className="has-text-centered">3</td>
                                <td className="has-text-centered">{isRaceDone(missionData.serpantin, 3)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Soufflet Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.soufflet, 2)}</td>
                                <td className="has-text-centered">2</td>
                                <td className="has-text-centered">{isRaceDone(missionData.soufflet, 2)}</td>
                            </tr>

                            <tr className="trRankings">
                                <td className="has-text-centered">Feross Shiny</td>
                                <td className="has-text-centered">{getCounter(missionData.feross, 1)}</td>
                                <td className="has-text-centered">1</td>
                                <td className="has-text-centered">{isRaceDone(missionData.feross, 1)}</td>
                            </tr>

                            </tbody>
                        </table>

                        <br></br>
                        <button className="hoverReturn" onClick={(e) => {props.returnToDinoz();}}>
                            <img alt="" src={getActionImageFromActionString("🡸")} />
                        </button>
                    </div>}
                </div>
            )}

            {(isLoading === false && missionData.complete && missionData.mannyLife > 0) && (
                <Dialog placeNumber={7.1} returnToDinoz={props.returnToDinoz} />
            )}

            {(isLoading === false && missionData.complete && missionData.mannyLife <= 0) && (
                <div>
                    <header className="pageCategoryHeader">{t("GardeGranitMissionStatus")}</header>
                    <div className="displayFusions">
                        <img alt="" className="imgIrma" src={gardien}/>
                        <span className="textCerbere">{t("mannyThanksText")}</span>
                    </div>

                    <a className="recetteElement">
                        <div className="resultCraftChaudron">
                            <img alt="" src={getCollectionEpicsImageByString("33")}/>
                            <>{"   "}</>
                            <>{"   "}</>
                            <span className="espaceNom">
                      {t("pda")}
                    </span>
                            <span
                                className="imageSpan"
                                lang={i18n.language}
                                data-place="right"
                                data-tip={t("collection.epic.33.tooltip")}
                            />
                            <ReactTooltip className="largetooltip" html={true} backgroundColor="transparent"/>
                        </div>
                    </a>

                    <br></br>

                    <ButtonWithIcon className="overrideCenterButton" iconSrc={getActionImageFromActionString("Gift")} onClick={() => {claimRewards(props.dinozId);}}>
                        {t("acceptClan")}
                    </ButtonWithIcon>
                </div>
            )}
            <ReactTooltip className="largetooltip" html={true} backgroundColor="transparent"/>
        </div>
    );
}
