import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useState } from "react";
import axios from "axios";
import i18n from "i18next";
import { apiUrl } from "../../../../index";
import ReactTooltip from "react-tooltip";
import demon_hall from "../../../../media/game/demon_hall.png";
import demon_hall_bis from "../../../../media/game/demon_hall_bis.png";
import demon_acceptance from "../../../../media/game/demon_accept.png";
import demon_refuse from "../../../../media/game/demon_refuse.png";
import getConsumableByKey from "../../../utils/ConsumableByKey";
import getCollectionImageByString from "../../../utils/CollectionImageByInteger";
import Section from "../../../../components/Section";
import Button from "../../../../components/Button";
import { Dinoz } from "../../../../types/dinoz";
import GameSection from "./shared/GameSection";
import ContentWithImage, {
  ImageWrap,
} from "../../../../components/ContentWithImage";
import urlJoin from "url-join";

const enum PageSection {
  intro,
  offer,
  result,
}

type Props = {
  dinoz: Dinoz;
  returnToDinoz: () => void;
};

export default function DemonHideoutEvent(props: Props) {
  const { t } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(false);
  let [sectionCurrentlyActive, setSectionCurrentlyActive] =
    useState<PageSection>(PageSection.intro);
  let [objectWonInTrade, setObjectWonInTrade] = useState(0);

  useEffect(() => {
    setIsLoading(false);
  }, [props.dinoz.id]);

  function continueIntoHideout() {
    setIsLoading(true);
    if (window.confirm(t("confirm"))) {
      axios.put(urlJoin(apiUrl, "account", store.getAccountId(), props.dinoz.id, "enterDemon"))
          .then(() => {
            setSectionCurrentlyActive(PageSection.offer);
            setIsLoading(false);
          });
    }
  }

  function offerPruniacToDemons() {
    setIsLoading(true);
    axios.put(urlJoin(apiUrl, "account", store.getAccountId(), props.dinoz.id, "givePruniacToDemons"))
        .then(({data}) => {
          setObjectWonInTrade(data);
          setSectionCurrentlyActive(PageSection.result);
          setIsLoading(false);
        });
  }

  return (
    <GameSection isLoading={isLoading} returnToDinoz={props.returnToDinoz}>
      <>
        {sectionCurrentlyActive === PageSection.intro && (
          <DemonEnter continueIntoHideout={continueIntoHideout} />
        )}

        {sectionCurrentlyActive === PageSection.offer && (
          <DemonOffer offerPruniacToDemons={offerPruniacToDemons} />
        )}

        {sectionCurrentlyActive === PageSection.result && (
          <DemonResult objectWonInTrade={objectWonInTrade} />
        )}
      </>
    </GameSection>
  );
}

const DemonEnter = ({continueIntoHideout,}: { continueIntoHideout: () => void; }) => {
  const { t } = useTranslation();
  return (
    <DemonSection title={t("Enter Demon")} imgSrc={demon_hall} content={
        <>
          <p>{t("Enter Demon Text")}</p>
          <div className="has-text-centered mt-4">
            <Button size="medium" onClick={() => {continueIntoHideout();}}>{t("continue")}</Button>
          </div>
        </>
      }
    />
  );
};

const DemonOffer = ({
  offerPruniacToDemons,
}: {
  offerPruniacToDemons: () => void;
}) => {
  const { t } = useTranslation();

  return (
    <DemonSection
      title="..."
      imgSrc={demon_hall_bis}
      content={
        <>
          <p>{t("Demon Offer")}</p>

          <img alt="" src={getConsumableByKey("Pruniac")} />
          <span className="titreObjet">
            <span className="marginRewardFuz">{" x1"}</span>
            <span className="espaceNom">{t("boutique.Pruniac")}</span>
            <span
              className="imageSpan"
              lang={i18n.language}
              data-place="right"
              data-tip={t("boutique.tooltip.Pruniac")}
            />
          </span>
          <ReactTooltip
            className="largetooltip"
            html={true}
            backgroundColor="transparent"
          />
          <div className="has-text-centered mt-4">
            <Button
              size="medium"
              onClick={(e) => {
                offerPruniacToDemons();
              }}
            >
              {t("Offer")}
            </Button>
          </div>
        </>
      }
    />
  );
};

const DemonResult = ({ objectWonInTrade }: { objectWonInTrade: number }) => {
  const { t } = useTranslation();

  return objectWonInTrade === 0 ? (
    <DemonSection
      title="..."
      imgSrc={demon_refuse}
      content={<p>{t("Demon_refuse")}</p>}
    />
  ) : (
    <DemonSection
      title="..."
      imgSrc={demon_acceptance}
      content={
        <>
          <p>{t("Demon_accept")}</p>

          <div
            data-place="right"
            data-tip={t("collection." + String(objectWonInTrade) + ".tooltip")}
            className="background_demon_object_won mx-auto"
          >
            <img
              className="demonCollectionObject"
              src={getCollectionImageByString(String(objectWonInTrade))}
              alt=""
            />
          </div>

          <ReactTooltip
            className="largetooltip"
            html={true}
            backgroundColor="transparent"
          />
        </>
      }
    />
  );
};

const DemonSection = ({
  title,
  imgSrc,
  content,
}: {
  title: string;
  imgSrc?: string;
  content: React.ReactNode;
}) => {
  return (
    <Section title={title}>
      <ContentWithImage>
        {imgSrc != null && (
          <ImageWrap>
            <img alt="" src={imgSrc} />
          </ImageWrap>
        )}
        {content}
      </ContentWithImage>
    </Section>
  );
};
