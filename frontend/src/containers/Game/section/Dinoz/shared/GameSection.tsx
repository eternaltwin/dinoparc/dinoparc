import React from "react";
import Action from "../../../../../components/Action";
import AsyncSection from "../../../../../components/AsyncSection";
import Section, {
  SectionContentNoPadding,
} from "../../../../../components/Section";
import getActionImageFromActionString from "../../../../utils/ActionImageFromString";

export { SectionContentNoPadding as GameSectionContentNoPadding } from "../../../../../components/Section";

type Props = {
  isLoading?: boolean;
  title?: React.ReactNode;
  children: React.ReactNode;
  returnToDinoz?: () => void;
};

export default function GameSection({
  isLoading,
  title,
  children,
  returnToDinoz,
}: Props) {
  return (
    <div className="TEMP_mainContainer">
      <AsyncSection loading={isLoading}>
        <Section title={title}>
          {title ? (
            children
          ) : (
            <SectionContentNoPadding>{children}</SectionContentNoPadding>
          )}
          <BottomActions returnToDinoz={returnToDinoz} />
        </Section>
      </AsyncSection>
    </div>
  );
}

const BottomActions = ({ returnToDinoz }: { returnToDinoz?: () => void }) => {
  return returnToDinoz != null ? (
    <div className="mt-5">
      <Action
        imgSrc={getActionImageFromActionString("🡸")}
        onClick={() => returnToDinoz()}
      />
    </div>
  ) : undefined;
};
