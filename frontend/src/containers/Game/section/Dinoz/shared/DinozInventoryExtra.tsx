import { useTranslation } from "react-i18next";
import React, { useEffect, useId, useState } from "react";
import axios from "axios";
import { apiUrl } from "../../../../../index";
import getConsumableByKey from "../../../../utils/ConsumableByKey";
import getConsumableTypeByKey from "../../../../utils/ConsumableTypeByKey";
import actApply from "../../../../../media/minis/buy.gif";
import { Dinoz } from "../../../../../types/dinoz";
import HelpTooltip from "../../../../../components/HelpTooltip";
import AsyncSection from "../../../../../components/AsyncSection";
import Tooltip from "../../../../../components/Tooltip";
import Message from "../../../../../components/Message";
import { useUserData } from "../../../../../context/userData";

import "./DinozInventory.scss";
import urlJoin from "url-join";

interface Response {
  successMessageFr: string;
  successMessageEs: string;
  successMessageEn: string;
}

type Props = {
  dinoz: Dinoz;
  reloader: { reload: boolean; setReload: (state: boolean) => void };
};

type Item = {
  id: string;
  quantity: number;
};

export default function DinozInventoryCharmes(props: Props) {
  const id = useId();
  const tooltipId = `lorem_${id}`;
  const dinozId = props.dinoz.id;
  const itemsMap: Map<string, string> = new Map([
    ["Bière de Dinojak", "/beer/" + dinozId + "/1"],
    ["bonus_claw", "/griffes/" + dinozId],
    ["bonus_tigereye", "/eye/" + dinozId],
    ["bonus_boostaggro", "/focus-aggro/" + dinozId + "/1"],
    ["bonus_boostnature", "/focus-nature/" + dinozId + "/1"],
    ["bonus_boostwater", "/focus-water/" + dinozId + "/1"],
    ["bonus_mahamuti", "/focus-mahamuti/" + dinozId + "/1"],
    ["Potion Sombre", "/darkpotion/" + dinozId + "/1"],
    ["Coulis Cerise", "/cherry/" + dinozId + "/1"],
    ["Médaille Chocolat", "/medaille/" + dinozId],
    ["Lait de Cargou", "/lait-cargou/" + dinozId],
    ["CanneAPeche", "/fishing-rod/" + dinozId]
  ]);

  const { t, i18n } = useTranslation();
  const { accountId } = useUserData();
  const [isLoading, setIsLoading] = useState(true);
  const [availableItems, setAvailableItems] = useState<Item[]>([]);
  const [inutileError, setInutileError] = useState(false);
  const [limitReachedError, setLimitReachedError] = useState(false);
  const [success, setSuccess] = useState(false);
  const [response, setResponse] = useState<Response | false>(false);

  useEffect(() => {
    axios
      .get(urlJoin(apiUrl, "account", accountId, "inventory"))
      .then(({ data }) => {
        const inventoryItemsMap: { [itemId: string]: number } = data.inventoryItemsMap;
        const items = Object.entries(inventoryItemsMap)
          .map(([id, quantity]) => ({
            id,
            quantity,
          }))
          .filter((item) => isConsumableInList(item.id) && getConsumableTypeByKey(item.id));
        setAvailableItems(items);
        setIsLoading(false);
      });
  }, [success, accountId]);

  function giveObject(key) {
    setLimitReachedError(false);
    setInutileError(false);
    setSuccess(false);

    axios
      .put(urlJoin(apiUrl, "account", accountId, itemsMap.get(key)))
      .then(({ data }) => {
        setInutileError(data.inutileError);
        setLimitReachedError(data.limitReached);
        if (data.success) {
          setResponse(data);
          setSuccess(data.success);

          setAvailableItems((availableItems) =>
            availableItems
              .map((item) =>
                item.id === key
                  ? { ...item, quantity: item.quantity - 1 }
                  : item
              )
              .filter((item) => item.quantity > 0)
          );
          props.reloader.setReload(!props.reloader.reload);
        }
      });
  }

  const errorMsg = inutileError ? t("inventaire.nouse") : limitReachedError ? t("limitReachedError") : null;
  const successMsg = success && response !== false ? getSuccessMsg(response, i18n.language) : null;

  function getOrder(item: Item) {
    if (item.id === "Médaille Chocolat") {
      return 1;
    } else if (item.id === "bonus_claw") {
      return 2;
    } else if (item.id === "Bière de Dinojak") {
      return 3;
    } else if (item.id === "bonus_boostaggro") {
      return 4;
    } else if (item.id === "bonus_boostnature") {
      return 5;
    } else if (item.id === "bonus_boostwater") {
      return 6;
    } else if (item.id === "bonus_mahamuti") {
      return 6;
    } else if (item.id === "Potion Sombre") {
      return 7;
    } else if (item.id === "Coulis Cerise") {
      return 8;
    } else if (item.id === "Lait de Cargou") {
      return 9;
    }
    return 10;
  }

  const visibleItems = availableItems
    .filter((item) => item.quantity > 0)
    .sort((a, b) => getOrder(a) - getOrder(b));

  return (
    <AsyncSection className="borderMap" loading={isLoading}>
      <>
        {errorMsg != null && (
          <Message className="m-1" type="danger">
            {errorMsg}
          </Message>
        )}
        {successMsg != null && (
          <Message className="m-1" type="success">
            {successMsg}
          </Message>
        )}

        {visibleItems.length !== 0 && (
          <>
            <table className="dinoInventoryTable">
              <tbody>
                {visibleItems.map((item) => (
                  <tr key={item.id}>
                    <td>
                      <img alt="" src={getConsumableByKey(item.id)} />
                    </td>

                    <td>
                      {t("boutique." + item.id)}
                      <HelpTooltip
                        className="ml-1"
                        tip={t("boutique.tooltip." + item.id)}
                      />
                    </td>

                    <td className="possession">({item.quantity})</td>

                    <td
                      className="submitDinozInventory"
                      data-place="right"
                      data-for={tooltipId}
                      data-tip={t("tooltip.giveItemDinoz")}
                      tabIndex={0}
                      onClick={() => giveObject(item.id)}
                    >
                      <img alt="" src={actApply} />
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
            <Tooltip id={tooltipId} />
          </>
        )}
      </>
    </AsyncSection>
  );
}

function isConsumableInList(consumable: string) {
  const list = [
    "Bière de Dinojak",
    "bonus_claw",
    "bonus_tigereye",
    "bonus_boostaggro",
    "bonus_boostnature",
    "bonus_boostwater",
    "bonus_mahamuti",
    "Potion Sombre",
    "Coulis Cerise",
    "Médaille Chocolat",
    "Lait de Cargou",
    "CanneAPeche"
  ];
  return list.includes(consumable);
}

function getSuccessMsg(response: Response, lang: string): string {
  switch (lang) {
    case "fr":
      return response.successMessageFr;
    case "es":
      return response.successMessageEs;
    case "en":
    default:
      return response.successMessageEn;
  }
}
