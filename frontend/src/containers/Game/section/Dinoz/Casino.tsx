import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useState } from "react";
import { MoonLoader } from "react-spinners";
import ReactTooltip from "react-tooltip";
import casino from "../../../../media/lieux/zone8.gif";
import axios from "axios";
import { apiUrl } from "../../../../index";
import getActionImageFromActionString from "../../../utils/ActionImageFromString";
import PrizeWheel from "./shared/PrizeWheel";
import urlJoin from "url-join";
import getConsumableByKey from "../../../utils/ConsumableByKey";
import {CasinoToken} from "../../../../types/casinotoken";

export default function Casino(props) {
  const { t } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(false);
  const [displayEndText, setDisplayEndText] = useState(false);
  const [displayWheel, setDisplayWheel] = useState(false);
  let [displayErrorNoMore, setDisplayErrorNoMore] = useState(false);
  const [cantClick, setCantClick] = useState("");
  let [valueToGamble, setValueToGamble] = useState(0);
  let [factorDrawn, setFactorDrawn] = useState("");
  const [casinoTokenCount, setCasinoTokenCount] = useState<CasinoToken>(null);
  const [refreshAmounts, setRefreshAmounts] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    axios.get<CasinoToken>(urlJoin(apiUrl, "utils", "prize", "casino", store.getAccountId(), "tokenQty"))
        .then(({data}) => {
          setCasinoTokenCount(data);
          if (data.bronze == 0 && data.silver == 0 && data.gold == 0) {
            setDisplayErrorNoMore(true);
          }
          setIsLoading(false);
          window.scrollTo(0, 0);
        });
  }, [props.dinozId, refreshAmounts]);

  function drawRandomPrize() {
    setIsLoading(true);
    axios.get(urlJoin(apiUrl, "utils", "prize", "casino", store.getAccountId(), String(valueToGamble)))
        .then(({ data }) => {
          setDisplayWheel(true);
          factorDrawn = data;
          setFactorDrawn(factorDrawn);
          setRefreshAmounts(!refreshAmounts);
          setIsLoading(false);
        });
  }

  function handleWheelEnd() {
    setDisplayEndText(true);
    props.refresh();
  }

  function doSomething() {
    setTimeout(function () {handleWheelEnd();}, 4250);
    setCantClick("CC");
  }

  function selectValueToGamble(event) {
    valueToGamble = event.target.value;
    setValueToGamble(valueToGamble);
  }

  function replay() {
    setDisplayWheel(false);
    setDisplayEndText(false);
    setCantClick("");
    setValueToGamble(0);
    setRefreshAmounts(!refreshAmounts);
  }

  return (
    <div>
      <div>
        <header className="pageCategoryHeader">{t("Casino")}</header>
        <div className="displayFusions">
          <img alt="" className="imgIrma" src={casino} />
          <span className="textIrma">{t("casino.talk")}</span>
        </div>

        {isLoading === true && (<MoonLoader color="#c37253" css="margin-left : 240px;" />)}
        {isLoading === false && (
            <div>
              {displayErrorNoMore && <div className="fusionMessage">{t("errorCantPlayCasino")}</div>}
              {!displayErrorNoMore && <a className="textIrma2">{t("casino.mise")}</a>}
              <br></br>
              <br></br>

              <div onChange={selectValueToGamble}>
                {casinoTokenCount !== null && casinoTokenCount.bronze >= 1 && <>
                  <input disabled={displayWheel} className="radioCasino" onChange={selectValueToGamble}
                         checked={valueToGamble == 1} type="radio" value="1" name="1"/>
                  <span className="casinoChoice">
                    <img alt="" src={getConsumableByKey("JetonCasinoBronze")}/>
                    <a>{" (" + casinoTokenCount.bronze + ")"}</a>
                  </span>
                </>
                }


                {casinoTokenCount !== null && casinoTokenCount.silver >= 1 && <>
                  <input disabled={displayWheel} className="radioCasino" onChange={selectValueToGamble}
                         checked={valueToGamble == 2} type="radio" value="2" name="2"/>
                  <span className="casinoChoice">
                  <img alt="" src={getConsumableByKey("JetonCasinoArgent")}/>
                    <a>{" (" + casinoTokenCount.silver + ")"}</a>
                </span>
                </>
                }

                {casinoTokenCount !== null && casinoTokenCount.gold >= 1 && <>
                  <input disabled={displayWheel} className="radioCasino" onChange={selectValueToGamble}
                         checked={valueToGamble == 3} type="radio" value="3" name="3"/>
                  <span className="casinoChoice">
                  <img alt="" src={getConsumableByKey("JetonCasinoOr")}/>
                    <a>{" (" + casinoTokenCount.gold + ")"}</a>
                </span>
                </>
                }
              </div>

              {(!displayWheel && !displayErrorNoMore && valueToGamble != 0) && (
                  <button id="btnGive" className="buttonGiftPrize" onClick={(e) => {if (window.confirm(t("confirm"))) {drawRandomPrize();}}}>
                    <img alt="Gold" src={getActionImageFromActionString("Merchant")} />
                    <span className="verticalAlignCenter">{t("casino.play")}</span>
                    <img alt="Gold" src={getActionImageFromActionString("Merchant")} />
                  </button>
              )}

              {isLoading === false && (
                  <div className={"textPrize" + cantClick} onClick={(e) => {doSomething();}}>
                    {(displayWheel && valueToGamble == 1) && (<PrizeWheel chosen={factorDrawn} items={["zeroBronze", "twoBronze", "threeBronze", "tenBronze"]}/>)}
                    {(displayWheel && valueToGamble == 2) && (<PrizeWheel chosen={factorDrawn} items={["zeroSilver", "twoSilver", "threeSilver", "tenSilver"]}/>)}
                    {(displayWheel && valueToGamble == 3) && (<PrizeWheel chosen={factorDrawn} items={["zeroGold", "twoGold", "threeGold", "tenGold"]}/>)}
                  </div>
              )}

              {displayWheel && displayEndText && (
                  <div className="textPrize">
                    <br></br>
                    <button id="btnGive" className="buttonSacrifice" onClick={(e) => {replay();}}>
                      <img alt="Gift" src={getActionImageFromActionString("Merchant")} />
                      <span className="verticalAlignCenter">{t("rejouer")}</span>
                      <img alt="Gift" src={getActionImageFromActionString("Merchant")} />
                    </button>
                  </div>
              )}
            </div>
        )}
        <ReactTooltip className="largetooltip" html={true} backgroundColor="transparent"/>
      </div>
    </div>
  );
}
