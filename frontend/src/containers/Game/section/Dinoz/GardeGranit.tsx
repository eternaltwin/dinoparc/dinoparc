import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useState } from "react";
import ReactTooltip from "react-tooltip";
import Dialog from "./Dialog";

export default function GardeGranit(props) {
    const { t } = useTranslation();
    const store = Store.getInstance();
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(false);
    }, [props.dinozId]);

    return (
        <div>
            <div>
                <div>
                    <Dialog placeNumber={7} returnToDinoz={props.returnToDinoz} />
                </div>
                <ReactTooltip className="largetooltip" html={true} backgroundColor="transparent"/>
            </div>
        </div>
    );
    }
