import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useState } from "react";
import { MoonLoader } from "react-spinners";
import ReactTooltip from "react-tooltip";
import axios from "axios";
import { apiUrl } from "../../../../index";
import getTotemImageByName from "../../../utils/TotemImageByName";
import merchantImg from "../../../../media/pnj/marchand.jpg";
import tinyCoin from "../../../../media/minis/tiny_coin.gif";
import actBuy from "../../../../media/actions/act_buy.gif";
import getConsumableByKey from "../../../utils/ConsumableByKey";
import {TradeCollectionResponseDto} from "../../../../types/trade-collection-dto";
import urlJoin from "url-join";

export default function Merchant(props) {
  const { t, i18n } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(true);
  const [quantityMap, setQuantityMap] = useState({});
  const [inutileError, setInutileError] = useState(false);
  const [inutileErrorMsg, setInutileErrorMsg] = useState("");
  const [success, setSuccess] = useState(false);
  const [successMsg, setSuccessMsg] = useState("");
  const [amount, setAmount] = useState(0);
  const [pricesList, setPricesList] = useState({});
  const [quantitiesSelected, setQuantitiesSelected] = useState({});
  const [successCollectionTrade, setSuccessCollectionTrade] = useState<Partial<TradeCollectionResponseDto>>({});
  //const [merchantBuysCollection, setMerchantBuysCollection] = useState(false);

  useEffect(() => {
    axios.get(urlJoin(apiUrl, "account", store.getAccountId(), "inventory"))
        .then(({data}) => {
          setQuantityMap(data.inventoryItemsMap);
          setPricesList(data.pricesList);
          setIsLoading(false);
          window.scrollTo(0, 0);
        });

    // axios.get(urlJoin(apiUrl, "utils", "merchant", "collection-buy", store.getAccountId()))
    //     .then(({data}) => {
    //       setMerchantBuysCollection(data);
    //     });
  }, [success, store]);

  const changeIngredientQuantity = (e, key) => {
    const newAmountOfIngredient = e.target.value;
    const valueOfIngredient = pricesList[key];
    let difference = newAmountOfIngredient;
    if (Object.keys(quantitiesSelected).includes(key)) {
      difference = newAmountOfIngredient - quantitiesSelected[key];
    }
    setQuantitiesSelected({
      ...quantitiesSelected,
      [key]: newAmountOfIngredient,
    });

    setAmount(amount + difference * valueOfIngredient);
  };

  const sellIngredients = () => {
      if (window.confirm(t("confirm"))) {
          setSuccess(false);
          setInutileError(false);
          if (amount === 0) {
              return;
          }
          setIsLoading(true);
          axios.post(
              urlJoin(apiUrl, "account", store.getAccountId(), props.dinoz.id, "/merchant"),
              quantitiesSelected)
              .then(({data}) => {
                  if (data) {
                      setSuccess(true);
                      setSuccessMsg(t("merchant.successSell"));
                  } else {
                      setInutileError(true);
                      setInutileErrorMsg(t("merchant.failSell"));
                  }
                  setAmount(0);
                  setQuantitiesSelected({});
                  setIsLoading(false);
                  props.refresh();
              });
      }
  };

    const sellAllIngredients = () => {
        if (window.confirm(t("confirm"))) {
            setSuccess(false);
            setInutileError(false);
            setIsLoading(true);

            axios.post(urlJoin(apiUrl, "account", store.getAccountId(), props.dinoz.id, "merchant", "all"))
                .then(({data}) => {
                    if (data) {
                        setSuccess(true);
                        setSuccessMsg(t("merchant.successSell"));
                    } else {
                        setInutileError(true);
                        setInutileErrorMsg(t("merchant.failSell"));
                    }
                    setAmount(0);
                    setQuantitiesSelected({});
                    setIsLoading(false);
                    props.refresh();
                });
        }
    };

    function keyIsFoodCraft(key) {
        return [
            "Feuille de Pacifique",
            "Oréade blanc",
            "Tige de Roncivore",
            "Anémone solitaire",
            "Perche perlée",
            "Grémille grelottante",
            "Cube de glu",
            "Anguille"
        ].some((item) => key.includes(item));
    }

  // function exchangeCollection() {
  //   if (window.confirm(t("confirm"))) {
  //     axios.put(urlJoin(apiUrl, "account", store.getAccountId(), props.dinoz.id.toString(), "merchant", "tradeCollection"))
  //       .then(({ data }) => {
  //         setSuccessCollectionTrade(data);
  //         setIsLoading(false);
  //         props.refresh();
  //       });
  //   }
  // }

  return (
    <div>
      <header className="pageCategoryHeader">{t("Merchant")}</header>
      {isLoading === true && (
        <MoonLoader color="#c37253" css="margin-left : 240px;" />
      )}

      {isLoading === false && (
        <div>
          <div className="flexDivsIrma">
            <img alt="" className="imgIrma imgFixedSize" src={merchantImg} />
            <p className="textNpc italic">{t("merchant.header")}</p>
          </div>

          {inutileError && (<div className="merchantError">{inutileErrorMsg}</div>)}
          {success && <div className="merchantMessage">{successMsg}</div>}
          <div data-tip={t("merchant.tooltip.buy")} className="merchantBuyPanel" onClick={sellIngredients}>
            <span id="ingredientsAmount">{amount}</span>
            <img alt="" src={actBuy} />
          </div>

          <table className="shopItemsMerchant">
            <tbody>
              <tr>
                <th className="empty" />
                <th className="nameTitle">{t("boutique.nom")}</th>
                <th className="quantityHeader">{t("boutique.quantity")}</th>
              </tr>

              {Object.keys(pricesList)
                .sort((a, b) => pricesList[a] - pricesList[b])
                .map(function (key) {
                  return (
                    <>
                      {quantityMap[key] > 0 && key.toString().startsWith("totem") && (
                          <tr className="tableColor">
                            <td className="icon">
                              <img alt="" src={getTotemImageByName(key)} />
                            </td>

                            <td className="name">
                              <p className="titreObjet">
                                <span className="espaceNom">{t(key)}</span>
                                <span
                                  className="imageSpan"
                                  lang={i18n.language}
                                  data-place="right"
                                  data-tip={t("tooltip." + key)}
                                />
                              </p>
                              <div className="possession">
                                {t("boutique.possession")}
                                {quantityMap[key]}
                              </div>
                            </td>

                            <td className="count">
                              <span className="colorBlackPOMerchant">
                                {pricesList[key]}
                                <img alt="" className="coin" src={tinyCoin} />
                              </span>
                              <input
                                className="numberFieldInv"
                                type="number"
                                min="0"
                                max={quantityMap[key]}
                                value={quantitiesSelected[key]}
                                id={key.toString()}
                                onChange={(e) => changeIngredientQuantity(e, key)}
                              />
                            </td>
                          </tr>
                        )}
                    </>
                  );
                })}

              {Object.keys(pricesList)
                  .sort((a, b) => pricesList[a] - pricesList[b])
                  .map(function (key) {
                      return (
                          <>
                              {(quantityMap[key] > 0 && keyIsFoodCraft(key) && !key.startsWith("totem")) && (
                                  <tr className="tableColor">
                                      <td className="icon">
                                          <img alt="" src={getConsumableByKey(key)} />
                                      </td>

                                      <td className="name">
                                          <p className="titreObjet">
                                              <span className="espaceNom">{t(key)}</span>
                                              <span
                                                  className="imageSpan"
                                                  lang={i18n.language}
                                                  data-place="right"
                                                  data-tip={t("tooltip." + key)}
                                              />
                                          </p>
                                          <div className="possession">
                                              {t("boutique.possession")}
                                              {quantityMap[key]}
                                          </div>
                                      </td>

                                      <td className="count">
                              <span className="colorBlackPOMerchant">
                                {pricesList[key]}
                                  <img alt="" className="coin" src={tinyCoin} />
                              </span>
                                          <input
                                              className="numberFieldInv"
                                              type="number"
                                              min="0"
                                              max={quantityMap[key]}
                                              value={quantitiesSelected[key]}
                                              id={key.toString()}
                                              onChange={(e) => changeIngredientQuantity(e, key)}
                                          />
                                      </td>
                                  </tr>
                              )}
                          </>
                      );
                  })}

            </tbody>
          </table>

          {/*{merchantBuysCollection && (*/}
          {/*    <div>*/}
          {/*      <br></br>*/}
          {/*      <div className="textMerchantExchange">{t("Merchant_trade_collection")}</div>*/}
          {/*      {!successCollectionTrade.success && (*/}
          {/*          <button className="buttonGiveaway" onClick={(e) => {exchangeCollection();}}>{t("exchange_collection")}</button>*/}
          {/*      )}*/}
          {/*      {successCollectionTrade.success &&*/}
          {/*          <div className="rewardMerchant">*/}
          {/*            <img alt="" src={getConsumableByKey("Ticket Cadeau")} />*/}
          {/*            <a className="titreObjet">*/}
          {/*              <a className="marginRewardFuz">{" x10"}</a>*/}
          {/*              <a className="espaceNom">{t("boutique." + "Ticket Cadeau")}</a>*/}
          {/*              <span className="imageSpan" lang={i18n.language} data-place="right" data-tip={t("boutique.tooltip." + "Ticket Cadeau")}/>*/}
          {/*            </a>*/}
          {/*            <br></br>*/}
          {/*            {successCollectionTrade.nbOfGiftMagikTickets > 0 && <>*/}
          {/*              <img alt="" src={getConsumableByKey("Ticket Magik")} />*/}
          {/*              <a className="titreObjet">*/}
          {/*                <a className="marginRewardFuz">{" x1"}</a>*/}
          {/*                <a className="espaceNom">{t("boutique." + "Ticket Magik")}</a>*/}
          {/*                <span className="imageSpan" lang={i18n.language} data-place="right" data-tip={t("boutique.tooltip." + "Ticket Magik")}/>*/}
          {/*              </a>*/}
          {/*            </>}*/}
          {/*            <ReactTooltip className="largetooltip" html={true} backgroundColor="transparent"/>*/}
          {/*          </div>*/}
          {/*      }*/}
          {/*    </div>*/}
          {/*)}*/}
            <ReactTooltip className="largetooltip" html={true} backgroundColor="transparent"/>
        </div>
      )}
      <br />
    </div>
  );
}