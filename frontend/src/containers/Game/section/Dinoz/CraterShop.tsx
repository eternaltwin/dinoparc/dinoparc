import { useTranslation } from "react-i18next";
import React from "react";
import cratere from "../../../../media/lieux/zone22.gif";
import GameSection from "./shared/GameSection";
import ContentWithImage, {
  ImageWrap,
} from "../../../../components/ContentWithImage";
import Content from "../../../../components/Content";
import ShopForm from "../../shared/ShopForm";

type Props = {
  dinozId: string;
  refresh: () => void;
  returnToDinoz: () => void;
};

export default function CraterShop({ dinozId, refresh, returnToDinoz }: Props) {
  const { t } = useTranslation();

  return (
    <GameSection title={t("AnomalyShop")} returnToDinoz={returnToDinoz}>
      <>
        <ContentWithImage>
          <ImageWrap>
            <img alt="" src={cratere}></img>
          </ImageWrap>
          <p>{t("boutiqueCrater.header")}</p>
        </ContentWithImage>
        <Content className="mt-2">{t("boutiqueCrater.text")}</Content>

        <ShopForm
          className="mt-5"
          dinozId={dinozId}
          type="crater"
          onSubmitted={() => {
            refresh();
          }}
          returnToDinoz={() => {returnToDinoz();}}
        />
      </>
    </GameSection>
  );
}
