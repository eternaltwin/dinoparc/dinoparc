import axios from "axios";
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import { apiUrl } from "../../../..";
import { useUserDinozList } from "../../../../context/userDinozList";
import { Dinoz } from "../../../../types/dinoz";
import getElementImageByString from "../../../utils/ElementImageByString";
import getElementStringByCompetence from "../../../utils/ElementNameByCompetence";
import getElementPositionByCompetence from "../../../utils/ElementPositionByCompetence";
import getCorrectLevelUrlFromInteger from "../../../utils/LevelUrlByInteger";
import getLocationByNumber from "../../../utils/LocationByNumber";
import getLocationDetailsByNumber from "../../../utils/LocationDetailsByNumber";
import getLocationImageByNumber, {
  PlaceNumber,
} from "../../../utils/LocationImageByPlaceNumber";
import getPassiveIconByKey from "../../../utils/PassiveIconByKey";
import DinozRenderTile from "../../shared/DinozRenderTile";

import maraisflood from "../../../../media/lieux/zone16_flood.gif";
import maraisfog from "../../../../media/lieux/zone16_fog.gif";
import DinoMap from "./shared/DinoMap";
import DinozInventoryCharmes from "./shared/DinozInventoryCharmes";
import DinozInventorySoins from "./shared/DinozInventorySoins";
import DinozInventoryExtra from "./shared/DinozInventoryExtra";
import getActionImageFromActionString from "../../../utils/ActionImageFromString";
import isDinozActionFromActionString from "../../../utils/ActionTypeFromString";
import { useUserData } from "../../../../context/userData";
import Progress from "../../../../components/Progress";
import Row from "../../../../components/Row";

import "./Home.scss";
import Message from "../../../../components/Message";
import GameSection, { GameSectionContentNoPadding } from "./shared/GameSection";
import { ActionKey, ProcessAction } from ".";
import { placeDialogs } from "./Dialog";
import Action from "../../../../components/Action";
import { useDebouncedLoading } from "../../../../hooks/useDebouncedLoading";
import AsyncSection from "../../../../components/AsyncSection";
import Tooltip from "../../../../components/Tooltip";
import HelpTooltip from "../../../../components/HelpTooltip";
import ContentWithImage, {
  ImageWrap,
} from "../../../../components/ContentWithImage";
import urlJoin from "url-join";

type Props = {
  dinoz: Dinoz;

  /** @deprecated */
  reloader: { reload: boolean; setReload: (state: boolean) => void };

  isLoading: boolean;
  hasActionLoading: boolean;
  top?: React.ReactNode;

  isMoving: boolean;
  setDinoz: (dinoz: Dinoz) => void;
  processAction: ProcessAction;
  activateTournamentViewPerDinozLocation: () => void;
  respawnDinoz: (dinozId: string) => void;
  setCourseMoveAgain: (state: boolean) => void;
  reloadLocations: () => void;
};

export default function HomeSection(props: Props) {
  const { t } = useTranslation();
  const { accountId } = useUserData();
  const store = Store.getInstance();
  const { actionMode, setActionMode } = useUserData();
  const {
    list,
    setSelectedDinozId,
    itemsPerPage,
    getDinozListPosition,
    hasNextPage,
    hasPreviousPage,
    goNextPage,
    goPreviousPage,
  } = useUserDinozList();

  const [isLoadingUnderMap, setIsLoadingUnderMap] = useState(false);
  const [availableLocations, setAvailableLocations] = useState<PlaceNumber[]>([]);
  const [actionViewToDisplay, setActionViewToDisplay] = useState<"dinozActions" | "locationActions">("dinozActions");
  const [dinozViewToDisplay, setDinozViewToDisplay] = useState<"map" | "charmes" | "soins" | "autre">("map");
  const [jungleStatus, setJungleStatus] = useState(false);

  const isLoading = props.isLoading;
  const dinoz = props.dinoz;
  const dinozId = dinoz.id;
  const hasPreviousDinoz = getDinozListPosition(dinoz.id) !== 0 || hasPreviousPage;
  const hasNextDinoz = getDinozListPosition(dinoz.id) !== list.length - 1 || hasNextPage;
  const hasActionsLoader = useDebouncedLoading({loading: props.hasActionLoading || isLoadingUnderMap,});

  useEffect(() => {
    refreshComponent(dinoz);
  }, [dinoz, props.reloader.reload]);

  useEffect(() => {
    const handlePrevNext = (e: KeyboardEvent) => {
      if (e.key === "ArrowLeft" && hasPreviousDinoz) {
        loadNextOrPreviousDinozInList("previous");
      }
      if (e.key === "ArrowRight" && hasNextDinoz) {
        loadNextOrPreviousDinozInList("next");
      }
    };

    document.addEventListener("keyup", handlePrevNext);

    return () => {
      document.removeEventListener("keyup", handlePrevNext);
    };
  });

  useEffect(() => {
    let ignore = false;
    if (props.isMoving) {
      setDinozViewToDisplay("map");
      setIsLoadingUnderMap(true);
      setAvailableLocations([]);

      axios.get<{ [key: number]: string }>(urlJoin(apiUrl, "account", accountId, dinozId, "availableLocations"))
        .then(({ data }) => {
          if (ignore) {
            return;
          }
          setAvailableLocations(Object.keys(data).map((key) => Number(key)));
          setIsLoadingUnderMap(false);
        });
    }

    return () => {
      ignore = true;
    };
  }, [props.isMoving, accountId, dinozId]);

  function refreshComponent(dinoz: Dinoz) {
    setIsLoadingUnderMap(false);
    if (dinoz.placeNumber === PlaceNumber.jungle) {
      axios
          .get<boolean>(urlJoin(apiUrl, "utils", "jungle-status"))
          .then(({ data }) => {
            setJungleStatus(data);
      });
    }
  }

  function reloadMyDinoz() {
    axios
      .get<Dinoz>(urlJoin(apiUrl, "account", accountId, dinozId))
      .then(({ data }) => {
        props.setDinoz(data);
        window.scrollTo(0, 0);
      });
  }

  function loadNextOrPreviousDinozInList(flag: "next" | "previous") {
    setDinozViewToDisplay("map");
    axios
      .get(urlJoin(apiUrl, "account", accountId, dinozId, flag))
      .then(({ data }) => {
        const oldDinozPosition = getDinozListPosition(dinoz.id);
        if (oldDinozPosition === 0 && flag === "previous") {
          goPreviousPage();
        } else if (oldDinozPosition === itemsPerPage - 1 && flag === "next") {
          goNextPage();
        }
        setSelectedDinozId(data.id);
      });
  }

  function processDeplacement(placeNumber) {
    setDinozViewToDisplay("map");
    setIsLoadingUnderMap(true);
    setAvailableLocations([]);
    axios.post<Dinoz>(urlJoin(apiUrl, "account", accountId, dinozId, placeNumber.toString()))
      .then(({ data }) => {
        props.setDinoz(data);
        props.reloadLocations();
        if (
          data.actionsMap["Deplacer"] &&
          !data.malusList.includes("Coulis Cerise")
        ) {
          props.setCourseMoveAgain(true);
        }
        setIsLoadingUnderMap(false);
      });
  }

  function returnToMapDisplay() {
    setDinozViewToDisplay("map");
    reloadMyDinoz();
  }

  function setActionsViewForUser(actionMode) {
    setActionViewToDisplay(actionMode);
  }

  const processAction = props.processAction;

  let actionsMessage: string;
  let dinozActions: ActionItem[] = [];
  let locationActions: ActionItem[] = [];
  if (dinoz.life <= 0) {
    dinozActions.push({
      key: "Respawn",
      imgSrc: getActionImageFromActionString("Respawn"),
      text: dinoz.name + " " + t("dinoz.mort"),
      onClick: () => {
        props.respawnDinoz(dinoz.id);
      },
    });
  } else if (dinoz.inTourney) {
    actionsMessage = dinoz.name + t("tournament.phrase");
    locationActions.push({
      key: "Tournament",
      imgSrc: getActionImageFromActionString("Tournament"),
      text: t("tournament.seeTourney"),
      onClick: () => {
        props.activateTournamentViewPerDinozLocation();
      },
      tooltipText: t("tooltip.tournament"),
    });
    if (dinoz.experience >= 100) {
      dinozActions.push({
        key: "LevelUp",
        imgSrc: getActionImageFromActionString("LevelUp"),
        text: t("LevelUp"),
        onClick: (e) => {
          processAction(dinoz.id, "LevelUp", e);
        },
        tooltipText: t("tooltip.LevelUp"),
      });
    }
  } else if (dinoz.bossInfos?.armyDinoz) {
    if (dinoz.actionsMap.Army_Quit) {
      locationActions.push({
        key: "Army_Quit",
        imgSrc: getActionImageFromActionString("Army_Quit"),
        text: t("Army_Quit"),
        onClick: (e) => {
          processAction(dinoz.id, "Army_Quit", e);
        },
        tooltipText: t("tooltip.Army_Quit"),
      });
    }

    if (dinoz.actionsMap.Army_See) {
      locationActions.push({
        key: "Army_See",
        imgSrc: getActionImageFromActionString("Army_See"),
        text: t("Army_See"),
        onClick: (e) => {
          processAction(dinoz.id, "Army_See", e);
        },
        tooltipText: t("tooltip.Army_See"),
      });
    }
  } else {
    const weightActionKeysMap = new Map<string, number>([
      ["Combat", -100],
      ["Deplacer", -99],
    ]);
    let actionKeys = Object.keys(dinoz.actionsMap)
      .filter((key) => dinoz.actionsMap[key] === true)
      .sort((a, b) => a.localeCompare(b))
      .sort((a, b) => {
        const aWeight = weightActionKeysMap.has(a)
          ? weightActionKeysMap.get(a)
          : 0;
        const bWeight = weightActionKeysMap.has(b)
          ? weightActionKeysMap.get(b)
          : 0;

        return aWeight < bWeight ? -1 : 1;
      });

    dinozActions = actionKeys
      .filter((key) => isDinozActionFromActionString(key) === true)    
      .map((key: ActionKey) => ({
        key,
        text: t(key),
        imgSrc:
          key === "Deplacer" && props.isMoving
            ? getActionImageFromActionString("Cancel")
            : getActionImageFromActionString(key),
        onClick: (e) => {
          props.processAction(dinoz.id, key, e);
        },
        tooltipText: t("tooltip." + key),
      }));

    locationActions = actionKeys
      .filter((key) => isDinozActionFromActionString(key) === false)    
      .map((key: ActionKey) => ({
        key,
        text: t(key),
        imgSrc: getActionImageFromActionString(key),
        onClick: (e) => {
          props.processAction(dinoz.id, key, e);
        },
        tooltipText: t("tooltip." + key),
      }));

    if (dinoz.placeNumber in placeDialogs) {
      const key = "Talk";
      locationActions.push({
        key,
        imgSrc: getActionImageFromActionString(key),
        text: t(placeDialogs[dinoz.placeNumber].id),
        tooltipText: t("tooltip.talk"),
        onClick: (e) => {
          props.processAction(dinoz.id, key, e);
        },
      });
    }
  }

  return (
    <GameSection
      isLoading={isLoading}
      title={
        <div className="sectionActions">
          {hasPreviousDinoz && (<button type="button" onClick={() => loadNextOrPreviousDinozInList("previous")}>{"⮘"}</button>)}
          <span>{dinoz.name}</span>
          {hasNextDinoz && (<button type="button" onClick={() => loadNextOrPreviousDinozInList("next")}>{"⮚"}</button>)}
        </div>
      }
    >
      <GameSectionContentNoPadding>
        {props.top}

        <div className="dinozView">
          <div className="dinozView-dinoz">
            <DinozInfos dinoz={dinoz} />
          </div>
          <div className="dinozView-main">
            <LocationInfos
              placeNumber={dinoz.placeNumber}
              jungleStatus={jungleStatus}
            />
            <AsyncSection loading={hasActionsLoader}>
              <>
                <div className="actionTitle">{t("action.title")}</div>

                {(store.getActionMode() === "ADVANCED") && (<div id="actionsDinozView" className="mt-2">
                  <button type="button" className={`dinoViewOption${(actionViewToDisplay === "dinozActions") ? " is-active" : ""}`} onClick={() => setActionsViewForUser("dinozActions")}>
                    {t("action.title.dinoz")}
                  </button>

                  <button type="button" className={`dinoViewOption${(actionViewToDisplay === "locationActions") ? " is-active" : ""}`} onClick={() => setActionsViewForUser("locationActions")}>
                    {t("action.title.location")}
                  </button>
                </div>)}

                {(store.getActionMode() === "STANDARD") && (
                    <AllActionsSection actions={dinozActions.concat(locationActions)} message={actionsMessage}/>
                )}

                {(store.getActionMode() === "ADVANCED" && actionViewToDisplay === "dinozActions") && (
                    <DinozActionsSection actions={dinozActions} message={actionsMessage} />
                )}

                {(store.getActionMode() === "ADVANCED" && actionViewToDisplay === "locationActions") && (
                    <LocationActionsSection actions={locationActions} message={actionsMessage} />
                )}

                <div id="selectableDinozView" className="mt-2">
                  <button
                    type="button"
                    className={`dinoViewOption${dinozViewToDisplay === "map" ? " is-active" : ""}`}
                    onClick={() => returnToMapDisplay()}
                  >
                    {t("mapLabel")}
                  </button>

                  <button type="button" className={`dinoViewOption${dinozViewToDisplay === "charmes" ? " is-active" : ""}`} onClick={() => setDinozViewToDisplay("charmes")}>
                    {t("charmesLabel")}
                  </button>

                  <button type="button" className={`dinoViewOption${dinozViewToDisplay === "soins" ? " is-active" : ""}`} onClick={() => setDinozViewToDisplay("soins")}>
                    {t("soinsLabel")}
                  </button>

                  <button type="button" className={`dinoViewOption${dinozViewToDisplay === "autre" ? " is-active" : ""}`} onClick={() => setDinozViewToDisplay("autre")}>
                    {t("autreLabel")}
                  </button>
                </div>

                {dinozViewToDisplay === "map" && (
                  <DinoMap
                    currentLocation={dinoz.placeNumber}
                    availableLocations={props.isMoving ? availableLocations : []}
                    processLocationClick={processDeplacement}
                  />
                )}
                {dinozViewToDisplay === "charmes" && (<DinozInventoryCharmes dinoz={dinoz} reloader={props.reloader} />)}
                {dinozViewToDisplay === "soins" && (<DinozInventorySoins dinoz={dinoz} reloader={props.reloader} />)}
                {dinozViewToDisplay === "autre" && (<DinozInventoryExtra dinoz={dinoz} reloader={props.reloader} />)}
              </>
            </AsyncSection>
          </div>
        </div>
      </GameSectionContentNoPadding>
    </GameSection>
  );
}

const DinozInfos = ({ dinoz }: { dinoz: Dinoz }) => {
  const { t } = useTranslation();

  return (
    <div className="dino">
      <div className="picCenter">
        <table>
          <tbody>
            <tr>
              <td className="charms">
                <p
                  className="passiveTitle"
                  data-for="dinozDetailTooltip"
                  data-place="right"
                  data-tip={t("tooltip.bonus")}
                >
                  {t("bonusLabel")}
                </p>
                {Object.keys(dinoz.passiveList).map(function (passive) {
                  return dinoz.passiveList[passive] > 0 ? (
                    <img
                      key={passive}
                      alt=""
                      className="transformCharm"
                      src={getPassiveIconByKey(passive)}
                      data-for="dinozDetailTooltip"
                      data-place="right"
                      data-tip={
                        t("tooltip." + passive) +
                        t("tooltip.quantity") +
                        dinoz.passiveList[passive] +
                        "</strong>"
                      }
                    />
                  ) : undefined;
                })}
              </td>
              <td>
                <div className="dinoSheetWrap">
                  <DinozRenderTile appCode={dinoz.appearanceCode} size={100} />
                </div>
              </td>
              <td className="charms">
                <p
                  className="passiveTitle"
                  data-for="dinozDetailTooltip"
                  data-place="right"
                  data-tip={t("tooltip.autre")}
                >
                  {t("autreLabel")}
                </p>
                {dinoz.malusList.slice(0, 8).map((malus) => (
                  <img
                    key={malus}
                    alt=""
                    className="transformCharm"
                    src={getPassiveIconByKey(malus)}
                    data-for="dinozDetailTooltip"
                    data-place="right"
                    data-tip={
                      malus === "Coulis Cerise"
                        ? t("tooltip." + malus) +
                          t("timeLeft") +
                          " : " +
                          dinoz.cherryEffectMinutesLeft +
                          " " +
                          t("minutes")
                        : t("tooltip." + malus)
                    }
                  />
                ))}
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <table className="def">
        <tbody>
          <tr className="minibottom">
            <th className="dinoTh">{t("race")}</th>
            <td className="dinoTd">
              <span className="minispace">{dinoz.race}</span>
              <HelpTooltip tip={t("tooltip." + dinoz.race)}/>
            </td>
          </tr>

          <tr className="minibottom">
            <th className="dinoTh">{t("vie")}</th>
            <td className="dinoTd">
              <PercentProgress percent={dinoz.life} />
            </td>
          </tr>

          <tr className="minibottom">
            <th className="dinoTh">{t("niveau")}</th>
            <td className="dinoTd">
              <span className="minispace">{dinoz.level}</span>
            </td>
          </tr>

          <tr className="minibottom">
            <th className="dinoTh">{t("Suivant")}</th>
            <td className="dinoTd">
              <PercentProgress percent={dinoz.experience} />
            </td>
          </tr>

          <tr className="minibottom">
            <th className="dinoTh">{t("Danger")}</th>
            <td className="dinoTd">
              <span className="minispace">{dinoz.danger}</span>
              <HelpTooltip tip={t("tooltip.danger")} />
            </td>
          </tr>

          <tr className="minibottom">
            <th className="dinoTh">{t("orderInList")}</th>
            <td className="dinoTd">
              <DinozPositionUpdater dinoz={dinoz} />
            </td>
          </tr>
        </tbody>
      </table>

      <div className="elementsDisplay">
        <div className="elementsLiFiche">
          <img alt="" src={getElementImageByString("Feu")} />{" "}
          {dinoz.elementsValues.Feu}
        </div>
        <div className="elementsLiFiche">
          <img alt="" src={getElementImageByString("Terre")} />{" "}
          {dinoz.elementsValues.Terre}
        </div>
        <div className="elementsLiFiche">
          <img alt="" src={getElementImageByString("Eau")} />{" "}
          {dinoz.elementsValues.Eau}
        </div>
        <div className="elementsLiFiche">
          <img alt="" src={getElementImageByString("Foudre")} />{" "}
          {dinoz.elementsValues.Foudre}
        </div>
        <div className="elementsLiFiche">
          <img alt="" src={getElementImageByString("Air")} />{" "}
          {dinoz.elementsValues.Air}
        </div>
      </div>

      <div>
        <ul className="ulCompDinoz">
          {Object.keys(dinoz.skillsMap)
            .sort((a, b) =>
              getElementPositionByCompetence(a).localeCompare(
                getElementPositionByCompetence(b)
              )
            )
            .map(function (key) {
              return (
                <div key={key}>
                  {dinoz.skillsMap[key] > 0 && (
                    <div
                      className={
                        "compHeight" + getElementStringByCompetence(key)
                      }
                    >
                      <div className="compName">{t(key.toString())}</div>
                      <div className="compStars">
                        <HelpTooltip tip={t("tooltip." + key)} />
                        <img
                          alt=""
                          className="imgOffset"
                          src={getCorrectLevelUrlFromInteger(
                            dinoz.skillsMap[key]
                          )}
                        />
                      </div>
                    </div>
                  )}
                </div>
              );
            })}
        </ul>
      </div>
      <Tooltip id="dinozDetailTooltip" />
    </div>
  );
};

const PercentProgress = ({ percent }: { percent: number }) => {
  return (
    <Row vcenter={true} gap="small">
      <Progress value={percent} max={100} />
      <span className="percentages">{percent}%</span>
    </Row>
  );
};

const DinozPositionUpdater = ({ dinoz }: { dinoz: Dinoz }) => {
  const { t } = useTranslation();
  const { moveDinozListPosition, itemsPerPage } = useUserDinozList();
  const [panelOpened, setPanelOpened] = useState(false);
  const [moveByPage, setMoveByPage] = useState(false);

  function moveDinozPositionInList(direction: 1 | -1) {
    moveDinozListPosition({
      dinozId: dinoz.id,
      moveValue: moveByPage ? itemsPerPage * direction : direction,
    });
  }

  return (
    <>
      <span
        onClick={() => {
          setPanelOpened(!panelOpened);
        }}
        className="modifyOrder"
      >
        {t("modify")}
      </span>
      <HelpTooltip tip={t("tooltip.orderInList")} />
      {panelOpened && (
        <p className="blockUpDown">
          <span
            onClick={() => {
              moveDinozPositionInList(1);
            }}
            className="orderSigns"
          >
            {"Monter"}
          </span>
          <a className="spacerA" />
          <span
            onClick={() => {
              moveDinozPositionInList(-1);
            }}
            className="orderSigns"
          >
            {"Descendre"}
          </span>
          <a className="spacerA" />
          <input
            type="checkbox"
            onChange={(e) => setMoveByPage(!moveByPage)}
            checked={moveByPage}
          />
          <>{t("dinoz-list-page-mode")}</>
        </p>
      )}
    </>
  );
};

function getMaraisState() {
  const now = new Date();
  switch (now.getDay()) {
    case 1:
      return "flood";
    case 3:
    case 5:
      return "fog";
    default:
      return null;
  }
}

const LocationInfos = ({
  placeNumber,
  jungleStatus,
}: {
  placeNumber: PlaceNumber;
  jungleStatus: boolean;
}) => {
  const { t } = useTranslation();
  let imgSrc = getLocationImageByNumber(placeNumber);
  let content = getLocationDetailsByNumber(placeNumber);

  const maraisState = getMaraisState();
  if (placeNumber === PlaceNumber.marais && maraisState != null) {
    imgSrc = maraisState === "flood" ? maraisflood : maraisfog;
    content =
      maraisState === "flood"
        ? t("location.marais.flood.details")
        : t("location.marais.brume.details");
  }

  return (
    <div className="dinozLocation">
      <div className="dinozLocation-title">
        {getLocationByNumber(placeNumber)}
      </div>
      <ContentWithImage>
        <ImageWrap className="dinozLocation-image">
          <img alt="" src={imgSrc} width="88" height="88" />
        </ImageWrap>

        <p className="dinozLocation-txt">{content}</p>
        {placeNumber === PlaceNumber.jungle && (
          <Message>
            {t(jungleStatus ? "jungleActive" : "jungleInactive")}
          </Message>
        )}
      </ContentWithImage>
    </div>
  );
};

type ActionItem = {
  key: string;
  imgSrc: string;
  text: string;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  tooltipText?: string;
};

type ActionsSectionProps = { message?: React.ReactNode; actions: ActionItem[] };

const DinozActionsSection = ({ message, actions }: ActionsSectionProps) => {
  const { t } = useTranslation();

  function getOrder(item: Element) {
    if (item.id === "bonus_fire") {
      return 1;
    } else if (item.id === "bonus_wood") {
      return 2;
    } else if (item.id === "bonus_water") {
      return 3;
    } else if (item.id === "bonus_thunder") {
      return 4;
    } else if (item.id === "bonus_air") {
      return 5;
    } else if (item.id === "bonus_prismatik") {
      return 6;
    }
    return 7;
  }

  function sortActions(actionA: ActionItem, actionB: ActionItem) {
    if (actionA.key.includes("Bain de Flammes")) {
      return 99;
    } else {
      return 0;
    }
  }

  return (
    <>
      <div className="actions">
        {message != null && <div className="actions-important">{message}</div>}
        {actions.sort((a, b) => sortActions(a, b))
            .map((action) => (
          <Action
            key={action.key}
            id={action.key}
            imgSrc={action.imgSrc}
            className="actions-item"
            data-place="right"
            data-for="dinozActionTooltip"
            data-tip={action.tooltipText}
            onClick={action.onClick}
          >
            {action.text}
          </Action>
        ))
        }
      </div>
      <Tooltip id="dinozActionTooltip" />
    </>
  );
};

const LocationActionsSection = ({ message, actions }: ActionsSectionProps) => {
  const { t } = useTranslation();

  function sortActions(actionA: ActionItem, actionB: ActionItem) {
    return 0;
  }

  return (
    <>
      {actions.length <= 0 &&
          <div className="actionsEmpty">
            {t('action.empty')}
          </div>
      }
      {actions.length > 0 &&
          <div className="actions">
            {message != null && <div className="actions-important">{message}</div>}
            {actions.sort((a, b) => sortActions(a, b))
                .map((action) => (
                    <Action
                        key={action.key}
                        id={action.key}
                        imgSrc={action.imgSrc}
                        className="actions-item"
                        data-place="right"
                        data-for="locationActionTooltip"
                        data-tip={action.tooltipText}
                        onClick={action.onClick}
                    >
                      {action.text}
                    </Action>
                ))
            }
          </div>
      }
      <Tooltip id="locationActionTooltip" />
    </>
  );
};

const AllActionsSection = ({ message, actions }: ActionsSectionProps) => {
  const { t } = useTranslation();

  function sortActions(actionA: ActionItem, actionB: ActionItem) {
    return 0;
  }

  return (
      <>
        <div className="actions">
          {message != null && <div className="actions-important">{message}</div>}
          {actions.sort((a, b) => sortActions(a, b))
              .map((action) => (
                  <Action
                      key={action.key}
                      id={action.key}
                      imgSrc={action.imgSrc}
                      className="actions-item"
                      data-place="right"
                      data-for="locationActionTooltip"
                      data-tip={action.tooltipText}
                      onClick={action.onClick}
                  >
                    {action.text}
                  </Action>
              ))
          }
        </div>
        <Tooltip id="locationActionTooltip" />
      </>
  );
};
