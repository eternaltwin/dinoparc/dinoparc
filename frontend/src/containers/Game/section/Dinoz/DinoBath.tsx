import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useState } from "react";
import { MoonLoader } from "react-spinners";
import ReactTooltip from "react-tooltip";
import plage from "../../../../media/lieux/zone3.gif";
import tinycoins from "../../../../media/minis/tiny_coin.gif";
import axios from "axios";
import { apiUrl } from "../../../../index";
import urlJoin from "url-join";

export default function DinoBath(props) {
  const { t } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(false);
  const [errorCash, setErrorCash] = useState(false);
  const [errorLife, setErrorLife] = useState(false);
  const [successBaths, setSuccessBaths] = useState(null);
  const [lifeWon, setLifeWon] = useState(0);

  useEffect(() => {
    setLifeWon(0);
    setSuccessBaths(null);
    setErrorLife(false);
    setErrorCash(false);
    setIsLoading(false);
  }, [props.dinozId]);

  function confirm(dinozId) {
    setIsLoading(true);
    axios.put(urlJoin(apiUrl, "account", store.getAccountId(), props.dinozId, "bath"))
      .then(({ data }) => {
        if (data >= 0) {
          setLifeWon(data);
          setSuccessBaths(true);
          props.refresh();
        } else if (data === -1) {
          setSuccessBaths(false);
          setErrorLife(true);
        } else if (data === -2) {
          setSuccessBaths(false);
          setErrorCash(true);
        }

        setIsLoading(false);
      });
  }

  return (
    <div>
      <div>
        <header className="pageCategoryHeader">{t("DinoBath")}</header>
        <div className="displayFusions">
          <img alt="" className="imgIrma" src={plage} />
          <span className="textIrma">{t("bathText")}</span>
        </div>
        {errorCash && (
          <div className="merchantError">{t("inutileErrorCashBaths")}</div>
        )}
        {errorLife && (
          <div className="merchantError">{t("inutileErrorLifeBaths")}</div>
        )}
        {successBaths && (
          <div className="merchantMessage">
            {t("successMsgBaths")}
            {" +"}
            {lifeWon}
            {" HP!"}
          </div>
        )}

        {isLoading === true && (
          <MoonLoader color="#c37253" css="margin-left : 240px;" />
        )}
        {isLoading === false && (
          <button
            id="btnGive"
            className="buttonGiveaway"
            onClick={(e) => {
              if (window.confirm(t("confirm"))) {
                confirm(props.dinozId);
              }
            }}
          >
            {t("takeBath")}
            <br />
            {"1700"}
            <img alt="" className="coin" src={tinycoins} />
          </button>
        )}
        <ReactTooltip
          className="largetooltip"
          html={true}
          backgroundColor="transparent"
        />
      </div>
    </div>
  );
}
