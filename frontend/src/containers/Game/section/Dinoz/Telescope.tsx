import { useTranslation } from "react-i18next";
import Store from "../../../../utils/Store";
import React, { useEffect, useState } from "react";
import { MoonLoader } from "react-spinners";
import ReactTooltip from "react-tooltip";
import bordeciel from "../../../../media/lieux/zone18.gif";
import tinycoins from "../../../../media/minis/tiny_coin.gif";
import axios from "axios";
import { apiUrl } from "../../../../index";
import getLocationByNumber from "../../../utils/LocationByNumber";
import DinozRenderTile from "../../shared/DinozRenderTile";
import urlJoin from "url-join";

export default function Telescope(props) {
  const allLocations: Array<number> = [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
    21, 22, 37,
  ];
  const { t } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(true);
  const [cashAmount, setCashAmount] = useState(0);
  let [notEnoughCashError, setNotEnoughCashError] = useState(false);
  let [fightingDinozList, setFightingDinozList] = useState([]);
  let [chosenLocation, setChosenLocation] = useState(0);
  let [hasCheckedOnce, setHasCheckedOnce] = useState(false);

  useEffect(() => {
    setChosenLocation(0);
    setNotEnoughCashError(false);
    setHasCheckedOnce(false);
    axios.get(urlJoin(apiUrl, "account", store.getAccountId()))
        .then(({ data }) => {
      setCashAmount(data.account.cash);
      setIsLoading(false);
    });
  }, [props.dinozId]);

  function checkLocation(location) {
    setChosenLocation(location);
    if (cashAmount >= 100) {
      setIsLoading(true);
      axios.get(urlJoin(apiUrl, "account", store.getAccountId(), props.dinozId, "availableEnnemiesFromTelescope", String(location)))
        .then(({ data }) => {
          setHasCheckedOnce(true);
          if (data.length >= 0) {
            setFightingDinozList(data);
          } else {
            setFightingDinozList([]);
          }
          setIsLoading(false);
          props.refresh();
        });
    } else {
      notEnoughCashError = true;
      setNotEnoughCashError(notEnoughCashError);
    }
  }

  return (
    <div>
      <div>
        <header className="pageCategoryHeader">{t("Telescope")}</header>
        {isLoading === true && (
          <MoonLoader color="#c37253" css="margin-left : 240px;" />
        )}
        {isLoading === false && (
          <div className="textnormal">
            <div className="displayFusions">
              <img alt="" className="imgIrma" src={bordeciel} />
              <span className="textIrma">
                {t("use-longuevue")}
                <br />
                <br />
                {t("cost-longuevue")}
                <span className="colorBlackPO">
                  {100}
                  <img alt="" className="coin" src={tinycoins} />
                </span>
              </span>
            </div>
            <br />
            <span>{t("choose-longuevue")}</span>

            {notEnoughCashError && (
              <div className="errorResultFuzPrix">{t("notenoughcoins")}</div>
            )}
            <div className="telescopeList">
              {allLocations.map((location) => {
                return (
                  <li
                    className="clickablePageFromClan"
                    onClick={(e) => {
                      checkLocation(location);
                    }}
                  >
                    {getLocationByNumber(location)}
                  </li>
                );
              })}
            </div>

            {fightingDinozList.length > 0 && (
              <div>
                <header className="pageCategoryHeaderBordeciel">
                  {t("adversaires") +
                    " (" +
                    getLocationByNumber(chosenLocation) +
                    ")"}
                </header>
                <div className="telescopeListDinoz">
                  {fightingDinozList.map(function (fightingDinoz, idx) {
                    return (
                      <div className="chapter" key={idx}>
                        <div className="borderDinozFightInProfil">
                          <div className="tbodyFightTelescope">
                            <td className="picBoxFight">
                              <div className="dinoSheet">
                                <div className="dinoSheetWrapFight">
                                  <DinozRenderTile
                                    appCode={fightingDinoz.appearanceCode}
                                    size={100}
                                  />
                                </div>
                              </div>
                            </td>
                            <td className="infoBoxFight">
                              <div className="fiche">{t("fiche")}</div>
                              <table className="ficheTableFight">
                                <tbody>
                                  <tr>
                                    <th>Dinoz</th>
                                    <th className="menuDinozFiche">
                                      {fightingDinoz.level > 0 && (
                                        <span className="paddingRace">
                                          {fightingDinoz.name}
                                        </span>
                                      )}
                                    </th>
                                  </tr>
                                  <tr>
                                    <th>{t("niveau")}</th>
                                    <th className="menuDinozFicheNiveau">
                                      {fightingDinoz.level > 0 && (
                                        <a className="levelColor">
                                          {fightingDinoz.level}
                                        </a>
                                      )}
                                    </th>
                                  </tr>
                                  <tr>
                                    {fightingDinoz.level > 0 &&
                                      fightingDinoz.id !== "admin-b-raid" && (
                                        <>
                                          <th>{t("maitre")}</th>
                                          <th className="playerLink">
                                            {fightingDinoz.masterName}
                                          </th>
                                        </>
                                      )}
                                  </tr>
                                </tbody>
                              </table>
                              {fightingDinoz.level > 0 && fightingDinoz.id !== "admin-b-raid" && fightingDinoz.taggedAsClanEnnemy &&
                                  <a className="clanEnemy">
                                    {t("clanEnemy")}
                                  </a>
                              }
                            </td>
                          </div>
                          <ReactTooltip
                            className="largetooltip"
                            html={true}
                            backgroundColor="transparent"
                          />
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            )}
            {fightingDinozList.length === 0 && hasCheckedOnce && (
              <div className="errorResultFuzPrix">
                {t("no-ennemy-bordeciel")}
              </div>
            )}
          </div>
        )}
        <ReactTooltip
          className="largetooltip"
          html={true}
          backgroundColor="transparent"
        />
      </div>
    </div>
  );
}
