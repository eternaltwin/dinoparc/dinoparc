import { useTranslation } from "react-i18next";
import Store from "../../../utils/Store";
import React, { useEffect, useState } from "react";
import { MoonLoader } from "react-spinners";
import axios from "axios";
import { apiUrl } from "../../../index";
import moveEast from "../../../media/actions/act_move_e.gif";
import moveWest from "../../../media/actions/act_move_w.gif";
import PublicClan from "../shared/PublicClan";
import MyClan from "./MyClan";
import { Clan } from "../../../types/clan";
import getFactionImageFromActionString from "../../utils/FactionImageFromString";
import ReactTooltip from "react-tooltip";
import urlJoin from "url-join";
import {useUserData} from "../../../context/userData";

type Props = {
  openPlayerProfile: (playerId: string) => void;
  refreshPage: boolean;
};

export default function Rankings({ openPlayerProfile, refreshPage }: Props) {
  const { t } = useTranslation();
  const { accountId } = useUserData();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(true);
  const [isGeneral, setIsGeneral] = useState(true);
  const [isMoyenne, setIsMoyenne] = useState(false);
  const [isClans, setIsClans] = useState(false);
  const [searchedPlayer, setSearchedPlayer] = useState("");
  const [allClansRankings, setAllClansRankings] = useState([]);
  const [renderPublicClanMode, setRenderPublicClanMode] = useState(false);
  const [loadedClanIsOwnClan, setLoadedClanIsOwnClan] = useState(false);
  const [isInAClan, setIsInAClan] = useState(false);
  const [clanName, setClanName] = useState("null");
  let [wantedPage, setWantedPage] = useState(0);
  let [wantedPageX, setWantedPageX] = useState(0);
  let [actualPage, setActualPage] = useState([]);
  let [totalPossiblePages, setTotalPossiblePages] = useState(0);
  let [publicClanVisited, setPublicClanVisited] = useState<Partial<Clan>>({});
  let [numberOfActives, setNumberOfActives] = useState(0);

  useEffect(() => {
    setRenderPublicClanMode(false);
    setLoadedClanIsOwnClan(false);

    axios
      .get(urlJoin(apiUrl, "utils", "rankings", "general", String(wantedPage)))
      .then(({ data }) => {
        setActualPage(data);
      });

    axios
      .get(urlJoin(apiUrl, "utils", "rankings", "general", "totalPages"))
      .then(({ data }) => {
        setTotalPossiblePages(data);
        window.scrollTo(0, 0);
      });

    axios
      .get(urlJoin(apiUrl, "clans", "isInAClan", store.getAccountId()))
      .then(({ data }) => {
        if (data.inAClan) {
          setIsInAClan(true);
          setClanName(data.clanName);
        }
      });

    axios
      .get(urlJoin(apiUrl, "clans", "displayAllRankings", store.getAccountId()))
      .then(({ data }) => {
        setAllClansRankings(data);
        setIsLoading(false);
      });

    axios.get(urlJoin(apiUrl, "account", accountId, "pastillesvertes", "quatrecentretrois"))
        .then(({ data }) => {
          setNumberOfActives(data);
        });
  }, [wantedPage, refreshPage]);

  function setGeneralActive() {
    setWantedPage(0);
    setIsGeneral(true);
    setIsMoyenne(false);
    setIsClans(false);

    axios
      .get(urlJoin(apiUrl, "utils", "rankings", "general", String(wantedPage)))
      .then(({ data }) => {
        actualPage = data;
        setActualPage(actualPage);
      });

    axios
      .get(urlJoin(apiUrl, "utils", "rankings", "general", "totalPages"))
      .then(({ data }) => {
        totalPossiblePages = data;
        setTotalPossiblePages(totalPossiblePages);
      });
  }

  function setMoyenneActive() {
    setWantedPageX(wantedPage);
    setIsGeneral(false);
    setIsMoyenne(true);
    setIsClans(false);

    axios
      .get(urlJoin(apiUrl, "utils", "rankings", "moyenne", String(wantedPage)))
      .then(({ data }) => {
        actualPage = data;
        setActualPage(actualPage);
      });
  }

  function setClansActive() {
    setIsGeneral(false);
    setIsMoyenne(false);
    setIsClans(true);
  }

  function openSelectedClan(clanId) {
    setRenderPublicClanMode(true);
    setIsLoading(true);

    axios
      .get(urlJoin(apiUrl, "clans", "public", clanId, store.getAccountId()))
      .then(({ data }) => {
        publicClanVisited = data;
        setPublicClanVisited(publicClanVisited);
        if (publicClanVisited.members.includes(store.getAccountId())) {
          setLoadedClanIsOwnClan(true);
        }
        setIsLoading(false);
      });
  }

  function goToPage(pageNumber) {
    wantedPage = pageNumber;
    setWantedPage(wantedPage);
    axios
      .get(urlJoin(apiUrl, "utils", "rankings", "general", pageNumber))
      .then(({ data }) => {
        actualPage = data;
        setActualPage(actualPage);
      });
  }

  function xGoToPage(pageNumber) {
    wantedPageX = pageNumber;
    setWantedPageX(wantedPageX);
    axios
      .get(urlJoin(apiUrl, "utils", "rankings", "moyenne", pageNumber))
      .then(({ data }) => {
        actualPage = data;
        setActualPage(actualPage);
      });
  }

  function plusOnePage() {
    if (wantedPage + 1 < totalPossiblePages) {
      wantedPage = wantedPage + 1;
      setWantedPage(wantedPage);
      axios
        .get(urlJoin(apiUrl, "utils", "rankings", "general", String(wantedPage)))
        .then(({ data }) => {
          actualPage = data;
          setActualPage(actualPage);
        });
    }
  }

  function minusOnePage() {
    if (wantedPage > 0) {
      wantedPage = wantedPage - 1;
      setWantedPage(wantedPage);
      axios
        .get(urlJoin(apiUrl, "utils", "rankings", "general", String(wantedPage)))
        .then(({ data }) => {
          actualPage = data;
          setActualPage(actualPage);
        });
    } else {
      axios.get(urlJoin(apiUrl, "utils", "rankings", "general", String(0))).then(({ data }) => {
        actualPage = data;
        setActualPage(actualPage);
      });
    }
  }

  function plusOnePageX() {
    if (wantedPageX + 1 < totalPossiblePages) {
      wantedPageX = wantedPageX + 1;
      setWantedPageX(wantedPageX);
      axios
        .get(urlJoin(apiUrl, "utils", "rankings", "moyenne", String(wantedPageX)))
        .then(({ data }) => {
          actualPage = data;
          setActualPage(actualPage);
        });
    }
  }

  function minusOnePageX() {
    if (wantedPageX > 0) {
      wantedPageX = wantedPageX - 1;
      setWantedPageX(wantedPageX);
      axios
        .get(urlJoin(apiUrl, "utils", "rankings", "moyenne", String(wantedPageX)))
        .then(({ data }) => {
          actualPage = data;
          setActualPage(actualPage);
        });
    } else {
      axios.get(urlJoin(apiUrl, "utils", "rankings", "moyenne", String(0))).then(({ data }) => {
        actualPage = data;
        setActualPage(actualPage);
      });
    }
  }

  function search(username) {
    axios
      .get(urlJoin(apiUrl, "utils", "rankings", "general", "user", username))
      .then(({ data }) => {
        goToPage(data);
      });
  }

  function searchX(username) {
    axios
      .get(urlJoin(apiUrl, "utils", "rankings", "moyenne", "user", username))
      .then(({ data }) => {
        xGoToPage(data);
      });
  }

  function getFixedDecimals(entry) {
    return entry.averageLevelPoints;
  }

  return (
    <div>
      {!renderPublicClanMode && (
        <div>
          <header className="pageCategoryHeader">{t("rankingsLabel")}</header>
          {isLoading === true && (
            <MoonLoader color="#c37253" css="margin-left : 240px;" />
          )}
          {isLoading === false && (
            <div>
              <div className="classementHeader">
                <button
                  className="classementCategory"
                  onClick={(e) => {
                    setGeneralActive();
                  }}
                >
                  {t("classement.general")}
                </button>
                <button className="classementCategory"> / </button>
                <button
                  className="classementCategory"
                  onClick={(e) => {
                    setMoyenneActive();
                  }}
                >
                  {t("classement.moyenne")}
                </button>
                <span className="classementCategory"> / </span>
                <button
                  className="classementCategory"
                  onClick={(e) => {
                    setClansActive();
                  }}
                >
                  {t("classement.clans")}
                </button>
              </div>

              <div className="containerClassements">
                {isGeneral && (
                  <div>
                      <span className="classementText">{t("classement.general.titre") + " "}</span>
                      <span className="classementText">{t("activePart1") + " " + numberOfActives + " " + t("activePart2")}</span>

                      <table className="rankingsTable">
                      <tr className="trRankings">
                        <th className="position">{t("classement.position")}</th>
                        <th className="active"></th>
                        <th className="pseudo">{t("classement.pseudo")}</th>
                        <td className="pseudo">{t("classement.nbDinoz")}</td>
                        <th className="nbPoints">{t("classement.nbPoints")}</th>
                      </tr>
                      {actualPage.map((entry, i) => {
                        return (
                          <tr
                            className="selectedEntry"
                            onClick={(e) => {
                              openPlayerProfile(entry.id);
                            }}
                          >
                            <td className="position">{i + 1 + 30 * wantedPage}</td>
                            <td className="pseudo">
                              {entry.active && <a data-place="right" data-tip={t("tooltip.isActive")}>🟢</a>}
                              {!entry.active && <a data-place="right" data-tip={t("tooltip.isActive")}>🟡</a>}
                            </td>
                            <td className="pseudo">{entry.name}</td>
                            <td className="nbdi">{entry.nbDinoz}</td>
                            <td className="nbPoints">{entry.nbPoints}</td>
                          </tr>
                        );
                      })}
                      <ReactTooltip className="largetooltip" html={true} backgroundColor="transparent"/>
                    </table>
                    <div className="rankingsBottom">
                      <img
                        alt=""
                        onClick={(e) => {
                          minusOnePage();
                        }}
                        className="returnArrowLeft"
                        src={moveWest}
                      />
                      <button
                        className="searchMyRank"
                        onClick={(e) => {
                          search(store.getUsername());
                        }}
                      >
                        {t("classement.searchMe")}
                      </button>
                      <input
                        onChange={(e) => setSearchedPlayer(e.target.value)}
                        value={searchedPlayer}
                        type="text"
                        className="widthRankings"
                      />
                      <input
                        onClick={(e) => {
                          search(searchedPlayer);
                        }}
                        type="submit"
                        value={t("classement.search") as string}
                        className="ButtonRankings"
                      />
                      <img
                        alt=""
                        onClick={(e) => {
                          plusOnePage();
                        }}
                        className="returnArrowRight"
                        src={moveEast}
                      />
                    </div>
                  </div>
                )}

                {isMoyenne && (
                  <div>
                    <span className="classementText">
                      {t("classement.moyenne.titre")}
                    </span>
                    <table className="rankingsTable">
                      <tr className="trRankings">
                        <th className="position">{t("classement.position")}</th>
                        <th className="pseudo">{t("classement.pseudo")}</th>
                        <th className="nbPoints">{t("classement.moyennex")}</th>
                      </tr>
                      {actualPage.map((entry, i) => {
                        return (
                          <tr
                            className="selectedEntry"
                            onClick={(e) => {
                              openPlayerProfile(entry.id);
                            }}
                          >
                            <td className="position">
                              {i + 1 + 30 * wantedPageX}
                            </td>
                            <td className="pseudo">{entry.name}</td>
                            <td className="nbPoints">
                              {getFixedDecimals(entry)}
                            </td>
                          </tr>
                        );
                      })}
                    </table>
                    <div className="rankingsBottom">
                      <img
                        alt=""
                        onClick={(e) => {
                          minusOnePageX();
                        }}
                        className="returnArrowLeft"
                        src={moveWest}
                      />
                      <button
                        className="searchMyRank"
                        onClick={(e) => {
                          searchX(store.getUsername());
                        }}
                      >
                        {t("classement.searchMe")}
                      </button>
                      <input
                        onChange={(e) => setSearchedPlayer(e.target.value)}
                        value={searchedPlayer}
                        type="text"
                        className="widthRankings"
                      />
                      <input
                        onClick={(e) => {
                          searchX(searchedPlayer);
                        }}
                        type="submit"
                        value={t("classement.search") as string}
                        className="ButtonRankings"
                      />
                      <img
                        alt=""
                        onClick={(e) => {
                          plusOnePageX();
                        }}
                        className="returnArrowRight"
                        src={moveEast}
                      />
                    </div>
                  </div>
                )}

                {isClans && (
                  <div>
                    <span className="classementText">
                      {t("classement.clans.titre")}
                    </span>
                    <table className="rankingsTable">
                      <tr className="trRankings">
                        <th className="position">{t("classement.position")}</th>
                        <th className="pseudo">{t("classement.clanName")}</th>
                        <th className="nbPoints">
                          {t("classement.clanReputation")}
                        </th>
                        <th className="nbPoints">
                          {t("classement.clanPoints")}
                        </th>
                      </tr>
                      {allClansRankings.map((clanDto, i) => {
                        return (
                          <>
                              <tr
                                className="selectedEntry"
                                onClick={(e) => {
                                  openSelectedClan(clanDto.clanId);
                                }}
                              >
                                <td className="position">
                                    {i + 1}
                                    {" "}
                                    <img alt="" src={getFactionImageFromActionString(clanDto.faction)}
                                         data-place="right"
                                         data-tip={t("tooltip." + clanDto.faction) + "</strong>"}
                                    />
                                    <ReactTooltip
                                        className="largetooltip"
                                        html={true}
                                        backgroundColor="transparent"
                                    />
                                </td>
                                <td className="pseudo">{clanDto.clanName}</td>
                                <td className="nbPoints">{clanDto.creatorName}</td>
                                <td className="nbPoints">{clanDto.pointsTotal}</td>
                              </tr>
                          </>
                        );
                      })}
                    </table>
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
      )}

      {renderPublicClanMode && (
        <div>
          {isLoading === true && (
            <MoonLoader color="#c37253" css="margin-left : 240px;" />
          )}
          {isLoading === false && !loadedClanIsOwnClan && (
            <PublicClan
              openPlayerProfile={openPlayerProfile}
              publicClanData={publicClanVisited}
              isInAClan={isInAClan}
            />
          )}
          {isLoading === false && loadedClanIsOwnClan && (
            <MyClan openPlayerProfile={openPlayerProfile} />
          )}
        </div>
      )}
    </div>
  );
}
