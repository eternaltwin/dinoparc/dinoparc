import { useTranslation } from "react-i18next";
import Store from "../../../utils/Store";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { apiUrl } from "../../../index";
import ReactTooltip from "react-tooltip";
import getLocationByNumber from "../../utils/LocationByNumber";
import speak from "../../../media/actions/act_speak.gif";
import moveEast from "../../../media/actions/act_move_e.gif";
import moveWest from "../../../media/actions/act_move_w.gif";
import skipEast from "../../../media/actions/act_moveSpec.gif";
import DinozRenderTile from "../shared/DinozRenderTile";
import RewardBox from "../shared/RewardBox";
import { useUserCollection } from "../../../hooks/useUserCollection";
import { Loader } from "../../../components/Loader";
import {Simulate} from "react-dom/test-utils";
import compositionUpdate = Simulate.compositionUpdate;
import urlJoin from "url-join";

// TODO set selectedUserId in props
type Props = {
  playerId: string;
  onMessage: (data: { name: string }) => void;
};

export default function Player({ playerId, onMessage }: Props) {
  const { t } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(true);
  const [dinozCount, setDinozCount] = useState(0);
  const [dinozCap, setDinozCap] = useState(300);
  const [points, setPoints] = useState(0);
  const [position, setPosition] = useState(0);
  const [name, setName] = useState("");
  const [clanName, setClanName] = useState("");
  const [hermitStage, setHermitStage] = useState<string | number>(1);
  const [nbCaptures, setNbCaptures] = useState<string | number>(0);
  let [dinozList, setDinozList] = useState([]);
  let [wantedPageX, setWantedPageX] = useState(0);
  let [totalPossiblePages, setTotalPossiblePages] = useState(0);

  useEffect(() => {
    const totalPagePromise = axios
      .get(urlJoin(apiUrl, "utils", "player", playerId, "totalpages"))
      .then(({ data }) => {
        const totalPossiblePages = data;
        setTotalPossiblePages(totalPossiblePages);
      });

    const dinozListPromise = axios
      .get(urlJoin(apiUrl, "account", playerId, "foreignDinozList", String(wantedPageX)))
      .then(({ data }) => {
        setDinozList(data);
      });

    const userPagePromise = axios
      .get(urlJoin(apiUrl, "utils", "player", playerId, String(wantedPageX)))
      .then(({ data }) => {
        setDinozCount(data.nbDinoz);
        setName(data.name);
        setPoints(data.nbPoints);
        setPosition(data.position);
        setDinozCap(data.cap);
        setNbCaptures(data.nbCaptures);
        setClanName(data.clanName);

        if (data.actualHermitStage == null) {
          setHermitStage("1");
        } else {
          setHermitStage(data.actualHermitStage);
        }
      });

    Promise.all([totalPagePromise, dinozListPromise, userPagePromise]).then(
      () => {
        setIsLoading(false);
      }
    );
  }, [playerId, wantedPageX,]);

  function plusOnePageX() {
    if (wantedPageX + 1 < totalPossiblePages) {
      setIsLoading(true);
      setDinozList([]);
      wantedPageX = wantedPageX + 1;
      setWantedPageX(wantedPageX);
      axios.get(urlJoin(apiUrl, "account", playerId, "foreignDinozList", String(wantedPageX)))
        .then(({ data }) => {
          setDinozList(data);
          axios
            .get(urlJoin(apiUrl, "utils", "player", playerId, "totalpages"))
            .then(({ data }) => {
              totalPossiblePages = data;
              setTotalPossiblePages(totalPossiblePages);
              window.scrollTo(0, 0);
              setIsLoading(false);
            });
        });
    }
  }

  function goToEnd() {
    setIsLoading(true);
    setDinozList([]);
    wantedPageX = totalPossiblePages - 1;
    setWantedPageX(wantedPageX);
    axios
      .get(urlJoin(apiUrl, "account", playerId, "foreignDinozList", String(wantedPageX)))
      .then(({ data }) => {
        setDinozList(data);
        axios
          .get(urlJoin(apiUrl, "utils", "player", playerId, "totalpages"))
          .then(({ data }) => {
            totalPossiblePages = data;
            setTotalPossiblePages(totalPossiblePages);
            window.scrollTo(0, 0);
            setIsLoading(false);
          });
      });
  }

  function minusOnePageX() {
    if (wantedPageX > 0) {
      setIsLoading(true);
      setDinozList([]);
      wantedPageX = wantedPageX - 1;
      setWantedPageX(wantedPageX);
      axios.get(urlJoin(apiUrl, "account", playerId, "foreignDinozList", String(wantedPageX)))
        .then(({ data }) => {
          setDinozList(data);
          axios
            .get(urlJoin(apiUrl, "utils", "player", playerId, "totalpages"))
            .then(({ data }) => {
              totalPossiblePages = data;
              setTotalPossiblePages(totalPossiblePages);
              window.scrollTo(0, 0);
              setIsLoading(false);
            });
        });
    } else {
      axios.get(urlJoin(apiUrl, "account", playerId, "foreignDinozList", String(wantedPageX)))
        .then(({ data }) => {
          setDinozList(data);
        });
      axios
        .get(urlJoin(apiUrl, "utils", "player", playerId, "totalpages"))
        .then(({ data }) => {
          totalPossiblePages = data;
          setTotalPossiblePages(totalPossiblePages);
          window.scrollTo(0, 0);
        });
    }
  }

  function sendMessage() {
    onMessage({ name });
  }

  return (
    <div>
      <header className="pageCategoryHeader">
        {t("ficheDe")}
        {name}
      </header>
      {isLoading === true && <Loader css="margin: 2rem auto" />}

      {isLoading === false && (
        <div className="profil">
          <div className="shiftInfos">
            <tr>
              <th>{t("classement.pseudo")}</th>
              <th className="classementInfos">
                <span className="paddingRace">{name}</span>
              </th>
            </tr>
            <tr>
              <th>{t("classement.position")}</th>
              <th className="classementInfos">
                <span className="paddingRace">
                  {"#"}
                  {position}
                </span>
              </th>
            </tr>
            <tr>
              <th>{t("classement.nbDinoz")}</th>
              <th className="classementInfos">
                <span className="paddingRace">
                  {dinozCount}
                  {" Dinoz "}
                </span>
              </th>
            </tr>
            <tr>
              <th>{t("classement.nbPoints")}</th>
              <th className="classementInfos">
                <span className="paddingRace">
                  {points} {t("points")}
                </span>
              </th>
            </tr>
            <tr>
              <th>{t("clan")}</th>
              <th className="classementInfos">
                <span className="paddingRace">{clanName}</span>
              </th>
            </tr>
            <tr>
              <th>{t("nbCaptures")}</th>
              <th className="classementInfos">
                <span className="paddingRace">{nbCaptures}</span>
              </th>
            </tr>
            <tr>
              <th>{t("arene")}</th>
              <th className="classementInfos">
                {hermitStage <= 500 && (
                  <span className="paddingRace">{"#" + hermitStage}</span>
                )}
                {hermitStage > 500 && (
                  <span className="paddingRace">{t("termine")}</span>
                )}
              </th>
            </tr>

            <br />
            {store.getAccountId() !== playerId && (
              <tr
                className="hoverReturn"
                onClick={(e) => {
                  sendMessage();
                }}
              >
                <th>{t("sendMsg")}</th>
                <th className="classementInfos">
                  <img alt="" className="hoverReturn" src={speak} />
                </th>
              </tr>
            )}
          </div>
            <PlayerCollections userId={playerId} userName={name}/>

          <div className="translateUp">
            <header className="pageCategoryHeader">Dinoz</header>
            <div className="paddingListDinoz" key={wantedPageX}>
              {dinozList.map(function (dinoz, idx) {
                return (
                  <div className="chapter" key={idx}>
                    <div className="borderDinozFightInProfil">
                      <div className="tbodyFight">
                        <td className="picBoxFight">
                          <div className="dinoSheet">
                            <div className="dinoSheetWrapFight">
                              <DinozRenderTile
                                appCode={dinoz.appearanceCode}
                                size={100}
                              />
                            </div>
                          </div>
                        </td>
                        <td className="infoBoxFight">
                          <div className="fiche">{t("fiche")}</div>
                          <table className="fichePlayerCard">
                            <tbody>
                              <tr>
                                <th>{t("evo.name")}</th>
                                <th className="menuDinozFichePlayer">
                                  <span className="paddingRace">
                                    {dinoz.name}
                                  </span>
                                </th>
                              </tr>
                              <tr>
                                <th>{t("race")}</th>
                                <th className="menuDinozFichePlayer">
                                    <span className="paddingRace">{dinoz.race}</span>
                                </th>
                              </tr>
                              <tr>
                                <th>{t("niveau")}</th>
                                <th className="menuDinozFichePlayer">
                                  {dinoz.level}
                                </th>
                              </tr>
                              <tr>
                                <th>{t("lieu")}</th>
                                <th className="menuDinozFichePlayer">
                                  {getLocationByNumber(dinoz.placeNumber)}
                                </th>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
            <div className="rankingsBottom">
              <button
                className="returnArrows"
                onClick={(e) => {
                  minusOnePageX();
                }}
              >
                <img alt="" className="returnArrowLeft" src={moveWest} />
              </button>
              <button
                className="returnArrows"
                onClick={(e) => {
                  plusOnePageX();
                }}
              >
                <img alt="" className="returnArrowRight" src={moveEast} />
              </button>
              <button
                className="returnArrows"
                onClick={(e) => {
                  goToEnd();
                }}
              >
                <img alt="" className="skipArrowRight" src={skipEast} />
              </button>
            </div>
          </div>
        </div>
      )}
      <ReactTooltip
        className="collectiontooltipSmallFont"
        html={true}
        backgroundColor="transparent"
      />
    </div>
  );
}

const PlayerCollections = ({
  userId,
  userName,
}: {
  userId: string;
  userName: string;
}) => {
  const { t } = useTranslation();
  const { collection, collectionEpic, isLoading } = useUserCollection({
    userId,
  });

  return (
    <div className="shiftCollection">
      {isLoading ? (
        <Loader css="margin: 2rem auto" />
      ) : (
        <>
          <RewardBox
            title={`${t("collection.of")} ${userName}`}
            collection={collection}
          />

          {collectionEpic.length !== 0 && (
            <RewardBox
              className="mt-5"
              title={t("collection.ep")}
              collection={collectionEpic}
              isEpic={true}
            />
          )}
        </>
      )}
    </div>
  );
};
