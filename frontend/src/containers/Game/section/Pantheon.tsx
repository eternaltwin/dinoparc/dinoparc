import { useTranslation } from "react-i18next";
import Store from "../../../utils/Store";
import React, { useEffect, useRef, useState } from "react";
import { MoonLoader } from "react-spinners";
import book from "../../../media/game/gods_book.png";
import stars from "../../../media/game/columnBgWin.gif";
import moveEast from "../../../media/actions/act_move_e.gif";
import moveWest from "../../../media/actions/act_move_w.gif";
import { apiUrl } from "../../../index";
import axios from "axios";
import DinozRenderTile from "../shared/DinozRenderTile";
import urlJoin from "url-join";

export default function Pantheon() {
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState(true);
  const selectChapterRef = useRef<HTMLSelectElement>();
  const store = Store.getInstance();
  const pageSize = 10;
  let [chapter, setChapter] = useState("");
  let [results, setResults] = useState([]);
  let [currentPage, setCurrentPage] = useState(0);
  let [totalPageNb, setTotalPageNb] = useState(0);

  useEffect(() => {
    window.scrollTo(0, 0);
    setIsLoading(false);
  }, []);

  function changeFilter() {
    setIsLoading(true);
    chapter = selectChapterRef.current.value;
    setChapter(chapter);
    setCurrentPage(0);
    reloadRanking(0);
  }

  function previousPage() {
    if (currentPage > 0) {
      const newPageNb = currentPage - 1;
      setCurrentPage(newPageNb);
      reloadRanking(newPageNb);
    }
  }

  function nextPage() {
    if (currentPage < totalPageNb - 1) {
      const newPageNb = currentPage + 1;
      setCurrentPage(newPageNb);
      reloadRanking(newPageNb);
    }
  }

  function reloadRanking(newPageNb) {
    axios
      .get(urlJoin(apiUrl, "dinozs", "chapter-rankings", chapter,
        "?limit=" + pageSize,
        "&offset=" + (newPageNb * pageSize)
      ))
      .then((response) => {
        const headers = response.headers;
        setTotalPageNb(Math.trunc((Number(headers['x-total-count']) - 1) / pageSize) + 1);
        results = response.data;
        setResults([]);
        setResults(results);
        setIsLoading(false);
      });
  }

  return (
    <div>
      <div>
        <header className="pageCategoryHeader">{t("pantheonLabel")}</header>
        <div className="noFlexTop">
          <img alt="" className="noFlexTopImg" src={book} />
          <span className="textPantheon">{t("pantheonText")}</span>
        </div>

        {isLoading === true && (
          <MoonLoader
            color="#c37253"
            css="margin-left : 250px; margin-top : 15px;"
          />
        )}
        {isLoading === false && (
          <div>
            <select
              className="choiceFilterPantheon"
              id="selectChapter"
              ref={selectChapterRef}
              onChange={(e) => {
                changeFilter();
              }}
            >
              <option value="default">{t("pantheonChapter")}</option>
              <option value="default">{t("classement.general")}</option>
              <option value="Moueffe">{"Moueffe"}</option>
              <option value="Picori">{"Picori"}</option>
              <option value="Castivore">{"Castivore"}</option>
              <option value="Sirain">{"Sirain"}</option>
              <option value="Winks">{"Winks"}</option>
              <option value="Gorilloz">{"Gorilloz"}</option>
              <option value="Cargou">{"Cargou"}</option>
              <option value="Hippoclamp">{"Hippoclamp"}</option>
              <option value="Rokky">{"Rokky"}</option>
              <option value="Pigmou">{"Pigmou"}</option>
              <option value="Wanwan">{"Wanwan"}</option>
              <option value="Goupignon">{"Goupignon"}</option>
              <option value="Kump">{"Kump"}</option>
              <option value="Pteroz">{"Pteroz"}</option>
              <option value="Santaz">{"Santaz"}</option>
              <option value="Ouistiti">{"Ouistiti"}</option>
              <option value="Korgon">{"Korgon"}</option>
              <option value="Kabuki">{"Kabuki"}</option>
              <option value="Serpantin">{"Serpantin"}</option>
              <option value="Soufflet">{"Soufflet"}</option>
              <option value="Feross">{"Feross"}</option>
            </select>

            {chapter === "default" && (
              <h2 className="miniHeaders2">
                {t("pantheonMiniHeader1")}
                {"Dinoz"}
                {t("pantheonMiniHeader2")}
              </h2>
            )}
            {chapter !== "" && chapter !== "default" && (
              <h2 className="miniHeaders2">
                {t("pantheonMiniHeader1")}
                {chapter}
                {"(s)"}
                {t("pantheonMiniHeader2")}
              </h2>
            )}

            {chapter !== "" && (
              <div className="inline">
                <img className="imgStarsPantheon" src={stars} alt="" />
                <div>
                  {results.map((dinoz, i) => {
                    return (
                      <div className="chapterPantheon" key={i}>
                        <div className={"borderDinozFightInPantheon"}>
                          <div className="tbodyFight">
                            <td className="picBoxFight">
                              <div className="dinoSheet">
                                <div className="dinoSheetWrapFight">
                                  <DinozRenderTile
                                    appCode={dinoz.appearanceCode}
                                    size={100}
                                  />
                                </div>
                              </div>
                            </td>
                            <td className="infoBoxFight">
                              <div className="fiche">{t("fiche")}</div>
                              <table className="fichePlayerCard">
                                <tbody>
                                  <tr>
                                    <th>{t("evo.name")}</th>
                                    <th className="menuDinozFichePlayer">
                                      <span className="paddingRace">
                                        {dinoz.name}
                                      </span>
                                    </th>
                                  </tr>
                                  <tr>
                                    <th>{t("maitre")}</th>
                                    <th className="menuDinozFichePlayer">
                                      {dinoz.masterName}
                                    </th>
                                  </tr>
                                  <tr>
                                    <th>{t("niveau")}</th>
                                    <th className="menuDinozFichePlayer">
                                      {dinoz.level}
                                    </th>
                                  </tr>
                                  <tr>
                                    <th>{t("position")}</th>
                                    <th className="menuDinozFichePlayer">
                                      {"#"}
                                      {i + 1 + pageSize * currentPage}
                                    </th>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
                <img className="imgStarsPantheon" src={stars} alt="" />
              </div>
            )}

            {chapter !== "" && (
              <div className="rankingsBottom">
                <img
                  alt=""
                  onClick={(e) => {
                    previousPage();
                  }}
                  className="returnArrowLeft"
                  src={moveWest}
                />
                <span className="spanPagination">{(currentPage + 1)} / {totalPageNb}</span>
                <img
                  alt=""
                  onClick={(e) => {
                    nextPage();
                  }}
                  className="returnArrowRight"
                  src={moveEast}
                />
              </div>
            )}

          </div>
        )}
      </div>
    </div>
  );
}
