import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { apiUrl } from "../../../index";
import getHistoryImageFromKey from "../../utils/HistoryImageFromKey";
import { useUserData } from "../../../context/userData";
import { History } from "../../../types/history";
import Section from "../../../components/Section";
import { Loader } from "../../../components/Loader";
import Content from "../../../components/Content";
import moveEast from "../../../media/actions/act_move_e.gif";
import moveWest from "../../../media/actions/act_move_w.gif";
import type { i18n } from "i18next";
import "./MyHistory.css";
import urlJoin from "url-join";

type Props = {};

export default function MyHistory(props: Props) {
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState(true);
  const [history, setHistory] = useState<History[]>([]);
  const { accountId } = useUserData();
  const pageSize = 100;
  let [currentPage, setCurrentPage] = useState(0);
  let [totalPageNb, setTotalPageNb] = useState(0);
  let [dailyShinies, setDailyShinies] = useState("0");
  let [dailyRaidFights, setDailyRaidFights] = useState("0");

  useEffect(() => {
    reloadHistory(0);
  }, [accountId]);

  function previousPage() {
    if (currentPage > 0) {
      const newPageNb = currentPage - 1;
      setCurrentPage(newPageNb);
      reloadHistory(newPageNb);
    }
  }

  function nextPage() {
    if (currentPage < totalPageNb - 1) {
      const newPageNb = currentPage + 1;
      setCurrentPage(newPageNb);
      reloadHistory(newPageNb);
    }
  }

  function reloadHistory(newPageNb) {
    window.scrollTo(0, 0);
    axios.get<History[]>(urlJoin(apiUrl, "history", accountId, "?limit=" + pageSize + "&offset=" + (newPageNb * pageSize)))
      .then((response) => {
        const headers = response.headers;
        setTotalPageNb(Math.trunc((Number(headers['x-total-count']) - 1) / pageSize) + 1);
        setHistory([]);
        setHistory(response.data);

        axios.get<string>(urlJoin(apiUrl, "account", accountId, "shinies"))
            .then((data) => {
              setDailyShinies(data.data);
            });

        axios.get<string>(urlJoin(apiUrl, "account", accountId, "dailyRaidFights"))
            .then((data) => {
              setDailyRaidFights(data.data);
              setIsLoading(false);
            });
      });
  }

  return (
    <div className="TEMP_mainContainer">
      <Section title={t("historyLabel")}>
        <div>
          <Content className="mb-4">
            <p>{t("history")}</p>
          </Content>
          {isLoading === true ? (
            <Loader css="margin: 2rem auto" />
          ) : (
              <div>
                <p>{t('shiniesFought')} {dailyShinies}</p>
                {(Number(dailyRaidFights) > 0) && <p>{t('raidFightsCount')} {dailyRaidFights}</p>}
                <HistoryList history={history} />
              </div>
          )}
        </div>
      </Section>
      
      <div className="rankingsBottom">
        <img alt="" onClick={(e) => {previousPage();}} className="returnArrowLeft" src={moveWest}/>
        <span className="spanPagination">{(currentPage + 1)} / {totalPageNb}</span>
        <img alt="" onClick={(e) => {nextPage();}} className="returnArrowRight" src={moveEast}/>
      </div>
    </div>
  );
}

const HistoryList = ({ history }: { history: History[] }) => {
  const { i18n } = useTranslation();
  return (
    <>
      {history.map((historyEvent) => {
        return (
          <HistoryItem
            key={historyEvent.numericalDate}
            seen={historyEvent.seen}
            icon={historyEvent.icon}
            stringDate={historyEvent.numericalDate}
            content={getContentFromHistory({ historyEvent, i18n })}
          />
        );
      })}
    </>
  );
};

function getContentFromHistory({
  historyEvent,
  i18n,
}: {
  historyEvent: History;
  i18n: i18n;
}) {
  switch (historyEvent.type) {
    case "fight":
      return getFightContentFromHistory({ historyEvent, i18n });

    case "sellIngredients":
      return getIngredientsContentFromHistory({ historyEvent, i18n });

    case "poison_regular":
      return getPoisonContentFromHistory({ historyEvent, i18n }, "regular");

    case "poison_piqure":
      return getPoisonContentFromHistory({ historyEvent, i18n }, "piqure");

    default:
      return (
        <span>{i18n.t(`history.phrase.${historyEvent.type}`, {...historyEvent, buyAmount: historyEvent.buyAmount ? historyEvent.buyAmount.toLocaleString() : null,})}</span>
      );
  }
}

function getIngredientsContentFromHistory({historyEvent, i18n,}: {
  historyEvent: History;
  i18n: i18n;
}) {
  const data = {
    ...historyEvent,
  };
  return (
      <span>
      {i18n.t(`history.phrase.sellIngredients`)}
        {data.buyAmount}
        {i18n.t(`phrase.buyDinoz2`)}
    </span>
  );
}

function getPoisonContentFromHistory({historyEvent, i18n,}: { historyEvent: History; i18n: i18n; }, poisonType) {
  const data = {...historyEvent,};
  if (poisonType.includes("regular")) {
    return (<span>{i18n.t(`history.phrase.poison_regular`)}{data.fromDinozName}{" ("}{data.fromUserName}{")."}</span>);
  } else {
    return (<span>{i18n.t(`history.phrase.poison_piqure`)}{data.fromDinozName}{" ("}{data.fromUserName}{")."}</span>);
  }
}

function getFightContentFromHistory({historyEvent, i18n,}: {
  historyEvent: History;
  i18n: i18n;
}) {
  const isDefense = historyEvent.icon === "hist_defense.gif";
  const victoryState =
    historyEvent.hasWon.toString() === "true"
      ? i18n.t("phrase.winA")
      : i18n.t("phrase.loss");
  const nameOfEnemyDinoz = getNameOfEnemyDinoz(historyEvent.toDinozName);

  function getNameOfEnemyDinoz(enemyDinozName: string) {
    if (enemyDinozName.charAt(0) !== null || enemyDinozName.charAt(0) === "@") {
      return i18n.t(enemyDinozName);
    } else if (enemyDinozName.length > 25) {
      return getCorrectNameForLang(enemyDinozName);
    }

    return enemyDinozName;
  }

  function getCorrectNameForLang(enemyDinozName: string) {
    const splitName = enemyDinozName.split("-");
    switch (i18n.language) {
      case "fr":
        return splitName[0];
      case "es":
        return splitName[1];
      case "en":
      default:
        return splitName[2];
    }
  }

  const data = {
    ...historyEvent,
    victoryState,
    nameOfEnemyDinoz,
  };

  return (
    <span>
      {i18n.t(
        `history.phrase.fight.${isDefense ? "defense" : "default"}`,
        data
      )}
    </span>
  );
}

const HistoryItem = ({
  icon,
  stringDate,
  seen,
  content,
}: {
  icon: string;
  stringDate: string;
  seen?: boolean;
  content: React.ReactNode;
}) => {
  return (
    <div className={"historyItem" + (!seen ? " is-notSeen" : "")}>
      <img alt="" src={getHistoryImageFromKey(icon)} />
      <div className="historyItem-content">
        <span>
          {stringDate}
          {" ⮚ "}
        </span>
        {content}
      </div>
    </div>
  );
};
