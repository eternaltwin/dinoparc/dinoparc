import { useTranslation } from "react-i18next";
import React from "react";
import Content from "../../../components/Content";
import GameSection from "./Dinoz/shared/GameSection";
import ShopForm from "../shared/ShopForm";

type Props = {
  callBack: () => void;
  refreshHistory: () => void;
};

export default function Shop({ callBack, refreshHistory }: Props) {
  const { t } = useTranslation();

  return (
    <GameSection title={t("shopLabel")}>
      <>
        <Content>{t("boutique.header")}</Content>

        <ShopForm
          className="mt-6"
          onSubmitted={() => {
            refreshHistory();
            callBack();
          }}
          returnToDinoz={() => {}}
        />
      </>
    </GameSection>
  );
}
