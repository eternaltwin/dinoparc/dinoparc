/* eslint-disable no-undef */
import { useTranslation } from "react-i18next";
import React from "react";
import Section from "../../../components/Section";
import { Loader } from "../../../components/Loader";
import Content from "../../../components/Content";

import "./MyCollection.css";
import RewardBox from "../shared/RewardBox";
import { useUserData } from "../../../context/userData";
import { useUserCollection } from "../../../hooks/useUserCollection";

type Props = {};

export default function MyCollection(props: Props) {
  const { t } = useTranslation();
  const { accountId, user } = useUserData();

  const { collection, collectionEpic, isLoading } = useUserCollection({
    userId: accountId,
  });

  return (
    <div className="TEMP_mainContainer">
      <Section title={t("collectionLabel")}>
        {isLoading === true ? (
          <Loader css="margin: 2rem auto" />
        ) : (
          <>
            <Content className="mb-3">
              <p>{t("collection.headers")}</p>
            </Content>
            <RewardBox
              title={`${t("collection.of")} ${user.userName}`}
              collection={collection}
            />

            {collectionEpic.length !== 0 && (
              <>
                <Content className="mt-5 mb-3">
                  <p>{t("collection.epic.headers")}</p>
                </Content>
                <RewardBox
                  title={t("collection.ep")}
                  collection={collectionEpic}
                  isEpic={true}
                />
              </>
            )}
          </>
        )}
      </Section>
    </div>
  );
}
