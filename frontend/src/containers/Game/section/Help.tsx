import { useTranslation } from "react-i18next";
import React, { useEffect } from "react";
import cycle from "../../../media/game/cycle.gif";
import Section from "../../../components/Section";
import SectionLight from "../../../components/SectionLight";
import Content from "../../../components/Content";
import TitleLight from "../../../components/TitleLight";

export default function Help() {
  const { t } = useTranslation();
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <div className="TEMP_mainContainer">
      <Section className="has-text-justified" title={t("helpLabel")}>
        <SectionLight title={t("help.intro.title")}>
          <Content>
            <p>{t("help.intro.content")}</p>
          </Content>
        </SectionLight>

        <SectionLight title={t("help.howtostart.title")}>
          <Content>
            <p>{t("help.start")}</p>
            <p>{t("help.howtostart.content")}</p>
          </Content>
        </SectionLight>

        <SectionLight title={t("help.training.title")}>
          <Content>
            <p>{t("help.training.content")}</p>
          </Content>
        </SectionLight>

        <SectionLight title={t("help.fight.title")}>
          <Content>
            <p>{t("help.fight.content0")}</p>
            <p>{t("help.fight.image")}</p>
            <img
              alt="Elemental Cycle"
              src={cycle}
              className="chapterImg image mx-auto my-4"
            />

            <p>{t("help.fight.content1")}</p>
            <TitleLight>{t("help.fight.danger.title")}</TitleLight>
            <p>{t("help.fight.danger.content")}</p>
          </Content>
        </SectionLight>

        <SectionLight title={t("help.tips.title")}>
          <Content>
            <TitleLight>{t("help.cheatsheet.title")}</TitleLight>

            <ul className="has-text-left">
              <li>{t("help.cheatsheet.content0")}</li>
              <li>{t("help.cheatsheet.content1")}</li>
              <li>{t("help.cheatsheet.content2")}</li>
              <li>{t("help.cheatsheet.content3")}</li>
              <li>{t("help.cheatsheet.content4")}</li>
              <li>{t("help.cheatsheet.content5")}</li>
              <li>{t("help.cheatsheet.content6")}</li>
              <li>{t("help.cheatsheet.content7")}</li>
              <li>{t("help.cheatsheet.content8")}</li>
            </ul>

            <TitleLight>{t("help.tips.shiny.title")}</TitleLight>
            <p>{t("help.tips.shiny.content")}</p>

            <TitleLight>{t("help.tips.wistiti.title")}</TitleLight>
            <p>{t("help.tips.wistiti.content")}</p>
            <p>{t("help.tips.wistiti.content2")}</p>

            <TitleLight>{t("help.tips.playinfinite.title")}</TitleLight>
            <p>{t("help.tips.playinfinite.content")}</p>
          </Content>
        </SectionLight>

        <SectionLight title={t("aide.clan")}>
          <Content>
              <p className="clanWarHelp">{t('aide.clan.dump')}</p>
          </Content>
        </SectionLight>

        <SectionLight title={t("help.faq.title")}>
          <Content>
            <p>{t("help.faq.content")}</p>

            <TitleLight>{t("help.faq.whatelementchoose.title")}</TitleLight>
            <p>{t("help.faq.whatelementchoose.content")}</p>

            <TitleLight>{t("help.faq.alwaysdied.title")}</TitleLight>
            <p>{t("help.faq.alwaysdied.content")}</p>

            <TitleLight>{t("help.faq.diffdinoparc.title")}</TitleLight>
            <p>{t("help.faq.diffdinoparc.content")}</p>

            <TitleLight>{t("help.faq.dinorpg.title")}</TitleLight>
            <p>{t("help.faq.dinorpg.content")}</p>

            <p>
              {t("stillQuestions")}
              <a
                className="elementOfMenu"
                href="https://discord.gg/VR8bjWpvs4"
                target="_blank"
                rel="noopener noreferrer"
              >
                {t("exchangeLabel")}
              </a>
            </p>
          </Content>
        </SectionLight>
      </Section>
    </div>
  );
}
