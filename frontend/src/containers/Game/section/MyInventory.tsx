import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { apiUrl } from "../../../index";
import getTotemImageByName from "../../utils/TotemImageByName";
import getConsumableByKey from "../../utils/ConsumableByKey";
import bag from "../../../media/game/inventoryBag.gif";
import { confirmAlert } from "react-confirm-alert";
import HelpTooltip from "../../../components/HelpTooltip";
import { useUserData } from "../../../context/userData";
import AsyncSection from "../../../components/AsyncSection";
import GameSection from "./Dinoz/shared/GameSection";
import ContentWithImage, {
  ImageWrap,
} from "../../../components/ContentWithImage";
import Content from "../../../components/Content";
import Message from "../../../components/Message";
import urlJoin from "url-join";

type Dinoz = {
  dinozId: string;
  dinozName: string;
  dinozLife: number;
};

type Item = {
  key: string;
  isProtected: boolean;
  quantity: number;
  actionType?: "USE_ONE" | "CHAMPIFUZ" | "DINOZ" | "CLAN";
  withElementChoice?: boolean;
};

type Props = {
  callBack: () => void;
  refreshCash: () => void;
};
export default function MyInventory({ callBack, refreshCash }: Props) {
  const { t, i18n } = useTranslation();
  const { accountId } = useUserData();
  const [isLoading, setIsLoading] = useState(true);
  const [quantityMap, setQuantityMap] = useState<{ [key: string]: number }>({});
  const [dinozList, setDinozList] = useState<Dinoz[]>([]);
  const [successMsg, setSuccessMsg] = useState<string | null>(null);
  const [errorType, setErrorType] = useState<"INUTILE" | "NO_STOCK" | "LIMIT_REACHED" | "BAD_NAME" | "API_ERROR" | null>(null);
  let [lootFromChocolateEgg, setLootFromChocolateEgg] = useState("");

  useEffect(() => {
    refreshInventoryData({ accountId });
  }, [accountId]);

  async function doPatchApiCall(apiCallPromise: () => Promise<any>) {
    setIsLoading(true);
    setErrorType(null);
    setSuccessMsg(null);
    setLootFromChocolateEgg("");
    try {
      await apiCallPromise();
      refreshInventoryData({ accountId });
    } catch (e) {
      setErrorType("API_ERROR");
      throw e;
    } finally {
      setIsLoading(false);
      scrollTop();
    }
  }

  function scrollTop() {
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  }

  function refreshInventoryData({ accountId }: { accountId: string }) {
    setIsLoading(true);
    axios
      .get<{
        inventoryItemsMap: { [key: string]: number };
        dinozList: Dinoz[];
      }>(urlJoin(apiUrl, "account", accountId, "inventory"))
      .then(({ data }) => {
        setQuantityMap(data.inventoryItemsMap);
        setDinozList(data.dinozList);
        setIsLoading(false);
        scrollTop();
      });
  }

  function getSuccessMsg(response: {
    successMessageFr?: string;
    successMessageEs?: string;
    successMessageEn?: string;
  }) {
    if (i18n.language === "fr") {
      return response.successMessageFr;
    } else if (i18n.language === "es") {
      return response.successMessageEs;
    } else if (i18n.language === "en") {
      return response.successMessageEn;
    }
  }

  function NameDinoz(dinozId: string) {
    confirmAlert({
      customUI: ({ onClose }) => {
        return (
          <form
            className="neoparcConfirmOverlay"
            action="#"
            onSubmit={(e) => {
              e.preventDefault();
              const form = e.target as HTMLFormElement;
              const formData = new FormData(form);
              const newName = formData.get("newName") as string;
              setNewDinozName({ newName: newName.substring(0, 15), dinozId });
              onClose();
            }}
          >
            <div className="promptName">
              <p>{t("giveNameToDinoz")}</p>
              <input
                id="dinozNameChange"
                name="newName"
                type="text"
                maxLength={15}
              />
              <button className="btnOkPrompt" type="submit">
                Ok!
              </button>
            </div>
          </form>
        );
      },
      closeOnEscape: true,
    });
  }

  async function setNewDinozName({
    newName,
    dinozId,
  }: {
    newName: string;
    dinozId: string;
  }) {
    await doPatchApiCall(async () => {
      const { data } = await axios.put<{
        badNameError: boolean;
        success: boolean;
        successMessageFr?: string;
        successMessageEs?: string;
        successMessageEn?: string;
      }>(urlJoin(apiUrl, "account", accountId, "pruniac", dinozId, newName));
      setErrorType(data.badNameError ? "BAD_NAME" : null);
      setSuccessMsg(data.success ? getSuccessMsg(data) : null);
      callBack();
    }).catch();
  }

  async function giveDinozObject({
                                   dinozId,
                                   itemKey,
                                   pillElement,
                                   pillQtyElement,
                                 }: {
    dinozId: string;
    itemKey: string;
    pillElement?: string;
    pillQtyElement?: number;
  }) {
    if (itemKey === "Pruniac") {
      NameDinoz(dinozId);
      return true;
    } else if (itemKey === "Eternity Pill") {
      return giveDinozEthernityPill({ dinozId, pillElement, pillQtyElement });
    }

    const putApiUrl = getPutDinozObjectUrl({ dinozId, itemKey });

    try {
      await doPatchApiCall(async () => {
        const { data } = await axios.put<{
          inutileError?: boolean;
          noStockError?: boolean;
          success?: boolean;
          successMessageFr?: string;
          successMessageEs?: string;
          successMessageEn?: string;
        }>(putApiUrl);

        setErrorType(data.inutileError ? "INUTILE" : data.noStockError ? "NO_STOCK" : null);
        setSuccessMsg(data.success ? getSuccessMsg(data) : null);
      });
      return true;
    } catch (e) {
      return false;
    }
  }

  async function giveDinozEthernityPill({
                                          dinozId,
                                          pillElement,
                                          pillQtyElement
                                        }: {
    dinozId: string;
    pillElement: string;
    pillQtyElement: number;
  }) {
    if (window.confirm(t("confirm") + " (+" + pillQtyElement + " " + t(pillElement) + ")")) {
      const apiPutUrl = urlJoin(apiUrl, "account", accountId, "lvlUpWithPill", dinozId, pillElement, String(pillQtyElement));
      try {
        await doPatchApiCall(async () => {
          const { data } = await axios.put<{
            inutileError?: boolean;
            noStockError?: boolean;
            success?: boolean;
            successMessageFr?: string;
            successMessageEs?: string;
            successMessageEn?: string;
          }>(apiPutUrl);

          setErrorType(data.inutileError ? "INUTILE" : data.noStockError ? "NO_STOCK" : null);
          setSuccessMsg(data.success ? getSuccessMsg(data) : null);
        });

        return true;
      } catch (e) {
        return false;
      }
    }
  }

  function getPutDinozObjectUrl({
    dinozId,
    itemKey,
  }: {
    dinozId: string;
    itemKey: string;
  }) {
    const baseUrl = urlJoin(apiUrl, "account", accountId);

    switch (itemKey) {
      case "Potion Irma":
        return baseUrl + "/irma/" + dinozId;
      case "Potion Ange":
        return baseUrl + "/ange/" + dinozId;
      case "Nuage Burger":
        return baseUrl + "/nuage/" + dinozId + "/1";
      case "Pain Chaud":
        return baseUrl + "/pain/" + dinozId;
      case "Tarte Viande":
        return baseUrl + "/tarte/" + dinozId;
      case "Médaille Chocolat":
        return baseUrl + "/medaille/" + dinozId;
      case "Lait de Cargou":
        return baseUrl + "/lait-cargou/" + dinozId;
      case "Potion Sombre":
        return baseUrl + "/darkpotion/" + dinozId + "/1";
      case "bonus_fire":
        return baseUrl + "/charme-feu/" + dinozId + "/1";
      case "bonus_wood":
        return baseUrl + "/charme-terre/" + dinozId + "/1";
      case "bonus_water":
        return baseUrl + "/charme-eau/" + dinozId + "/1";
      case "bonus_thunder":
        return baseUrl + "/charme-foudre/" + dinozId + "/1";
      case "bonus_air":
        return baseUrl + "/charme-air/" + dinozId + "/1";
      case "bonus_prismatik":
        return baseUrl + "/charme-prismatik/" + dinozId + "/1";
      case "bonus_claw":
        return baseUrl + "/griffes/" + dinozId;
      case "Bave Loupi":
        return baseUrl + "/bave/" + dinozId;
      case "CanneAPeche":
        return baseUrl + "/fishing-rod/" + dinozId;
      case "Bol Ramens":
        return baseUrl + "/ramens/" + dinozId + "/1";
      case "bonus_tigereye":
        return baseUrl + "/eye/" + dinozId;
      case "bonus_pill":
        return baseUrl + "/pill/" + dinozId;
      case "bonus_boostaggro":
        return baseUrl + "/focus-aggro/" + dinozId + "/1";
      case "bonus_boostnature":
        return baseUrl + "/focus-nature/" + dinozId + "/1";
      case "bonus_boostwater":
        return baseUrl + "/focus-water/" + dinozId + "/1";
      case "bonus_mahamuti":
        return baseUrl + "/focus-mahamuti/" + dinozId + "/1";
      case "Coulis Cerise":
        return baseUrl + "/cherry/" + dinozId + "/1";
      case "Tisane des bois":
        return baseUrl + "/tea/" + dinozId + "/1";
      case "Bière de Dinojak":
        return baseUrl + "/beer/" + dinozId + "/1";
    }
  }

  async function sendElementsToClan({
    itemKey,
    quantity,
  }: {
    itemKey: string;
    quantity: number;
  }) {
    await doPatchApiCall(async () => {
      const { data } = await axios.post<{
        inutileError?: boolean;
        noStockError?: boolean;
        success?: boolean;
        successMessageFr?: string;
        successMessageEs?: string;
        successMessageEn?: string;
      }>(urlJoin(apiUrl, "clans", accountId, itemKey, String(quantity)));
      setErrorType(
        data.inutileError ? "INUTILE" : data.noStockError ? "NO_STOCK" : null
      );
      setSuccessMsg(getSuccessMsg(data));
    }).catch();
  }

  function keyIsOtherItem(key) {
    return [
      "Champifuz",
      "Bons du Trésor",
      "Ticket Cadeau",
      "EGG",
      "Goutte Glu",
      "Monochrome",
      "Magik",
      "JetonCasinoBronze",
      "JetonCasinoArgent",
      "JetonCasinoOr"
    ].some((item) => key.includes(item));
  }

  function keyIsFoodCraft(key) {
    return [
      "Feuille de Pacifique",
      "Oréade blanc",
      "Tige de Roncivore",
      "Anémone solitaire",
      "Perche perlée",
      "Grémille grelottante",
      "Cube de glu",
      "Anguille"
    ].some((item) => key.includes(item));
  }

  function isItemUsable(key) {
    return ["Champifuz", "Monochrome", "Magik", "EGG_CHOC"].some((item) => key.includes(item));
  }

  async function utiliserSpecialItem(specialItem) {
    if (specialItem.includes("Champifuz")) {
      await doPatchApiCall(async () => {
        const { data } = await axios.put<boolean>(urlJoin(apiUrl, "account", accountId, "champifuz"));

        if (data) {
          setSuccessMsg(t("champifuzSuccess"));
        }
      }).catch();
    }

    if (specialItem.includes("EGG_CHOC")) {
      if (window.confirm(t("confirm"))) {
        await doPatchApiCall(async () => {
          const { data } = await axios.put(urlJoin(apiUrl, "account", accountId, "chocolate-egg"));
          if (data) {
            setLootFromChocolateEgg(data);
            refreshCash();
          }
        }).catch();
      }
    }

    if (specialItem.includes("Ticket Monochrome")) {
      await doPatchApiCall(async () => {
        const { data } = await axios.put<boolean>(urlJoin(apiUrl, "account", accountId, "champifuz-monochrome"));

        if (data) {
          setSuccessMsg(t("champifuzSuccess"));
        }
      }).catch();
    }

    if (specialItem.includes("Ticket Magik")) {
      await doPatchApiCall(async () => {
        const { data } = await axios.put<boolean>(urlJoin(apiUrl, "account", accountId, "champifuz-magik"));

        if (data) {
          setSuccessMsg(t("champifuzSuccess"));
        }
      }).catch();
    }
  }

  async function utiliserSpecialChampifuzTicket({itemKey, specialChampifuz,}: {
    itemKey: string;
    specialChampifuz: string;
  }) {
    if (!itemKey.includes("Champifuz Special")) {
      return false;
    }

    try {
      await doPatchApiCall(async () => {
        const { data } = await axios.put<boolean>(urlJoin(apiUrl, "account", accountId, "champifuz-special", specialChampifuz));
        if (data) {
          setSuccessMsg(t("champifuzSuccess"));
        }
      });
      return true;
    } catch {
      return false;
    }
  }

  function isItemProtected(key: string) {
    if (
        key === "Potion Sombre" ||
        key === "Coulis Cerise" ||
        key === "Eternity Pill" ||
        key === "EGG_11" ||
        key === "EGG_14" ||
        key === "EGG_18" ||
        key === "EGG_20" ||
        key === "EGG_X" ||
        key === "Ticket Magik" ||
        key === "Potion Irma Surplus" ||
        key === "Tisane des bois" ||
        key === "Bière de Dinojak" ||
        key === "Bons du Trésor"
    ) {
      return true;
    }
    return false;
  }

  function isItemIrmaPotion(key: string) {
    if (key === "Potion Irma" || key === "Potion Irma Surplus") {
      return true;
    }
    return false;
  }

  const irmaItems: Item[] = Object.entries(quantityMap)
    .filter(([key, quantity]) => quantity > 0 && isItemIrmaPotion(key))
    .map(([key, quantity]) => ({
      key,
      quantity,
      isProtected: isItemProtected(key),
    }));

  const dinozItems: Item[] = Object.entries(quantityMap)
    .filter(
      ([key, quantity]) =>
        quantity > 0 &&
        !isItemIrmaPotion(key) &&
        !key.startsWith("totem") &&
        !keyIsOtherItem(key) &&
        !keyIsFoodCraft(key)
    )
    .sort(([key1], [key2]) =>
      t("boutique." + key1).localeCompare(t("boutique." + key2))
    )
    .map(([key, quantity]) => ({
      key,
      quantity,
      isProtected: isItemProtected(key),
      actionType: "DINOZ",
      withElementChoice: key.startsWith("Eternity Pill"),
    }));

  const totemItems: Item[] = Object.entries(quantityMap)
    .filter(([key, quantity]) => quantity > 0 && key.startsWith("totem"))
      .sort(([key1], [key2]) =>
          t("boutique." + key1).localeCompare(t("boutique." + key2))
      )
    .map(([key, quantity]) => ({
      key,
      quantity,
      isProtected: isItemProtected(key),
      actionType: "CLAN",
    }));

  const otherItems: Item[] = Object.entries(quantityMap)
    .filter(
      ([key, quantity]) =>
        quantity > 0 && !key.startsWith("totem") && keyIsOtherItem(key)
    )
      .sort(([key1], [key2]) =>
          t("boutique." + key1).localeCompare(t("boutique." + key2))
      )
    .map(([key, quantity]) => ({
      key,
      quantity,
      isProtected: isItemProtected(key),
      actionType: isItemUsable(key)
        ? (key.startsWith("Ticket Champifuz Special"))
          ? "CHAMPIFUZ"
          : "USE_ONE"
        : undefined,
    }));

  const foodItems: Item[] = Object.entries(quantityMap)
    .filter(
      ([key, quantity]) =>
        quantity > 0 && keyIsFoodCraft(key) && !key.startsWith("totem")
    )
      .sort(([key1], [key2]) =>
          t("boutique." + key1).localeCompare(t("boutique." + key2))
      )
    .map(([key, quantity]) => ({
      key,
      quantity,
      isProtected: isItemProtected(key),
      actionType: isItemUsable(key) ? "USE_ONE" : undefined,
    }));

  const allTables: {
    key: string;
    itemHeadLabel?: string;
    actionHeadLabel?: string;
    isTotem?: boolean;
    items: Item[];
  }[] = [
    {
      key: "irma",
      items: irmaItems,
    },
    {
      key: "dinoz",
      actionHeadLabel: t("Dinoz"),
      items: dinozItems,
    },
    {
      key: "totem",
      itemHeadLabel: t("boutique.nom.ing"),
      actionHeadLabel: t("Totem"),
      items: totemItems,
      isTotem: true,
    },
    {
      key: "other",
      actionHeadLabel: t("objetsUniques"),
      items: otherItems,
    },
    {
      key: "food",
      actionHeadLabel: t("naturalResources"),
      items: foodItems,
    },
  ];

  const errorMessage =
    errorType === "LIMIT_REACHED"
      ? t("limitReachedError")
      : errorType === "BAD_NAME"
      ? t("mauvaisNomPruniac")
      : errorType === "INUTILE"
      ? t("inventaire.nouse")
      : errorType === "NO_STOCK"
      ? t("inventaire.nostock")
      : errorType === "API_ERROR"
      ? t("api.error")
      : undefined;

  return (
    <GameSection title={t("inventoryLabel")}>
      <ContentWithImage className="mb-6">
        <ImageWrap noBorder={true}>
          <img alt="" src={bag} />
        </ImageWrap>
        <Content>{t("inventaire.header")}</Content>
      </ContentWithImage>
      {errorMessage != null && <Message type="danger">{errorMessage}</Message>}
      {successMsg != null && <Message type="success">{successMsg}</Message>}

      {lootFromChocolateEgg !== "" && (
        <Message>
          <>
            {t("EGG_CHOC_CONTENT") +
              lootFromChocolateEgg.substring(
                lootFromChocolateEgg.indexOf(";") + 1,
                lootFromChocolateEgg.length
              ) + "  "}
            <img className="imageFromChocolateEgg" alt="" src={getConsumableByKey(lootFromChocolateEgg.substring(0, lootFromChocolateEgg.indexOf(";")))}/>
          </>
        </Message>
      )}

      <AsyncSection loading={isLoading}>
        <>
          {allTables.map(
            ({ key, itemHeadLabel, actionHeadLabel, items, isTotem }) => (
              <Table key={key} itemHeadLabel={itemHeadLabel || t("boutique.nom")} actionHeadLabel={actionHeadLabel}>
                <>
                  {items.map(
                    ({
                      key,
                      quantity,
                      isProtected,
                      actionType,
                      withElementChoice,
                    }) => (
                      <TableItem
                        key={key}
                        itemKey={key}
                        quantity={quantity}
                        isProtected={isProtected}
                        isTotem={isTotem}
                      >
                        {actionType === "USE_ONE" ? (
                          <button className="useOne" type="button" onClick={(e) => {utiliserSpecialItem(key);}}>{t("use")}</button>
                        ) : actionType === "CLAN" ? (
                          <form
                            action="#"
                            onSubmit={(e) => {
                              e.preventDefault();

                              const formData = new FormData(
                                e.target as HTMLFormElement
                              );
                              const quantity = Number(formData.get("quantity"));

                              if (isNaN(quantity) || quantity <= 0) {
                                alert(t("inventaire.donnerClanQuantityError"));
                                return;
                              }

                              sendElementsToClan({
                                itemKey: key,
                                quantity,
                              });
                            }}
                          >
                            <button className="giveClan" type="submit">
                              {t("inventaire.donnerClan")}
                            </button>
                            <input
                              className="numberFieldInv"
                              type="number"
                              min="0"
                              max={quantity}
                              name="quantity"
                            />
                          </form>
                        ) : actionType === "CHAMPIFUZ" ? (
                          <form
                            action="#"
                            onSubmit={(e) => {
                              e.preventDefault();
                              const formData = new FormData(
                                e.target as HTMLFormElement
                              );

                              utiliserSpecialChampifuzTicket({
                                itemKey: key,
                                specialChampifuz: formData.get(
                                  "specialChampifuz"
                                ) as string,
                              });
                            }}
                          >
                            <select
                              className="choiceInventoryWithSelect"
                              id="specialChampifuz"
                              name="specialChampifuz"
                            >
                              <option value="Moueffe">{"Moueffe"}</option>
                              <option value="Picori">{"Picori"}</option>
                              <option value="Castivore">{"Castivore"}</option>
                              <option value="Sirain">{"Sirain"}</option>
                              <option value="Winks">{"Winks"}</option>
                              <option value="Gorilloz">{"Gorilloz"}</option>
                              <option value="Cargou">{"Cargou"}</option>
                              <option value="Hippoclamp">{"Hippoclamp"}</option>
                              <option value="Rokky">{"Rokky"}</option>
                              <option value="Pigmou">{"Pigmou"}</option>
                              <option value="Wanwan">{"Wanwan"}</option>
                              <option value="Kump">{"Kump"}</option>
                              <option value="Pteroz">{"Pteroz"}</option>
                              <option value="Korgon">{"Korgon"}</option>
                            </select>

                            <button className="useOne" type="submit">
                              {t("use")}
                            </button>
                          </form>
                        ) : actionType === "DINOZ" ? (
                          <form
                            action="#"
                            onSubmit={async (e) => {
                              e.preventDefault();
                              const formElement = e.target as HTMLFormElement;
                              const formData = new FormData(formElement);
                              const success = await giveDinozObject({
                                itemKey: key,
                                dinozId: formData.get("dinozId") as string,
                                pillElement: (formData.get("pillElement") as | string | null) || undefined,
                                pillQtyElement: (formData.get("pillQtyElement") as undefined),
                              });

                              if (success) {
                                const dinozSelect = formElement.querySelector('select[name="dinozId"]') as HTMLSelectElement | null;
                                if (dinozSelect) {
                                  dinozSelect.value = "";
                                }
                                const pillSelect = formElement.querySelector('select[name="pillElement"]') as HTMLSelectElement | null;
                                if (pillSelect) {
                                  pillSelect.value = "";
                                }
                                const pillQtySelect = formElement.querySelector('select[name="pillQtyElement"]') as HTMLSelectElement | null;
                                if (pillQtySelect) {
                                  pillQtySelect.value = "1";
                                }
                              }
                            }}
                          >
                            {withElementChoice === true && (
                                <>
                                  <select className="choiceInventoryWithSelect" id="pillQtyElement" name="pillQtyElement">
                                    <option value="1">1</option>
                                    <option value="3">3</option>
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="100">100</option>
                                  </select>

                                  <select className="choiceInventoryWithSelect" id="pillElement" name="pillElement">
                                    <option value="Feu">{t("Feu")}</option>
                                    <option value="Terre">{t("Terre")}</option>
                                    <option value="Eau">{t("Eau")}</option>
                                    <option value="Foudre">{t("Foudre")}</option>
                                    <option value="Air">{t("Air")}</option>
                                  </select>
                                </>
                            )}
                            <select
                              className="choiceInventory"
                              id={key}
                              name="dinozId"
                              onChange={(e) => {
                                const target = e.target as HTMLSelectElement;
                                const form = target.closest("form");
                                const submit = form.querySelector(
                                  '[type="submit"]'
                                ) as HTMLButtonElement;
                                submit.click();
                              }}
                            >
                              <option value="">{t("inventaire.choisir")}</option>
                              {dinozList.map(
                                ({ dinozId, dinozName, dinozLife }) => {
                                  return (
                                    <option key={dinozId} value={dinozId}>
                                      {dinozName}
                                      {" : "}
                                      {dinozLife}
                                      {"% "}
                                      {t("inventaire.devie")}
                                    </option>
                                  );
                                }
                              )}
                            </select>
                            <button className="sr-only" type="submit">
                              Give
                            </button>
                          </form>
                        ) : null}
                      </TableItem>
                    )
                  )}
                </>
              </Table>
            )
          )}
        </>
      </AsyncSection>
    </GameSection>
  );
}

type TableProps = {
  itemHeadLabel: string;
  actionHeadLabel?: string;
  children: React.ReactNode;
};
const Table = ({ itemHeadLabel, actionHeadLabel, children }: TableProps) => {
  return (
    <table className="shopItems">
      <thead>
        <tr>
          <th className="empty" />
          <th className="nameTitle">{itemHeadLabel}</th>
          <th className="quantityHeader">{actionHeadLabel || ""}</th>
        </tr>
      </thead>

      <tbody>{children}</tbody>
    </table>
  );
};

type TableItemProps = {
  isTotem?: boolean;
  itemKey: string;
  quantity: number;
  isProtected?: boolean;
  children?: React.ReactNode | null;
};
const TableItem = ({
  isTotem,
  itemKey,
  quantity,
  isProtected,
  children,
}: TableItemProps) => {
  const { t } = useTranslation();

  return (
    <tr className="tableColor">
      <td className="icon">
        <img alt="" src={isTotem ? getTotemImageByName(itemKey) : getConsumableByKey(itemKey)}/>
      </td>
      <td className="name">
        <p className="titreObjet">
          <span className="espaceNom">
            {isTotem ? t(itemKey) : t("boutique." + itemKey)}
          </span>
          <HelpTooltip
            tip={
              isTotem
                ? t("tooltip." + itemKey)
                : t("boutique.tooltip." + itemKey)
            }
          />
        </p>
        <div className="possession">
          {t("boutique.possession")}
          {quantity}
          {isProtected === true && <span>{t("notsteal")}</span>}
        </div>
      </td>
      {children !== undefined && <td className="count">{children}</td>}
    </tr>
  );
};
