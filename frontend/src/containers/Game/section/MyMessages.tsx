import { useTranslation } from "react-i18next";
import React, { useState, useEffect, useRef } from "react";
import Select from "react-select";
import Store from "../../../utils/Store";
import { MoonLoader } from "react-spinners";
import ReactTooltip from "react-tooltip";
import axios from "axios";
import { apiUrl } from "../../../index";
import goBack from "../../../media/actions/act_move_w.gif";
import arrowRight from "../../../media/actions/act_move_e.gif";
import msgIcon from "../../../media/actions/act_speak.gif";
import cancelIcon from "../../../media/actions/act_cancel.gif";
import letterIcon from "../../../media/collection/letter.gif";
import deathIcon from "../../../media/actions/act_unsubscribe.jpg";
import checkIcon from "../../../media/actions/act_check.gif";
import errorIcon from "../../../media/actions/hist_error.gif";
import downloadIcon from "../../../media/actions/act_download.svg";
import urlJoin from "url-join";

const DEFAULT_N_THREAD_MESSAGES = 100;
const MESSAGES_PER_PAGE = 10;

interface ValueAndLabel {
  value: string;
  label: unknown;
}

export default function MyMessages(props) {
  const { t, i18n } = useTranslation();
  const store = Store.getInstance();
  const [isLoading] = useState(false);
  const [messages, setMessages] = useState({});
  const [selectedAccountId, setSelectedAccountId] = useState("");
  const [messagesPreview, setMessagesPreview] = useState(false);
  const [multipleSelectionMessages, setMultipleSelectionMessages] = useState<
    Set<unknown>
  >(new Set());
  const [reload, setReload] = useState<boolean>();
  const [showNewMessage, setShowNewMessage] = useState(false);
  const [receptorAccountId, setReceptorAccountId] = useState<
    Partial<ValueAndLabel>
  >({});
  const [avaiableDestinators, setAvaiableDestinators] =
    useState<Record<string, string>>();
  const [newMsgBody, setNewMsgBody] = useState("");
  const [threadNewMsgBody, setThreadNewMsgBody] = useState("");
  const [blackList, setBlackList] = useState({});
  const [showBlackList, setShowBlackList] = useState(false);
  const [newBlackListAccount, setNewBlackListAccount] = useState<
    Partial<ValueAndLabel>
  >({});
  const [triggerDestinatorSelected, setTriggerDestinationSelected] =
    useState(false);
  const [messageSending, setMessageSending] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState();
  const [messageError, setMessageError] = useState();
  const [showStickyOverlay, setShowStickyOverlay] = useState(true);
  const [threadMsgsNumber, setThreadMsgsNumber] = useState(
    DEFAULT_N_THREAD_MESSAGES
  );
  const [activePage, setActivePage] = useState(0);
  const scrollBottomRef = useRef<HTMLDivElement>();
  const messagesThread = useRef();

  useEffect(() => {
    window.scrollTo(0, 0);
    if (props.preSelectedDestinationAccount) {
      setReceptorAccountId({
        value: Object.keys(props.preSelectedDestinationAccount)[0],
        label: Object.values(props.preSelectedDestinationAccount)[0],
      });
      setTriggerDestinationSelected(!triggerDestinatorSelected);
    }

    axios.get(urlJoin(apiUrl, "messages", store.getAccountId()))
        .then(({ data }) => {
      // sorting by date of last message in each thread
      let msgs = Object.keys(data);
      msgs.sort(function (a, b) {
        if (
          data[a].second[0].numericalDate >= data[b].second[0].numericalDate
        ) {
          return -1;
        } else {
          return 1;
        }
      });

      let sortedThreads = {};
      for (let thread of msgs) {
        sortedThreads[thread] = data[thread];
      }
      setMessages(sortedThreads);
      setMessagesPreview(true);
    });
  }, [reload]);

  useEffect(() => {
    if (Object.keys(receptorAccountId).length > 0) {
      displayDestinators();
    }
  }, [triggerDestinatorSelected]);

  useEffect(() => {
    if (selectedAccountId.length > 0) {
      scrollBottomRef.current.scrollIntoView({
        block: "end",
        inline: "nearest",
        behavior: "smooth",
      });
    }
    if (messagesThread.current) {
      toggleStickyLayout(messagesThread.current);
    }
  }, [selectedAccountId]);

  useEffect(() => {
    if (DEFAULT_N_THREAD_MESSAGES != threadMsgsNumber) {
      const threadMessages = document.querySelectorAll(".messageDetail");
      const currentMessageNumber =
        messages[selectedAccountId].second.length - DEFAULT_N_THREAD_MESSAGES;
      threadMessages[currentMessageNumber].scrollIntoView();
    }
  }, [threadMsgsNumber]);

  const goingIntoMessageThread = (destinationAccountId) => {
    if (
      messages[destinationAccountId].second[0].seen ||
      messages[destinationAccountId].second[0].accountDestination !=
        store.getAccountId()
    ) {
      setSelectedAccountId(destinationAccountId);
      setMessagesPreview(false);
    } else {
      axios
        .put(urlJoin(apiUrl, "messages", store.getAccountId(), "read"), [
          destinationAccountId,
        ])
        .then(({ data }) => {
          if (data) {
            setSelectedAccountId(destinationAccountId);
            setMessagesPreview(false);
          } else {
            displayTempError("msg.ErrorGoingIntoThread");
          }
        });
    }
    setShowNewMessage(false);
  };

  const markMessagesAsReaded = (destinationAccountIds) => {
    if (destinationAccountIds.size > 0) {
      axios.put(urlJoin(apiUrl, "messages", store.getAccountId(), "read" + Array.from(destinationAccountIds)))
        .then(({ data }) => {
          if (data) {
            setReload(!reload);
            displayTempError("msg.SuccessMarkAsReaded");
          } else {
            displayTempError("msg.ErrorMarkAsReaded");
          }
        });
    }
  };

  const deleteMessages = (destinationAccountIds) => {
    if (destinationAccountIds.size > 0) {
      axios
        .delete(urlJoin(apiUrl, "messages", store.getAccountId()), {
          headers: {
            "Content-Type": "application/json",
          },
          data: Array.from(destinationAccountIds),
        })
        .then(({ data }) => {
          if (data) {
            setReload(!reload);
            displayTempError("msg.SuccessDeleteMsgs");
          } else {
            displayTempError("msg.ErrorDeleteMsgs");
          }
        });
    }
  };

  const toggleMessageSelected = (e, destinationAccountId) => {
    if (e.target.checked) {
      setMultipleSelectionMessages(
        new Set([...multipleSelectionMessages, destinationAccountId])
      );
    } else {
      let baseSelectedValues = new Set([...multipleSelectionMessages]);
      baseSelectedValues.delete(destinationAccountId);
      setMultipleSelectionMessages(baseSelectedValues);
    }
  };

  const toggleSelectionAllMessages = (e) => {
    if (e.target.checked) {
      setMultipleSelectionMessages(new Set(Object.keys(messages)));
    } else {
      setMultipleSelectionMessages(new Set());
    }
  };

  const fillDestinatorsSelect = () => {
    return axios.get(urlJoin(apiUrl, "utils", "players"))
        .then(({ data }) => {
      // Cleaning players in black list and own account
      delete data[store.getAccountId()];
      Object.keys(data).forEach((player) => {
        if (Object.keys(blackList).includes(player)) {
          delete data[player];
        }
      });
      setAvaiableDestinators(data);
    });
  };

  const displayDestinators = () => {
    if (!showNewMessage) {
      fillDestinatorsSelect().then(() => {
        setShowNewMessage(true);
      });
    } else {
      setShowNewMessage(false);
    }
  };

  const clearNewMsg = () => {
    setShowNewMessage(false);
    setReceptorAccountId({});
    setNewMsgBody("");
  };

  const sendNewMsg = () => {
    if (newMsgBody.length > 1 && Object.keys(receptorAccountId).length === 2) {
      setMessageSending(true);
      axios
        .post(urlJoin(apiUrl, "messages", store.getAccountId()), {
          accountDestination: receptorAccountId.value,
          messageBody: newMsgBody,
        })
        .then(({ data }) => {
          if (data) {
            clearNewMsg();
            setReload(!reload);
            setMessageSending(false);
            displayTempSuccess("msg.SuccessSendMsg");
          } else {
            displayTempError("msg.ErrorSendMsg");
          }
        });
    }
  };

  const sendThreadNewMsg = () => {
    if (threadNewMsgBody.length > 1 && selectedAccountId.length > 0) {
      setMessageSending(true);
      axios
        .post(urlJoin(apiUrl, "messages", store.getAccountId()), {
          accountDestination: selectedAccountId,
          messageBody: threadNewMsgBody,
        })
        .then(({ data }) => {
          if (data) {
            setThreadNewMsgBody("");
            let currentThread = messages[selectedAccountId];
            currentThread.second.unshift(data);

            setMessages({ ...messages, [selectedAccountId]: currentThread });
            scrollBottomRef.current.scrollIntoView({
              block: "end",
              inline: "nearest",
              behavior: "smooth",
            });
            setMessageSending(false);
            displayTempSuccess("msg.SuccessSendMsg");
          } else {
            setMessageSending(false);
            displayTempError("msg.ErrorSendMsg");
          }
        });
    }
  };

  const displayTempSuccess = (textKey) => {
    if (messageError) {
      setMessageError(null);
    }
    setMessageSuccess(textKey);
    setTimeout(() => {
      setMessageSuccess(null);
    }, 4000);
  };

  const displayTempError = (textKey) => {
    if (messageSuccess) {
      setMessageSuccess(null);
    }
    setMessageError(textKey);
    setTimeout(() => {
      setMessageError(null);
    }, 4000);
  };

  const getBlackList = () => {
    axios
      .get(urlJoin(apiUrl, "account", store.getAccountId(), "blackList"))
      .then(({ data }) => {
        if (data) {
          fillDestinatorsSelect().then(() => {
            setBlackList(data);
            setMessagesPreview(false);
            setShowBlackList(true);
          });
        }
      });
  };

  const addToBlackList = () => {
    if (Object.keys(newBlackListAccount).length === 0) {
      return;
    }
    axios.post(urlJoin(apiUrl, "account", store.getAccountId(), "blackList", newBlackListAccount.value))
      .then(({ data }) => {
        if (data) {
          let currentAvailableDestinators = avaiableDestinators;
          delete currentAvailableDestinators[newBlackListAccount.value];
          setAvaiableDestinators(currentAvailableDestinators);
          setBlackList({
            ...blackList,
            [newBlackListAccount.value]: newBlackListAccount.label,
          });
          setNewBlackListAccount({});
        }
      });
  };

  const removeFromBlackList = (accountToRemoveId) => {
    if (!Object.keys(blackList).includes(accountToRemoveId)) {
      return;
    }
    axios.delete(urlJoin(apiUrl, "account", store.getAccountId(), "blackList", accountToRemoveId))
      .then(({ data }) => {
        if (data) {
          let currentBlackList = blackList;
          let accountToRemoveName = blackList[accountToRemoveId];
          delete currentBlackList[accountToRemoveId];
          setAvaiableDestinators({
            ...avaiableDestinators,
            [accountToRemoveId]: accountToRemoveName,
          });
          setBlackList(currentBlackList);
        }
      });
  };

  const changePage = (direction) => {
    if (direction === "prev" && activePage > 0) {
      setActivePage(activePage - 1);
    } else if (
      direction === "next" &&
      activePage < Math.floor(Object.keys(messages).length / MESSAGES_PER_PAGE)
    ) {
      setActivePage(activePage + 1);
    }
  };

  const toggleStickyLayout = (element) => {
    if (element.scrollTop === 0) {
      setShowStickyOverlay(false);
    } else if (element.scrollTop > 0 && !showStickyOverlay) {
      setShowStickyOverlay(true);
    }
  };

  const downloadThread = (partnerId) => {
    axios.get(urlJoin(apiUrl, "messages", store.getAccountId(), "download", partnerId))
      .then((csv) => {
        const url = window.URL.createObjectURL(new Blob([csv.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute(
          "download",
          store.getUsername() + "_" + messages[partnerId].first + ".csv"
        );
        document.body.appendChild(link);
        link.click();
      });
  };

  return (
    <div>
      <header className="pageCategoryHeader">{t("messagesLabel")}</header>
      {isLoading === true && (
        <MoonLoader color="#c37253" css="margin-left : 240px;"></MoonLoader>
      )}
      {isLoading === false && (
        <div className="headerHistory">
          {messageError && (
            <div className="digResult">
              <img className="digImageTranslate" src={errorIcon} alt="" />
              <span className="digText">{t("msg.ErrorSendMsg")}</span>
            </div>
          )}

          {messageSuccess && (
            <div className="digResult">
              <img className="digImageTranslate" src={checkIcon} alt="" />
              <span className="digText">{t("msg.SuccessSendMsg")}</span>
            </div>
          )}
          {messagesPreview && (
            <>
              <p>{t("msg.HeaderText")}</p>
              <div id="messagesHeader" className="padBot">
                <div id="createNewMsg" onClick={displayDestinators}>
                  <img alt="Icon to create new message" src={msgIcon} />
                  <span className="msgButtons">{t("msg.New")}</span>
                </div>
                <div id="messageActions">
                  <span
                    className="msgButtons"
                    onClick={() =>
                      markMessagesAsReaded(multipleSelectionMessages)
                    }
                  >
                    {t("msg.MarkAsReadSelected")}
                  </span>
                  <span
                    className="msgButtons"
                    onClick={() => deleteMessages(multipleSelectionMessages)}
                  >
                    {t("msg.RemoveSelected")}
                  </span>
                  <span className="msgButtons" onClick={getBlackList}>
                    {t("msg.BlackList")}
                  </span>
                </div>
              </div>
              {showNewMessage && (
                <>
                  <div id="newMsgHeader">
                    <span onClick={clearNewMsg}>
                      <img
                        alt="Button to cancel new message creation"
                        src={cancelIcon}
                        lang={i18n.language}
                        data-place="left"
                        data-tip={t("msg.Tooltip.ClearMessageToSend")}
                      />
                    </span>
                    <Select
                      options={Object.keys(avaiableDestinators).map(
                        (account) => ({
                          value: account,
                          label: avaiableDestinators[account],
                        })
                      )}
                      value={receptorAccountId}
                      onChange={setReceptorAccountId}
                      placeholder={t("msg.Select.Placeholder")}
                      noOptionsMessage={() => t("msg.Select.NoOptions")}
                    />
                    <span onClick={sendNewMsg}>
                      <img
                        alt="Button to submit new message creation"
                        src={letterIcon}
                        lang={i18n.language}
                        data-place="left"
                        data-tip={t("msg.Tooltip.SendMessage")}
                      />
                    </span>
                    <ReactTooltip className="smalltooltip" html={true} />
                  </div>
                  <div className="bodyMsgWrapper">
                    <span>{newMsgBody.length}/1000</span>
                    <textarea
                      id="newMsgBody"
                      maxLength={1000}
                      rows={10}
                      value={newMsgBody}
                      onChange={(e) => setNewMsgBody(e.target.value)}
                    ></textarea>
                  </div>
                </>
              )}

              <table id="messageList">
                <tbody>
                  <tr className="messageListLayout">
                    <th>{t("msg.From")}</th>
                    <th>{t("msg.Message")}</th>
                    <th>{t("msg.Date")}</th>
                    <th style={{ textAlign: "center" }}>
                      <input
                        type="checkbox"
                        onChange={(e) => toggleSelectionAllMessages(e)}
                      />
                    </th>
                  </tr>
                  {Object.keys(messages)
                    .slice(
                      activePage * MESSAGES_PER_PAGE,
                      activePage * MESSAGES_PER_PAGE + MESSAGES_PER_PAGE
                    )
                    .map((message, i) => {
                      return (
                        <tr
                          key={i}
                          className={
                            !messages[message].second[0].seen &&
                            messages[message].second[0].accountSender !=
                              store.getAccountId()
                              ? "messageListLayout unseenMsg"
                              : "messageListLayout"
                          }
                        >
                          <td
                            className="messageFrom"
                            onClick={() => goingIntoMessageThread(message)}
                          >
                            {messages[message].first}
                          </td>
                          <td
                            className="messagePreview"
                            onClick={() => goingIntoMessageThread(message)}
                          >
                            {messages[message].second[0].body}
                          </td>
                          <td className="messageDateTime">
                            {messages[message].second[0].numericalDate}
                          </td>
                          <td className="messageCheckbox">
                            <input
                              type="checkbox"
                              onChange={(e) =>
                                toggleMessageSelected(e, message)
                              }
                              checked={multipleSelectionMessages.has(message)}
                            />
                            <img
                              alt="Download selected thread as CSV"
                              src={downloadIcon}
                              onClick={() => downloadThread(message)}
                            />
                          </td>
                        </tr>
                      );
                    })}
                </tbody>
              </table>
              <div id="previewPagination">
                <img
                  alt="Go to previous page of your message threads"
                  src={goBack}
                  onClick={() => changePage("prev")}
                />
                <img
                  alt="Go to next page of your message threads"
                  src={arrowRight}
                  onClick={() => changePage("next")}
                />
              </div>
            </>
          )}

          {selectedAccountId && (
            <div id="threadView">
              <div id="threadPlayerName">
                {messages[selectedAccountId].first}
              </div>
              <div
                id="messageThread"
                ref={messagesThread}
                onScroll={(e) => toggleStickyLayout(e.target)}
              >
                {showStickyOverlay && <div id="stickyThreadOverlay"></div>}

                {threadMsgsNumber <
                  messages[selectedAccountId].second.length && (
                  <div
                    id="threadShowAllMsgs"
                    onClick={() => {
                      setThreadMsgsNumber(
                        messages[selectedAccountId].second.length
                      );
                    }}
                  >
                    {t("msg.ThreadShowAll")}
                  </div>
                )}
                <div id="threadMsgsWrapper">
                  {messages[selectedAccountId].second
                    .slice(0, threadMsgsNumber)
                    .reverse()
                    .map((message, i) => (
                      <>
                        {i === messages[selectedAccountId].second.length - 1 ||
                        i === 9 ? (
                          <div
                            key={i}
                            ref={scrollBottomRef}
                            className={
                              message.accountSender === selectedAccountId
                                ? "foreignMessage messageDetail " +
                                  (message.seen ? "" : "unSeenMsg")
                                : "ownMessage messageDetail"
                            }
                          >
                            <span>{message.body}</span>
                            <p className="threadMsgDate">
                              {message.numericalDate}
                            </p>
                          </div>
                        ) : (
                          <>
                            <div
                              key={i}
                              className={
                                message.accountSender === selectedAccountId
                                  ? "foreignMessage messageDetail " +
                                    (message.seen ? "" : "unSeenMsg")
                                  : "ownMessage messageDetail "
                              }
                            >
                              <span>{message.body}</span>
                              <p className="threadMsgDate">
                                {message.numericalDate}
                              </p>
                            </div>
                          </>
                        )}
                      </>
                    ))}
                </div>
              </div>
              <div id="threadNewMsg">
                {messageSending && (
                  <div className="messageSendingSpinner">
                    <MoonLoader></MoonLoader>
                    <p className="textWarning">{t("msg.SendingMessage")}</p>
                  </div>
                )}
                <div className="bodyMsgWrapper">
                  <span id="threadMsgBodyCounter">
                    {threadNewMsgBody.length}/1000
                  </span>
                  <textarea
                    id="threadNewMsgBody"
                    maxLength={1000}
                    rows={4}
                    value={threadNewMsgBody}
                    onChange={(e) => setThreadNewMsgBody(e.target.value)}
                  >
                    {" "}
                  </textarea>
                  <span className="centerSpan" onClick={sendThreadNewMsg}>
                    <img
                      alt="Button to submit new message creation"
                      src={letterIcon}
                      lang={i18n.language}
                      data-place="left"
                      data-tip={t("msg.Tooltip.SendMessageThread")}
                    />
                  </span>
                  <ReactTooltip className="smalltooltip" html={true} />
                </div>
              </div>

              <img
                id="goBackToMsgList"
                alt="Go back to message list preview"
                src={goBack}
                onClick={() => {
                  setReload(!reload);
                  setSelectedAccountId("");
                  setMessagesPreview(true);
                  setThreadMsgsNumber(DEFAULT_N_THREAD_MESSAGES);
                }}
              ></img>
              <p className="textWarning">{t("msg.ThreadWarning")}</p>
            </div>
          )}

          {showBlackList && (
            <>
              <p>{t("msg.BlackListDescription")}</p>
              <div id="blackListAddition">
                <Select
                  options={Object.keys(avaiableDestinators).map((account) => ({
                    value: account,
                    label: avaiableDestinators[account],
                  }))}
                  value={newBlackListAccount}
                  onChange={setNewBlackListAccount}
                  placeholder={t("msg.Select.Placeholder")}
                  noOptionsMessage={() => t("msg.Select.NoOptions")}
                />
                <span>
                  <img
                    alt="Add player to your black list"
                    src={deathIcon}
                    lang={i18n.language}
                    data-place="left"
                    data-tip={t("msg.Tooltip.AddToBlackList")}
                    onClick={addToBlackList}
                  />
                  <ReactTooltip className="smalltooltip" html={true} />
                </span>
              </div>
              <div id="blackListView">
                {Object.keys(blackList).map((account, i) => (
                  <div key={i} className="blackListPlayer">
                    <span className="blackListPlayerName">
                      {blackList[account]}
                    </span>
                    <img
                      alt="Remove this player from your black list"
                      src={cancelIcon}
                      lang={i18n.language}
                      data-place="left"
                      data-tip={t("msg.Tooltip.RemoveFromBlackList")}
                      onClick={() => removeFromBlackList(account)}
                    />
                    <ReactTooltip className="smalltooltip" html={true} />
                  </div>
                ))}
              </div>
              <img
                id="goBackToMsgList"
                alt="Go back to message list preview"
                src={goBack}
                onClick={() => {
                  setReload(!reload);
                  setShowBlackList(false);
                }}
              />
            </>
          )}
        </div>
      )}
    </div>
  );
}
