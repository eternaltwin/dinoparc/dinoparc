import { useTranslation } from "react-i18next";
import React from "react";
import { useUserData } from "../../../../context/userData";
import Section from "../../../../components/Section";
import { ActionMode, ImageMode } from "../../../../types/account";
import Button from "../../../../components/Button";
import Content from "../../../../components/Content";
import Row from "../../../../components/Row";

function getImageTranslationKey({ mode }: { mode: ImageMode }) {
  const base = "moncompte.imgMode.";
  switch (mode) {
    case "RUFFLE":
      return base + "ruffle";
    case "BASE_64":
      return base + "base64";
    case "HYBRID":
      return base + "hybrid";
  }
}

function getActionTranslationKey({ mode }: { mode: ActionMode }) {
  const base = "moncompte.actionMode.";
  switch (mode) {
    case "STANDARD":
      return base + "standard";
    case "ADVANCED":
      return base + "advanced";
  }
}

type Props = {};

export default function AccountParametersSection(props: Props) {
  const { t } = useTranslation();
  const { accountId, imageMode, setImageMode, actionMode, setActionMode } = useUserData();

  function getImageText({ mode }: { mode: ImageMode }) {
    const key = getImageTranslationKey({ mode });
    const text = t(key);
    return mode === imageMode ? "🟢 " + text : text;
  }

  function getActionText({ mode }: { mode: ActionMode }) {
    const key = getActionTranslationKey({ mode });
    const text = t(key);
    return mode === actionMode ? "🟢 " + text : text;
  }

  return (
    <Section title={t("moncompte.gestion")}>
      <Content className="has-text-centered">
        <p>{t("moncompte.imgMode.header")}</p>
        <p>{t('imagemode_hybride')}</p>
        <Row className="imgModeBtnSpacing">
          {(["BASE_64", "HYBRID"] as const).map((mode) => (
            <Button key={mode} onClick={() => setImageMode(mode)} active={imageMode === mode} disabled={imageMode === mode}>{getImageText({ mode })}</Button>
          ))}
        </Row>
        <div style={{marginTop: "20px", marginBottom: "5px"}}>
          <p>{t("moncompte.actionMode.header")}</p>
        </div>
        <Row className="actionModeBtnSpacing">
          {(["STANDARD", "ADVANCED"] as const).map((mode) => (
            <Button key={mode} onClick={() => setActionMode(mode)} active={actionMode === mode} disabled={actionMode === mode}>{getActionText({ mode })}</Button>
          ))}
        </Row>
      </Content>
    </Section>
  );
}
