import {useTranslation} from "react-i18next";
import React, {useEffect, useState} from "react";
import axios from "axios";
import {apiUrl} from "../../../../index";
import {useUserData} from "../../../../context/userData";
import Section from "../../../../components/Section";
import Button from "../../../../components/Button";
import Content from "../../../../components/Content";
import {MoonLoader} from "react-spinners";
import {News as TNews} from "../../../../types/news";
import {confirmAlert} from "react-confirm-alert";
import urlJoin from "url-join";

type Props = {};

export default function HelpFlagToggleSection(props: Props) {
    const {t} = useTranslation();
    const {accountId} = useUserData();
    const [isLoading, setIsLoading] = useState(true);
    let [status, setStatus] = useState(false);

    useEffect(() => {
        axios.get(urlJoin(apiUrl, "account", accountId, "help-flag"))
            .then(({ data }) => {
                setStatus(data);
                setIsLoading(false);
            });
    }, []);

    function changeHelpFlagValueForPlayer(newValue) {
        axios.put(urlJoin(apiUrl, "account", accountId, "help-flag", "switch", (newValue.toString())))
            .then(({ data }) => {
                setStatus(newValue);
            });
    }

    return (
        <Section title={t("help-section")}>
            {isLoading === true && (
                <MoonLoader color="#c37253" css="margin-left : 240px;" />
            )}
            {isLoading === false && (
                <Content>
                    <div className="has-text-centered">
                        {status == true && <Button onClick={() => changeHelpFlagValueForPlayer(false)}>{t("🟢 ON")}</Button>}
                        {status == false && <Button onClick={() => changeHelpFlagValueForPlayer(true)}>{t("🟡 OFF")}</Button>}
                    </div>
                </Content>
            )}
        </Section>
    );
}
