import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { apiUrl } from "../../../../index";
import { useUserData } from "../../../../context/userData";
import { Loader } from "../../../../components/Loader";
import Section, {
  SectionContentNoPadding,
} from "../../../../components/Section";
import Button from "../../../../components/Button";
import Message from "../../../../components/Message";
import Content from "../../../../components/Content";
import Row from "../../../../components/Row";
import urlJoin from "url-join";

const SERVERS = ["fr", "es", "en"] as const;
type ServerKey = typeof SERVERS[number];
// TODO change the translation value to better name (eg: moncompte.send.fr)
const serverTranslationKey: Record<ServerKey, string> = {
  fr: "moncompte.sendFr",
  es: "moncompte.sendEs",
  en: "moncompte.sendEn",
};

function getHasAlreadyImportedData({
  accountId,
  server,
}: {
  accountId: string;
  server: ServerKey;
}) {
  return axios
    .get<boolean>(urlJoin(apiUrl, "account", accountId, "import", "check", server))
    .then(({ data }) => {
      return data;
    });
}

type Props = {};

export default function AccountImportSection(props: Props) {
  const { t } = useTranslation();
  const [hasImportLoading, setIsImportLoading] = useState(false);
  const [importResult, setImportResult] = useState<null | boolean>(null);
  const [alreadyImportedServer, setAlreadyImportedServer] = useState<
    ServerKey[]
  >([]);
  const { accountId } = useUserData();
  const availableServerImport = SERVERS.filter(
    (server) => !alreadyImportedServer.includes(server)
  );

  useEffect(() => {
    SERVERS.map((server) =>
      getHasAlreadyImportedData({ accountId, server }).then(
        (alreadyImported) => {
          if (alreadyImported) {
            setAlreadyImportedServer((servers) => [...servers, server]);
          }
        }
      )
    );
  }, [accountId]);

  function handleImportSubmit(server: ServerKey) {
    setImportResult(null);
    setIsImportLoading(true);

    axios
      .post<boolean>(urlJoin(apiUrl, "account", accountId, "import", server))
      .then(({ data }) => {
        setImportResult(data);
        if (data) {
          setAlreadyImportedServer((servers) => [...servers, server]);
        }
        setIsImportLoading(false);
        window.scrollTo(0, 0);
      })
      .catch(() => {
        setImportResult(false);
        setIsImportLoading(false);
      });
  }

  if (availableServerImport.length === 0) {
    return;
  }

  return (
    <Section title={t("moncompte.importation")}>
      <Content>
        <p>{t("moncompte.steps")}</p>
      </Content>
      {importResult === true && (
        <SectionContentNoPadding>
          <Message type="info">{t("import.success")}</Message>
        </SectionContentNoPadding>
      )}
      {importResult === false && (
        <SectionContentNoPadding>
          <Message type="danger">{t("import.fail")}</Message>
        </SectionContentNoPadding>
      )}
      {hasImportLoading === true && (
        <Loader size="small" css="margin: 0 auto 1rem" />
      )}
      <Row gap="large" className="mt-4">
        {availableServerImport.map((server) => (
          <Button
            key={server}
            onClick={() => handleImportSubmit(server)}
            disabled={hasImportLoading}
          >
            {t(serverTranslationKey[server])}
          </Button>
        ))}
      </Row>
    </Section>
  );
}
