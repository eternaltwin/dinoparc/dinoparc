import React from "react";
import { useTranslation } from "react-i18next";
import getCollectionImageByInteger from "../../utils/CollectionImageByInteger";
import getCollectionEpicsImageByString from "../../utils/CollectionEpicsImageByInteger";
import emptyCase from "../../../media/game/empty.gif";
import ReactTooltip from "react-tooltip";

import "./RewardBox.css";

type Props = React.HTMLAttributes<HTMLDivElement> & {
  title: string;
  collection: number[];
  maxLength?: number;
  isEpic?: boolean;
};

let currentIndex = 1;

export default function RewardBox({
  title,
  collection,
  isEpic,
  maxLength = isEpic ? undefined : 49,
  className = "",
  ...attrs
}: Props) {
  currentIndex += 1;
  const tooltipId = `rewardBoxTooltip-${currentIndex}`;

  return (
    <div className={`rewardBox ${className}`.trim()} {...attrs}>
      <div className="rewardBox-header">
        <div>{title}</div>
        {maxLength != null && <div>{`${collection.length}/${maxLength}`}</div>}
      </div>
      <CollectionList
        collection={collection}
        maxLength={maxLength}
        isEpic={isEpic}
        tooltipId={tooltipId}
      />

      <ReactTooltip
        id={tooltipId}
        className="collectiontooltipSmallFont"
        html={true}
        backgroundColor="transparent"
      />
    </div>
  );
}

const CollectionList = ({
  collection,
  maxLength = collection.length,
  isEpic,
  tooltipId,
}: {
  collection: number[];
  maxLength?: number;
  isEpic?: boolean;
  tooltipId: string;
}) => {
  const trueMaxLength = Math.ceil(maxLength / 7) * 7;
  const allItems = new Array(trueMaxLength)
    .fill(0)
    .map((_, i) => (isEpic ? collection[i] : i + 1));

  return (
    <div className="rewardBox-grid">
      {allItems.map((index) => {
        return (
          <CollectionItem
            key={index}
            index={collection.includes(index) ? index : -1}
            isEpic={isEpic}
            tooltipId={tooltipId}
          />
        );
      })}
    </div>
  );
};

const CollectionItem = ({
  index,
  isEpic,
  tooltipId,
}: {
  index: number;
  isEpic?: boolean;
  tooltipId: string;
}) => {
  const { t } = useTranslation();

  const itemSrc = isEpic
    ? getCollectionEpicsImageByString("" + index)
    : getCollectionImageByInteger("" + index);
  const tooltipText = isEpic
    ? t(`collection.epic.${index}.tooltip`)
    : t(`collection.${index}.tooltip`);
  return index === -1 || itemSrc == null ? (
    <img src={emptyCase} alt="" />
  ) : (
    <img
      alt=""
      src={itemSrc}
      data-place="right"
      data-tip={tooltipText}
      data-for={tooltipId}
    />
  );
};
