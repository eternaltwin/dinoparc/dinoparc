import React from "react";
import ReactSWF from "react-swf";
import ErrorBoundary from "../../../components/ErrorBoundary";
import { Loader } from "../../../components/Loader";
import dinoSwf from "../../../media/swf/dinoz.swf";
import emptyDinoz from "../../../media/dinoz/emptyDinoz.png";
import { useUserData } from "../../../context/userData";
const DinozCanvas = React.lazy(() => import("./Dinoz/index"));

type Props = React.HTMLAttributes<HTMLDivElement> & {
    appCode: string;
    size?: number;
    marginTop?: number;
    marginLeft?: number;
    transformScaleX?: number;
    transformScaleY?: number;
};
export default function DinozRenderTile({appCode, size = 100, transformScaleX = 1, transformScaleY = 1, marginLeft = 0, marginTop = 0, ...attrs}: Props) {
    const { imageMode } = useUserData();

    function copyCode(e: React.MouseEvent<HTMLDivElement>) {
        const target = e.target as HTMLElement;
        if (
            typeof target?.closest === "function" &&
            target.closest("button") !== null
        )
            return;
        navigator.clipboard.writeText(appCode);
    }

    return (
        <div
            key={appCode}
            style={{
                width: `${size}px`,
                height: `${size}px`,
                marginLeft: `${marginLeft}px`,
                marginTop: `${marginTop}px`,
                alignItems: "center",
                justifyContent: "center",
                transform: `scale(${transformScaleX}, ${transformScaleY})`,
            }}
            title={appCode}
            onClick={copyCode}
            {...attrs}
        >
            {imageMode === "NONE" ? (
                <img alt="" src={emptyDinoz} width={size} height={size} />
            ) : (imageMode === "BASE_64" || imageMode === "HYBRID") ? (
                <ErrorBoundary error={<DinozSWF appCode={appCode} size={size} />}>
                    <React.Suspense fallback={<Loader />}>
                        <DinozCanvas appCode={appCode} size={size} />
                    </React.Suspense>
                </ErrorBoundary>
            ) : (
                <DinozSWF appCode={appCode} size={size} />
            )}
        </div>
    );
}

function DinozSWF({ appCode, size }: { appCode: string; size: number }) {
    return (
        <div>
            {/* wrapper div is required for ruffle remove element */}
            <ReactSWF
                src={dinoSwf}
                flashVars={"data=" + appCode}
                width={size}
                height={size}
                quality="high"
            />
        </div>
    );
}
