import { useTranslation } from "react-i18next";
import React, { useEffect, useState } from "react";
import actBuy from "../../../media/actions/act_buy.gif";
import actBill from "../../../media/actions/act_bill.gif";
import { useUserData } from "../../../context/userData";
import AsyncSection from "../../../components/AsyncSection";
import CashAmount from "../../../components/CashAmount";
import BonsAmount from "../../../components/BonsAmount";
import HelpTooltip from "../../../components/HelpTooltip";
import Tooltip from "../../../components/Tooltip";
import {
  ShopType,
  getShopInformation,
  postShopBuy,
  shopItemBasePriceMap,
} from "../../../services/shop.api";
import getConsumableByKey from "../../utils/ConsumableByKey";

const MAX_QUANTITY_BY_ITEM = 99;

type Props = React.HTMLAttributes<HTMLFormElement> & {
  dinozId?: string;
  type?: ShopType;
  onSubmitted: () => void;
  returnToDinoz: () => void;
};

export default function ShopForm({dinozId, type, onSubmitted, returnToDinoz, ...attrs}: Props) {
  const { t } = useTranslation();
  const { accountId } = useUserData();
  const [isLoading, setIsLoading] = useState(true);
  const [buySuccess, setBuySuccess] = useState(true);
  const [accountCashAmount, setAccountCashAmount] = useState(0);
  const [customerPriceMap, setCustomerPriceMap] = useState<{ [key: string]: number; }>({});
  const [customerQuantityMap, setCustomerQuantityMap] = useState<{ [key: string]: number; }>({});
  const [quantityToBuyMap, setQuantityToBuyMap] = useState<{ [key: string]: number; }>({});
  const itemsKey = Object.keys(customerPriceMap);
  const totalPrice = Object.entries(customerPriceMap).reduce((acc, [key, price]) => acc + (price || 0) * (quantityToBuyMap[key] || 0), 0);

  useEffect(() => {
    setIsLoading(true);
    getShopInformation({ accountId, dinozId, type }).then(({ data }) => {
      setAccountCashAmount(data.accountCashAmount);
      setCustomerPriceMap(data.pricesMap);
      setCustomerQuantityMap(data.quantityMap);
      setQuantityToBuyMap(Object.fromEntries(Object.keys(data.pricesMap).map((key) => [key, 0])));
      setIsLoading(false);
      window.scrollTo(0, 0);
    });
  }, [accountId, dinozId, type]);

  function buyAllItems() {
    if (totalPrice <= 0) {
      alert(t("boutique.nothingtobuy"));
      return;
    } else if (totalPrice > accountCashAmount) {
      alert(t("boutique.notenoughmoney"));
      return;
    }

    setIsLoading(true);

    postShopBuy({
      accountId,
      dinozId,
      type,
      totalPrice,
      ...quantityToBuyMap,
    })
      .then((response) => {
        setBuySuccess(response.data);
        if (response.data) {
          onSubmitted();
        } else {
          alert(t("pirates.alreadyBought"));
          returnToDinoz();
        }
      })
      .finally(() => {
        setIsLoading(false);
      });
  }

  function getUpdateItemQuantityHandler(key: string) {
    return (quantity: number) => {
      setQuantityToBuyMap((prev) => ({ ...prev, [key]: quantity }));
    };
  }

  function getBasePriceByKey(key: string) {
    if (type === "crater") {
      return 200;
    }
    return shopItemBasePriceMap[key];
  }

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        buyAllItems();
      }}
      {...attrs}
    >
      <ShopCart
        totalPrice={totalPrice}
        quantityMap={quantityToBuyMap}
        onBuyAll={buyAllItems}
        shopType={type}
      />

      <AsyncSection loading={isLoading}>
        <table className="shopItemsBoutique">
          <tbody>
            <tr>
              <th className="empty"></th>
              <th className="nameTitle">{t("boutique.nom")}</th>
              {type !== "pirates" && <th className="stock">{t("boutique.prixR")}</th>}
              <th className="priceHeader">{t("boutique.prixV")}</th>
              <th className="quantityHeader">{t("boutique.quantity")}</th>
            </tr>

            {itemsKey.map((key) => (
              <ShopItem
                key={key}
                name={t(`boutique.${key}`)}
                image={getConsumableByKey(key)}
                tooltip={t(`boutique.tooltip.${key}`)}
                quantity={customerQuantityMap[key]}
                basePrice={getBasePriceByKey(key)}
                customerPrice={customerPriceMap[key]}
                currentQuantity={quantityToBuyMap[key]}
                max={type === "pirates" ? 1 : MAX_QUANTITY_BY_ITEM}
                onQuantityChange={getUpdateItemQuantityHandler(key)}
                shopType={type}
              />
            ))}
          </tbody>
        </table>
      </AsyncSection>
    </form>
  );
}

type ShopCartProps = {
  totalPrice: number;
  quantityMap: Record<string, number>;
  onBuyAll: () => void;
  shopType: string;
};

const ShopCart = ({ totalPrice, quantityMap, onBuyAll, shopType }: ShopCartProps) => {
  let { t } = useTranslation();

  const itemsWithQuantity: { key: string; quantity: number }[] = [];
  for (const [key, quantity] of Object.entries<number>(quantityMap)) {
    if (quantity > 0) {
      itemsWithQuantity.push({ key, quantity });
    }
  }

  return (
    <div
      className="buySommaire"
      onClick={onBuyAll}
      data-for="cart-tooltip"
      data-tip={t("boutique.tooltip.acheter")}
    >
      <span className="textBoutiqueSommaire">{t("boutique.sommaire")}</span>

      <span className="textBoutiqueTotal">
        {t("boutique.sommaire.total")}
        {shopType !== "pirates" && <CashAmount amount={totalPrice}/>}
        {shopType === "pirates" && <BonsAmount amount={totalPrice}/>}
      </span>

      <br></br>

      {itemsWithQuantity.length === 0 ? (
        <span className="textBoutique">{t("boutique.sommaire.empty")}</span>
      ) : (
        itemsWithQuantity.map(({ key, quantity }) => (
          <p key={key} className="textBoutiqueElement">
            {"- " + t(`boutique.${key}`) + " (x" + quantity + ")"}
          </p>
        ))
      )}
      {shopType !== "pirates" && <img alt="" className="buyPic" src={actBuy}/>}
      {shopType === "pirates" && <img alt="" className="buyPic" src={actBill}/>}
      <button type="submit" className="sr-only">
        Submit
      </button>
      <Tooltip id="cart-tooltip" />
    </div>
  );
};

type ShopItemProps = {
  name: string;
  image: string;
  tooltip: string;
  quantity: number;
  basePrice: number;
  customerPrice: number;
  currentQuantity: number;
  max: number;
  onQuantityChange: (newQuantity: number) => void;
  shopType: string;
};
const ShopItem = ({
  name,
  image,
  tooltip,
  quantity,
  basePrice,
  customerPrice,
  currentQuantity = 0,
  max,
  onQuantityChange, shopType
}: ShopItemProps) => {
  let { t } = useTranslation();

  const onInput = (e: React.FormEvent<HTMLInputElement>) => {
    const newQuantity = Number((e.target as HTMLInputElement).value);
    if (Number.isNaN(newQuantity)) {
      return;
    }
    onQuantityChange(Math.max(0, Math.min(max, newQuantity)));
  };

  return (
    <tr>
      <td className="icon">
        <img alt={name} src={image} />
      </td>
      <td className="name">
        <span className="titreObjet">
          <span className="espaceNom">{name}</span>
          <HelpTooltip tip={tooltip} />
        </span>
        <div className="possession">
          {t("boutique.possession")}
          {quantity}
        </div>
      </td>
      {shopType !== "pirates" &&
          <td className="price">
            {shopType !== "pirates" && <CashAmount amount={basePrice}/>}
            {shopType === "pirates" && <BonsAmount amount={basePrice}/>}
          </td>
      }
      <td className="price">
        {shopType !== "pirates" && <CashAmount amount={customerPrice}/>}
        {shopType === "pirates" && <BonsAmount amount={customerPrice}/>}
      </td>
      <td className="count">
        {shopType !== "pirates" &&
            <input
                className="numberField"
                type="number"
                min="0"
                max={max}
                value={currentQuantity}
                onInput={onInput}
            />
        }

        {shopType === "pirates" &&
            <input
                className="numberField"
                type="number"
                min="0"
                max={1}
                value={currentQuantity}
                onInput={onInput}
            />
        }
      </td>
    </tr>
  );
};
