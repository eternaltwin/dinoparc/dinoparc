import { useTranslation } from "react-i18next";
import Store from "../../../utils/Store";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { apiUrl } from "../../../index";
import getActionImageFromActionString from "../../utils/ActionImageFromString";
import getHistoryImageFromKey from "../../utils/HistoryImageFromKey";
import { ClanPage } from "../../../types/clan-page";
import getFactionImageFromActionString from "../../utils/FactionImageFromString";
import { MoonLoader } from "react-spinners";
import ReactTooltip from "react-tooltip";
import urlJoin from "url-join";

type Props = {
  openPlayerProfile: (playerId: string) => void;
  publicClanData: any;
  isInAClan: boolean;
  /** @deprecated */
  refreshMoneyAfterApplication?: () => void;
};

export default function PublicClan(props: Props) {
  const { t } = useTranslation();
  const store = Store.getInstance();
  const [isLoading, setIsLoading] = useState(true);
  const [alliances, setAlliances] = useState([]);
  let [clanPages, setClanPages] = useState({});
  let [actualSelectedPage, setActualSelectedPage] = useState<Partial<ClanPage>>(
    {}
  );
  let [clanMembers, setClanMembers] = useState([]);
  let [applicationSuccess, setApplicationSuccess] = useState(null);

  useEffect(() => {
    setApplicationSuccess(null);
    clanPages = Object.values(props.publicClanData.publicPages);
    setClanPages(clanPages);
    setAlliances(props.publicClanData.allies);
    setClanPages(clanPages);
    actualSelectedPage = clanPages[0];
    setActualSelectedPage(actualSelectedPage);

    axios.get(urlJoin(apiUrl, "clans", "members", props.publicClanData.clanId, store.getAccountId()))
      .then(({ data }) => {
        clanMembers = data;
        setClanMembers(clanMembers);
        setIsLoading(false);
      });
  }, [props.publicClanData]);

  function changePage(newPage) {
    actualSelectedPage = newPage;
    setActualSelectedPage(actualSelectedPage);
  }

  function getTotalNumberOfPoints() {
    return (
      props.publicClanData.nbPointsOccupation +
      props.publicClanData.nbPointsFights +
      props.publicClanData.nbPointsTotem
    );
  }

  function applyToClan() {
    if (window.confirm(t("confirm"))) {
      axios.put(urlJoin(apiUrl, "clans", "apply", props.publicClanData.clanId, store.getAccountId()))
        .then(({ data }) => {
          setApplicationSuccess(data);
          props.refreshMoneyAfterApplication();
        });
    }
  }
  return (
    <div>
      {isLoading && <MoonLoader color="#c37253" css="margin-left : 240px;" />}
      {!isLoading && (
        <div>
          <div className="clanPane">
            <header className="pageCategoryHeader">
              {props.publicClanData.name}
            </header>
            <h2 className="miniHeaders2">{t("clan.menu")}</h2>
            <br />
            {Object.keys(clanPages).map(function (key) {
              return (
                <ul className="elemPagesClan">
                  {!clanPages[key].private && (
                    <li
                      className="clickablePageFromClan"
                      onClick={(e) => {
                        changePage(clanPages[key]);
                      }}
                    >
                      {clanPages[key].pageTitle}
                    </li>
                  )}
                </ul>
              );
            })}

            {/*//Pages ici*/}
            <h2 className="miniHeaders2">{actualSelectedPage.pageTitle}</h2>
            <div className="clanSection">
              {actualSelectedPage.rawContent && (
                <div
                  className="privatePageLabel"
                  dangerouslySetInnerHTML={{
                    __html: actualSelectedPage.rawContent,
                  }}
                />
              )}
            </div>

            <h2 className="miniHeaders2">{t("clan.guerre")}</h2>
            <div className="clanSection">
              <div>
                <span className="statsClan">
                  {t("factionClan")}
                  {" : "}
                  <img alt="" src={getFactionImageFromActionString(props.publicClanData.faction)}
                       data-place="right"
                       data-tip={t("tooltip." + props.publicClanData.faction) + "</strong>"}
                  />
                  {","}
                  <ReactTooltip
                      className="largetooltip"
                      html={true}
                      backgroundColor="transparent"
                  />
                </span>
                <span className="statsClan">
                  {t("classement.position")}
                  {" : "}
                  <strong>{props.publicClanData.position}</strong>
                  {","}
                </span>
                <span className="statsClan">
                  Dinoz{" : "}
                  <strong>{props.publicClanData.nbDinozTotal}</strong>
                  {","}
                </span>
                <span className="statsClan">
                  Dinoz {t("nbDinozWarriorClan")}
                  {" : "}
                  <strong>{props.publicClanData.nbDinozWarriors}</strong>
                  {","}
                </span>
                <span className="statsClan">
                  {t("creationDateClan")}
                  {" : "}
                  <strong>{props.publicClanData.creationDate}</strong>
                </span>
              </div>

              <table className="pointsSummary">
                <tr>
                  <td>
                    <img src={getActionImageFromActionString("Rock")} alt="" />
                  </td>
                  <td>{t("alliedClans")}</td>
                </tr>
                <>
                  {alliances !== undefined &&
                      <>
                      {alliances.map((alliance, i) => {
                          return (
                              <>
                                <tr>
                                  <td>
                                    <img
                                        src={getHistoryImageFromKey("hist_xp.gif")}
                                        alt=""
                                    />
                                  </td>
                                  <td>{alliance}</td>
                                </tr>
                              </>
                          );
                        })}
                      </>
                  }
                </>
              </table>

              <table className="pointsSummary">
                <tr>
                  <td>
                    <img
                      src={getActionImageFromActionString("Pyramids")}
                      alt=""
                    />
                  </td>
                  <td>{t("nbPointsOccupationClan")}</td>
                  <td>{props.publicClanData.nbPointsOccupation}</td>
                </tr>
                <tr>
                  <td>
                    <img
                      src={getActionImageFromActionString("Combat")}
                      alt=""
                    />
                  </td>
                  <td>{t("nbPointsCombatsClan")}</td>
                  <td>{props.publicClanData.nbPointsFights}</td>
                </tr>
                <tr>
                  <td>
                    <img
                      src={getActionImageFromActionString("Fouiller")}
                      alt=""
                    />
                  </td>
                  <td>{t("nbPointsTotemClan")}</td>
                  <td>{props.publicClanData.nbPointsTotem}</td>
                </tr>
                <tr>
                  <td> </td>
                  <td>{t("boutique.sommaire.total")}</td>
                  <td>{getTotalNumberOfPoints()}</td>
                </tr>
              </table>
            </div>

            <h2 className="miniHeaders2">{t("clan.listemembres")}</h2>
            <div className="clanSection">
              <table className="publicClanMembersTable">
                <tbody>
                  <tr>
                    <th>{t("classement.pseudo")}</th>
                  </tr>
                  {clanMembers.map((clanMember, i) => {
                    return (
                      <tr>
                        <td>
                          <span
                            onClick={(e) => {
                              props.openPlayerProfile(clanMember.playerId);
                            }}
                            className="clickablePageFromClan"
                          >
                            {clanMember.playerName}
                          </span>
                          {clanMember.title && (
                            <span className="blackTitle">
                              ({clanMember.title})
                            </span>
                          )}
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
          {!props.isInAClan && applicationSuccess === true && (
            <div className="fusionMessage">{t("clan.apply.success")}</div>
          )}
          {!props.isInAClan && applicationSuccess === false && (
            <div className="errorResultFuzPrix">{t("clan.apply.failure")}</div>
          )}

          {!props.isInAClan && applicationSuccess === null && (
            <div
              className="clickablePageFromClan"
              onClick={(e) => {
                applyToClan();
              }}
            >
              {t("clan.apply") + "? (10 000" + t("phrase.buyDinoz2") + ")"}
            </div>
          )}
        </div>
      )}
    </div>
  );
}
