export default function getElementStringByCompetence(competence) {
  if (competence === "Force") {
    return "Feu";
  } else if (competence === "Endurance") {
    return "Terre";
  } else if (competence === "Perception") {
    return "Eau";
  } else if (competence === "Intelligence") {
    return "Foudre";
  } else if (competence === "Agilité") {
    return "Air";
  } else if (competence === "Fouiller") {
    return "Terre";
  } else if (competence === "Rock") {
    return "Foudre";
  } else if (competence === "Arts Martiaux") {
    return "Feu";
  } else if (competence === "Camouflage") {
    return "Air";
  } else if (competence === "Médecine") {
    return "Foudre";
  } else if (competence === "Natation") {
    return "Eau";
  } else if (competence === "Escalade") {
    return "Terre";
  } else if (competence === "Survie") {
    return "Feu";
  } else if (competence === "Stratégie") {
    return "Air";
  } else if (competence === "Commerce") {
    return "Foudre";
  } else if (competence === "Navigation") {
    return "Eau";
  } else if (competence === "Course") {
    return "Terre";
  } else if (competence === "Contre Attaque") {
    return "Feu";
  } else if (competence === "Chance") {
    return "Air";
  } else if (competence === "Musique") {
    return "Foudre";
  } else if (competence === "Cuisine") {
    return "Eau";
  } else if (competence === "Saut") {
    return "Terre";
  } else if (competence === "Voler") {
    return "Foudre";
  } else if (competence === "Taunt") {
    return "Eau";
  } else if (competence === "Apprenti Feu") {
    return "Feu";
  } else if (competence === "Apprenti Terre") {
    return "Terre";
  } else if (competence === "Apprenti Eau") {
    return "Eau";
  } else if (competence === "Apprenti Foudre") {
    return "Foudre";
  } else if (competence === "Pouvoir Sombre") {
    return "Air";
  } else if (competence === "Protection du Feu") {
    return "Feu";
  } else if (competence === "Solubilité") {
    return "Eau";
  } else if (competence === "Résilience") {
    return "Feu";
  } else if (competence === "Bain de Flammes") {
    return "Feu";
  } else if (competence === "Cueillette") {
    return "Terre";
  } else if (competence === "Pêche") {
    return "Eau";
  } else if (competence === "Ingénieur") {
    return "Foudre";
  } else if (competence === "Coup Critique") {
    return "Air";
  } else if (competence === "Chasseur") {
    return "Air";
  } else if (competence === "Piqûre") {
    return "Air";
  } else if (competence === "Forteresse") {
    return "Terre";
  }
}
