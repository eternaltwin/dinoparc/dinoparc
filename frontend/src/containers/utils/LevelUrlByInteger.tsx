import lvl1 from "../../media/dinoz/lvl1.gif";
import lvl2 from "../../media/dinoz/lvl2.gif";
import lvl3 from "../../media/dinoz/lvl3.gif";
import lvl4 from "../../media/dinoz/lvl4.gif";
import lvl5 from "../../media/dinoz/lvl5.gif";

export default function getCorrectLevelUrlFromInteger(level) {
  if (level === 0) {
    return lvl1;
  } else if (level === 1) {
    return lvl1;
  } else if (level === 2) {
    return lvl2;
  } else if (level === 3) {
    return lvl3;
  } else if (level === 4) {
    return lvl4;
  } else if (level === 5) {
    return lvl5;
  }
}
