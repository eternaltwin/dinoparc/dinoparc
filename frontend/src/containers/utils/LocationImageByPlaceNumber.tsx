import dinoville from "../../media/lieux/zone0.gif";
import irma from "../../media/lieux/zone1.gif";
import clairiere from "../../media/lieux/zone2.gif";
import dinoplage from "../../media/lieux/zone3.gif";
import barrage from "../../media/lieux/zone4.gif";
import falaise from "../../media/lieux/zone5.gif";
import montdino from "../../media/lieux/zone6.gif";
import porte from "../../media/lieux/zone7.gif";
import gredins from "../../media/lieux/zone8.gif";
import foret from "../../media/lieux/zone9.gif";
import temple from "../../media/lieux/zone10.gif";
import port from "../../media/lieux/zone11.gif";
import pitie from "../../media/lieux/zone12.gif";
import mayinca from "../../media/lieux/zone13.gif";
import credit from "../../media/lieux/zone14.gif";
import cratere from "../../media/lieux/zone22.gif";
import plaines from "../../media/lieux/zone37.gif";

import bazar from "../../media/lieux/zone15.gif";
import marais from "../../media/lieux/zone16.gif";
import jungle from "../../media/lieux/zone17.gif";
import bordeciel from "../../media/lieux/zone18.gif";
import source from "../../media/lieux/zone19.gif";
import anomalie from "../../media/lieux/zone20.gif";
import hutte from "../../media/lieux/zone21.gif";

import bastion from "../../media/lieux/zone23.png";
import portroyal from "../../media/lieux/zone24.png";
import suntzu from "../../media/lieux/zone25.png";
import labyrinthe from "../../media/lieux/zone26.png";
import bosquet from "../../media/lieux/zone27.png";
import stiou from "../../media/lieux/zone28.png";
import universite from "../../media/lieux/zone29.png";
import labo from "../../media/lieux/zone30.png";
import puit from "../../media/lieux/zone31.png";
import everouest from "../../media/lieux/zone32.gif";
import lacceleste from "../../media/lieux/zone33.png";
import repaire from "../../media/lieux/zone34.png";
import villagefantome from "../../media/lieux/zone35.png";
import dinocropole from "../../media/lieux/zone36.png";

import dinovilleXmas from "../../media/lieux/zone0_xmas.gif";
import portv2 from "../../media/lieux/zone11b.gif";
import balez from "../../media/lieux/balez.gif";

import map3 from "../../media/lieux/map3.png";
import map4 from "../../media/lieux/map4.png";
import islandView from "../../media/lieux/islandView.jpg";

import Faction1 from "../../media/lieux/temple2.gif"
import Faction2 from "../../media/lieux/temple1.gif";
import Faction3 from "../../media/lieux/temple0.gif";

export const enum PlaceNumber {
  Dinoville = 0,
  Imra,
  Clairiere,
  Dinoplage,
  Barrage,
  Falaise,
  montdino,
  porte,
  gredins,
  foret,
  temple,
  port,
  pitie,
  mayinca,
  credit,
  bazar,
  marais,
  jungle,
  bordeciel,
  source,
  anomalie,
  hutte,
  cratere,

  bastion,
  portroyal,
  suntzu,
  labyrinthe,
  bosquet,
  stiou,
  universite,
  labo,
  puit,
  everouest,
  lacceleste,
  repaire,
  villagefantome,
  dinocropole,

  plaines = 37,

  balez,
  retour,

  map3 = 103,
  map4,
  islandView = 201,
  portv2 = 301,
  faction1,
  faction2,
  faction3
}

export default function getLocationImageByNumber(placeNumber: PlaceNumber) {
  switch (placeNumber) {
    case PlaceNumber.Dinoville:
      return dinoville;
    case PlaceNumber.Imra:
      return irma;
    case PlaceNumber.Clairiere:
      return clairiere;
    case PlaceNumber.Dinoplage:
      return dinoplage;
    case PlaceNumber.Barrage:
      return barrage;
    case PlaceNumber.Falaise:
      return falaise;
    case PlaceNumber.montdino:
      return montdino;
    case PlaceNumber.porte:
      return porte;
    case PlaceNumber.gredins:
      return gredins;
    case PlaceNumber.foret:
      return foret;
    case PlaceNumber.temple:
      return temple;
    case PlaceNumber.port:
      return port;
    case PlaceNumber.pitie:
      return pitie;
    case PlaceNumber.mayinca:
      return mayinca;
    case PlaceNumber.credit:
      return credit;
    case PlaceNumber.bazar:
      return bazar;
    case PlaceNumber.marais:
      return marais;
    case PlaceNumber.jungle:
      return jungle;
    case PlaceNumber.bordeciel:
      return bordeciel;
    case PlaceNumber.source:
      return source;
    case PlaceNumber.anomalie:
      return anomalie;
    case PlaceNumber.hutte:
      return hutte;
    case PlaceNumber.cratere:
      return cratere;
    case PlaceNumber.plaines:
      return plaines;

    case PlaceNumber.bastion:
      return bastion;
    case PlaceNumber.portroyal:
      return portroyal;
    case PlaceNumber.suntzu:
      return suntzu;
    case PlaceNumber.labyrinthe:
      return labyrinthe;
    case PlaceNumber.bosquet:
      return bosquet;
    case PlaceNumber.stiou:
      return stiou;
    case PlaceNumber.universite:
      return universite;
    case PlaceNumber.lacceleste:
      return lacceleste;
    case PlaceNumber.labo:
      return labo;
    case PlaceNumber.puit:
      return puit;
    case PlaceNumber.everouest:
      return everouest;
    case PlaceNumber.repaire:
      return repaire;
    case PlaceNumber.villagefantome:
      return villagefantome;
    case PlaceNumber.dinocropole:
      return dinocropole;

    case PlaceNumber.retour:
      return labyrinthe;
    case PlaceNumber.balez:
      return balez;

    case PlaceNumber.map3:
      return map3;
    case PlaceNumber.map4:
      return map4;
    case PlaceNumber.islandView:
      return islandView;
    case PlaceNumber.portv2:
      return portv2;
    case PlaceNumber.faction1:
      return Faction1;
    case PlaceNumber.faction2:
      return Faction2;
    case PlaceNumber.faction3:
      return Faction3;

    default:
      break;
  }
}
