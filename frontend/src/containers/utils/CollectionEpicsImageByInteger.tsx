import emptyCase from "../../media/game/empty.gif";
import star from "../../media/épiques/star.gif";
import kabuki from "../../media/épiques/kabuki.gif";
import hat from "../../media/épiques/hat.gif";

import charbon from "../../media/épiques/globe_charbon.gif";
import bronze from "../../media/épiques/globe_bronze.gif";
import argent from "../../media/épiques/globe_argent.gif";
import or from "../../media/épiques/globe_or.gif";
import platine from "../../media/épiques/globe_platine.gif";
import diamant from "../../media/épiques/globe_diamant.gif";

import gold_pumpkin from "../../media/épiques/pumpkin_gold.gif";
import silver_pumpkin from "../../media/épiques/pumpkin_silver.gif";
import bronze_pumpkin from "../../media/épiques/pumpkin_bronze.gif";
import normal_pumpkin from "../../media/épiques/pumpkin_normal.gif";

import hat_gold from "../../media/épiques/hat_gold.png";
import hat_silver from "../../media/épiques/hat_silver.png";
import hat_bronze from "../../media/épiques/hat_bronze.png";
import hat_participation from "../../media/épiques/hat_participation.png";

import kabuki_gold_bag from "../../media/épiques/picsou.png";

import w1 from "../../media/épiques/wistiti_bronze.gif";
import w2 from "../../media/épiques/wistiti_silver.gif";
import w3 from "../../media/épiques/wistiti_gold.gif";
import w4 from "../../media/épiques/wistiti_platine.gif";
import w5 from "../../media/épiques/wistiti_diamond.gif";
import w6 from "../../media/épiques/wistiti_master.gif";

import cupGold from "../../media/épiques/cup_gold.gif";
import cupSilver from "../../media/épiques/cup_silver.gif";
import cupBronze from "../../media/épiques/cup_bronze.gif";

import war1a from "../../media/épiques_guerres/war1a.gif";
import war1b from "../../media/épiques_guerres/war1b.gif";
import war2a from "../../media/épiques_guerres/war2a.gif";
import war2b from "../../media/épiques_guerres/war2b.gif";
import war3a from "../../media/épiques_guerres/war3a.gif";
import war3b from "../../media/épiques_guerres/war3b.gif";

import halloween23 from "../../media/épiques/beer.gif";
import pda from "../../media/épiques/pda.png";

export default function getCollectionEpicsImageByString(object) {
  if (object === "1") {
    return star;
  } else if (object === "3") {
    return kabuki;
  } else if (object === "4") {
    return charbon;
  } else if (object === "5") {
    return bronze;
  } else if (object === "6") {
    return argent;
  } else if (object === "7") {
    return or;
  } else if (object === "8") {
    return platine;
  } else if (object === "9") {
    return diamant;
  } else if (object === "10") {
    return gold_pumpkin;
  } else if (object === "11") {
    return silver_pumpkin;
  } else if (object === "12") {
    return bronze_pumpkin;
  } else if (object === "13") {
    return normal_pumpkin;
  } else if (object === "14") {
    return hat;
  } else if (object === "15") {
    return w1;
  } else if (object === "16") {
    return w2;
  } else if (object === "17") {
    return w3;
  } else if (object === "18") {
    return w4;
  } else if (object === "19") {
    return w5;
  } else if (object === "20") {
    return w6;
  } else if (object === "21") {
    return cupGold;
  } else if (object === "22") {
    return cupSilver;
  } else if (object === "23") {
    return cupBronze;
  } else if (object === "24") {
    return hat_gold;
  } else if (object === "25") {
    return hat_silver;
  } else if (object === "26") {
    return hat_bronze;
  } else if (object === "27") {
    return hat_participation;
  } else if (object === "28") {
    return war1a;
  } else if (object === "29") {
    return war1b;
  } else if (object === "30") {
    return war2a;
  } else if (object === "31") {
    return war2b;
  } else if (object === "32") {
    return halloween23;
  } else if (object === "33") {
    return pda;
  } else if (object === "34") {
    return war3a;
  } else if (object === "35") {
    return war3b;
  } else if (object === "36") {
    return kabuki_gold_bag;
  }  else {
    return emptyCase;
  }
}
