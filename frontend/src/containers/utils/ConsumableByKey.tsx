import irma from "../../media/consumables/Potion Irma.gif";
import irma_surplus from "../../media/consumables/Potion Irma Surplus.gif";
import ange from "../../media/consumables/Potion Ange.gif";
import nuage from "../../media/consumables/Nuage Burger.gif";
import pain from "../../media/consumables/Pain Chaud.gif";
import tarte from "../../media/consumables/Tarte Viande.gif";
import medaille from "../../media/consumables/Médaille Chocolat.gif";
import feu from "../../media/consumables/bonus_fire.gif";
import wood from "../../media/consumables/bonus_wood.gif";
import water from "../../media/consumables/bonus_water.gif";
import thunder from "../../media/consumables/bonus_thunder.gif";
import air from "../../media/consumables/bonus_air.gif";
import prismatik from "../../media/consumables/bonus_prismatik.png";
import griffes from "../../media/consumables/bonus_claw.gif";
import bave from "../../media/consumables/Bave Loupi.gif";
import ramens from "../../media/consumables/Bol Ramens.gif";
import oeil from "../../media/consumables/bonus_tigereye.gif";
import antidote from "../../media/consumables/bonus_pill.gif";
import bons from "../../media/consumables/bons.gif";
import buffaggro from "../../media/consumables/buff_aggro.gif";
import buffnature from "../../media/consumables/buff_nature.gif";
import buffwater from "../../media/consumables/buff_water.gif";
import buffmahamuti from "../../media/consumables/buff_mahamuti.gif";
import pruniac from "../../media/consumables/liquor.gif";
import cherries from "../../media/consumables/cherryLiquor.gif";
import potiondark from "../../media/consumables/potiondark.gif";
import champifuz from "../../media/consumables/ticket_champifuz.gif";
import champifuz_special from "../../media/consumables/ticket_champifuz_special.gif";
import champifuz_monochrome from "../../media/consumables/ticket_monochrome.gif";
import champifuz_magik from "../../media/consumables/ticket_magik.gif";
import pieces from "../../media/actions/hist_buy.gif";
import globe_charbon from "../../media/épiques/globe_charbon.gif";
import globe_bronze from "../../media/épiques/globe_bronze.gif";
import globe_argent from "../../media/épiques/globe_argent.gif";
import globe_or from "../../media/épiques/globe_or.gif";
import globe_platine from "../../media/épiques/globe_platine.gif";
import globe_diamant from "../../media/épiques/globe_diamant.gif";
import gift from "../../media/consumables/ticket_cadeau.gif";
import egg_11 from "../../media/consumables/egg_11.png";
import egg_14 from "../../media/consumables/egg_14.png";
import egg_18 from "../../media/consumables/egg_18.png";
import egg_20 from "../../media/consumables/egg_20.png";
import goutte_glu from "../../media/consumables/goutte_glu.gif";
import eternity_pill from "../../media/consumables/pill_up.gif";
import fish1 from "../../media/cuisine/fish1.png";
import fish2 from "../../media/cuisine/fish2.png";
import fish3 from "../../media/cuisine/fish3.png";
import fish4 from "../../media/cuisine/fish4.png";
import picking1 from "../../media/cuisine/picking1.png";
import picking2 from "../../media/cuisine/picking2.png";
import picking3 from "../../media/cuisine/picking3.png";
import picking4 from "../../media/cuisine/picking4.png";
import tisane from "../../media/consumables/tisane.png";
import biere from "../../media/consumables/biere.png";
import egg_x from "../../media/consumables/egg_x.png";
import egg_choc from "../../media/consumables/egg_chocolate.png";
import unknown from "../../media/consumables/unknown.png";
import war_potion from "../../media/consumables/fight_potion.png";
import x0 from "../../media/game/x0.png";
import x2 from "../../media/game/x2.png";
import x3 from "../../media/game/x3.png";
import x10 from "../../media/game/x10.png";
import cargouMilk from "../../media/consumables/cargou_milk.png";
import canneAPeche from "../../media/tools/rod.png";

import JetonCasinoBronze from "../../media/consumables/jeton_casino_bronze.png";
import JetonCasinoArgent from "../../media/consumables/jeton_casino_argent.png";
import JetonCasinoOr from "../../media/consumables/jeton_casino_or.png";

export default function getConsumableByKey(key) {
  switch (key) {
    case "Potion Irma":
      return irma;
    case "Potion Irma Surplus":
      return irma_surplus;
    case "Potion Ange":
      return ange;
    case "Nuage Burger":
      return nuage;
    case "Pain Chaud":
      return pain;
    case "Tarte Viande":
      return tarte;
    case "Médaille Chocolat":
      return medaille;
    case "bonus_fire":
      return feu;
    case "bonus_wood":
      return wood;
    case "bonus_water":
      return water;
    case "bonus_thunder":
      return thunder;
    case "bonus_air":
      return air;
    case "bonus_prismatik":
      return prismatik;
    case "bonus_claw":
      return griffes;
    case "Bave Loupi":
      return bave;
    case "Bol Ramens":
      return ramens;
    case "bonus_tigereye":
      return oeil;
    case "bonus_pill":
      return antidote;
    case "Bons du Trésor":
      return bons;
    case "Ticket Cadeau":
      return gift;
    case "Pruniac":
      return pruniac;
    case "bonus_boostaggro":
      return buffaggro;
    case "bonus_boostnature":
      return buffnature;
    case "bonus_boostwater":
      return buffwater;
    case "bonus_mahamuti":
      return buffmahamuti;
    case "Coulis Cerise":
      return cherries;
    case "Potion Sombre":
      return potiondark;
    case "champifuzz":
      return champifuz;
    case "Ticket Champifuz":
      return champifuz;
    case "Goutte Glu":
      return goutte_glu;
    case "Lait de Cargou":
      return cargouMilk;
    case "Eternity Pill":
      return eternity_pill;
    case "COINS":
      return pieces;
    case "globe_charbon":
      return globe_charbon;
    case "globe_bronze":
      return globe_bronze;
    case "globe_argent":
      return globe_argent;
    case "globe_or":
      return globe_or;
    case "globe_platine":
      return globe_platine;
    case "globe_diamant":
      return globe_diamant;
    case "EGG_11":
      return egg_11;
    case "EGG_14":
      return egg_14;
    case "EGG_18":
      return egg_18;
    case "EGG_20":
      return egg_20;
    case "EGG_X":
      return egg_x;
    case "EGG_CHOC":
      return egg_choc;
    case "Feuille de Pacifique":
      return picking1;
    case "Oréade blanc":
      return picking2;
    case "Tige de Roncivore":
      return picking3;
    case "Anémone solitaire":
      return picking4;
    case "Perche perlée":
      return fish1;
    case "Grémille grelottante":
      return fish2;
    case "Cube de glu":
      return fish3;
    case "Anguille":
      return fish4;
    case "Tisane des bois":
      return tisane;
    case "Bière de Dinojak":
      return biere;
    case "Potion Guerre":
      return war_potion;
    case "Ticket Champifuz Special":
      return champifuz_special;
    case "Ticket Monochrome":
      return champifuz_monochrome;
    case "Ticket Magik":
      return champifuz_magik;
    case "GOLD":
      return pieces;
    case "CanneAPeche":
      return canneAPeche;

      //Casino :
    case "JetonCasinoBronze":
      return JetonCasinoBronze;
    case "JetonCasinoArgent":
      return JetonCasinoArgent;
    case "JetonCasinoOr":
      return JetonCasinoOr;

    default:
      return unknown;
  }
}
