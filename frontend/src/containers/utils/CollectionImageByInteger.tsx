import medalGold from "../../media/collection/medal_gold.gif";
import medalSilver from "../../media/collection/medal_silver.gif";
import medalBronze from "../../media/collection/medal_bronze.gif";
import aphro from "../../media/collection/aphro.gif";
import burger from "../../media/collection/burger.gif";
import charm from "../../media/collection/charm.gif";
import cheese from "../../media/collection/cheese.gif";
import chest from "../../media/collection/chest.gif";
import chocapic from "../../media/collection/chocapic.gif";
import claw from "../../media/collection/claw.gif";
import cloud from "../../media/collection/cloud.gif";
import clover from "../../media/collection/clover.gif";
import coingold from "../../media/collection/coin_gold.gif";
import coinsilver from "../../media/collection/coin_silver.gif";
import edmond from "../../media/collection/edmond.gif";
import eye from "../../media/collection/eye.gif";
import fire from "../../media/collection/fire.gif";
import fruit from "../../media/collection/fruit.gif";
import gem from "../../media/collection/gem.gif";
import glove from "../../media/collection/glove.gif";
import hook from "../../media/collection/hook.gif";
import oldBuze from "../../media/collection/oldBuze.gif";
import pacific from "../../media/collection/pacific.gif";
import pearl from "../../media/collection/pearl.gif";
import piou from "../../media/collection/piou.gif";
import powder from "../../media/collection/powder.gif";
import ring from "../../media/collection/ring.gif";
import scroll from "../../media/collection/scroll.gif";
import socrateTooth from "../../media/collection/socrate_tooth.gif";
import staff from "../../media/collection/staff.gif";
import star1 from "../../media/collection/star1.gif";
import star2 from "../../media/collection/star2.gif";
import star3 from "../../media/collection/star3.gif";
import star4 from "../../media/collection/star4.gif";
import star5 from "../../media/collection/star5.gif";
import valve from "../../media/collection/valve.gif";
import web from "../../media/collection/web.gif";

export default function getCollectionImageByString(object: string) {
  if (object === "1") {
    return medalGold;
  } else if (object === "2") {
    return medalSilver;
  } else if (object === "3") {
    return medalBronze;
  } else if (object === "4") {
    return medalGold;
  } else if (object === "5") {
    return medalSilver;
  } else if (object === "6") {
    return medalBronze;
  } else if (object === "7") {
    return medalGold;
  } else if (object === "8") {
    return medalSilver;
  } else if (object === "9") {
    return medalBronze;
  } else if (object === "10") {
    return medalGold;
  } else if (object === "11") {
    return medalSilver;
  } else if (object === "12") {
    return medalBronze;
  } else if (object === "13") {
    return medalGold;
  } else if (object === "14") {
    return medalSilver;
  } else if (object === "15") {
    return medalBronze;
  } else if (object === "16") {
    return cheese;
  } else if (object === "17") {
    return aphro;
  } else if (object === "18") {
    return chest;
  } else if (object === "19") {
    return claw;
  } else if (object === "20") {
    return clover;
  } else if (object === "21") {
    return coingold;
  } else if (object === "22") {
    return coinsilver;
  } else if (object === "23") {
    return fire;
  } else if (object === "24") {
    return fruit;
  } else if (object === "25") {
    return gem;
  } else if (object === "26") {
    return glove;
  } else if (object === "27") {
    return pacific;
  } else if (object === "28") {
    return pearl;
  } else if (object === "29") {
    return powder;
  } else if (object === "30") {
    return scroll;
  } else if (object === "31") {
    return chocapic;
  } else if (object === "32") {
    return socrateTooth;
  } else if (object === "33") {
    return burger;
  } else if (object === "34") {
    return eye;
  } else if (object === "35") {
    return hook;
  } else if (object === "36") {
    return star1;
  } else if (object === "37") {
    return star2;
  } else if (object === "38") {
    return star3;
  } else if (object === "39") {
    return star4;
  } else if (object === "40") {
    return star5;
  } else if (object === "41") {
    return web;
  } else if (object === "42") {
    return edmond;
  } else if (object === "43") {
    return oldBuze;
  } else if (object === "44") {
    return ring;
  } else if (object === "45") {
    return staff;
  } else if (object === "46") {
    return valve;
  } else if (object === "47") {
    return cloud;
  } else if (object === "48") {
    return charm;
  } else if (object === "49") {
    return piou;
  }
}
