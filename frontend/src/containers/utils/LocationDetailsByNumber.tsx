import i18n from "i18next";

export default function getLocationDetailsByNumber(placeNumber) {
  switch (placeNumber) {
    case 0:
      return i18n.t("location.dinoville.details");
    case 1:
      return i18n.t("location.caverneirma.details");
    case 2:
      return i18n.t("location.clairiere.details");
    case 3:
      return i18n.t("location.dinoplage.details");
    case 4:
      return i18n.t("location.barrage.details");
    case 5:
      return i18n.t("location.falaise.details");
    case 6:
      return i18n.t("location.montdino.details");
    case 7:
      return i18n.t("location.porte.details");
    case 8:
      return i18n.t("location.gredins.details");
    case 9:
      return i18n.t("location.foret.details");
    case 10:
      return i18n.t("location.temple.details");
    case 11:
      return i18n.t("location.port.details");
    case 12:
      return i18n.t("location.pitie.details");
    case 13:
      return i18n.t("location.ruines.details");
    case 14:
      return i18n.t("location.credit.details");
    case 15:
      return i18n.t("location.bazar.details");
    case 16:
      return i18n.t("location.marais.normal.details");
    case 17:
      return i18n.t("location.jungle.details");
    case 18:
      return i18n.t("location.bordeciel.details");
    case 19:
      return i18n.t("location.source.details");
    case 20:
      return i18n.t("location.anomalie.details");
    case 21:
      return i18n.t("location.hutte.details");
    case 22:
      return i18n.t("location.toutchaud.details");
    case 23:
      return i18n.t("location.bastion.details");
    case 24:
      return i18n.t("location.portroyal.details");
    case 25:
      return i18n.t("location.suntzu.details");
    case 26:
      return i18n.t("location.labyrinthe.details");
    case 27:
      return i18n.t("location.bosquet.details");
    case 28:
      return i18n.t("location.stiou.details");
    case 29:
      return i18n.t("location.universite.details");
    case 30:
      return i18n.t("location.labo.details");
    case 31:
      return i18n.t("location.puit.details");
    case 32:
      return i18n.t("location.everouest.details");
    case 33:
      return i18n.t("location.lacceleste.details");
    case 34:
      return i18n.t("location.repaire.details");
    case 35:
      return i18n.t("location.villagefantome.details");
    case 36:
      return i18n.t("location.dinocropole.details");
    case 37:
      return i18n.t("location.plaines.details");
    default:
      break;
  }
}
