import elem0 from "../../media/dinoz/element0.gif";
import elem1 from "../../media/dinoz/element1.gif";
import elem2 from "../../media/dinoz/element2.gif";
import elem3 from "../../media/dinoz/element3.gif";
import elem4 from "../../media/dinoz/element4.gif";

export default function getElementImageByString(element) {
  if (element === "Feu" || element === 0) {
    return elem0;
  } else if (element === "Terre" || element === 1) {
    return elem1;
  } else if (element === "Eau" || element === 2) {
    return elem2;
  } else if (element === "Foudre" || element === 3) {
    return elem3;
  } else if (element === "Air" || element === 4) {
    return elem4;
  }
}