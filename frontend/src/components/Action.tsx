import React from "react";

import "./Action.scss";

type Props = React.HTMLAttributes<HTMLButtonElement> & {
  imgSrc: string;
  imgAlt?: string;
  children?: React.ReactNode;
  onClick: React.MouseEventHandler<HTMLButtonElement>;
};

export default function Action({
  className = "",
  imgSrc,
  imgAlt = "",
  children,
  onClick,
  ...attrs
}: Props) {
  return (
    <button
      className={`action ${className}`.trim()}
      onClick={onClick}
      type="button"
      {...attrs}
    >
      <img alt={imgAlt} src={imgSrc} />
      {children != null && <span className="actionText">{children}</span>}
    </button>
  );
}
