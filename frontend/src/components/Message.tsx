import React from "react";
import iconDefault from "../assets/hist_default.gif";
import iconDanger from "../assets/hist_error.gif";
import iconSearch from "../assets/act_here.gif";

import "./Message.css";

type Props = React.HTMLAttributes<HTMLDivElement> & {
  type?: "info" | "danger" | "success" | "search";
  iconSrc?: string;
  children: React.ReactNode;
};

export default function Message({
  type = "info",
  iconSrc,
  className = "",
  children,
}: Props) {
  if (!iconSrc) {
    if (type == "search") {
      iconSrc = iconSearch;
    } else if (type == "danger") {
      iconSrc = iconDanger;
    } else {
      iconSrc = iconDefault;
    }
  }
  return (
    <div className={`message is-${type} ${className}`.trim()}>
      <img className="message-icon" alt="" src={iconSrc} />
      <div>{children}</div>
    </div>
  );
}
