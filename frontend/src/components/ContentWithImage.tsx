import React from "react";
import Content from "./Content";

import "./ContentWithImage.scss";

type Props = React.HTMLAttributes<HTMLDivElement> & {
  children: React.ReactNode;
};

export default function ContentWithImage({
  className = "",
  children,
  ...attrs
}: Props) {
  return (
    <Content className={`contentWithImage ${className}`.trim()} {...attrs}>
      {children}
    </Content>
  );
}

type ImageWrapProps = React.HTMLAttributes<HTMLDivElement> & {
  children: React.ReactNode;
  noBorder?: boolean;
};
export const ImageWrap = ({
  className = "",
  noBorder,
  children,
  ...attrs
}: ImageWrapProps) => {
  return (
    <div
      className={`contentWithImage-image${
        noBorder === true ? " contentWithImage-image--noBorder" : ""
      } ${className}`.trim()}
      {...attrs}
    >
      {children}
    </div>
  );
};
