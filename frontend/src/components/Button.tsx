import React, { ButtonHTMLAttributes, MouseEvent } from "react";
import "./Button.css";

export type Props = ButtonHTMLAttributes<HTMLButtonElement> & {
  onClick?: (event?: MouseEvent) => void;
  size?: "larger" | "large" | "medium" | "normal" | "small";
  active?: boolean;
  children: React.ReactNode;
};

export default function Button({
  onClick,
  children,
  size = "normal",
  active,
  className = "",
  type = "button",
  ...attrs
}: Props) {
  return (
    <button
      className={`button ${size !== "normal" ? `is-${size}` : ""}${
        active ? " is-active" : ""
      } ${className}`.trim()}
      type={type}
      onClick={onClick}
      {...attrs}
    >
      {children}
    </button>
  );
}
