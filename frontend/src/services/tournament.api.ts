import axios from "axios";
import { apiUrl } from "..";
import { FightDataPayload } from "../types/fight-data-payload";
import { Tourney } from "../types/tourney";

type TournamentProps = {
  accountId: string;
  dinozId: string;
  tournament: string;
};

export const getTournamentAccess = ({
  accountId,
  dinozId,
  tournament,
}: TournamentProps) =>
  axios.get<boolean>(
    `${apiUrl}/account/${accountId}/${dinozId}/${tournament}/access`
  );

export const getTournament = ({
  accountId,
  dinozId,
  tournament,
}: TournamentProps) =>
  axios.get<Tourney>(
    `${apiUrl}/account/${accountId}/${dinozId}/tournament/${tournament}`
  );

export const getTournamentFight = ({
  accountId,
  dinozId,
  tournament,
}: TournamentProps) =>
  axios.get<FightDataPayload | "">(
    `${apiUrl}/account/${accountId}/${dinozId}/fight/${tournament}`
  );

export const getTournamentQuit = ({
  accountId,
  dinozId,
  tournament,
}: TournamentProps) =>
  axios.get<boolean>(
    `${apiUrl}/account/${accountId}/${dinozId}/tournament/${tournament}/quit`
  );
