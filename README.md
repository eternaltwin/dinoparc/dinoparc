# Neoparc

Main repository for Neoparc.

## Requirements

- Java 15
- PgSql
- Node 14.13
- Yarn

## Getting started

1. If you intend to submit merge requests, fork the repo
2. Clone the repo
3. Copy `etwin.toml.example` into `etwin.toml`
4. Copy `backend/src/main/resources/application.yml` into `backend/application.yml`
5. Install the dependencies `yarn install`.
6. Start Eternal-Twin: `yarn etwin start`
7. Start the API server: `cd backend && ./gradlew flywayMigrate && ./gradlew run`
8. Start the frontend: `cd frontend && yarn start`
