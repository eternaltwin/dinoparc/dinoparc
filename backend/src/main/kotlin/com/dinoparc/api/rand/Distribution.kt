package com.dinoparc.api.rand

interface Distribution<T> {
    fun sample(rng: RngCore): T
}
