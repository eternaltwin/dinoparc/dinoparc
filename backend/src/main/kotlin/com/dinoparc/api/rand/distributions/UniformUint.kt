package com.dinoparc.api.rand.distributions

import com.dinoparc.api.rand.Distribution
import com.dinoparc.api.rand.RngCore

/**
 * Unbiased uniform sampler for a range of uints. `low` and `high` are both
 * inclusive.
 *
 * We first define the size of the range as the number of possible values.
 * The inner RNG produces 2³² values, if the size is 2³² we simply return the
 * random value, otherwise we reduce with a modulo (while taking care of bias).
 * Taking a modulo immediately would result in a bias for smaller values.
 * We avoid it by first picking a window that is a multiple of `size`. If the
 * inner RNG returns a value outside the window, we reject it and generate a
 * new one.
 * Any multiple of `size` works for the window (as long as it is smaller than
 * 2³²) but the larger it is, the less inner RNG values are rejected.
 * We pick `2³²-(2³² mod size)` as our window size, it will always contain at
 * least half of the values of the inner RNG so we avoid minimising too much.
 * Another valid window size is `2^k * size`, which may be computed with
 * bitshifts. It is smaller but still covers half of the inner RNG.
 *
 * The final modulo + window check is computed with the widening multiplication
 * because it is faster.
 */
class UniformUint(val low: UInt, val high: UInt) : Distribution<UInt> {
    init {
        assert(low <= high)
    }

    override fun sample(rng: RngCore): UInt {
        val size = this.high - this.low + 1U;
        if (size == 0U) {
            // Wrapping overflow to 0 means that the size is actually 2^32: all uints
            return rng.randUint()
        }
        // The "1U" is there to allow for `<=` comparisons and deal with `2³²`.
        val windowMinusOne = UInt.MAX_VALUE - ((UInt.MAX_VALUE - size + 1U) % size)
        // val windowMinusOne = size.shl(size.countLeadingZeroBits()) - 1U;
        while(true) {
            val inner = rng.randUint();
            val modAndDust = inner.toULong() * size.toULong();
            val dust = modAndDust.toUInt();
            val mod = modAndDust.shr(32).toUInt();
            if (dust < windowMinusOne) {
                return mod
            }
        }
    }
}
