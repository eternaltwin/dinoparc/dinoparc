create table player_clan
(
    player_id                  uuid             not null,
    clan_id                    uuid             not null,
    clan_name                  text             not null,
    role                       text,
    active_warrior             boolean          default true,
    nb_points_occupation       integer          default 0 not null,
    nb_points_fights           integer          default 0 not null,
    nb_points_totem            integer          default 0 not null,

    PRIMARY KEY (player_id, clan_id)
);

create table clan
(
    id                                   text             primary key not null,
    creator_id                           text             not null,
    name                                 text             not null,
    nb_dinoz_total                       integer          default 1 not null,
    nb_dinoz_warriors                    integer          default 1 not null,
    nb_points_occupation                 integer          default 0 not null,
    nb_points_fights                     integer          default 0 not null,
    nb_points_totem                      integer          default 0 not null,
    position                             integer          default 0 not null,
    faction                              text,
    creation_date                        text             not null,
    pages                                jsonb            not null,
    totem                                jsonb            not null,
    members                              jsonb            not null,
    candidates                           jsonb            not null
);

create table clan_messages
(
    clan_id                                   text             primary key not null,
    messages                                  jsonb            not null
);

create index player_clan_player_id on player_clan(player_id);