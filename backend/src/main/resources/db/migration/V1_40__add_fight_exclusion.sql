create table fight_exclusion
(
  id               uuid             primary key not null,
  from_player_id   uuid             not null,
  to_player_id     uuid             not null,
  reason           text             default '' not null,
  numericalDate    timestamp with time zone default now() not null
);
