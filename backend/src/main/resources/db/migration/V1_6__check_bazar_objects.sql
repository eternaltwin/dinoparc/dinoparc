CREATE FUNCTION test_bazar_objects(
  IN objects JSONB
) RETURNS BOOLEAN
  LANGUAGE sql
  IMMUTABLE AS
$$
SELECT JSONB_TYPEOF($1) = 'object' AND FALSE NOT IN (
  SELECT (JSONB_TYPEOF(object_count.value) = 'number' AND 0 <= object_count.value::INT) AS is_ok
  FROM JSONB_EACH($1) AS object_count
);
$$;

ALTER TABLE bazar
  ADD CONSTRAINT check_objects CHECK (test_bazar_objects(objects));
