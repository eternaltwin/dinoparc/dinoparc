create table granit_mission_quest
(
    id                     uuid primary key not null,
    player_id              uuid             not null,
    isComplete             boolean          not null default false,
    isCompleteAndValidated boolean          not null default false,
    moueffe                bigint           not null default 0,
    picori                 bigint           not null default 0,
    castivore              bigint           not null default 0,
    sirain                 bigint           not null default 0,
    winks                  bigint           not null default 0,
    gorilloz               bigint           not null default 0,
    cargou                 bigint           not null default 0,
    hippoclamp             bigint           not null default 0,
    rokky                  bigint           not null default 0,
    pigmou                 bigint           not null default 0,
    wanwan                 bigint           not null default 0,
    goupignon              bigint           not null default 0,
    kump                   bigint           not null default 0,
    pteroz                 bigint           not null default 0,
    santaz                 bigint           not null default 0,
    korgon                 bigint           not null default 0,
    kabuki                 bigint           not null default 0,
    serpantin              bigint           not null default 0,
    soufflet               bigint           not null default 0,
    feross                 bigint           not null default 0
);