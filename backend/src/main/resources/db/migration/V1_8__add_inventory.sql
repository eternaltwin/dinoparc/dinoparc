create table inventory
(
  player_id                  uuid             not null,
  name                       text,
  quantity                   integer,
  PRIMARY KEY (player_id, name)
);

create index inventory_player_id on inventory(player_id);