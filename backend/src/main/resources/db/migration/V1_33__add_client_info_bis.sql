create table client_information_bis
(
  player_id                                  uuid             not null,
  dinoz_id                                   text,
  first                                      bytea,
  fourth                                     text,
  skip_info                                  jsonb,
  rank                                       integer          not null default 0,
  retrieved_date                             timestamp with time zone not null
);

alter table player add history_skip boolean default false not null;