alter table player_stat add column nb_fight_race_j         bigint           not null default 0;
alter table player_stat add column nb_fight_race_k         bigint           not null default 0;
alter table player_stat add column nb_fight_race_l         bigint           not null default 0;
alter table player_stat add column nb_shiny_race_j         bigint           not null default 0;
alter table player_stat add column nb_shiny_race_k         bigint           not null default 0;
alter table player_stat add column nb_shiny_race_l         bigint           not null default 0;