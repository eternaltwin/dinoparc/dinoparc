create table client_information_detail
(
  account_id         uuid     not null,
  dinoz_id           text     not null default '',
  detail_type        text     not null default '',
  first              text     not null default '',
  second             text     not null default '',
  retrieved_date     timestamp with time zone not null
);