create table game_master_actions
(
  source_account_id       uuid      not null,
  target_account_id       uuid,
  action_type             text      not null,
  action_detail           text      not null,
  result                  text      not null,
  success                 boolean   default false,
  forbidden               boolean   default false,
  numericalDate   timestamp with time zone default now() not null
);