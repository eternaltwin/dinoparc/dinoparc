package com.dinoparc.api.services;

import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.account.PlayerActionModeEnum;
import com.dinoparc.api.domain.account.PlayerImgModeEnum;
import com.dinoparc.api.repository.PgPlayerRepository;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class PlayerService {
    private final PgPlayerRepository playerRepository;

    public PlayerService(
        PgPlayerRepository playerRepository
    ) {
        this.playerRepository = playerRepository;
    }

    public Player updatePlayerImgMode(Player player, String imgMode) {
        if (Arrays.stream(PlayerImgModeEnum.values()).anyMatch(imgModeEnum -> imgModeEnum.toString().equals(imgMode))) {
            return playerRepository.updateImgMode(player.getId(), imgMode);
        }
        return player;
    }

    public Player updatePlayerActionMode(Player player, String actionMode) {
        if (Arrays.stream(PlayerActionModeEnum.values()).anyMatch(actionModeEnum -> actionModeEnum.toString().equals(actionMode))) {
            return playerRepository.updateActionMode(player.getId(), actionMode);
        }
        return player;
    }
}
