package com.dinoparc.api.services;

import com.dinoparc.api.controllers.dto.HOFDinozDto;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.misc.Pagination;
import com.dinoparc.api.repository.PgDinozRepository;
import com.dinoparc.api.repository.PgPlayerRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class DinozService {

    private final PgDinozRepository dinozRepository;
    private final PgPlayerRepository playerRepository;

    public DinozService(PgDinozRepository dinozRepository, PgPlayerRepository playerRepository) {
        this.dinozRepository = dinozRepository;
        this.playerRepository = playerRepository;
    }

    public Pagination<HOFDinozDto> getRaceRankings(String chapter, Integer offset, Integer limit) {
        if (limit == null || limit < 1 || limit > 100) {
            return dinozRepository.getRanking(chapter, offset, 100);
        }
        return dinozRepository.getRanking(chapter, offset, limit);
    }

    public void updateInactivePlayerDinozDanger(String dinozId, int danger) {
        Optional<Dinoz> dinozOpt = dinozRepository.findById(dinozId);
        if (dinozOpt.isEmpty() || (danger >= 0 && DinoparcConstants.OUISTITI.equals(dinozOpt.get().getRace()))) {
            return;
        }
        Player dinozPlayer = playerRepository.findPlayerByDinozId(dinozId);
        if (!dinozPlayer.isBlocked() && !dinozPlayer.activeDuringLast7Days()) {
            dinozRepository.setDanger(dinozId, danger);
        }
    }
}
