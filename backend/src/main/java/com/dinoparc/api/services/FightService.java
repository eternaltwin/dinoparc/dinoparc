package com.dinoparc.api.services;

import com.dinoparc.api.configuration.NeoparcConfig;
import com.dinoparc.api.controllers.dto.FightSummaryDto;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.History;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.account.RandomCollection;
import com.dinoparc.api.domain.clan.Clan;
import com.dinoparc.api.domain.clan.PlayerClan;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.dinoz.DinozUtils;
import com.dinoparc.api.domain.dinoz.FightElements;
import com.dinoparc.api.domain.dinoz.Tournament;
import com.dinoparc.api.domain.mission.GranitMissionDao;
import com.dinoparc.api.rand.Rng;
import com.dinoparc.api.repository.*;
import com.dinoparc.tables.records.PlayerStatRecord;
import discord4j.core.spec.EmbedCreateSpec;
import discord4j.rest.util.Color;
import kotlin.Pair;
import org.jooq.TableField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import static com.dinoparc.api.domain.dinoz.Dinoz.isDinozMaxed;

@Component
public class FightService {

  private final PgPlayerRepository playerRepository;
  private final PgPlayerStatRepository playerStatRepository;
  private final PgDinozRepository dinozRepository;
  private final PgHistoryRepository historyRepository;
  private final PgCollectionRepository collectionRepository;
  private final PgInventoryRepository inventoryRepository;
  private final PgClanRepository clanRepository;
  private final NeoparcConfig neoparcConfig;
  private final ClanService clanService;
  private final PgGranitMissionRepository granitMissionRepository;
  private final DiscordService discordService;

  @Autowired
  public FightService(PgPlayerRepository playerRepository, PgPlayerStatRepository playerStatRepository, PgDinozRepository dinozRepository,
                      PgHistoryRepository historyRepository, PgCollectionRepository collectionRepository,
                      PgInventoryRepository inventoryRepository, PgClanRepository clanRepository,
                      NeoparcConfig neoparcConfig, ClanService clanService, PgGranitMissionRepository granitMissionRepository, DiscordService discordService) {
    this.playerRepository = playerRepository;
    this.playerStatRepository = playerStatRepository;
    this.dinozRepository = dinozRepository;
    this.historyRepository = historyRepository;
    this.collectionRepository = collectionRepository;
    this.inventoryRepository = inventoryRepository;
    this.clanRepository = clanRepository;
    this.neoparcConfig = neoparcConfig;
    this.clanService = clanService;
    this.granitMissionRepository = granitMissionRepository;
    this.discordService = discordService;
  }

  public FightSummaryDto fightDinoz(Dinoz homeDinoz, Dinoz foreignDinoz, Boolean inTourney) {
    FightElements fightElements = new FightElements(homeDinoz, foreignDinoz);
    FightSummaryDto summary = initFight(homeDinoz, foreignDinoz);
    Integer initialHomeDinozLife = homeDinoz.getLife();
    Integer initialForeignDinozLife = foreignDinoz.getLife();
    summary.setFinalLifeLoss(0);
    calculateFirstRoundEffects(summary, homeDinoz, foreignDinoz, fightElements);
    calculateSecondRoundEffects(summary, homeDinoz, foreignDinoz, fightElements);
    calculateThirdRoundEffects(summary, homeDinoz, foreignDinoz, fightElements);
    Integer foreignExpWon = calculateFinalInfos(summary, homeDinoz, foreignDinoz, initialHomeDinozLife);
    updateStats(homeDinoz, summary);

    if (foreignExpWon >= 0) {
      Integer foreignLifeLost = initialForeignDinozLife - foreignDinoz.getLife();

      homeDinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);
      homeDinoz.getActionsMap().put(DinoparcConstants.COMBAT, checkForCherryEffects(homeDinoz));

      dinozRepository.processFight(homeDinoz.getId(), homeDinoz.getActionsMap(), homeDinoz.getLife(), homeDinoz.getExperience(), homeDinoz.getDanger(),
              homeDinoz.getPassiveList(), homeDinoz.getMalusList());

      if (foreignDinoz.getId() != null && !foreignDinoz.getId().contains(DinoparcConstants.PVE_FACTION)) {
        dinozRepository.processFight(foreignDinoz.getId(), foreignDinoz.getActionsMap(), foreignDinoz.getLife(), foreignDinoz.getExperience(), foreignDinoz.getDanger(),
                foreignDinoz.getPassiveList(), foreignDinoz.getMalusList());
      } else if (foreignDinoz.getId() != null) {
        clanService.occupationSummary.alivePVEWarEnnemies.removeIf(dinoz -> dinoz.getLife() <= 0);
      }

      concatHomeHistoryEventToDeque(homeDinoz, summary, inTourney);
      if (foreignDinoz.getLevel() > 0 && foreignDinoz.getId() != null && !foreignDinoz.getId().contains(DinoparcConstants.PVE_FACTION) && !foreignDinoz.getId().equalsIgnoreCase("@MANNY")) {
        concatForeignHistoryEventToDeque(homeDinoz, foreignDinoz, summary, foreignExpWon, foreignLifeLost);
      }

      checkForQuestUpdates(homeDinoz, foreignDinoz, summary);
      checkForFocusVanishing(homeDinoz, summary);
      checkForPrismatikVanishing(homeDinoz, summary);
      checkForSteal(homeDinoz, foreignDinoz, summary);
      checkForClanPointsScored(homeDinoz, foreignDinoz, summary);
      return summary;
    }
    return null;
  }

  private void updateStats(Dinoz homeDinoz, FightSummaryDto summary) {
    List statsToUpdate;
    if (summary.isHaveWon()) {
      statsToUpdate = List.of(
          new Pair(PgPlayerStatRepository.NB_FIGHT, 1),
          new Pair(PgPlayerStatRepository.NB_WON_FIGHT, 1)
      );
    } else {
      statsToUpdate = List.of(
          new Pair(PgPlayerStatRepository.NB_FIGHT, 1),
          new Pair(PgPlayerStatRepository.NB_LOST_FIGHT, 1)
      );
    }
    playerStatRepository.addStats(UUID.fromString(homeDinoz.getMasterId()), homeDinoz.getId(), statsToUpdate);
  }

  public boolean checkForCherryEffects(Dinoz homeDinoz) {
    if (homeDinoz.getMalusList().contains(DinoparcConstants.COULIS_CERISE)) {
      Long minutesLeft = ChronoUnit.MINUTES.between(ZonedDateTime.now(ZoneId.of("Europe/Paris")),
              ZonedDateTime.ofInstant(Instant.ofEpochSecond(homeDinoz.getEpochSecondsEndOfCherryEffect()),ZoneId.of("Europe/Paris")));
      homeDinoz.setCherryEffectMinutesLeft(minutesLeft);
      if (minutesLeft >= 0) {
        // Nothing to do on malus list
        return true;
      }
      homeDinoz.getMalusList().remove(DinoparcConstants.COULIS_CERISE);
      homeDinoz.setCherryEffectMinutesLeft(0);
      dinozRepository.processCurrentCherry(homeDinoz.getId(), homeDinoz.getMalusList(), 0);
    }
    return false;
  }

  private void checkForSteal(Dinoz homeDinoz, Dinoz foreignDinoz, FightSummaryDto summary) {
    if (homeDinoz.getSkillsMap().get(DinoparcConstants.VOLER) != null && homeDinoz.getSkillsMap().get(DinoparcConstants.VOLER) > 0) {
      Integer stealLevel = homeDinoz.getSkillsMap().get(DinoparcConstants.VOLER);
      Integer stealSuccess = ThreadLocalRandom.current().nextInt(1, 120 + 1);

      if (stealSuccess <= stealLevel && foreignDinoz.getMasterId() != null && !foreignDinoz.getId().contains(DinoparcConstants.PVE_FACTION) && foreignDinoz.getLevel() > 0) {
        Player foreignAccount = this.getAccountById(foreignDinoz.getMasterId());
        Player homeAccount = this.getAccountById(homeDinoz.getMasterId());
        var foreignInventory = inventoryRepository.getAll(foreignAccount.getId());

        if (!foreignInventory.isEmpty()) {
          Random generator = new Random();
          List<String> objects = foreignInventory.entrySet()
                  .stream()
                  .filter(entry -> entry.getValue() > 0)
                  .filter(entry -> !entry.getKey().equalsIgnoreCase(DinoparcConstants.POTION_IRMA_SURPLUS))
                  .filter(entry -> !entry.getKey().equalsIgnoreCase(DinoparcConstants.TISANE))
                  .filter(entry -> !entry.getKey().equalsIgnoreCase(DinoparcConstants.BIERE))
                  .filter(entry -> !entry.getKey().equalsIgnoreCase(DinoparcConstants.POTION_SOMBRE))
                  .filter(entry -> !entry.getKey().equalsIgnoreCase(DinoparcConstants.COULIS_CERISE))
                  .filter(entry -> !entry.getKey().equalsIgnoreCase(DinoparcConstants.ETERNITY_PILL))
                  .filter(entry -> !entry.getKey().equalsIgnoreCase(DinoparcConstants.OEUF_GLUON))
                  .filter(entry -> !entry.getKey().equalsIgnoreCase(DinoparcConstants.OEUF_SANTAZ))
                  .filter(entry -> !entry.getKey().equalsIgnoreCase(DinoparcConstants.OEUF_SERPANTIN))
                  .filter(entry -> !entry.getKey().equalsIgnoreCase(DinoparcConstants.OEUF_FEROSS))
                  .filter(entry -> !entry.getKey().equalsIgnoreCase(DinoparcConstants.OEUF_COBALT))
                  .filter(entry -> !entry.getKey().equalsIgnoreCase(DinoparcConstants.MAGIK_CHAMPIFUZ))
                  .filter(entry -> !entry.getKey().equalsIgnoreCase(DinoparcConstants.BONS))
                  .filter(entry -> !entry.getKey().startsWith("totem"))
                  .map(Map.Entry::getKey)
                  .toList();

          String randomKey = objects.get(generator.nextInt(objects.size() - 1));

          if (foreignInventory.get(randomKey) != null) {
            summary.setStolenObject(randomKey);
            inventoryRepository.substractInventoryItem(foreignAccount.getId(), randomKey, 1);
            inventoryRepository.addInventoryItem(homeAccount.getId(), randomKey, 1);

            History foreignHistoryEvent = new History();
            foreignHistoryEvent.setPlayerId(foreignAccount.getId());
            foreignHistoryEvent.setObject(randomKey);
            foreignHistoryEvent.setIcon("hist_sneak.gif");
            foreignHistoryEvent.setType("steal_victim");
            foreignHistoryEvent.setFromDinozName(homeDinoz.getName());
            historyRepository.save(foreignHistoryEvent);

            History homeHistoryEvent = new History();
            homeHistoryEvent.setPlayerId(homeAccount.getId());
            homeHistoryEvent.setObject(randomKey);
            homeHistoryEvent.setIcon("hist_sneak.gif");
            homeHistoryEvent.setType("steal_home");
            homeHistoryEvent.setFromDinozName(homeDinoz.getName());
            historyRepository.save(homeHistoryEvent);
          }
        }
      }

      else if (foreignDinoz.getMasterId() == null
              && foreignDinoz.getLevel() <= 0
              && !foreignDinoz.getName().startsWith("@")
              && foreignDinoz.getId() == null
              && foreignDinoz.getAppearanceCode().startsWith("L")) {

        Player playerAccount = this.getAccountById(homeDinoz.getMasterId());
        summary.setStolenObject(DinoparcConstants.FOCUS_MAHAMUTI);
        inventoryRepository.addInventoryItem(playerAccount.getId(), DinoparcConstants.FOCUS_MAHAMUTI, 1);

        History historyEvent = new History();
        historyEvent.setPlayerId(playerAccount.getId());
        historyEvent.setObject(DinoparcConstants.FOCUS_MAHAMUTI);
        historyEvent.setIcon("hist_sneak.gif");
        historyEvent.setType("steal_home");
        historyEvent.setFromDinozName(homeDinoz.getName());
        historyRepository.save(historyEvent);
      }

//      else if (homeDinoz.getPlaceNumber() == AccountService.getHalloweenHordesLocation() && isHalloweenEventAndObjectsAreStealableAtThisHour(foreignDinoz, homeDinoz)) {
//        Player playerAccount = this.getAccountById(homeDinoz.getMasterId());
//        String randomLoot = getRandomStealableObject(playerAccount.getDailyShiniesFought());
//        summary.setStolenObject(randomLoot);
//        inventoryRepository.addInventoryItem(playerAccount.getId(), randomLoot, 1);
//
//        History historyEvent = new History();
//        historyEvent.setPlayerId(playerAccount.getId());
//        historyEvent.setObject(randomLoot);
//        historyEvent.setIcon("hist_sneak.gif");
//        historyEvent.setType("steal_home");
//        historyEvent.setFromDinozName(homeDinoz.getName());
//        historyRepository.save(historyEvent);
//
//        if (null == playerAccount.getEventCounter()) {
//          playerAccount.setEventCounter(0);
//        }
//
//        playerAccount.setEventCounter(playerAccount.getEventCounter() + 1);
//        playerStatRepository.addStats(UUID.fromString(homeDinoz.getMasterId()), homeDinoz.getId(), List.of(new Pair(PgPlayerStatRepository.EVENT_COUNTER, 1)));
//
//        Long eventCounter = playerStatRepository.getCounter(UUID.fromString(playerAccount.getId()), PLAYER_STAT.EVENT_COUNTER.getName());
//        switch (eventCounter.intValue()) {
//          case 250 :
//            inventoryRepository.addInventoryItem(playerAccount.getId(), DinoparcConstants.POTION_SOMBRE, 1);
//            break;
//
//          case 500 :
//            inventoryRepository.addInventoryItem(playerAccount.getId(), DinoparcConstants.MAGIK_CHAMPIFUZ, 1);
//            break;
//
//          case 750 :
//            inventoryRepository.addInventoryItem(playerAccount.getId(), DinoparcConstants.OEUF_FEROSS, 1);
//            break;
//
//          case 1000 :
//            var playerCollection = collectionRepository.getPlayerCollection(UUID.fromString(playerAccount.getId()));
//            if (!playerCollection.getEpicCollection().contains("36")) {
//              playerCollection.getEpicCollection().add("36");
//              collectionRepository.updateEpicCollection(playerCollection.getId(), playerCollection.getEpicCollection());
//            }
//            break;
//
//          default :
//            break;
//        }
//      }
    }
  }

  public static String getRandomStealableObject(Integer dailyShiniesFought) {
    RandomCollection<Object> randomElement = new RandomCollection<Object>()
              .add(5, DinoparcConstants.GIFT)
              .add(5, DinoparcConstants.POTION_ANGE)
              .add(5, DinoparcConstants.NUAGE_BURGER)
              .add(5, DinoparcConstants.PAIN_CHAUD)
              .add(5, DinoparcConstants.TARTE_VIANDE)
              .add(5, DinoparcConstants.MEDAILLE_CHOCOLAT)
              .add(5, DinoparcConstants.CHARME_FEU)
              .add(5, DinoparcConstants.CHARME_TERRE)
              .add(5, DinoparcConstants.CHARME_EAU)
              .add(5, DinoparcConstants.CHARME_FOUDRE)
              .add(5, DinoparcConstants.CHARME_AIR)
              .add(5, DinoparcConstants.CHARME_PRISMATIK)
              .add(5, DinoparcConstants.CHAMPIFUZ)
              .add(5, DinoparcConstants.SPECIAL_CHAMPIFUZ)
              .add(5, DinoparcConstants.GRIFFES_EMPOISONNÉES)
              .add(5, DinoparcConstants.ANTIDOTE)
              .add(5, DinoparcConstants.MONO_CHAMPIFUZ)
              .add(5, DinoparcConstants.PRUNIAC)
              .add(5, DinoparcConstants.TISANE)
              .add(5, DinoparcConstants.BAVE_LOUPI)
              .add(5, DinoparcConstants.LAIT_DE_CARGOU)
              .add(5, DinoparcConstants.RAMENS)
              .add(3, DinoparcConstants.JETON_CASINO_BRONZE)
              .add(2, DinoparcConstants.JETON_CASINO_ARGENT)
              .add(1, DinoparcConstants.JETON_CASINO_OR)
              .add(5, DinoparcConstants.FOCUS_AGGRESIVITE)
              .add(5, DinoparcConstants.FOCUS_NATURE)
              .add(5, DinoparcConstants.FOCUS_SIRAINS)
              .add(5, DinoparcConstants.FOCUS_MAHAMUTI);

    return (String) randomElement.next();
  }

  public FightSummaryDto fightDinozInTournament(Dinoz homeDinoz, Dinoz foreignDinoz) {
    FightSummaryDto fightSummaryDto = fightDinoz(homeDinoz, foreignDinoz, true);
    String tournament = Tournament.getTournamentByPlaceNumber(homeDinoz.getPlaceNumber());

    if (fightSummaryDto.isHaveWon()) {
      homeDinoz.getTournaments().get(tournament).setActualPoints(homeDinoz.getTournaments().get(tournament).getActualPoints() + 1);
      homeDinoz.getTournaments().get(tournament).setCurrentWins(homeDinoz.getTournaments().get(tournament).getCurrentWins() + 1);
      homeDinoz.getTournaments().get(tournament).setTotalWins(homeDinoz.getTournaments().get(tournament).getTotalWins() + 1);

      if (homeDinoz.getTournaments().get(tournament).getActualPoints() > homeDinoz.getTournaments().get(tournament).getRecord()) {
        homeDinoz.getTournaments().get(tournament).setRecord(homeDinoz.getTournaments().get(tournament).getActualPoints());
      }

      checkForMedalsDistribution(homeDinoz, tournament);

    } else {
      if (homeDinoz.getTournaments().get(tournament).getActualPoints() > 0) {
        homeDinoz.getTournaments().get(tournament).setActualPoints(homeDinoz.getTournaments().get(tournament).getActualPoints() - 1);
      }
      homeDinoz.getTournaments().get(tournament).setCurrentLosses(homeDinoz.getTournaments().get(tournament).getCurrentLosses() + 1);
      homeDinoz.getTournaments().get(tournament).setTotalLosses(homeDinoz.getTournaments().get(tournament).getTotalLosses() + 1);
    }

    dinozRepository.setTournaments(homeDinoz.getId(), homeDinoz.getTournaments(), true);

    return fightSummaryDto;
  }

  private void checkForMedalsDistribution(Dinoz homeDinoz, String tournament) {
    var collection = collectionRepository.getPlayerCollection(UUID.fromString(homeDinoz.getMasterId()));
    var playerCollection = collection.getCollection();
    var collectionAdded = false;

    // Won Bronze Medal :
    if (homeDinoz.getTournaments().get(tournament).getActualPoints() >= 5) {
      switch (homeDinoz.getPlaceNumber()) {
        case 0:
          playerCollection.add("15");
          collectionAdded = true;
          break;
        case 3:
          playerCollection.add("3");
          collectionAdded = true;
          break;
        case 6:
          playerCollection.add("6");
          collectionAdded = true;
          break;
        case 10:
          playerCollection.add("9");
          collectionAdded = true;
          break;
        case 13:
          playerCollection.add("12");
          collectionAdded = true;
          break;
        default:
          break;
      }
    }

    // Won Silver Medal :
    if (homeDinoz.getTournaments().get(tournament).getActualPoints() >= 10) {
      switch (homeDinoz.getPlaceNumber()) {
        case 0:
          playerCollection.add("14");
          collectionAdded = true;
          break;
        case 3:
          playerCollection.add("2");
          collectionAdded = true;
          break;
        case 6:
          playerCollection.add("5");
          collectionAdded = true;
          break;
        case 10:
          playerCollection.add("8");
          collectionAdded = true;
          break;
        case 13:
          playerCollection.add("11");
          collectionAdded = true;
          break;
        default:
          break;
      }
    }

    // Won Gold Medal :
    if (homeDinoz.getTournaments().get(tournament).getActualPoints() >= 15) {
      switch (homeDinoz.getPlaceNumber()) {
        case 0:
          playerCollection.add("13");
          collectionAdded = true;
          break;
        case 3:
          playerCollection.add("1");
          collectionAdded = true;
          break;
        case 6:
          playerCollection.add("4");
          collectionAdded = true;
          break;
        case 10:
          playerCollection.add("7");
          collectionAdded = true;
          break;
        case 13:
          playerCollection.add("10");
          collectionAdded = true;
          break;
        default:
          break;
      }
    }

    if (collectionAdded) {
      collectionRepository.updateCollection(collection.getId(), playerCollection);
    }
  }

  private void concatForeignHistoryEventToDeque(Dinoz homeDinoz, Dinoz foreignDinoz, FightSummaryDto summary, Integer foreignExpWon, Integer foreignLifeLost) {
    History foreignHistoryEvent = new History();
    foreignHistoryEvent.setPlayerId(foreignDinoz.getMasterId());
    foreignHistoryEvent.setExpGained(foreignExpWon);
    foreignHistoryEvent.setLifeLost(foreignLifeLost);
    foreignHistoryEvent.setFromDinozName(homeDinoz.getName());
    foreignHistoryEvent.setFromUserName(this.getAccountByDinozId(homeDinoz.getId()).getName());
    foreignHistoryEvent.setHasWon(!summary.isHaveWon());
    foreignHistoryEvent.setToDinozName(summary.getEnnemyMasterDinozName());
    foreignHistoryEvent.setToUserName(summary.getEnnemyMasterName());
    foreignHistoryEvent.setIcon("hist_defense.gif");
    foreignHistoryEvent.setType("fight");
    historyRepository.save(foreignHistoryEvent);
  }

  public void concatHomeHistoryEventToDeque(Dinoz homeDinoz, FightSummaryDto summary, Boolean inTourney) {
    Player homeAccount = this.getAccountByDinozId(homeDinoz.getId());
    History homeHistoryEvent = new History();
    homeHistoryEvent.setPlayerId(homeDinoz.getMasterId());
    homeHistoryEvent.setExpGained(summary.getExperienceWon());
    homeHistoryEvent.setFromDinozName(homeDinoz.getName());
    homeHistoryEvent.setFromUserName(homeAccount.getName());
    homeHistoryEvent.setHasWon(summary.isHaveWon());
    homeHistoryEvent.setLifeLost(summary.getFinalLifeLoss());
    homeHistoryEvent.setToDinozName(summary.getEnnemyMasterDinozName());
    homeHistoryEvent.setToUserName(summary.getEnnemyMasterName());
    homeHistoryEvent.setBuyAmount(homeDinoz.getSkips());
    setIconFromFight(homeHistoryEvent, inTourney);
    homeHistoryEvent.setType("fight");
    historyRepository.save(homeHistoryEvent);
  }

  private void concatForeignHistoryPoisonEventToDeque(Dinoz homeDinoz, Dinoz foreignDinoz, Boolean isFromPiqure) {
    String poisonType = "regular";
    if (isFromPiqure) {
      poisonType = "piqure";
    }

    History foreignHistoryEvent = new History();
    foreignHistoryEvent.setPlayerId(foreignDinoz.getMasterId());
    foreignHistoryEvent.setFromDinozName(homeDinoz.getName());
    foreignHistoryEvent.setFromUserName(this.getAccountByDinozId(homeDinoz.getId()).getName());
    foreignHistoryEvent.setIcon("hist_poison.gif");
    foreignHistoryEvent.setType("poison_" + poisonType);
    historyRepository.save(foreignHistoryEvent);
  }

  private void setIconFromFight(History historyEvent, Boolean inTourney) {
    if (inTourney) {
      historyEvent.setIcon("hist_tournament.gif");

    } else {
      historyEvent.setIcon("hist_fight.gif");
    }
  }

  public String getCharmFromCode(String charmType, String lang) {
    String fullCharmName = "";

    switch (charmType) {
      case "bonus_air":
        switch (lang) {
          case "fr": fullCharmName = "Charme des Vents"; break;
          case "es": fullCharmName = "Hechizo de los Vientos"; break;
          case "en": fullCharmName = "Wind Spell"; break;
        }
        break;
      case "bonus_water":
        switch (lang) {
          case "fr": fullCharmName = "Charme Volvik"; break;
          case "es": fullCharmName = "Hechizo Volvik"; break;
          case "en": fullCharmName = "Volvik Spell"; break;
        }
        break;
      case "bonus_fire":
        switch (lang) {
          case "fr": fullCharmName = "Charme Incandescent"; break;
          case "es": fullCharmName = "Hechizo Incandescente"; break;
          case "en": fullCharmName = "Incandescent Spell"; break;
        }
        break;
      case "bonus_wood":
        switch (lang) {
          case "fr": fullCharmName = "Charme Luxuriant"; break;
          case "es": fullCharmName = "Hechizo Titán"; break;
          case "en": fullCharmName = "Titan Spell"; break;
        }
        break;
      case "bonus_thunder":
        switch (lang) {
          case "fr": fullCharmName = "Charme 220V"; break;
          case "es": fullCharmName = "Hechizo 220V"; break;
          case "en": fullCharmName = "220V Spell"; break;
        }
        break;
      case "bonus_prismatik":
        switch (lang) {
          case "fr": fullCharmName = "Charme Prismatik"; break;
          case "es": fullCharmName = "Hechizo Prismatik"; break;
          case "en": fullCharmName = "Prismatik Spell"; break;
        }
        break;
    }

    return fullCharmName;
  }

  private Integer calculateFinalInfos(FightSummaryDto summary, Dinoz homeDinoz, Dinoz foreignDinoz, int initialHomeDinozLife) {
    Player account = this.getAccountByDinozId(homeDinoz.getId());
    setWinnerOrLoser(summary, foreignDinoz);
    Integer foreignExpWon = calculateExperienceAndDangerWonByBothSides(summary, homeDinoz, foreignDinoz);

    //Moyenne de gain de base à 430 pièces :
    Integer moneyWon = ThreadLocalRandom.current().nextInt(300, 560 + 1);
    List statsToUpdate = new ArrayList();

    if (foreignDinoz.getName() != null
            && foreignDinoz.getMasterName() == null
            && foreignDinoz.getName().equalsIgnoreCase("BOT")
            && foreignDinoz.getAppearanceCode().endsWith("&")) {

      if (account.getDailyShiniesFought() == null) {
        account.setDailyShiniesFought(0);
      }

      Integer bonus = 0;
      if (account.getDailyShiniesFought() <= 30) {
        bonus = (20 * account.getDailyShiniesFought());
      } else if (account.getDailyShiniesFought() <= 70) {
        bonus = 600 + (5 * account.getDailyShiniesFought());
      }

      //Moyenne de gain shiny à 4900 pièces :
      moneyWon += (ThreadLocalRandom.current().nextInt(4600, 5200 + 1) + bonus);
      playerRepository.updateDailyShiniesFought(account.getId(), 1);
      statsToUpdate.add(new Pair(PgPlayerStatRepository.NB_SHINY, 1));
      statsToUpdate.add(new Pair(getPlayerStatDinozShinyField(foreignDinoz.getAppearanceCode()), 1));
      statsToUpdate.add(new Pair(getPlayerStatDinozFightField(foreignDinoz.getAppearanceCode()), 1));
      checkForGranitMissionProgression(homeDinoz, foreignDinoz, account, summary);

    } else {
      statsToUpdate.add(new Pair(getPlayerStatDinozFightField(foreignDinoz.getAppearanceCode()), 1));
    }

    moneyWon = applyLuckToMoneyWon(summary, homeDinoz, moneyWon);
    moneyWon = applyBeerEffectsToMoneyWon(summary, homeDinoz, moneyWon);
    summary.setMoneyWon(moneyWon);

    statsToUpdate.add(new Pair(PgPlayerStatRepository.SUM_GOLD_WON, moneyWon));
    playerStatRepository.addStats(UUID.fromString(homeDinoz.getMasterId()), homeDinoz.getId(), statsToUpdate);
    playerRepository.updateCash(account.getId(), summary.getMoneyWon());

    applyForceToFightEffects(summary, homeDinoz, foreignDinoz);
    applyHealToFightEffects(summary, homeDinoz, initialHomeDinozLife);
    applyMartialArtsToFightEffects(summary, homeDinoz, foreignDinoz);
    applyVirusAndPoisonTransmission(summary, homeDinoz, foreignDinoz);
    applySurvieToFightEffects(summary, homeDinoz);

    if (homeDinoz.getLife() < 1) {
      homeDinoz.setLife(0);
      summary.getSkillsEffectsFr().add("Affaibli par ses blessures, " + homeDinoz.getName() + " s'écroule à la fin du combat !");
      summary.getSkillsEffectsEs().add("Debilitado por sus heridas, " + homeDinoz.getName() + " se derrumba al final de la pelea!");
      summary.getSkillsEffectsEn().add("Weakened by his wounds, " + homeDinoz.getName() + " collapses at the end of the fight!");

      History homeHistoryEvent = new History();
      homeHistoryEvent.setPlayerId(homeDinoz.getMasterId());
      homeHistoryEvent.setIcon("hist_death.gif");
      homeHistoryEvent.setType("death");
      historyRepository.save(homeHistoryEvent);
    }

    if (foreignDinoz.getLife() < 1
            && !foreignDinoz.getName().equalsIgnoreCase("BOT")
            && foreignDinoz.getMasterName() != null
            && !foreignDinoz.getMasterName().equalsIgnoreCase("Hermit")) {

      foreignDinoz.setLife(0);
      summary.getSkillsEffectsFr().add("Votre adversaire est <b>K.O!</b>");
      summary.getSkillsEffectsEs().add("¡Tu oponente es <b>K.O!</b>");
      summary.getSkillsEffectsEn().add("Your opponent is <b>K.O!</b>");
      playerStatRepository.addStats(UUID.fromString(homeDinoz.getMasterId()), homeDinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_KO, 1)));

      History foreignHistoryEvent = new History();
      foreignHistoryEvent.setPlayerId(foreignDinoz.getMasterId());

      if (foreignDinoz.getRace().equalsIgnoreCase(DinoparcConstants.OUISTITI) && foreignDinoz.getLevel() < 20) {
        //Vol de ouistiti ici :
        Player foreignAccount = playerRepository.findById(foreignDinoz.getMasterId()).get();
        dinozRepository.setMasterInfos(foreignDinoz.getId(), account.getId(), account.getName());
        playerRepository.addDinoz(account.getId(), foreignDinoz.getId());
        playerRepository.deleteDinoz(foreignAccount.getId(), foreignDinoz.getId());
        foreignHistoryEvent.setIcon("hist_sneak.gif");
        foreignHistoryEvent.setType("vol-wistiti");

        EmbedCreateSpec embedDiscordMessage = EmbedCreateSpec
                .builder()
                .color(Color.BROWN)
                .thumbnail("https://i.ibb.co/Kcf5Rqdv/act-sneak.jpg")
                .title("Attention au danger... (" + foreignDinoz.getDanger() + ")")
                .description(foreignDinoz.getMasterName() + " vient de se faire voler " + foreignDinoz.getName() + " par " + homeDinoz.getMasterName())
                .footer("Généré par le serveur Neoparc", "https://i.ibb.co/bB74rVG/hist-reset.gif")
                .build();
        discordService.postEmbedMessage(DinoparcConstants.CHANNEL_CERBERE, embedDiscordMessage);

      } else {
        foreignHistoryEvent.setIcon("hist_death.gif");
        foreignHistoryEvent.setType("death");
      }

      if (dinozRepository.findById(foreignDinoz.getId()).isPresent()) {
        historyRepository.save(foreignHistoryEvent);
      } else if (!foreignDinoz.getId().contains(DinoparcConstants.PVE_FACTION)) {
        return -1;
      }

    } else if (foreignDinoz.getLife() < 1 && foreignDinoz.getName().contains("BOT")) {
      summary.getSkillsEffectsFr().add("Votre adversaire est <b>K.O!</b>");
      summary.getSkillsEffectsEs().add("¡Tu oponente es <b>K.O!</b>");
      summary.getSkillsEffectsEn().add("Your opponent is <b>K.O!</b>");
      playerStatRepository.addStats(UUID.fromString(homeDinoz.getMasterId()), homeDinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_KO, 1)));
    }

    if (summary.getLifeLostPreventedFirstRound() != 0 && summary.getRoundOneScore() < 0) {
      summary.getCharmsEffectsFr().add("Votre <b>" + getCharmFromCode(summary.getCharmTypeFirstRound(),"fr") + "</b> a été activé et vous a permis d'éviter la perte de <b>" + summary.getLifeLostPreventedFirstRound() + "%</b> de vie! (Round #1)");
      summary.getCharmsEffectsEs().add("Tu <b>" + getCharmFromCode(summary.getCharmTypeFirstRound(),"es") + "</b> se ha activado y eso te ha hecho evitar la pérdida de <b>" + summary.getLifeLostPreventedFirstRound() + "%</b> de vida! (Round #1)");
      summary.getCharmsEffectsEn().add("Your <b>" + getCharmFromCode(summary.getCharmTypeFirstRound(),"en") + "</b> has been activated and that prevented you to lost <b>" + summary.getLifeLostPreventedFirstRound() + "%</b> of life! (Round #1)");
    }

    if (summary.getLifeLostPreventedSecondRound() != 0 && summary.getRoundTwoScore() < 0) {
      summary.getCharmsEffectsFr().add("Votre <b>" + getCharmFromCode(summary.getCharmTypeSecondRound(),"fr") + "</b> a été activé et vous a permis d'éviter la perte de <b>" + summary.getLifeLostPreventedSecondRound() + "%</b> de vie! (Round #2)");
      summary.getCharmsEffectsEs().add("Tu <b>" + getCharmFromCode(summary.getCharmTypeSecondRound(),"es") + "</b> se ha activado y eso te ha hecho evitar la pérdida de <b>" + summary.getLifeLostPreventedSecondRound() + "%</b> de vida! (Round #2)");
      summary.getCharmsEffectsEn().add("Your <b>" + getCharmFromCode(summary.getCharmTypeSecondRound(),"en") + "</b> has been activated and that prevented you to lost <b>" + summary.getLifeLostPreventedSecondRound() + "%</b> of life! (Round #2)");
    }

    if (summary.getLifeLostPreventedThirdRound() != 0 && summary.getRoundThreeScore() < 0) {
      summary.getCharmsEffectsFr().add("Votre <b>" + getCharmFromCode(summary.getCharmTypeThirdRound(),"fr") + "</b> a été activé et vous a permis d'éviter la perte de <b>" + summary.getLifeLostPreventedThirdRound() + "%</b> de vie! (Round #3)");
      summary.getCharmsEffectsEs().add("Tu <b>" + getCharmFromCode(summary.getCharmTypeThirdRound(),"es") + "</b> se ha activado y eso te ha hecho evitar la pérdida de <b>" + summary.getLifeLostPreventedThirdRound() + "%</b> de vida! (Round #3)");
      summary.getCharmsEffectsEn().add("Your <b>" + getCharmFromCode(summary.getCharmTypeThirdRound(),"en") + "</b> has been activated and that prevented you to lost <b>" + summary.getLifeLostPreventedThirdRound() + "%</b> of life! (Round #3)");
    }

    return foreignExpWon;
  }

  private Integer calculateExperienceAndDangerWonByBothSides(FightSummaryDto summary, Dinoz homeDinoz, Dinoz foreignDinoz) {
    summary.setExperienceWon(getExperienceFromFight(isDinozMaxed(homeDinoz, !AccountService.getAvailableLearnings(homeDinoz).isEmpty()), homeDinoz.getSkillsMap().get(DinoparcConstants.INTELLIGENCE), homeDinoz.getLevel(), foreignDinoz.getLevel()));
    if (homeDinoz.getExperience() + summary.getExperienceWon() >= 100) {
      homeDinoz.setExperience(100);
      homeDinoz.getActionsMap().put(DinoparcConstants.LEVELUP, true);
      summary.getSkillsEffectsFr().add("<b>Level Up</b> -> " + homeDinoz.getName() + " a accumulé assez d'expérience pour gagner un niveau supplémentaire!");
      summary.getSkillsEffectsEs().add("<b>Level Up</b> -> " + homeDinoz.getName() + " ¡ha acumulado suficiente experiencia para ganar un nivel adicional!");
      summary.getSkillsEffectsEn().add("<b>Level Up</b> -> " + homeDinoz.getName() + " has accumulated enough experience to gain an additional level!");

    } else if (summary.getExperienceWon() > 0) {
      homeDinoz.setExperience(homeDinoz.getExperience() + summary.getExperienceWon());
    } else {
      homeDinoz.setExperience(0);
    }

    Integer experienceWonByForeign = 0;
    if (foreignDinoz.getSkillsMap().get(DinoparcConstants.CONTRE_ATTAQUE) != null && foreignDinoz.getSkillsMap().get(DinoparcConstants.CONTRE_ATTAQUE) > 0) {
      experienceWonByForeign = ThreadLocalRandom.current().nextInt(2, 5 + 1);
    } else {
      experienceWonByForeign = ThreadLocalRandom.current().nextInt(1, 3 + 1);
    }

    if (foreignDinoz.getExperience() + experienceWonByForeign >= 100) {
      foreignDinoz.setExperience(100);
      foreignDinoz.getActionsMap().put(DinoparcConstants.LEVELUP, true);

    } else {
      foreignDinoz.setExperience(foreignDinoz.getExperience() + experienceWonByForeign);
    }

    setDangerWonByHomeDinoz(summary, homeDinoz);
    Integer dangerLossByForeign = ThreadLocalRandom.current().nextInt(5, 12 + 1);

    if (foreignDinoz.getDanger() - dangerLossByForeign < 1) {
      foreignDinoz.setDanger(0);
    } else {
      foreignDinoz.setDanger(foreignDinoz.getDanger() - dangerLossByForeign);
    }

    return experienceWonByForeign;
  }

  public void setDangerWonByHomeDinoz(FightSummaryDto summary, Dinoz homeDinoz) {
    Integer dangerAccumulated = ThreadLocalRandom.current().nextInt(75, 125 + 1);
    Integer camouflageBuff = 0;
    if (homeDinoz.getSkillsMap().get(DinoparcConstants.CAMOUFLAGE) != null && homeDinoz.getSkillsMap().get(DinoparcConstants.CAMOUFLAGE) > 0) {
      Integer camouflageLevel = homeDinoz.getSkillsMap().get(DinoparcConstants.CAMOUFLAGE);
      camouflageBuff = ThreadLocalRandom.current().nextInt(camouflageLevel, (camouflageLevel * 5) + 1);
      summary.getSkillsEffectsFr().add("Votre compétence <b>Camouflage</b> vous a permi de diminuer votre gain de danger de " + camouflageBuff + " points.");
      summary.getSkillsEffectsEs().add("Tu habilidad de <b>Camuflaje</b> te permitió disminuir tu ganancia de peligro en " + camouflageBuff + " puntos.");
      summary.getSkillsEffectsEn().add("Your <b>Camouflage</b> skill allowed you to decrease your danger gain by " + camouflageBuff + " points.");
    }

    homeDinoz.setDanger(homeDinoz.getDanger() + (dangerAccumulated - camouflageBuff));
    summary.setDangerAccumulated(dangerAccumulated);
    summary.setCamouflageBuff(camouflageBuff);
  }

  private void applyVirusAndPoisonTransmission(FightSummaryDto summary, Dinoz homeDinoz, Dinoz foreignDinoz) {
    if (foreignDinoz.getPassiveList().get(DinoparcConstants.GRIFFES_EMPOISONNÉES) != null) {
      if (foreignDinoz.getPassiveList().get(DinoparcConstants.GRIFFES_EMPOISONNÉES) > 0 && !homeDinoz.getMalusList().contains(DinoparcConstants.EMPOISONNE)) {
        homeDinoz.getMalusList().add(DinoparcConstants.EMPOISONNE);
        homeDinoz.setLife(homeDinoz.getLife() - 5);
        foreignDinoz.getPassiveList().put(DinoparcConstants.GRIFFES_EMPOISONNÉES, foreignDinoz.getPassiveList().get(DinoparcConstants.GRIFFES_EMPOISONNÉES) - 1);
        summary.getSkillsEffectsFr().add("Vous avez été <b>empoisonné</b> par le Dinoz adverse avec ses griffes spécialement préparées! Vous perdez <b>5%</b> de vie supplémentaire...");
        summary.getSkillsEffectsEs().add("¡Has sido <b>envenenado</b> por el oponente Dinoz con sus garras especialmente preparadas! Pierdes un <b>5%</b> más de vida ...");
        summary.getSkillsEffectsEn().add("You have been <b>poisoned</b> by the opposing Dinoz with its specially prepared claws! You lose <b>5%</b> more percent of life ...");
        summary.setFinalLifeLoss(summary.getFinalLifeLoss() + 5);
      }
    }

    if (homeDinoz.getPassiveList().get(DinoparcConstants.GRIFFES_EMPOISONNÉES) != null) {
      if (homeDinoz.getPassiveList().get(DinoparcConstants.GRIFFES_EMPOISONNÉES) > 0) {
        playerStatRepository.addStats(UUID.fromString(homeDinoz.getMasterId()), homeDinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_POISON_INFECTION, 1)));
        foreignDinoz.getMalusList().add(DinoparcConstants.EMPOISONNE);
        foreignDinoz.setLife(foreignDinoz.getLife() - 5);
        homeDinoz.getPassiveList().put(DinoparcConstants.GRIFFES_EMPOISONNÉES, homeDinoz.getPassiveList().get(DinoparcConstants.GRIFFES_EMPOISONNÉES) - 1);
        summary.getSkillsEffectsFr().add("<b>Vous avez empoisonné</b> le Dinoz adverse avec vos griffes spécialement préparées!");
        summary.getSkillsEffectsEs().add("¡<b>Has envenenado</b> al rival Dinoz con tus garras especialmente preparadas!");
        summary.getSkillsEffectsEn().add("<b>You have poisoned</b> the opposing Dinoz with your specially prepared claws!");

        if (Objects.nonNull(foreignDinoz.getId())
                && !foreignDinoz.getId().startsWith("@")
                && !foreignDinoz.getMasterId().equals("admin_Pve_Faction")) {
          concatForeignHistoryPoisonEventToDeque(homeDinoz, foreignDinoz, false);
        }
      }
    }

    if (homeDinoz.getSkillsMap().containsKey(DinoparcConstants.PIQURE)) {
      Integer piqureLevel = homeDinoz.getSkillsMap().get(DinoparcConstants.PIQURE);
      if (piqureLevel != null && piqureLevel >= 1) {
          if (ThreadLocalRandom.current().nextInt(1, 100 + 1) <= piqureLevel) {
            playerStatRepository.addStats(UUID.fromString(homeDinoz.getMasterId()), homeDinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_POISON_INFECTION, 1)));
            foreignDinoz.getMalusList().add(DinoparcConstants.EMPOISONNE);
            foreignDinoz.setLife(foreignDinoz.getLife() - 5);
            summary.getSkillsEffectsFr().add(homeDinoz.getName() + " a réussi à infliger une <b>piqûre</b> à son adversaire, le laissant ainsi empoisonné!");
            summary.getSkillsEffectsEs().add(homeDinoz.getName() + " logró infligir un <b>aguijón</b> a su oponente, dejándolo envenenado!");
            summary.getSkillsEffectsEn().add(homeDinoz.getName() + " managed to inflict a <b>sting</b> on his opponent, leaving him poisoned!");

            if (Objects.nonNull(foreignDinoz.getId()) && !foreignDinoz.getMasterId().equals("admin_Pve_Faction")) {
              concatForeignHistoryPoisonEventToDeque(homeDinoz, foreignDinoz, true);
            }
          }
      }
    }

    if (homeDinoz.getMalusList().contains(DinoparcConstants.CHIKABUM)) {
      Integer chancesOfInfection = ThreadLocalRandom.current().nextInt(0, 100 + 1);
      if (chancesOfInfection <= 15) {
        if (foreignDinoz.getPassiveList().get(DinoparcConstants.IMMUNITÉ) != null && foreignDinoz.getPassiveList().get(DinoparcConstants.IMMUNITÉ) > 0) {
          foreignDinoz.getPassiveList().put(DinoparcConstants.IMMUNITÉ, 0);
          summary.getSkillsEffectsFr().add("Votre tentative d'infection Chikabum a échouée. Le Dinoz adverse avait l'immunité de la Source Chantante.");
          summary.getSkillsEffectsEs().add("Tu intento de infectar a Chikabum ha fallado. El oponente Dinoz tenía inmunidad a Singing Spring.");
          summary.getSkillsEffectsEn().add("Your attempt to infect Chikabum has failed. The opposing Dinoz had Singing Spring immunity.");

        } else if (foreignDinoz.getPassiveList().get(DinoparcConstants.ANTIDOTE) != null &&
                foreignDinoz.getPassiveList().containsKey(DinoparcConstants.ANTIDOTE)
                && foreignDinoz.getPassiveList().get(DinoparcConstants.ANTIDOTE) >= 1) {

          foreignDinoz.getPassiveList().replace(DinoparcConstants.ANTIDOTE, foreignDinoz.getPassiveList().get(DinoparcConstants.ANTIDOTE) - 1);

        } else if (foreignDinoz.getMalusList().contains(DinoparcConstants.CHIKABUM)) {
          playerStatRepository.addStats(UUID.fromString(homeDinoz.getMasterId()), homeDinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_CHIKABUM_INFECTION, 1)));
          summary.getSkillsEffectsFr().add("Vous avez infecté le Dinoz adverse avec le virus Chikabum!");
          summary.getSkillsEffectsEs().add("¡Has infectado al oponente Dinoz con el virus Chikabum!");
          summary.getSkillsEffectsEn().add("You infected the opposing Dinoz with the Chikabum virus!");

        } else {
          playerStatRepository.addStats(UUID.fromString(homeDinoz.getMasterId()), homeDinoz.getId(), List.of(new Pair(PgPlayerStatRepository.NB_CHIKABUM_INFECTION, 1)));
          foreignDinoz.getMalusList().add(DinoparcConstants.CHIKABUM);
          summary.getSkillsEffectsFr().add("Vous avez infecté le Dinoz adverse avec le virus Chikabum!");
          summary.getSkillsEffectsEs().add("¡Has infectado al oponente Dinoz con el virus Chikabum!");
          summary.getSkillsEffectsEn().add("You infected the opposing Dinoz with the Chikabum virus!");
        }

      } else {
        summary.getSkillsEffectsFr().add("Votre tentative d'infection Chikabum a échouée. Le Dinoz adverse a été chanceux...");
        summary.getSkillsEffectsEs().add("Tu intento de infectar a Chikabum ha fallado. El oponente Dinoz tuvo suerte ...");
        summary.getSkillsEffectsEn().add("Your attempt to infect Chikabum has failed. The opposing Dinoz was lucky ...");
      }
    }
  }

  public void applySurvieToFightEffects(FightSummaryDto summary, Dinoz homeDinoz) {
    if (homeDinoz.getSkillsMap().get(DinoparcConstants.SURVIE) != null
            && homeDinoz.getSkillsMap().get(DinoparcConstants.SURVIE) > 0
            && homeDinoz.getLife() <= 0) {

      Integer randomSurvie = ThreadLocalRandom.current().nextInt(1, 100 + 1);

      switch (homeDinoz.getSkillsMap().get(DinoparcConstants.SURVIE)) {
        case 1:
          if (randomSurvie <= 10) {
            homeDinoz.setLife(1);
            dinozHasSurvived(summary);

          } else {
            dinozHasNotSurvived(summary);
          }
          break;

        case 2:
          if (randomSurvie <= 20) {
            homeDinoz.setLife(1);
            dinozHasSurvived(summary);

          } else {
            dinozHasNotSurvived(summary);
          }
          break;

        case 3:
          if (randomSurvie <= 30) {
            homeDinoz.setLife(1);
            dinozHasSurvived(summary);

          } else {
            dinozHasNotSurvived(summary);
          }
          break;

        case 4:
          if (randomSurvie <= 40) {
            homeDinoz.setLife(1);
            dinozHasSurvived(summary);

          } else {
            dinozHasNotSurvived(summary);
          }
          break;

        case 5:
          if (randomSurvie <= 50) {
            homeDinoz.setLife(1);
            dinozHasSurvived(summary);

          } else {
            dinozHasNotSurvived(summary);
          }
          break;

        default:
          break;
      }
    }
  }

  private void dinozHasSurvived(FightSummaryDto summary) {
    summary.getSkillsEffectsFr().add("Vous étiez K.O, mais votre compétence <b>Survie</b> a été en mesure de garder votre Dinoz en vie! (1%)");
    summary.getSkillsEffectsEs().add("¡Tu habilidad de <b>Supervivencia</b> ha podido mantener vivo a tu Dinoz! (1%)");
    summary.getSkillsEffectsEn().add("You were knocked out but your <b>Survival</b> skill has been able to keep your Dinoz alive! (1%)");
  }

  private void dinozHasNotSurvived(FightSummaryDto summary) {
    summary.getSkillsEffectsFr().add("Votre compétence survie n'a pas été en mesure de vous sauver des dégats infligés par votre adversaire...");
    summary.getSkillsEffectsEs().add("Tu habilidad de supervivencia no pudo salvarte del daño infligido por tu oponente...");
    summary.getSkillsEffectsEn().add("Your survival skill was not able to save you from the damage inflicted by your opponent...");
  }

  public Integer applyLuckToMoneyWon(FightSummaryDto summary, Dinoz homeDinoz, Integer moneyWon) {
    if (homeDinoz.getSkillsMap().get(DinoparcConstants.CHANCE) != null && homeDinoz.getSkillsMap().get(DinoparcConstants.CHANCE) > 0) {
      if (ThreadLocalRandom.current().nextInt(1, 50 + 1) <= homeDinoz.getSkillsMap().get(DinoparcConstants.CHANCE)) {
        summary.getSkillsEffectsFr().add("La <b>Chance</b> vous sourit! Vous avez réussi à <b>doubler</b> le nombre de pièces d'or de ce combat! (" + moneyWon + " x 2)");
        summary.getSkillsEffectsEs().add("¡La <b>Suerte</b> te sonríe! ¡Lograste <b>duplicar</b> la cantidad de monedas de oro en esta pelea! (" + moneyWon + " x 2)");
        summary.getSkillsEffectsEn().add("You are a <b>lucky</b> Dinomaster! You managed to <b>double</b> the number of gold coins earned in this fight! (" + moneyWon + " x 2)");
        moneyWon = moneyWon * 2;
      }
    }
    return moneyWon;
  }

  public Integer applyBeerEffectsToMoneyWon(FightSummaryDto summary, Dinoz homeDinoz, Integer moneyWon) {
    if (homeDinoz.getMalusList() != null && homeDinoz.getMalusList().contains(DinoparcConstants.BIERE)) {
      Integer originalMoneyWonValue = moneyWon;
      float moneyBuffBeerPct = ThreadLocalRandom.current().nextFloat(1f, 3f);
      moneyWon = Math.round(((float) moneyWon) * (moneyBuffBeerPct));
      Integer difference = moneyWon - originalMoneyWonValue;

      summary.getSkillsEffectsFr().add("La bière de Dinojak vous a permis de gagner " + difference + " pièces d'or de plus durant ce combat.");
      summary.getSkillsEffectsEs().add("La cerveza de Dinojak te permitió ganar " + difference + " más de oro durante esta pelea.");
      summary.getSkillsEffectsEn().add("Dinojak's beer allowed you to gain " + difference + " more gold coins during this fight.");

      Integer durability = 20;
      if (homeDinoz.getSkillsMap().containsKey(DinoparcConstants.INGENIEUR) && homeDinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR) > 0) {
        durability -= homeDinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR);
      }

      if (ThreadLocalRandom.current().nextInt(1, 100 + 1) <= durability) {
        homeDinoz.getMalusList().remove(DinoparcConstants.BIERE);
        summary.getSkillsEffectsFr().add("À la fin de ce combat, les effets de la bière de Dinojak sont partis...");
        summary.getSkillsEffectsEs().add("Al final de esta pelea, los efectos de la cerveza de Dinojak se han ido...");
        summary.getSkillsEffectsEn().add("At the end of this fight, the effects of Dinojak's beer are gone...");
      }
    }
    return moneyWon;
  }

  private void setWinnerOrLoser(FightSummaryDto summary, Dinoz foreignDinoz) {
    if (foreignDinoz.getLevel() != 0 && foreignDinoz.getId() != null && !foreignDinoz.getId().contains(DinoparcConstants.PVE_FACTION)) {
      summary.setEnnemyMasterDinozName(foreignDinoz.getName());
      if (foreignDinoz.getId() == null) {
        summary.setEnnemyMasterName(DinoparcConstants.HERMIT);
      } else if ("@MANNY".equalsIgnoreCase(foreignDinoz.getId())) {
        summary.setEnnemyMasterDinozName("Manny");
        summary.setEnnemyMasterName("Gardien de la Porte");
      } else {
        summary.setEnnemyMasterName(getAccountByDinozId(foreignDinoz.getId()).getName());
      }
    } else if (foreignDinoz.getId() == null) {
      summary.setEnnemyMasterDinozName("Bot");
      summary.setEnnemyMasterName("admin");

    } else if (foreignDinoz.getId().contains(DinoparcConstants.PVE_FACTION)) {
      summary.setEnnemyMasterDinozName(foreignDinoz.getName());
      summary.setEnnemyMasterName("???");

    } else if (foreignDinozIsAWildKabuki(foreignDinoz) || foreignDinozIsManny(foreignDinoz)) {
      summary.setEnnemyMasterDinozName(foreignDinoz.getName());
      summary.setEnnemyMasterName("admin");
    }

    Integer roundSum = summary.getRoundOneScore() + summary.getRoundTwoScore() + summary.getRoundThreeScore();
    if (roundSum > 0) {
      summary.setHaveWon(true);
    } else {
      summary.setHaveWon(false);
    }
  }

  private void applyMartialArtsToFightEffects(FightSummaryDto summary, Dinoz homeDinoz, Dinoz foreignDinoz) {
    if (foreignDinoz.getSkillsMap().get(DinoparcConstants.ARTS_MARTIAUX) != null) {
      Integer homeDinozMartialArts = 0;

      if (homeDinoz.getSkillsMap().get(DinoparcConstants.ARTS_MARTIAUX) != null) {
        homeDinozMartialArts = homeDinoz.getSkillsMap().get(DinoparcConstants.ARTS_MARTIAUX);
      }

      Integer martialArtsDifference = foreignDinoz.getSkillsMap().get(DinoparcConstants.ARTS_MARTIAUX) - homeDinozMartialArts;

      if (martialArtsDifference < 0) {
        martialArtsDifference = 0;
      }

      Integer initialLifeToLose = (int) Math.ceil(foreignDinoz.getSkillsMap().get(DinoparcConstants.ARTS_MARTIAUX) / 2.0);
      Integer lifeLost = (int) Math.ceil(martialArtsDifference / 2.0);

      if (lifeLost > 0) {
        homeDinoz.setLife(homeDinoz.getLife() - lifeLost);
      }

      String savedLifeTextFr = "";
      String savedLifeTextEn = "";
      String savedLifeTextEs = "";

      if (lifeLost != initialLifeToLose) {
        savedLifeTextFr = " Toutefois, votre compétence en <b>Arts Martiaux</b> vous a évité de perdre <b>" + (initialLifeToLose - lifeLost) + " %</b> de vie!";
        savedLifeTextEn = " However, your <b>Martial Arts</b> skill avoided a loss of  <b>" + (initialLifeToLose - lifeLost) + " %</b> of life";
        savedLifeTextEs = " Pero tu dominio de las <b>Artes Marciales</b> te evitó perder <b>" + (initialLifeToLose - lifeLost) + " %</b> de vida";
      }

      summary.getSkillsEffectsFr().add("Votre adversaire maitrisait les <b>Arts Martiaux!</b> Il vous a fait perdre <b>" + initialLifeToLose+ "%</b> de vie supplémentaire pendant le combat!" + savedLifeTextFr);
      summary.getSkillsEffectsEs().add("¡Tu oponente domina las <b>Artes Marciales!</b> Te hizo perder <b>" + initialLifeToLose + "%</b> mas de vida." + savedLifeTextEs);
      summary.getSkillsEffectsEn().add("Your opponent mastered the <b>Martial Arts!</b> He made you lose <b>" + initialLifeToLose+ "</b> more % of life during the fight." + savedLifeTextEn);

    }
  }

  public void applyHealToFightEffects(FightSummaryDto summary, Dinoz homeDinoz, int initialHomeDinozLife) {
    Integer enduranceLevel = 0;
    if (homeDinoz.getSkillsMap().get(DinoparcConstants.ENDURANCE) != null) {
      enduranceLevel = homeDinoz.getSkillsMap().get(DinoparcConstants.ENDURANCE);
    }

    Integer medecineLevel = 0;
    if (homeDinoz.getSkillsMap().get(DinoparcConstants.MEDECINE) != null) {
      medecineLevel = homeDinoz.getSkillsMap().get(DinoparcConstants.MEDECINE);
    }

    Integer tauntLevel = 0;
    if (homeDinoz.getSkillsMap().get(DinoparcConstants.TAUNT) != null) {
      tauntLevel = homeDinoz.getSkillsMap().get(DinoparcConstants.TAUNT);
    }

    Integer enduranceHeal = 0;
    Integer medecineHeal = 0;

    switch (enduranceLevel) {
      case 1:
      case 2:
      case 3:
      case 4:
        enduranceHeal = 1;
        break;
      case 5:
        enduranceHeal = 2;
        break;
      default:
        break;
    }

    switch (medecineLevel) {
      case 1:
      case 2:
        medecineHeal = 1;
        break;
      case 3:
      case 4:
        medecineHeal = 2;
        break;
      case 5:
        medecineHeal = 3;
        break;
      default:
        break;
    }

    if (summary.getFinalLifeLoss() > 0) {
      Integer sumHeal = enduranceHeal + medecineHeal;
      Integer resultLife = Math.min(initialHomeDinozLife - summary.getFinalLifeLoss() + sumHeal, (medecineHeal > 0) ? Math.min(initialHomeDinozLife + 1, (100 + (tauntLevel * 10))) : initialHomeDinozLife);
      Integer realLifeLoss = initialHomeDinozLife - resultLife;
      Integer ptsHeal = summary.getFinalLifeLoss() - realLifeLoss;
      Integer realEnduranceSave = 0;
      Integer realMedecinSave = 0;

      if (realLifeLoss == -1) {
        realMedecinSave += 1;
        ptsHeal -= 1;
      }

      realEnduranceSave += Math.min(ptsHeal, enduranceHeal);
      realMedecinSave += ptsHeal - realEnduranceSave;
      homeDinoz.setLife(Math.max(resultLife, 0));

      if (realEnduranceSave > 0) {
        summary.getSkillsEffectsFr().add("Votre <b>Endurance</b> vous a permis de prévenir la perte de <b>"+ realEnduranceSave + "%</b> de vie durant le combat!");
        summary.getSkillsEffectsEs().add("Su <b>Resistencia</b> le ha permitido evitar la pérdida de <b>" + realEnduranceSave + "%</b> de vida.");
        summary.getSkillsEffectsEn().add("Your <b>Stamina</b> has enabled you to prevent the loss of <b>"+ realEnduranceSave + "%</b> of life during the fight.");
      }

      if (realMedecinSave > 0) {
        summary.getSkillsEffectsFr().add("Votre <b>Médecine</b> vous a permis de prévenir la perte de <b>"+ realMedecinSave + "%</b> de vie durant le combat!");
        summary.getSkillsEffectsEs().add("Su <b>Medicina</b> le ha permitido evitar la pérdida de <b>" + realMedecinSave + "%</b> de vida.");
        summary.getSkillsEffectsEn().add("Your <b>Medicine</b> has enabled you to prevent the loss of <b>"+ realMedecinSave + "%</b> of life during the fight.");
      }
    }
  }

  private void applyForceToFightEffects(FightSummaryDto summary, Dinoz homeDinoz, Dinoz foreignDinoz) {
    if (homeDinoz.getSkillsMap().get(DinoparcConstants.FORCE) != null) {
      Integer forcePoints = ThreadLocalRandom.current().nextInt(1, homeDinoz.getSkillsMap().get(DinoparcConstants.FORCE) + 3);

      if (foreignDinoz.getSkillsMap().get(DinoparcConstants.RESILIENCE) != null && foreignDinoz.getSkillsMap().get(DinoparcConstants.RESILIENCE) >= 1) {
        Integer resilienceLevel = foreignDinoz.getSkillsMap().get(DinoparcConstants.RESILIENCE);
        if (ThreadLocalRandom.current().nextInt(1, 10 + 1) <= (resilienceLevel * 2)) {
          summary.getSkillsEffectsFr().add("La <b>Résilience</b> de votre adversaire a été suffisante pour <b>annuler</b> les dégâts directs que vous tentiez de lui infliger avec votre <b>Force</b>!");
          summary.getSkillsEffectsEs().add("¡La <b>Resiliencia</b> de tu oponente fue suficiente para negar el daño directo que estabas tratando de infligir con tu <b>Fuerza</b>!");
          summary.getSkillsEffectsEn().add("Your opponent's <b>Resilience</b> was enough to negate the direct damage you were trying to inflict on them with your <b>Strength</b>!");

        } else {
          summary.getSkillsEffectsFr().add("La <b>Résilience</b> de votre adversaire n'a pas été en mesure de bloquer vos dégâts infligés par <b>Force</b>!");
          summary.getSkillsEffectsEs().add("¡La <b>Resiliencia</b> de tu oponente no pudo bloquear tu daño de <b>Fuerza</b>!");
          summary.getSkillsEffectsEn().add("Your opponent's <b>Resilience</b> was unable to block your <b>Strength</b> damage!");
          foreignDinoz.setLife(foreignDinoz.getLife() - forcePoints);
          summary.getSkillsEffectsFr().add("Votre <b>Force</b> vous a permis de faire perdre <b>" + forcePoints + "%</b> de vie supplémentaire à votre adversaire!");
          summary.getSkillsEffectsEs().add("Tu <b>Fuerza</b> te ha permitido perder un <b>" + forcePoints + "%</b> más de vida de tu oponente!");
          summary.getSkillsEffectsEn().add("Your <b>Strength</b> made your opponent lose <b>" + forcePoints + "</b> more percent of life!");
        }

      } else {
        foreignDinoz.setLife(foreignDinoz.getLife() - forcePoints);
        summary.getSkillsEffectsFr().add("Votre <b>Force</b> vous a permis de faire perdre <b>" + forcePoints + "%</b> de vie supplémentaire à votre adversaire!");
        summary.getSkillsEffectsEs().add("Tu <b>Fuerza</b> te ha permitido perder un <b>" + forcePoints + "%</b> más de vida de tu oponente!");
        summary.getSkillsEffectsEn().add("Your <b>Strength</b> made your opponent lose <b>" + forcePoints + "</b> more percent of life!");
      }
    }
  }

  public Player getAccountByDinozId(String dinozId) {
    return playerRepository.findPlayerByDinozId(dinozId);
  }

  public Player getAccountById(String accountId) {
    return playerRepository.findById(accountId).get();
  }

  public void computeFinalScoreStepByStep(FightSummaryDto summary, Integer firstRoundResult) {
    if (firstRoundResult > 0) {
      summary.setScoreFinalHome(summary.getScoreFinalHome() + firstRoundResult);
    } else {
      summary.setScoreFinalEnnemy(summary.getScoreFinalEnnemy() - firstRoundResult);
    }
  }

  private void calculateFirstRoundEffects(FightSummaryDto summary, Dinoz homeDinoz, Dinoz foreignDinoz, FightElements fightElements) {
    Integer roundFactor = getRoundFactorByElements(fightElements.getHighestLeft(), fightElements.getHighestRight());

    boolean homeDinozHasTheCharm = false;
    if (homeDinoz.getPassiveList().get(getCharmNameFromElement(fightElements.getHighestRight())) != null
            && homeDinoz.getPassiveList().get(getCharmNameFromElement(fightElements.getHighestRight())) > 0) {
      homeDinozHasTheCharm = true;
    }

    boolean homeDinozHasThePrismatik = false;
    if (homeDinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) != null
            && homeDinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) > 0) {
      homeDinozHasThePrismatik = true;
    }

    boolean foreignDinozHasTheCharm = false;
    if (foreignDinoz.getPassiveList().get(getCharmNameFromElement(fightElements.getHighestLeft())) != null
            && foreignDinoz.getPassiveList().get(getCharmNameFromElement(fightElements.getHighestLeft())) > 0) {
      foreignDinozHasTheCharm = true;
    }

    Integer firstRoundResult = 0;
    if (roundFactor < 0) {
      firstRoundResult = (roundFactor * foreignDinoz.getElementsValues().get(fightElements.getHighestRight())) + homeDinoz.getElementsValues().get(fightElements.getHighestLeft());
    } else if (roundFactor > 0) {
      firstRoundResult = (roundFactor * homeDinoz.getElementsValues().get(fightElements.getHighestLeft())) - (foreignDinoz.getElementsValues().get(fightElements.getHighestRight()));
    }

    Integer lifeLossByLoser = firstRoundResult / 5;
    Integer lifeLostPreventedLoser = 0;
    String charmUsed = "";

    // Life loss by the foreign dinoz :
    if (lifeLossByLoser > 0 && foreignDinozHasTheCharm) {
      String charmName = getCharmNameFromElement(fightElements.getHighestLeft());
      foreignDinoz.getPassiveList().put(charmName, foreignDinoz.getPassiveList().get(charmName) - 1);
      summary.setCharmFirstRoundForLoser(1);
      lifeLostPreventedLoser = lifeLossByLoser * -1;
      charmUsed = charmName;
      lifeLossByLoser = 0;
    }

    // Life loss by the home dinoz :
    if ((lifeLossByLoser < 0 && homeDinozHasTheCharm) || (lifeLossByLoser < 0 && homeDinozHasThePrismatik)) {
      String charmName = "";
      if (homeDinozHasThePrismatik) {
        charmName = DinoparcConstants.CHARME_PRISMATIK;

      } else {
        charmName = getCharmNameFromElement(fightElements.getHighestRight());
        homeDinoz.getPassiveList().put(charmName, homeDinoz.getPassiveList().get(charmName) - 1);
      }

      summary.setCharmFirstRoundForLoser(1);
      lifeLostPreventedLoser = lifeLossByLoser * -1;
      charmUsed = charmName;
      lifeLossByLoser = 0;
    }

    // Life loss by the foreign dinoz (no charms) :
    if (lifeLossByLoser > 0 && !foreignDinozHasTheCharm) {
      if (homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) != null && homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) >= 1) {
        Integer critiqueLevel = homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE);
        if (ThreadLocalRandom.current().nextInt(1, 100 + 1) <= critiqueLevel) {
          lifeLossByLoser = (lifeLossByLoser * 2);
          summary.getSkillsEffectsFr().add("Vous avez infligé un <b>Coup Critique</b> à votre adversaire, <b>doublant</b> ainsi les dégâts générés au Round #1!");
          summary.getSkillsEffectsEs().add("¡Has golpeado <b>Críticamente</b> a tu oponente, <b>duplicando</b> el daño generado desde la Ronda #1!");
          summary.getSkillsEffectsEn().add("You have <b>Critically Hit</b> your opponent, <b>doubling</b> the damage generated on Round #1!");
        }
      }

      foreignDinoz.setLife(foreignDinoz.getLife() - lifeLossByLoser);
    }

    // Life loss by the home dinoz (no charms) :
    if (lifeLossByLoser < 0 && !homeDinozHasTheCharm) {
      homeDinoz.setLife(homeDinoz.getLife() + lifeLossByLoser);
    }

    summary.setFirstElementLeft(getElementNumberFromElementValue(fightElements.getHighestLeft()));
    summary.setFirstElementRight(getElementNumberFromElementValue(fightElements.getHighestRight()));
    summary.setRoundOneScore(firstRoundResult);
    summary.setLifeLossFirstRoundByLoser(Math.abs(lifeLossByLoser));
    summary.setLifeLostPreventedFirstRound(lifeLostPreventedLoser);
    summary.setCharmTypeFirstRound(charmUsed);

    if (lifeLossByLoser < 0) {
      summary.setFinalLifeLoss(summary.getFinalLifeLoss() + Math.abs(lifeLossByLoser));
    }

    computeFinalScoreStepByStep(summary, firstRoundResult);
  }

  private void calculateSecondRoundEffects(FightSummaryDto summary, Dinoz homeDinoz, Dinoz foreignDinoz, FightElements fightElements) {
    Integer roundFactor = getRoundFactorByElements(fightElements.getSecondHighestLeft(), fightElements.getSecondHighestRight());

    Boolean homeDinozIsAlive;
    if (homeDinoz.getLife() > 0) {
      homeDinozIsAlive = true;
    } else {
      homeDinozIsAlive = false;
    }

    Boolean foreignDinozIsAlive;
    if (foreignDinoz.getLife() > 0) {
      foreignDinozIsAlive = true;
    } else {
      foreignDinozIsAlive = false;
    }

    boolean homeDinozHasTheCharm = false;
    if (homeDinoz.getPassiveList().get(getCharmNameFromElement(fightElements.getSecondHighestRight())) != null
            && homeDinoz.getPassiveList().get(getCharmNameFromElement(fightElements.getSecondHighestRight())) > 0) {
      homeDinozHasTheCharm = true;
    }

    boolean homeDinozHasThePrismatik = false;
    if (homeDinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) != null
            && homeDinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) > 0) {
      homeDinozHasThePrismatik = true;
    }

    boolean foreignDinozHasTheCharm = false;
    if (foreignDinoz.getPassiveList().get(getCharmNameFromElement(fightElements.getSecondHighestLeft())) != null
            && foreignDinoz.getPassiveList().get(getCharmNameFromElement(fightElements.getSecondHighestLeft())) > 0) {
      foreignDinozHasTheCharm = true;
    }

    Integer secondRoundResult = 0;
    if (roundFactor < 0) {
      secondRoundResult = (roundFactor * foreignDinoz.getElementsValues().get(fightElements.getSecondHighestRight()))
              + homeDinoz.getElementsValues().get(fightElements.getSecondHighestLeft());
    } else if (roundFactor > 0) {
      secondRoundResult = (roundFactor * homeDinoz.getElementsValues().get(fightElements.getSecondHighestLeft()))
              - (foreignDinoz.getElementsValues().get(fightElements.getSecondHighestRight()));
    }


    Integer lifeLossByLoser;
    if (secondRoundResult < 0 && !foreignDinozIsAlive) {
      lifeLossByLoser = 0;
    } else if (secondRoundResult > 0 && !homeDinozIsAlive) {
      lifeLossByLoser = 0;
    } else {
      lifeLossByLoser = secondRoundResult / 5;
    }

    Integer lifeLostPreventedLoser = 0;
    String charmUsed = "";

    // Life loss by the foreign dinoz :
    if (lifeLossByLoser > 0 && foreignDinozHasTheCharm) {
      String charmName = getCharmNameFromElement(fightElements.getSecondHighestLeft());
      foreignDinoz.getPassiveList().put(charmName,
              foreignDinoz.getPassiveList().get(charmName) - 1);
      summary.setCharmSecondRoundForLoser(1);
      lifeLostPreventedLoser = lifeLossByLoser * -1;
      charmUsed = charmName;
      lifeLossByLoser = 0;
    }

    // Life loss by the home dinoz :
    if ((lifeLossByLoser < 0 && homeDinozHasTheCharm) || (lifeLossByLoser < 0 && homeDinozHasThePrismatik)) {
      String charmName = "";
      if (homeDinozHasThePrismatik) {
        charmName = DinoparcConstants.CHARME_PRISMATIK;

      } else {
        charmName = getCharmNameFromElement(fightElements.getSecondHighestRight());
        homeDinoz.getPassiveList().put(charmName, homeDinoz.getPassiveList().get(charmName) - 1);
      }

      summary.setCharmSecondRoundForLoser(1);
      lifeLostPreventedLoser = lifeLossByLoser * -1;
      charmUsed = charmName;
      lifeLossByLoser = 0;
    }

    // Life loss by the foreign dinoz (no charms) :
    if (lifeLossByLoser > 0 && !foreignDinozHasTheCharm) {
      if (homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) != null && homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) >= 1) {
        Integer critiqueLevel = homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE);
        if (ThreadLocalRandom.current().nextInt(1, 100 + 1) <= critiqueLevel) {
          lifeLossByLoser = (lifeLossByLoser * 2);
          summary.getSkillsEffectsFr().add("Vous avez infligé un <b>Coup Critique</b> à votre adversaire, <b>doublant</b> ainsi les dégâts générés au Round #2!");
          summary.getSkillsEffectsEs().add("¡Has golpeado <b>Críticamente</b> a tu oponente, <b>duplicando</b> el daño generado desde la Ronda #2!");
          summary.getSkillsEffectsEn().add("You have <b>Critically hit</b> your opponent, <b>doubling</b> the damage generated on Round #2!");
        }
      }
      foreignDinoz.setLife(foreignDinoz.getLife() - lifeLossByLoser);
    }

    // Life loss by the home dinoz (no charms) :
    if (lifeLossByLoser < 0 && !homeDinozHasTheCharm) {
      homeDinoz.setLife(homeDinoz.getLife() + lifeLossByLoser);
    }

    summary.setSecondElementLeft(getElementNumberFromElementValue(fightElements.getSecondHighestLeft()));
    summary.setSecondElementRight(getElementNumberFromElementValue(fightElements.getSecondHighestRight()));
    summary.setRoundTwoScore(secondRoundResult);
    summary.setLifeLossSecondRoundByLoser(Math.abs(lifeLossByLoser));
    summary.setLifeLostPreventedSecondRound(lifeLostPreventedLoser);
    summary.setCharmTypeSecondRound(charmUsed);

    if (lifeLossByLoser < 0) {
      summary.setFinalLifeLoss(summary.getFinalLifeLoss() + Math.abs(lifeLossByLoser));
    }

    computeFinalScoreStepByStep(summary, secondRoundResult);
  }

  private void calculateThirdRoundEffects(FightSummaryDto summary, Dinoz homeDinoz, Dinoz foreignDinoz, FightElements fightElements) {
    Integer roundFactor = getRoundFactorByElements(fightElements.getThirdHighestLeft(), fightElements.getThirdHighestRight());

    Boolean homeDinozIsAlive;
    if (homeDinoz.getLife() > 0) {
      homeDinozIsAlive = true;
    } else {
      homeDinozIsAlive = false;
    }

    Boolean foreignDinozIsAlive;
    if (foreignDinoz.getLife() > 0) {
      foreignDinozIsAlive = true;
    } else {
      foreignDinozIsAlive = false;
    }

    boolean homeDinozHasTheCharm = false;
    if (homeDinoz.getPassiveList().get(getCharmNameFromElement(fightElements.getThirdHighestRight())) != null
            && homeDinoz.getPassiveList().get(getCharmNameFromElement(fightElements.getThirdHighestRight())) > 0) {
      homeDinozHasTheCharm = true;
    }

    boolean homeDinozHasThePrismatik = false;
    if (homeDinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) != null
            && homeDinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) > 0) {
      homeDinozHasThePrismatik = true;
    }

    boolean foreignDinozHasTheCharm = false;
    if (foreignDinoz.getPassiveList().get(getCharmNameFromElement(fightElements.getThirdHighestLeft())) != null
            && foreignDinoz.getPassiveList().get(getCharmNameFromElement(fightElements.getThirdHighestLeft())) > 0) {
      foreignDinozHasTheCharm = true;
    }

    Integer thirdRoundResult = 0;
    if (roundFactor < 0) {
      thirdRoundResult = (roundFactor * foreignDinoz.getElementsValues().get(fightElements.getThirdHighestRight()))
              + homeDinoz.getElementsValues().get(fightElements.getThirdHighestLeft());
    } else if (roundFactor > 0) {
      thirdRoundResult = (roundFactor * homeDinoz.getElementsValues().get(fightElements.getThirdHighestLeft()))
              - (foreignDinoz.getElementsValues().get(fightElements.getThirdHighestRight()));
    }

    Integer lifeLossByLoser;
    if (thirdRoundResult < 0 && !foreignDinozIsAlive) {
      lifeLossByLoser = 0;
    } else if (thirdRoundResult > 0 && !homeDinozIsAlive) {
      lifeLossByLoser = 0;
    } else {
      lifeLossByLoser = thirdRoundResult / 5;
    }

    Integer lifeLostPreventedLoser = 0;
    String charmUsed = "";

    // Life loss by the foreign dinoz :
    if (lifeLossByLoser > 0 && foreignDinozHasTheCharm) {
      String charmName = getCharmNameFromElement(fightElements.getThirdHighestLeft());
      foreignDinoz.getPassiveList().put(charmName,
              foreignDinoz.getPassiveList().get(charmName) - 1);
      summary.setCharmThirdRoundForLoser(1);
      lifeLostPreventedLoser = lifeLossByLoser * -1;
      charmUsed = charmName;
      lifeLossByLoser = 0;
    }

    // Life loss by the home dinoz :
    if ((lifeLossByLoser < 0 && homeDinozHasTheCharm) || (lifeLossByLoser < 0 && homeDinozHasThePrismatik)) {
      String charmName = "";
      if (homeDinozHasThePrismatik) {
        charmName = DinoparcConstants.CHARME_PRISMATIK;

      } else {
        charmName = getCharmNameFromElement(fightElements.getThirdHighestRight());
        homeDinoz.getPassiveList().put(charmName, homeDinoz.getPassiveList().get(charmName) - 1);
      }

      summary.setCharmThirdRoundForLoser(1);
      lifeLostPreventedLoser = lifeLossByLoser * -1;
      charmUsed = charmName;
      lifeLossByLoser = 0;
    }

    // Life loss by the foreign dinoz (no charms) :
    if (lifeLossByLoser > 0 && !foreignDinozHasTheCharm) {
      if (homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) != null && homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) >= 1) {
        Integer critiqueLevel = homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE);
        if (ThreadLocalRandom.current().nextInt(1, 100 + 1) <= critiqueLevel) {
          lifeLossByLoser = (lifeLossByLoser * 2);
          summary.getSkillsEffectsFr().add("Vous avez infligé un <b>Coup Critique</b> à votre adversaire, <b>doublant</b> ainsi les dégâts générés au Round #3!");
          summary.getSkillsEffectsEs().add("¡Has golpeado <b>Críticamente</b> a tu oponente, <b>duplicando</b> el daño generado desde la Ronda #3!");
          summary.getSkillsEffectsEn().add("You have <b>Critically hit</b> your opponent, <b>doubling</b> the damage generated on Round #3!");
        }
      }
      foreignDinoz.setLife(foreignDinoz.getLife() - lifeLossByLoser);
    }

    // Life loss by the home dinoz (no charms) :
    if (lifeLossByLoser < 0 && !homeDinozHasTheCharm) {
      homeDinoz.setLife(homeDinoz.getLife() + lifeLossByLoser);
    }

    summary.setThirdElementLeft(getElementNumberFromElementValue(fightElements.getThirdHighestLeft()));
    summary.setThirdElementRight(getElementNumberFromElementValue(fightElements.getThirdHighestRight()));
    summary.setRoundThreeScore(thirdRoundResult);
    summary.setLifeLossThirdRoundByLoser(Math.abs(lifeLossByLoser));
    summary.setLifeLostPreventedThirdRound(lifeLostPreventedLoser);
    summary.setCharmTypeThirdRound(charmUsed);

    if (lifeLossByLoser < 0) {
      summary.setFinalLifeLoss(summary.getFinalLifeLoss() + Math.abs(lifeLossByLoser));
    }

    computeFinalScoreStepByStep(summary, thirdRoundResult);
  }

  private void checkForGranitMissionProgression(Dinoz homeDinoz, Dinoz foreignDinoz, Player account, FightSummaryDto summary) {
    UUID playerId = UUID.fromString(account.getId());
    Optional<GranitMissionDao> granitMissionData = granitMissionRepository.getMissionData(UUID.fromString(account.getId()));
    if (homeDinoz.getPlaceNumber() == 7 && granitMissionData.isPresent() && !granitMissionData.get().getComplete()) {
      incrementCorrectCounterForShinyInGranitQuest(homeDinoz, foreignDinoz);
      summary.getSkillsEffectsFr().add("Vous avez progressé dans la quête de chasse au Shiny donnée par le Gardien de la Porte!");
      summary.getSkillsEffectsEs().add("¡Has progresado en la misión de caza Shiny encomendada por el Guardián de la Puerta!");
      summary.getSkillsEffectsEn().add("You have progressed in the Shiny hunting quest given by the Gate Keeper!");

      GranitMissionDao granitMissionDataDao = granitMissionRepository.getMissionData(playerId).get();
      granitMissionDataDao.validateStatus();
      if (granitMissionDataDao.getComplete()) {
        granitMissionRepository.setCompleted(playerId);
      }
    }
  }

  private void checkForQuestUpdates(Dinoz homeDinoz, Dinoz foreignDinoz, FightSummaryDto summary) {
    if (foreignDinozIsAWildKabuki(foreignDinoz) && summary.isHaveWon()) {

      if (!homeDinoz.getMalusList().contains(DinoparcConstants.KABUKI_QUETE)) {
        homeDinoz.getMalusList().add(DinoparcConstants.KABUKI_QUETE);
      }

      homeDinoz.setKabukiProgression(homeDinoz.getKabukiProgression() + 1);

      if (homeDinoz.getKabukiProgression() < 10) {
        summary.getSkillsEffectsFr().add("Vous avez progressé dans la quête des Kabukis! (" + homeDinoz.getKabukiProgression() + "/10)");
        summary.getSkillsEffectsEs().add("¡Has progresado en la búsqueda de los Kabukis! (" + homeDinoz.getKabukiProgression() + "/10)");
        summary.getSkillsEffectsEn().add("You have progressed Kabuki's quest! (" + homeDinoz.getKabukiProgression() + "/10)");

      } else {
        summary.getSkillsEffectsFr().add("Vous avez terminé la quête des Kabukis! Félicitations!");
        summary.getSkillsEffectsEs().add("¡Has completado la misión de Kabukis! ¡Felicidades!");
        summary.getSkillsEffectsEn().add("You have completed the Kabuki's quest! Congratulation!");

        homeDinoz.getMalusList().remove(DinoparcConstants.KABUKI_QUETE);

        var collection = collectionRepository.getPlayerCollection(UUID.fromString(homeDinoz.getMasterId()));
        var epicCollection = collection.getEpicCollection();

        if (!epicCollection.contains("3")) {
          epicCollection.add("3");
          collectionRepository.updateEpicCollection(collection.getId(), epicCollection);
          Player account = this.getAccountByDinozId(homeDinoz.getId());
          account.getUnlockedDinoz().add(17);
          playerRepository.updateUnlockedDinoz(account.getId(), account.getUnlockedDinoz());
        }
      }

      dinozRepository.setKabukiProgression(homeDinoz.getId(), homeDinoz.getMalusList(), homeDinoz.getKabukiProgression());

    } else if (foreignDinozIsManny(foreignDinoz)) {
      Integer[] newLocations = AccountService.locationsCompatibility.get(foreignDinoz.getPlaceNumber());
      Integer newLocation = newLocations[new Random().nextInt(newLocations.length)];
      if (foreignDinoz.getPlaceNumber() == 5) {
        newLocation = 4;
      } else if (foreignDinoz.getPlaceNumber() == 2) {
        newLocation = 15;
      } else if (foreignDinoz.getPlaceNumber() == 20 || foreignDinoz.getPlaceNumber() == 21) {
        newLocation = 8;
      }
      granitMissionRepository.updateMannysData(UUID.fromString(homeDinoz.getMasterId()), foreignDinoz.getLife(), newLocation);

      if (foreignDinoz.getLife() <= 0) {
        summary.getSkillsEffectsFr().add("Manny est maintenant maîtrisé! Ramenez-le au Gardien de la Porte avant qu'il ne reprenne conscience!");
        summary.getSkillsEffectsEs().add("¡Manny ahora está dominado! ¡Llévalo de vuelta con el Guardián antes de que recupere el conocimiento!");
        summary.getSkillsEffectsEn().add("Manny is now overpowered! Bring him back to the Gatekeeper before he regains consciousness!");
      }
    }
  }

  public void checkForFocusVanishing(Dinoz homeDinoz, FightSummaryDto summary) {
    boolean focusVanished = false;
    if (homeDinoz.getPassiveList().containsKey(DinoparcConstants.FOCUS_AGGRESIVITE) && homeDinoz.getPassiveList().get(DinoparcConstants.FOCUS_AGGRESIVITE) > 0) {
      Integer focusVanishOdds = ThreadLocalRandom.current().nextInt(0, 100 + 1);
      Integer durability = 20;
      if (homeDinoz.getSkillsMap().containsKey(DinoparcConstants.INGENIEUR) && homeDinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR) > 0) {
        durability -= homeDinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR);
      }
      if (focusVanishOdds < durability) {
        homeDinoz.getPassiveList().replace(DinoparcConstants.FOCUS_AGGRESIVITE, homeDinoz.getPassiveList().get(DinoparcConstants.FOCUS_AGGRESIVITE) - 1);
        homeDinoz.getElementsValues().replace(DinoparcConstants.FEU, homeDinoz.getElementsValues().get(DinoparcConstants.FEU) - 2);
        homeDinoz.getElementsValues().replace(DinoparcConstants.FOUDRE, homeDinoz.getElementsValues().get(DinoparcConstants.FOUDRE) - 2);
        summary.getSkillsEffectsFr().add("Votre focus (+2 feu & +2 foudre) s'est brisé pendant le combat. Quel dommage...");
        summary.getSkillsEffectsEs().add("Tu poder (+2 fuego y +2 rayo) se rompió durante el combate. Qué pena...");
        summary.getSkillsEffectsEn().add("Your power (+2 fire & +2 thunder) broke during fight. Too bad...");
        focusVanished = true;
      }
    }

    if (homeDinoz.getPassiveList().containsKey(DinoparcConstants.FOCUS_NATURE) && homeDinoz.getPassiveList().get(DinoparcConstants.FOCUS_NATURE) > 0) {
      Integer focusVanishOdds = ThreadLocalRandom.current().nextInt(0, 100 + 1);
      Integer durability = 20;
      if (homeDinoz.getSkillsMap().containsKey(DinoparcConstants.INGENIEUR) && homeDinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR) > 0) {
        durability -= homeDinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR);
      }
      if (focusVanishOdds < durability) {
        homeDinoz.getPassiveList().replace(DinoparcConstants.FOCUS_NATURE, homeDinoz.getPassiveList().get(DinoparcConstants.FOCUS_NATURE) - 1);
        homeDinoz.getElementsValues().replace(DinoparcConstants.TERRE, homeDinoz.getElementsValues().get(DinoparcConstants.TERRE) - 2);
        homeDinoz.getElementsValues().replace(DinoparcConstants.AIR, homeDinoz.getElementsValues().get(DinoparcConstants.AIR) - 2);
        summary.getSkillsEffectsFr().add("Votre focus (+2 terre & +2 air) s'est brisé pendant le combat. Quel dommage...");
        summary.getSkillsEffectsEs().add("Tu poder (+2 tierra y +2 aire) se rompió durante el combate. Qué pena...");
        summary.getSkillsEffectsEn().add("Your power (+2 earth & +2 air) broke during fight. Too bad...");
        focusVanished = true;
      }
    }

    if (homeDinoz.getPassiveList().containsKey(DinoparcConstants.FOCUS_SIRAINS) && homeDinoz.getPassiveList().get(DinoparcConstants.FOCUS_SIRAINS) > 0) {
      Integer focusVanishOdds = ThreadLocalRandom.current().nextInt(0, 100 + 1);
      Integer durability = 20;
      if (homeDinoz.getSkillsMap().containsKey(DinoparcConstants.INGENIEUR) && homeDinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR) > 0) {
        durability -= homeDinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR);
      }
      if (focusVanishOdds < durability) {
        homeDinoz.getPassiveList().replace(DinoparcConstants.FOCUS_SIRAINS, homeDinoz.getPassiveList().get(DinoparcConstants.FOCUS_SIRAINS) - 1);
        homeDinoz.getElementsValues().replace(DinoparcConstants.EAU, homeDinoz.getElementsValues().get(DinoparcConstants.EAU) - 5);
        summary.getSkillsEffectsFr().add("Votre focus (+5 eau) s'est brisé pendant le combat. Quel dommage...");
        summary.getSkillsEffectsEs().add("Tu poder (+5 agua) se rompió durante el combate. Qué pena...");
        summary.getSkillsEffectsEn().add("Your power (+5 water) broke during fight. Too bad...");
        focusVanished = true;
      }
    }

    if (homeDinoz.getPassiveList().containsKey(DinoparcConstants.FOCUS_MAHAMUTI) && homeDinoz.getPassiveList().get(DinoparcConstants.FOCUS_MAHAMUTI) > 0) {
      Integer focusVanishOdds = ThreadLocalRandom.current().nextInt(0, 100 + 1);
      Integer durability = 20;
      if (homeDinoz.getSkillsMap().containsKey(DinoparcConstants.INGENIEUR) && homeDinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR) > 0) {
        durability -= homeDinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR);
      }
      if (focusVanishOdds < durability) {
        homeDinoz.getPassiveList().replace(DinoparcConstants.FOCUS_MAHAMUTI, homeDinoz.getPassiveList().get(DinoparcConstants.FOCUS_MAHAMUTI) - 1);
        homeDinoz.getElementsValues().replace(DinoparcConstants.TERRE, homeDinoz.getElementsValues().get(DinoparcConstants.TERRE) - 5);
        summary.getSkillsEffectsFr().add("Votre focus (+5 terre) s'est brisé pendant le combat. Quel dommage...");
        summary.getSkillsEffectsEs().add("Tu poder (+5 arbol) se rompió durante el combate. Qué pena...");
        summary.getSkillsEffectsEn().add("Your power (+5 earth) broke during fight. Too bad...");
        focusVanished = true;
      }
    }

    if (focusVanished) {
      dinozRepository.processFocus(homeDinoz.getId(), homeDinoz.getPassiveList(), homeDinoz.getElementsValues());
    }
  }

  public void checkForPrismatikVanishing(Dinoz homeDinoz, FightSummaryDto summary) {
    if (homeDinoz.getPassiveList().containsKey(DinoparcConstants.CHARME_PRISMATIK) && homeDinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) > 0) {
      Integer charmeVanishOdds = ThreadLocalRandom.current().nextInt(1, 100 + 1);
      Integer durability = 10;
      if (homeDinoz.getSkillsMap().containsKey(DinoparcConstants.INGENIEUR) && homeDinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR) > 0) {
        durability -= homeDinoz.getSkillsMap().get(DinoparcConstants.INGENIEUR);
      }
      if (charmeVanishOdds < durability) {
        homeDinoz.getPassiveList().replace(DinoparcConstants.CHARME_PRISMATIK, homeDinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) - 1);
        summary.getCharmsEffectsFr().add("Votre Charme Prismatik s'est brisé pendant le combat. Quel dommage...");
        summary.getCharmsEffectsEs().add("Tu Hechizo Prismatik se rompió durante el combate. Qué pena...");
        summary.getCharmsEffectsEn().add("Your Prismatik Spell broke during fight. Too bad...");
        dinozRepository.setPassiveList(homeDinoz.getId(), homeDinoz.getPassiveList());
      }
    }
  }

  public String getCharmNameFromElement(String element) {
    switch (element) {
      case DinoparcConstants.FEU:
        return DinoparcConstants.CHARME_FEU;
      case DinoparcConstants.TERRE:
        return DinoparcConstants.CHARME_TERRE;
      case DinoparcConstants.EAU:
        return DinoparcConstants.CHARME_EAU;
      case DinoparcConstants.FOUDRE:
        return DinoparcConstants.CHARME_FOUDRE;
      case DinoparcConstants.AIR:
        return DinoparcConstants.CHARME_AIR;
    }
    return null;
  }

  public Integer getRoundFactorByElements(String leftElement, String rightElement) {
    switch (leftElement) {
      case DinoparcConstants.FEU:
        switch (rightElement) {
          case DinoparcConstants.FEU:
            return 1;
          case DinoparcConstants.TERRE:
            return 5;
          case DinoparcConstants.EAU:
            return 3;
          case DinoparcConstants.FOUDRE:
            return -3;
          case DinoparcConstants.AIR:
            return -5;
        }
      case DinoparcConstants.TERRE:
        switch (rightElement) {
          case DinoparcConstants.FEU:
            return -5;
          case DinoparcConstants.TERRE:
            return 1;
          case DinoparcConstants.EAU:
            return 5;
          case DinoparcConstants.FOUDRE:
            return 3;
          case DinoparcConstants.AIR:
            return -3;
        }
      case DinoparcConstants.EAU:
        switch (rightElement) {
          case DinoparcConstants.FEU:
            return -3;
          case DinoparcConstants.TERRE:
            return -5;
          case DinoparcConstants.EAU:
            return 1;
          case DinoparcConstants.FOUDRE:
            return 5;
          case DinoparcConstants.AIR:
            return 3;
        }
      case DinoparcConstants.FOUDRE:
        switch (rightElement) {
          case DinoparcConstants.FEU:
            return 3;
          case DinoparcConstants.TERRE:
            return -3;
          case DinoparcConstants.EAU:
            return -5;
          case DinoparcConstants.FOUDRE:
            return 1;
          case DinoparcConstants.AIR:
            return 5;
        }
      case DinoparcConstants.AIR:
        switch (rightElement) {
          case DinoparcConstants.FEU:
            return 5;
          case DinoparcConstants.TERRE:
            return 3;
          case DinoparcConstants.EAU:
            return -3;
          case DinoparcConstants.FOUDRE:
            return -5;
          case DinoparcConstants.AIR:
            return 1;
        }
      default:
        return 0;
    }
  }

  private FightSummaryDto initFight(Dinoz homeDinoz, Dinoz foreignDinoz) {
    FightSummaryDto summary = new FightSummaryDto();
    summary.setSkillsEffectsFr(new ArrayList<String>());
    summary.setSkillsEffectsEs(new ArrayList<String>());
    summary.setSkillsEffectsEn(new ArrayList<String>());
    summary.setCharmsEffectsFr(new ArrayList<String>());
    summary.setCharmsEffectsEs(new ArrayList<String>());
    summary.setCharmsEffectsEn(new ArrayList<String>());
    summary.setLeftDinozName(homeDinoz.getName());
    summary.setRightDinozName(foreignDinoz.getName());
    summary.setLeftDinozAppCode(homeDinoz.getAppearanceCode());
    summary.setRightDinozAppCode(foreignDinoz.getAppearanceCode());
    summary.setLeftDinozBeginMessage(homeDinoz.getBeginMessage());
    summary.setRightDinozBeginMessage(foreignDinoz.getBeginMessage());
    summary.setLeftDinozEndMessage(homeDinoz.getEndMessage());
    summary.setRightDinozEndMessage(foreignDinoz.getEndMessage());
    summary.setInitialHpLeft(homeDinoz.getLife());
    summary.setInitialHpRight(foreignDinoz.getLife());
    summary.setScoreFinalHome(0);
    summary.setScoreFinalEnnemy(0);

    return summary;
  }

  public Integer getElementNumberFromElementValue(String elementValue) {
    switch (elementValue) {
      case DinoparcConstants.FEU:
        return 0;
      case DinoparcConstants.TERRE:
        return 1;
      case DinoparcConstants.EAU:
        return 2;
      case DinoparcConstants.FOUDRE:
        return 3;
      case DinoparcConstants.AIR:
        return 4;
      default:
        return -1;
    }
  }

  private boolean foreignDinozIsAWildKabuki(Dinoz foreignDinoz) {
    return foreignDinoz.getId() != null && foreignDinoz.getId().startsWith("@") && !foreignDinoz.getId().startsWith("@MANNY");
  }

  private boolean foreignDinozIsManny(Dinoz foreignDinoz) {
    return foreignDinoz.getId() != null && foreignDinoz.getId().startsWith("@") && foreignDinoz.getId().startsWith("@MANNY");
  }

  public Integer getExperienceFromFight(Boolean isMaxed, Integer intelligence, int level, int foreignDinozLevel) {
    if (!isMaxed) {
      Integer experience = 0;
      if (level < 5) {
        experience = ThreadLocalRandom.current().nextInt(25, 31 + 1);
      } else if (level >= 5 && level < 10) {
        experience = ThreadLocalRandom.current().nextInt(19, 24 + 1);
      } else if (level >= 10 && level < 15) {
        experience = ThreadLocalRandom.current().nextInt(13, 17 + 1);
      } else if (level >= 15 && level < 20) {
        experience = ThreadLocalRandom.current().nextInt(12, 16 + 1);
      } else if (level >= 20 && level < 25) {
        experience = ThreadLocalRandom.current().nextInt(11, 15 + 1);
      } else if (level >= 25 && level < 30) {
        experience = ThreadLocalRandom.current().nextInt(10, 14 + 1);
      } else if (level >= 30 && level < 45) {
        experience = ThreadLocalRandom.current().nextInt(8, 11 + 1);
      } else if (level >= 45 && level < 70) {
        experience = ThreadLocalRandom.current().nextInt(7, 10 + 1);
      } else if (level >= 70 && level < 100) {
        experience = ThreadLocalRandom.current().nextInt(5, 7 + 1);
      } else if (level >= 100 && level < 120) {
        experience = ThreadLocalRandom.current().nextInt(3, 5 + 1);
      } else if (level >= 120) {
        experience = ThreadLocalRandom.current().nextInt(1, 3 + 1);
      }

      // Bonus for intelligence :
      if (intelligence != null) {
        if (intelligence == 1 || intelligence == 2) {
          experience = experience + 1;

        } else if (intelligence == 3 || intelligence == 4) {
          experience = experience + 2;

        } else if (intelligence == 5) {
          experience = experience + 3;
        }
      }

      if (foreignDinozLevel == 0) {
        return experience;

      } else {
        if (foreignDinozLevel - level > 0) {
          // Bonus for fighting higher level dinoz:
          experience = experience + ThreadLocalRandom.current().nextInt(1, 3 + 1);
        }

        return experience;
      }
    }
    return 0;
  }

  private TableField<PlayerStatRecord, Long> getPlayerStatDinozFightField(String appearanceCode) {
    var raceLetter = appearanceCode.charAt(0);
    switch (raceLetter) {
      case '0':
        return PgPlayerStatRepository.NB_FIGHT_RACE_0;
      case '1':
        return PgPlayerStatRepository.NB_FIGHT_RACE_1;
      case '2':
        return PgPlayerStatRepository.NB_FIGHT_RACE_2;
      case '3':
        return PgPlayerStatRepository.NB_FIGHT_RACE_3;
      case '4':
        return PgPlayerStatRepository.NB_FIGHT_RACE_4;
      case '5':
        return PgPlayerStatRepository.NB_FIGHT_RACE_5;
      case '6':
        return PgPlayerStatRepository.NB_FIGHT_RACE_6;
      case '7':
        return PgPlayerStatRepository.NB_FIGHT_RACE_7;
      case '8':
        return PgPlayerStatRepository.NB_FIGHT_RACE_8;
      case '9':
        return PgPlayerStatRepository.NB_FIGHT_RACE_9;
      case 'A':
        return PgPlayerStatRepository.NB_FIGHT_RACE_A;
      case 'B':
        return PgPlayerStatRepository.NB_FIGHT_RACE_B;
      case 'C':
        return PgPlayerStatRepository.NB_FIGHT_RACE_C;
      case 'D':
        return PgPlayerStatRepository.NB_FIGHT_RACE_D;
      case 'E':
        return PgPlayerStatRepository.NB_FIGHT_RACE_E;
      case 'F':
        return PgPlayerStatRepository.NB_FIGHT_RACE_F;
      case 'G':
        return PgPlayerStatRepository.NB_FIGHT_RACE_G;
      case 'H':
        return PgPlayerStatRepository.NB_FIGHT_RACE_H;
      case 'I':
        return PgPlayerStatRepository.NB_FIGHT_RACE_I;
      case 'J':
        return PgPlayerStatRepository.NB_FIGHT_RACE_J;
      case 'K':
        return PgPlayerStatRepository.NB_FIGHT_RACE_K;
      case 'L':
        return PgPlayerStatRepository.NB_FIGHT_RACE_L;
    }
    return PgPlayerStatRepository.NB_FIGHT_RACE_0;
  }

  private TableField<PlayerStatRecord, Long> getPlayerStatDinozShinyField(String appearanceCode) {
    var raceLetter = appearanceCode.charAt(0);
    switch (raceLetter) {
      case '0':
        return PgPlayerStatRepository.NB_SHINY_RACE_0;
      case '1':
        return PgPlayerStatRepository.NB_SHINY_RACE_1;
      case '2':
        return PgPlayerStatRepository.NB_SHINY_RACE_2;
      case '3':
        return PgPlayerStatRepository.NB_SHINY_RACE_3;
      case '4':
        return PgPlayerStatRepository.NB_SHINY_RACE_4;
      case '5':
        return PgPlayerStatRepository.NB_SHINY_RACE_5;
      case '6':
        return PgPlayerStatRepository.NB_SHINY_RACE_6;
      case '7':
        return PgPlayerStatRepository.NB_SHINY_RACE_7;
      case '8':
        return PgPlayerStatRepository.NB_SHINY_RACE_8;
      case '9':
        return PgPlayerStatRepository.NB_SHINY_RACE_9;
      case 'A':
        return PgPlayerStatRepository.NB_SHINY_RACE_A;
      case 'B':
        return PgPlayerStatRepository.NB_SHINY_RACE_B;
      case 'C':
        return PgPlayerStatRepository.NB_SHINY_RACE_C;
      case 'D':
        return PgPlayerStatRepository.NB_SHINY_RACE_D;
      case 'E':
        return PgPlayerStatRepository.NB_SHINY_RACE_E;
      case 'F':
        return PgPlayerStatRepository.NB_SHINY_RACE_F;
      case 'G':
        return PgPlayerStatRepository.NB_SHINY_RACE_G;
      case 'H':
        return PgPlayerStatRepository.NB_SHINY_RACE_H;
      case 'I':
        return PgPlayerStatRepository.NB_SHINY_RACE_I;
      case 'J':
        return PgPlayerStatRepository.NB_SHINY_RACE_J;
      case 'K':
        return PgPlayerStatRepository.NB_SHINY_RACE_K;
      case 'L':
        return PgPlayerStatRepository.NB_SHINY_RACE_L;
    }
    return PgPlayerStatRepository.NB_SHINY_RACE_0;
  }

  private void incrementCorrectCounterForShinyInGranitQuest(Dinoz homeDinoz, Dinoz foreignDinoz) {
    switch (Dinoz.getRaceFromAppearanceCode(foreignDinoz.getAppearanceCode())) {
      case DinoparcConstants.MOUEFFE:
        granitMissionRepository.incrementMoueffe(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.PICORI:
        granitMissionRepository.incrementPicori(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.CASTIVORE:
        granitMissionRepository.incrementCastivore(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.SIRAIN:
        granitMissionRepository.incrementSirain(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.WINKS:
        granitMissionRepository.incrementWinks(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.GORILLOZ:
        granitMissionRepository.incrementGorilloz(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.CARGOU:
        granitMissionRepository.incrementCargou(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.HIPPOCLAMP:
        granitMissionRepository.incrementHippoclamp(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.ROKKY:
        granitMissionRepository.incrementRokky(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.PIGMOU:
        granitMissionRepository.incrementPigmou(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.WANWAN:
        granitMissionRepository.incrementWanwan(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.GOUPIGNON:
        granitMissionRepository.incrementGoupignon(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.KUMP:
        granitMissionRepository.incrementKump(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.PTEROZ:
        granitMissionRepository.incrementPteroz(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.SANTAZ:
        granitMissionRepository.incrementSantaz(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.KORGON:
        granitMissionRepository.incrementKorgon(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.KABUKI:
        granitMissionRepository.incrementKabuki(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.SERPANTIN:
        granitMissionRepository.incrementSerpantin(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.SOUFFLET:
        granitMissionRepository.incrementSoufflet(UUID.fromString(homeDinoz.getMasterId()));
        break;
      case DinoparcConstants.FEROSS:
        granitMissionRepository.incrementFeross(UUID.fromString(homeDinoz.getMasterId()));
        break;
      default:
        break;
    }
  }

  private boolean isHalloweenEventAndObjectsAreStealableAtThisHour(Dinoz foreignDinoz, Dinoz homeDinoz) {
    return foreignDinoz.getMasterId() == null
            && foreignDinoz.getLevel() <= 0
            && !foreignDinoz.getName().startsWith("@")
            && foreignDinoz.getId() == null
            && foreignDinoz.getAppearanceCode().endsWith("&");
  }

  private Boolean areObjectsStealableFromShiniesAtThisDateAndTime(Dinoz homeDinoz) {
    ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));

    Rng rng = neoparcConfig.getRngFactory()
            .string("getHourOfDayThatObjectsAreStealableFromShiniesV1")
            .string(String.valueOf(now.getDayOfYear()))
            .string(String.valueOf(now.getHour()))
            .toRng();

    Boolean isStealable = (rng.randIntBetween(1, (4 + 1)) == 1);
    Integer raceOfDinozForHordesLocation = Dinoz.getRaceOfDinozForLocation(homeDinoz.getPlaceNumber());

    return (isStealable
            || (homeDinoz.getDanger() < 1000
            && raceOfDinozForHordesLocation != null
            && DinozUtils.getRaceNumber(homeDinoz.getRace()) == raceOfDinozForHordesLocation
    ));
  }

  private boolean checkIfDinozIsWarrior(List<String> malusList) {
    return (malusList.contains(DinoparcConstants.FACTION_1_GUERRIER)
            || malusList.contains(DinoparcConstants.FACTION_2_GUERRIER)
            || malusList.contains(DinoparcConstants.FACTION_3_GUERRIER)
    );
  }

  private void checkForClanPointsScored(Dinoz homeDinoz, Dinoz foreignDinoz, FightSummaryDto summary) {
    if (checkIfDinozIsWarrior(homeDinoz.getMalusList())) {
      Clan homeDinozClan = clanRepository.getClanOfPlayer(homeDinoz.getMasterId());
      PlayerClan homePlayerClan = clanRepository.getPlayerClan(homeDinoz.getMasterId()).get();

      if (checkIfDinozIsWarrior(foreignDinoz.getMalusList())) {
        Clan foreignDinozClan = clanRepository.getClanOfPlayer(foreignDinoz.getMasterId());
        if (homeDinozClan != null && foreignDinozClan != null && DinoparcConstants.WAR_MODE && !homeDinozClan.getFaction().equals(foreignDinozClan.getFaction())) {
          distributePointsToHomeDinozClan(homeDinoz, foreignDinoz, summary, homeDinozClan, homePlayerClan);
        }
      } else if (foreignDinoz.getId() != null && checkIfDinozIsWarrior(homeDinoz.getMalusList()) && foreignDinoz.getId().contains(DinoparcConstants.PVE_FACTION)) {
        distributePointsToHomeDinozClan(homeDinoz, foreignDinoz, summary, homeDinozClan, homePlayerClan);
      }
    }
  }

  private void distributePointsToHomeDinozClan(Dinoz homeDinoz, Dinoz foreignDinoz, FightSummaryDto summary, Clan homeDinozClan, PlayerClan homePlayerClan) {
    if (summary.isHaveWon()) {
      Boolean victoryIsKO = (foreignDinoz.getLife() <= 0);
      if (homeDinoz.getSkillsMap().containsKey(DinoparcConstants.POUVOIR_SOMBRE) && homeDinoz.getSkillsMap().get(DinoparcConstants.POUVOIR_SOMBRE) == 5) {
        if (victoryIsKO) {
          //+9 pts : Victoire (K.O) du Dinoz attaquant avec pouvoir sombre :
          homePlayerClan.setNbPointsFights(homePlayerClan.getNbPointsFights() + 9);
          clanRepository.updatePlayerClan(homePlayerClan);
          clanRepository.addClanPointsFights(homeDinozClan.getId(), 9);
          summary.getSkillsEffectsFr().add("Vous avez marqué 9 points pour votre clan!");
          summary.getSkillsEffectsEs().add("¡Has obtenido 9 puntos para tu clan!");
          summary.getSkillsEffectsEn().add("You scored 9 points for your clan!");
        } else {
          //+3 pts : Victoire du Dinoz attaquant avec pouvoir sombre :
          homePlayerClan.setNbPointsFights(homePlayerClan.getNbPointsFights() + 3);
          clanRepository.updatePlayerClan(homePlayerClan);
          clanRepository.addClanPointsFights(homeDinozClan.getId(), 3);
          summary.getSkillsEffectsFr().add("Vous avez marqué 3 points pour votre clan!");
          summary.getSkillsEffectsEs().add("¡Has obtenido 3 puntos para tu clan!");
          summary.getSkillsEffectsEn().add("You scored 3 points for your clan!");
        }

      } else {
        if (victoryIsKO) {
          //+3 pts : Victoire (K.O) du Dinoz attaquant sans le pouvoir sombre :
          homePlayerClan.setNbPointsFights(homePlayerClan.getNbPointsFights() + 3);
          clanRepository.updatePlayerClan(homePlayerClan);
          clanRepository.addClanPointsFights(homeDinozClan.getId(), 3);
          summary.getSkillsEffectsFr().add("Vous avez marqué 3 points pour votre clan!");
          summary.getSkillsEffectsEs().add("¡Has obtenido 3 puntos para tu clan!");
          summary.getSkillsEffectsEn().add("You scored 3 points for your clan!");
        } else {
          //+1 pts : Victoire du Dinoz attaquant sans le pouvoir sombre :
          homePlayerClan.setNbPointsFights(homePlayerClan.getNbPointsFights() + 1);
          clanRepository.updatePlayerClan(homePlayerClan);
          clanRepository.addClanPointsFights(homeDinozClan.getId(), 1);
          summary.getSkillsEffectsFr().add("Vous avez marqué 1 point pour votre clan!");
          summary.getSkillsEffectsEs().add("¡Has obtenido 1 punto para tu clan!");
          summary.getSkillsEffectsEn().add("You scored 1 point for your clan!");
        }
      }
    } else {
      Boolean defeatIsKO = (homeDinoz.getLife() <= 0);
      if (foreignDinoz.getSkillsMap().containsKey(DinoparcConstants.POUVOIR_SOMBRE) && foreignDinoz.getSkillsMap().get(DinoparcConstants.POUVOIR_SOMBRE) == 5) {
        if (defeatIsKO) {
          //-9 pts : Défaite (K.O) du Dinoz attaquant avec pouvoir sombre sur le Dinoz adverse :
          homePlayerClan.setNbPointsFights(homePlayerClan.getNbPointsFights() - 9);
          clanRepository.updatePlayerClan(homePlayerClan);
          clanRepository.addClanPointsFights(homeDinozClan.getId(), -9);
          summary.getSkillsEffectsFr().add("Vous fait perdre 9 points à votre clan...");
          summary.getSkillsEffectsEs().add("Has perdido 9 puntos para tu clan...");
          summary.getSkillsEffectsEn().add("You lost 9 points for your clan...");
        } else {
          //-3 pts : Défaite du Dinoz attaquant avec pouvoir sombre sur le Dinoz adverse :
          homePlayerClan.setNbPointsFights(homePlayerClan.getNbPointsFights() - 3);
          clanRepository.updatePlayerClan(homePlayerClan);
          clanRepository.addClanPointsFights(homeDinozClan.getId(), -3);
          summary.getSkillsEffectsFr().add("Vous avez fait perdre 3 points à votre clan...");
          summary.getSkillsEffectsEs().add("Has perdido 3 puntos para tu clan...");
          summary.getSkillsEffectsEn().add("You lost 3 points for your clan...");
        }
      } else {
        if (defeatIsKO) {
          //-3 pts : Défaite (K.O) du Dinoz attaquant sans le pouvoir sombre sur le Dinoz adverse :
          homePlayerClan.setNbPointsFights(homePlayerClan.getNbPointsFights() - 3);
          clanRepository.updatePlayerClan(homePlayerClan);
          clanRepository.addClanPointsFights(homeDinozClan.getId(), -3);
          summary.getSkillsEffectsFr().add("Vous avez fait perdre 3 points à votre clan...");
          summary.getSkillsEffectsEs().add("Has perdido 3 puntos para tu clan...");
          summary.getSkillsEffectsEn().add("You lost 3 points for your clan...");
        } else {
          //-1 pts : Défaite du Dinoz attaquant sans le pouvoir sombre sur le Dinoz adverse :
          homePlayerClan.setNbPointsFights(homePlayerClan.getNbPointsFights() - 1);
          clanRepository.updatePlayerClan(homePlayerClan);
          clanRepository.addClanPointsFights(homeDinozClan.getId(), -1);
          summary.getSkillsEffectsFr().add("Vous avez fait perdre 1 point à votre clan...");
          summary.getSkillsEffectsEs().add("Has perdido 1 punto para tu clan...");
          summary.getSkillsEffectsEn().add("You lost 1 point for your clan...");
        }
      }
    }
  }
}
