package com.dinoparc.api.services.migration;

import net.eternaltwin.dinoparc.DinoparcServer;
import net.eternaltwin.user.UserId;

public final class ImportOptions {
  public final UserId userId;
  public final DinoparcServer server;
  public final boolean allowStaleDinozList;
  public final boolean ignoreAlreadyImportedDinoz;

  public ImportOptions(final UserId userId, final DinoparcServer server, final boolean allowStaleDinozList, final boolean ignoreAlreadyImportedDinoz) {
    this.userId = userId;
    this.server = server;
    this.allowStaleDinozList = allowStaleDinozList;
    this.ignoreAlreadyImportedDinoz = ignoreAlreadyImportedDinoz;
  }
}
