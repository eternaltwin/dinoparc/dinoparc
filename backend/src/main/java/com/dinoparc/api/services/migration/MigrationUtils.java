package com.dinoparc.api.services.migration;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.stereotype.Component;
import net.eternaltwin.client.Auth;
import net.eternaltwin.client.EtwinClient;
import net.eternaltwin.core.LatestTemporal;
import net.eternaltwin.dinoparc.DinoparcDinozId;
import net.eternaltwin.dinoparc.DinoparcDinozIdRef;
import net.eternaltwin.dinoparc.DinoparcServer;
import net.eternaltwin.dinoparc.EtwinDinoparcDinoz;
import net.eternaltwin.dinoparc.EtwinDinoparcUser;
import net.eternaltwin.dinoparc.GetDinoparcDinozOptions;
import net.eternaltwin.dinoparc.GetDinoparcUserOptions;
import net.eternaltwin.dinoparc.ShortDinoparcUser;
import net.eternaltwin.link.DinoparcLink;
import net.eternaltwin.link.VersionedDinoparcLink;
import net.eternaltwin.link.VersionedLinks;
import net.eternaltwin.user.MaybeCompleteUser;
import net.eternaltwin.user.UserId;

@Component
public final class MigrationUtils {
  public static final Duration DINOZ_LIST_MAX_AGE = Duration.ofHours(72);
  private final EtwinClient etwinClient;

  public MigrationUtils(final EtwinClient etwinClient) {
    this.etwinClient = etwinClient;
  }

  public Map<DinoparcDinozId, EtwinDinoparcDinoz> importUser(final ImportOptions options) throws ImportCheckException {
    
    final DinoparcLink link = this.getDinoparcLink(options.userId, options.server).orElseThrow(() -> new MissingDinoparcLinkException(options.userId, options.server));
    final ShortDinoparcUser shortDparcUser = link.getUser();
    final GetDinoparcUserOptions getDparcUserOpts = new GetDinoparcUserOptions(shortDparcUser.getServer(), shortDparcUser.getId());
    final EtwinDinoparcUser dparcUser = this.etwinClient.getDinoparcUser(Auth.GUEST, getDparcUserOpts);
    final LatestTemporal<List<DinoparcDinozIdRef>> dinozList = dparcUser.getDinoz();
    
    if (dinozList == null) {
      throw new MissingDinozListException(shortDparcUser);
    }
    
    final Instant now = Instant.now();
    {
      final Instant dinozListRetrieval = dinozList.getLatest().getRetrieved().getLatest();
      final Duration dinozListAge = Duration.between(dinozListRetrieval, now);

      if (dinozListAge.isNegative()) {
        throw new AssertionError("Retrieval time should be in the past");
      }
      
      if (!options.allowStaleDinozList && dinozListAge.compareTo(DINOZ_LIST_MAX_AGE) > 0) {
        throw new StaleDinozListException(shortDparcUser);
      }
    }
    
    return this.getDinozList(dparcUser.getServer(), dinozList.getLatest().getValue().stream().map(DinoparcDinozIdRef::getId));
  }

  private Optional<DinoparcLink> getDinoparcLink(final UserId userId, final DinoparcServer server) {
    final MaybeCompleteUser user = this.etwinClient.getUser(Auth.GUEST, userId);
    final VersionedLinks links = user.getLinks();
    final VersionedDinoparcLink vlink = switch (server) {
      
      case DinoparcCom -> links.getDinoparcCom();
      case EnDinoparcCom -> links.getEnDinoparcCom();
      case SpDinoparcCom -> links.getSpDinoparcCom();
      
      default -> throw new IllegalArgumentException("UnexpectedDinoparcServer");
    };
    
    return Optional.ofNullable(vlink.getCurrent());
  }

  private Map<DinoparcDinozId, EtwinDinoparcDinoz> getDinozList(final DinoparcServer server,
      final Stream<DinoparcDinozId> dinozIds) {
    final HashMap<DinoparcDinozId, EtwinDinoparcDinoz> dinoz = new HashMap<>();
    
    dinozIds.forEach((DinoparcDinozId did) -> {
      final GetDinoparcDinozOptions options = new GetDinoparcDinozOptions(server, did);
      EtwinDinoparcDinoz d = this.etwinClient.getDinoparcDinoz(Auth.GUEST, options);
      dinoz.put(did, d);
    });
    
    return dinoz;
  }
}
