package com.dinoparc.api.services;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dinoparc.api.repository.PgMessageRepository;
import kotlin.Pair;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.dinoparc.api.controllers.dto.MessageDto;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.account.Message;
import com.dinoparc.api.repository.PgPlayerRepository;


@Component
public class MessageService {
    private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    private final PgMessageRepository messageRepository;
    private final PgPlayerRepository playerRepository;
    private final AccountService accountService;

    @Autowired
    public MessageService(PgMessageRepository messageRepository,
                          PgPlayerRepository playerRepository,
                          AccountService accountService) {
        this.messageRepository = messageRepository;
        this.playerRepository = playerRepository;
        this.accountService = accountService;
    }

    public Message sendMessage(Player account, MessageDto messageRequest) {
        Player destinationAccount;
        try {
            destinationAccount = playerRepository.findById(messageRequest.getAccountDestination()).get();
        } catch (Exception e){
            return null;
        }
        if (account.getBlackList().containsKey(messageRequest.getAccountDestination())) {
            return null; // destination user is in the black list of this account

        } else if (destinationAccount.getBlackList().containsKey(account.getId())) {
            return null; // destination user has this account in their black list
        }

        String date = ZonedDateTime.now(ZoneId.of("Europe/Paris")).format(formatter);
        Message message = new Message(false, messageRequest.getAccountDestination(), messageRequest.getMessageBody(),
                date, account.getId() );

        messageRepository.save(message);
        playerRepository.updateMessages(destinationAccount.getId(), true);

        return message;
    }

    public Map<String, Pair<String, List<Message>>> getMessages(String accountId) {
        List<Message> messages = messageRepository.findByAccountDestinationOrAccountSender(accountId);

        // Grouping array response into Map with a key for each foreign player
        Map<String, ArrayList<Message>> groupedMessages = new HashMap<>();

        for (var message : messages) {
            String foreignAccountId;
            if (message.getAccountSender().equals(accountId)) {
                foreignAccountId = message.getAccountDestination();
            } else {
                foreignAccountId = message.getAccountSender();
            }
            if (groupedMessages.keySet().contains(foreignAccountId)) {
                groupedMessages.get(foreignAccountId).add(message);
            } else {
                ArrayList<Message> newForeignPlayerArray = new ArrayList<Message>();
                newForeignPlayerArray.add(message);
                groupedMessages.put(foreignAccountId, newForeignPlayerArray);
            }
        }

        // Adding destination player name property to map
        Map<String, Pair<String, List<Message>>> messagesByUserName = new HashMap<>();

        for (var entry : groupedMessages.entrySet()) {
            // Can fail if account is not in the accounts list (eg: account deleted). So we just continue
            // and don't include them in the response
            try {
                Collections.sort(entry.getValue(), Comparator.comparing(Message::getNumericalDate));
                Collections.reverse(entry.getValue());

                Pair<String, List<Message>> pair = new Pair(
                        playerRepository.findById(entry.getKey()).get().getName(),
                        groupedMessages.get(entry.getKey()));

                messagesByUserName.put(entry.getKey(), pair);

            } catch (Exception e) {
                continue;
            }
        }
        return messagesByUserName;
    }

    public boolean readMessages(String accountId, ArrayList<String> accountsIdToRead) {
        accountsIdToRead.forEach( senderAccountId -> {
            List<Message> messages = messageRepository.findByAccountDestinationAndSeenAndAccountSender(accountId, false, senderAccountId);

            messages.forEach( message -> {
                messageRepository.setPlayerSeen(message);
            });
        });
        return true;
    }

    public boolean deleteMessages(Player account, ArrayList<String> accountsIdTodelete) {
        accountsIdTodelete.forEach( accountId -> {
            messageRepository.deleteByAccountDestination(accountId);
        });
        return true;
    }

    public ByteArrayInputStream downloadThread(String accountId, String partnerId) {
        List<Message> messagesThread = messageRepository.findByAccountDestinationAndAccountSender(accountId, partnerId);
        messagesThread.addAll(messageRepository.findByAccountDestinationAndAccountSender(partnerId, accountId));
        messagesThread.sort((m1, m2) -> m2.getNumericalDate().compareTo(m1.getNumericalDate()));
        final CSVFormat format = CSVFormat.DEFAULT.withQuoteMode(QuoteMode.MINIMAL);

        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
             CSVPrinter csvPrinter = new CSVPrinter(new PrintWriter(out), format)) {
            csvPrinter.printRecord(Arrays.asList(
                    "Date",
                    "Name",
                    "Message"
            ));

            for (Message message : messagesThread) {
                String senderName;
                if (message.getAccountSender().equals(accountId)) {
                    senderName = accountService.getAccountById(accountId).get().getName();
                } else {
                    senderName = accountService.getAccountById(partnerId).get().getName();
                }
                List<String> data = Arrays.asList(
                        message.getNumericalDate(),
                        senderName,
                        message.getBody()
                );
                csvPrinter.printRecord(data);
            }
            csvPrinter.flush();
            return new ByteArrayInputStream(out.toByteArray());

        } catch (IOException e) {
            throw new RuntimeException("fail to import data to CSV file: " + e.getMessage());
        }
    }
}
