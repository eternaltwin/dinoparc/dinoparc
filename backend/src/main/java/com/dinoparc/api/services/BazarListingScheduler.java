package com.dinoparc.api.services;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.bazar.BazarListing;
import com.dinoparc.api.repository.*;
import kotlin.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
@EnableScheduling
public class BazarListingScheduler {
    private final PgBazarRepository bazarRepository;
    private final PgInventoryRepository inventoryRepository;
    private final PgPlayerRepository playerRepository;
    private final PgPlayerStatRepository playerStatRepository;
    private final PgDinozRepository dinozRepository;

    private Long nextExpiration;

    @Autowired
    public BazarListingScheduler (
            PgBazarRepository bazarRepository,
            PgInventoryRepository inventoryRepository,
            PgPlayerRepository playerRepository,
            PgPlayerStatRepository playerStatRepository, PgDinozRepository dinozRepository) {

        this.bazarRepository = bazarRepository;
        this.inventoryRepository = inventoryRepository;
        this.playerRepository = playerRepository;
        this.playerStatRepository = playerStatRepository;
        this.dinozRepository = dinozRepository;
        this.nextExpiration = null;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void start() {
        processBids();
        this.scheduleTimer();
    }

    private void scheduleTimer() {
        Long delay = this.nextExpiration - ZonedDateTime.now(ZoneId.of("Europe/Paris")).toEpochSecond();
        if (delay <= 0) {
            delay = 1l;
        }

        new java.util.Timer().schedule(new java.util.TimerTask() {
            @Override
            public void run() {
                processBids();
                scheduleTimer();
            }}, (delay * 1000)
        );
    }

    private void processBids() {
        Long nowInEpochSeconds = ZonedDateTime.now(ZoneId.of("Europe/Paris")).toEpochSecond();
        for (BazarListing bazarListing : bazarRepository.findAllActive()) {
            if (bazarListing.getEndDateInEpochSeconds() < nowInEpochSeconds) {
                if (bazarListing.getLastBiderId() == null) {
                    bazarListing.setLastBiderId(bazarListing.getSellerId());
                    bazarListing.setLastBiderName(bazarListing.getLastBiderName());
                }

                if (!bazarListing.getSellerId().contains("NPC_")) {
                    Player sellerAccount = playerRepository.findById(bazarListing.getSellerId()).get();
                    Player buyerAccount = playerRepository.findById(bazarListing.getLastBiderId()).get();
                    if (bazarListing.getSellerId().equalsIgnoreCase(bazarListing.getLastBiderId())) {
                        processUnsoldListing(bazarListing, sellerAccount);
                    } else {
                        processSoldListing(bazarListing, sellerAccount, buyerAccount);
                    }

                } else {
                    if (!bazarListing.getLastBiderId().contains("NPC_")) {
                        processSoldListing(bazarListing, null, playerRepository.findById(bazarListing.getLastBiderId()).get());
                    } else {
                        bazarRepository.deleteById(bazarListing.getId());
                    }
                }
            }
        }
        this.nextExpiration = getNextExpiration();
    }

    private Long getNextExpiration() {
        Long nextListingTimestamp =  ZonedDateTime.now(ZoneId.of("Europe/Paris")).plusDays(365).toEpochSecond();
        Boolean isBazarEmpty = true;

        Optional<BazarListing> lastActive = bazarRepository.getNextActive();
        if (lastActive.isPresent() && lastActive.get().getEndDateInEpochSeconds() < nextListingTimestamp) {
            isBazarEmpty = false;
            nextListingTimestamp = lastActive.get().getEndDateInEpochSeconds();
        }

        if (isBazarEmpty) {
            return ZonedDateTime.now(ZoneId.of("Europe/Paris")).plusDays(3).toEpochSecond();
        }
        return nextListingTimestamp;
    }

    private void processSoldListing(BazarListing bazarListing, Player sellerAccount, Player buyerAccount) {
        if (listingContainsAtLeastOneDinoz(bazarListing)) {
            playerRepository.addDinoz(buyerAccount.getId(), bazarListing.getDinozId());
            dinozRepository.setBazar(bazarListing.getDinozId(), false);
            dinozRepository.setMasterInfos(bazarListing.getDinozId(), buyerAccount.getId(), buyerAccount.getName());
        }

        for (String soldObject : bazarListing.getObjects().keySet()) {
            if (bazarListing.getObjects().get(soldObject) != null && bazarListing.getObjects().get(soldObject) > 0) {
                inventoryRepository.addInventoryItem(buyerAccount.getId(), soldObject, bazarListing.getObjects().get(soldObject));
            }
        }

        if (sellerAccount != null) {
            inventoryRepository.addInventoryItem(sellerAccount.getId(), DinoparcConstants.BONS, bazarListing.getLastPrice());
            playerStatRepository.addStats(UUID.fromString(sellerAccount.getId()), null, List.of(new Pair(PgPlayerStatRepository.NB_BAZAR_SELL, 1)));
        }

        inventoryRepository.substractInventoryItem(buyerAccount.getId(), DinoparcConstants.BONS, bazarListing.getLastPrice());
        playerStatRepository.addStats(UUID.fromString(buyerAccount.getId()), null, List.of(new Pair(PgPlayerStatRepository.NB_BAZAR_BUY, 1)));

        bazarListing.setActive(false);
        bazarRepository.update(bazarListing);
    }

    private void processUnsoldListing(BazarListing bazarListing, Player sellerAccount) {
        if (listingContainsAtLeastOneDinoz(bazarListing)) {
            playerRepository.addDinoz(sellerAccount.getId(), bazarListing.getDinozId());
            dinozRepository.setBazar(bazarListing.getDinozId(), false);
            dinozRepository.setMasterInfos(bazarListing.getDinozId(), sellerAccount.getId(), sellerAccount.getName());
        }

        for (String unsoldObject : bazarListing.getObjects().keySet()) {
            if (bazarListing.getObjects().get(unsoldObject) > 0) {
                inventoryRepository.addInventoryItem(sellerAccount.getId(), unsoldObject, bazarListing.getObjects().get(unsoldObject));
            }
        }

        bazarListing.setActive(false);
        bazarRepository.unactive(bazarListing);
    }

    private boolean listingContainsAtLeastOneDinoz(BazarListing bazarListing) {
        return bazarListing.getDinozId() != null && !bazarListing.getDinozId().isEmpty();
    }
}
