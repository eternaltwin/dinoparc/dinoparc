package com.dinoparc.api.services;

import com.dinoparc.api.domain.mission.Reward;
import com.dinoparc.api.repository.PgCollectionRepository;
import com.dinoparc.api.repository.PgInventoryRepository;
import com.dinoparc.api.repository.PgPlayerRepository;
import com.dinoparc.api.repository.PgRewardRepository;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RewardService {
    private final PgCollectionRepository collectionRepository;
    private final PgInventoryRepository inventoryRepository;
    private final PgPlayerRepository playerRepository;
    private final PgRewardRepository rewardRepository;

    public RewardService(PgCollectionRepository collectionRepository, PgInventoryRepository inventoryRepository, PgPlayerRepository playerRepository, PgRewardRepository rewardRepository) {
        this.collectionRepository = collectionRepository;
        this.inventoryRepository = inventoryRepository;
        this.playerRepository = playerRepository;
        this.rewardRepository = rewardRepository;
    }

    public void applyReward(UUID playerId, UUID rewardId) {
        var reward = rewardRepository.get(rewardId);

        // Manage gold reward
        if (reward.getGoldAmount() != null) {
            playerRepository.updateCash(playerId.toString(), reward.getGoldAmount());
        }

        // Manage collection reward
        if (reward.getCollectionContent() != null) {
            var playerCollection = collectionRepository.getPlayerCollection(playerId);
            var playerEpicCollection = playerCollection.getEpicCollection();
            var itemAdded = false;
            for (String item : reward.getCollectionContent()) {
                if (!playerEpicCollection.contains(item)) {
                    playerEpicCollection.add(item);
                    itemAdded = true;
                }
            }
            if (itemAdded) {
                collectionRepository.updateEpicCollection(playerCollection.getId(), playerEpicCollection);
            }
        }

        // Manage inventory reward
        if (reward.getInventoryContent() != null) {
            for (var entry : reward.getInventoryContent().entrySet()) {
                inventoryRepository.addInventoryItem(playerId.toString(), entry.getKey(), entry.getValue());
            }
        }
    }

    public Reward getReward(UUID rewardId) {
        return rewardRepository.get(rewardId);
    }
}
