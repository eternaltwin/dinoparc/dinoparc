package com.dinoparc.api.services;

import com.dinoparc.api.domain.account.RandomCollection;
import com.dinoparc.api.repository.PgRecMissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RecMissionsService {

    private final PgRecMissionRepository recMissionRepository;

    @Autowired
    public RecMissionsService(PgRecMissionRepository recMissionRepository) {
        this.recMissionRepository = recMissionRepository;
    }

    public Integer getRandomMissionTypeWithWeights() {
            RandomCollection<Integer> randomElement = new RandomCollection<Integer>()
                    .add(65, 1)
                    .add(30, 2)
                    .add(5, 3);
            return randomElement.next();
    }
}
