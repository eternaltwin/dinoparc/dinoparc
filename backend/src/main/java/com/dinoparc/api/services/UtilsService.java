package com.dinoparc.api.services;

import com.dinoparc.api.configuration.NeoparcConfig;
import com.dinoparc.api.controllers.AccountController;
import com.dinoparc.api.controllers.dto.CasinoTokenCountDto;
import com.dinoparc.api.controllers.dto.HOFDinozDto;
import com.dinoparc.api.controllers.dto.PublicAccountDto;
import com.dinoparc.api.controllers.dto.WheelPrizeDto;
import com.dinoparc.api.domain.account.*;
import com.dinoparc.api.domain.clan.Clan;
import com.dinoparc.api.domain.clan.OccupationData;
import com.dinoparc.api.domain.clan.OccupationSummary;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.dinoz.DinozUtils;
import com.dinoparc.api.domain.dinoz.EventDinozDto;
import com.dinoparc.api.domain.misc.RaidBossInfo;
import com.dinoparc.api.domain.mission.PlayerMission;
import com.dinoparc.api.rand.Rng;
import com.dinoparc.api.repository.*;
import discord4j.core.spec.EmbedCreateSpec;
import discord4j.rest.util.Color;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.dinoparc.tables.PlayerStat.PLAYER_STAT;

@Component
@EnableScheduling
public class UtilsService {
  private static final int dinozPageSize = 30;

  public final NeoparcConfig neoparcConfig;
  private final PgPlayerRepository playerRepository;
  private final PgPlayerStatRepository playerStatRepository;
  private final PgDinozRepository dinozRepository;
  private final PgNewsRepository newsRepository;
  private final PgInventoryRepository inventoryRepository;
  private final AccountService accountService;
  private final PgBossRepository raidBossRepository;
  private final PgWildWistitiRepository wildWistitiRepository;
  private final PgClanRepository clanRepository;
  private final PgArmyDinozRepository armyDinozRepository;
  private final PgPlayerMissionRepository playerMissionRepository;
  private final PgHistoryRepository historyRepository;
  private final ClanService clanService;
  private final DiscordService discordService;

  private Boolean wistitiLogsNotYetPosted = true;
  private Iterator<Player> iterator;
  private Iterator<Player> iteratorX;
  private List<PublicAccountDto> generalRankingsList = new ArrayList<PublicAccountDto>();
  private List<PublicAccountDto> averageRankingsList = new ArrayList<PublicAccountDto>();
  private List<Integer> tournoisLieux = Arrays.asList(0, 3, 6, 10, 13);

  @Autowired
  public UtilsService(
          PgPlayerRepository playerRepository,
          PgPlayerStatRepository playerStatRepository, PgNewsRepository newsRepository,
          PgDinozRepository dinozRepository,
          PgInventoryRepository inventoryRepository,
          AccountService accountService,
          PgBossRepository raidBossRepository,
          PgWildWistitiRepository wildWistitiRepository,
          PgClanRepository clanRepository,
          PgArmyDinozRepository armyDinozRepository,
          PgPlayerMissionRepository playerMissionRepository,
          PgHistoryRepository historyRepository,
          ClanService clanService,
          NeoparcConfig neoparcConfig,
          DiscordService discordService) {

    this.playerRepository = playerRepository;
    this.playerStatRepository = playerStatRepository;
    this.newsRepository = newsRepository;
    this.dinozRepository = dinozRepository;
    this.inventoryRepository = inventoryRepository;
    this.raidBossRepository = raidBossRepository;
    this.wildWistitiRepository = wildWistitiRepository;
    this.accountService = accountService;
    this.clanRepository = clanRepository;
    this.armyDinozRepository = armyDinozRepository;
    this.playerMissionRepository = playerMissionRepository;
    this.historyRepository = historyRepository;
    this.clanService = clanService;
    this.neoparcConfig = neoparcConfig;
    this.discordService = discordService;

    if (wildWistitiRepository.getWildWistiti().getLife() <= 0) {
      wistitiLogsNotYetPosted = false;
    }
  }

  @Scheduled(cron = "0 0 0 * * ?", zone = "Europe/Paris")
  public void maintenanceGenerale() {
    wistitiLogsNotYetPosted = true;
    List<Player> accountsToUpdate = new ArrayList<>();
    Iterable<Player> allAccounts = playerRepository.findAll();

    // Maintenance générale pour tous les joueurs :
    for (Player account : allAccounts) {
      var playerDinozs = dinozRepository.findByPlayerId(account.getId());
      if (!playerDinozs.isEmpty()) {
        for (var playerDinoz : playerDinozs) {
          resetDailyActions(playerDinoz);
          toggleSpecialActions(playerDinoz);
          AccountService.applyMaraisStateByDay(playerDinoz);
          processCargouMilkRemoval(playerDinoz);

          if (Objects.nonNull(playerDinoz.isInTourney()) && playerDinoz.isInTourney() && !tournoisLieux.contains(playerDinoz.getPlaceNumber())) {
            dinozRepository.setInTournament(playerDinoz.getId(), false);
          }

          if (Objects.nonNull(account.getLastLogin()) && isPlayerActiveSince7Days(account.getLastLogin())) {
            dinozRepository.processDailyReset(playerDinoz.getId(), playerDinoz.getLife(), playerDinoz.getActionsMap(), playerDinoz.getLastFledEnnemy(), true);

          } else if (!account.isBlocked()) {
            processResetForInactiveDinoz(playerDinoz);
          }
        }

        this.refreshPoints(account);
        playerRepository.dailyReset(account);
        playerStatRepository.resetAllPlayerStat(UUID.fromString(account.getId()), PgPlayerStatRepository.NB_WISTITI_SKIP);
        playerStatRepository.resetAllPlayerStat(UUID.fromString(account.getId()), PgPlayerStatRepository.NB_DAILY_RAID_FIGHT);
      }

      //Put non stackable potions on account :
      Integer playerIrmasSurplusStock = inventoryRepository.getQty(account.getId(), DinoparcConstants.POTION_IRMA_SURPLUS);
      Integer surplusPotionsForPlayer = Math.floorDiv((60 * (750 - playerRepository.countDinoz(account.getId()))), 100);
      if (surplusPotionsForPlayer > 0 && playerIrmasSurplusStock < surplusPotionsForPlayer) {
        inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.POTION_IRMA_SURPLUS, (surplusPotionsForPlayer - playerIrmasSurplusStock));
      }

      //Remove completed missions and expired missions from player's list :
      for (PlayerMission currentMission : playerMissionRepository.getAllMissions(UUID.fromString(account.getId()))) {
        if (currentMission.isRewardClaimed() || ZonedDateTime.now(ZoneId.of("Europe/Paris")).toEpochSecond() >= currentMission.getExpirationDate() / 1000) {
          playerMissionRepository.delete(currentMission.getId());
        }
      }

      //Only include ACTIVE accounts with at least one Dinoz to the wistiti shuffle :
      if (Objects.nonNull(account.getNbPoints()) && account.getNbPoints() > 0 && (Objects.nonNull(account.getLastLogin()) && isPlayerActiveSince7Days(account.getLastLogin()))) {
        accountsToUpdate.add(account);
      }
    }

    // Visibilité des ouistitis sous le niveau 20 :
    List<Dinoz> allStealableWistitis = dinozRepository.getAllStealableWistitis();
    if (!allStealableWistitis.isEmpty()) {
      Integer randomIndex = new Random().nextInt(allStealableWistitis.size());
      Dinoz chosenWistitiOfTheDay = allStealableWistitis.get(randomIndex);
      if (ThreadLocalRandom.current().nextInt(1, 4 + 1) == 1) {
        dinozRepository.setDanger(chosenWistitiOfTheDay.getId(), ThreadLocalRandom.current().nextInt(1, 20));
      }
    }
    for (Dinoz stealableWistiti : allStealableWistitis) {
      if (stealableWistiti.getLife() <= 0 && stealableWistiti.isDinozIsActive()) {
        //Le ouistiti s'échappe :
        Player player = playerRepository.findPlayerByDinozId(stealableWistiti.getId());
        playerRepository.deleteDinoz(player.getId(), stealableWistiti.getId());
        dinozRepository.deleteById(stealableWistiti.getId());

        History homeHistoryEvent = new History();
        homeHistoryEvent.setPlayerId(player.getId());
        homeHistoryEvent.setIcon("hist_error.gif");
        homeHistoryEvent.setType("flea-wistiti");
        historyRepository.save(homeHistoryEvent);
      }
    }

    // Reset du ouistiti sauvage :
    Collections.shuffle(accountsToUpdate);
    List<List<Player>> groups = IntStream.range(0, accountsToUpdate.size())
            .boxed()
            .collect(Collectors.groupingBy(i -> i % 20))
            .values()
            .stream()
            .map(il -> il.stream().map(accountsToUpdate::get).collect(Collectors.toList()))
            .collect(Collectors.toList());

    List<String> groups1Ids = new ArrayList<String>();
    List<String> groups2Ids = new ArrayList<String>();
    List<String> groups3Ids = new ArrayList<String>();
    List<String> groups4Ids = new ArrayList<String>();
    List<String> groups5Ids = new ArrayList<String>();
    splitPlayersIntoFiveGroups(groups, groups1Ids, groups2Ids, groups3Ids, groups4Ids, groups5Ids);
    accountService.getHunterGroups().get(1).setAccountIds(groups1Ids);
    accountService.getHunterGroups().get(1).setHour(ThreadLocalRandom.current().nextInt(0, 6 + 1)); //7 creneaux au total en G1
    accountService.getHunterGroups().get(2).setAccountIds(groups2Ids);
    accountService.getHunterGroups().get(2).setHour(ThreadLocalRandom.current().nextInt(7, 13 + 1)); //7 creneaux au total en G2
    accountService.getHunterGroups().get(3).setAccountIds(groups3Ids);
    accountService.getHunterGroups().get(3).setHour(ThreadLocalRandom.current().nextInt(14, 18 + 1)); //5 creneaux au total en G3
    accountService.getHunterGroups().get(4).setAccountIds(groups4Ids);
    accountService.getHunterGroups().get(4).setHour(ThreadLocalRandom.current().nextInt(19, 21 + 1)); //3 creneaux au total en G4
    accountService.getHunterGroups().get(5).setAccountIds(groups5Ids);
    accountService.getHunterGroups().get(5).setHour(ThreadLocalRandom.current().nextInt(22, 23 + 1)); //2 creneaux au total en G5

    updateClansData();
    regenerateWistiti();
    EventFightService.wistitiWasCaughtToday = false;
    EventFightService.wistitiLogs.clear();

    // Clean army dinozs of every boss
    armyDinozRepository.cleanAll();

    // Try to compute raid boss
    computeRaidBoss(false);
    EventFightService.raidBossLogs.clear();

    // Clean stats
    AccountController.connectedToday.clear();
    AccountController.enrolledToday.clear();

    accountService.getLabyrintheValidRaces().clear();
    for (int race = 0; race <= 21; race++) {
      if (ThreadLocalRandom.current().nextInt(0, 7) == 0 || race == 11) {
        accountService.getLabyrintheValidRaces().add(String.valueOf(race));
      }
    }
  }

  private void processResetForInactiveDinoz(Dinoz playerDinoz) {
    if (playerDinoz.getLife() <= 0 && ThreadLocalRandom.current().nextInt(1, 10) == 1) {
      playerDinoz.setLife(100);
      if (playerDinoz.getDanger() > 1) {
        playerDinoz.setDanger(playerDinoz.getDanger() - (playerDinoz.getDanger() / 10));
      } else {
        playerDinoz.setDanger(1);
      }
    } else {
      playerDinoz.setDanger(playerDinoz.getDanger() + 1);
      if (playerDinoz.getRace().equalsIgnoreCase(DinoparcConstants.OUISTITI) && playerDinoz.getLevel() < 20) {
        playerDinoz.setPlaceNumber(ThreadLocalRandom.current().nextInt(0, 4 + 1));
        playerDinoz.setDanger(-200);
      }
    }
    dinozRepository.processDailyResetForInactiveDinoz(
            playerDinoz.getId(),
            playerDinoz.getLife(),
            playerDinoz.getActionsMap(),
            playerDinoz.getLastFledEnnemy(),
            playerDinoz.getDanger(),
            playerDinoz.getPlaceNumber());
  }

  private void updateClansData() {
    for (Clan clan : clanRepository.getAllClans()) {
      if (clan.getMembers().size() > 0) {
        Integer nbDinozOfClan = 0;
        Integer nbWarriorDinozOfClan = 0;
        for (String playerId : clan.getMembers()) {
          nbDinozOfClan = nbDinozOfClan + playerRepository.countDinoz(playerId);
          nbWarriorDinozOfClan = nbWarriorDinozOfClan
                  + dinozRepository.findByPlayerId(playerId)
                  .stream()
                  .filter(dinoz -> (
                                  dinoz.getMalusList().contains(DinoparcConstants.FACTION_1_GUERRIER)
                                  || dinoz.getMalusList().contains(DinoparcConstants.FACTION_2_GUERRIER)
                                  || dinoz.getMalusList().contains(DinoparcConstants.FACTION_3_GUERRIER)
                          )
                  )
                  .toList()
                  .size();
        }
        clanRepository.updateClanNbOfDinoz(clan, nbDinozOfClan);
        clanRepository.updateClanNbOfWarriorDinoz(clan, nbWarriorDinozOfClan);

        if (!clan.getMembers().contains(clan.getCreatorId())) {
          clanRepository.updateChef(clan.getId(), clan.getMembers().get(0));
        }
      } else {
        clanRepository.deleteGhostClan(clan.getId());
      }
    }
  }

  @Scheduled(cron = "0 0 * * * ?", zone = "Europe/Paris")
  public void resetWistitiStatsNextHour() {
    AccountService.wistitiCheckers.clear();
    EventDinozDto eventDinozDto = wildWistitiRepository.getWildWistiti();

    Integer currentHourInDay = ZonedDateTime.now(ZoneId.of("Europe/Paris")).getHour();
    if (eventDinozDto.getLife() > 0) {
      Integer randomLife = 500 + (currentHourInDay * (200));

      Integer fireCharms = 99999;
      Integer woodCharms = 99999;
      Integer waterCharms = 99999;
      Integer thunderCharms = 99999;
      Integer airCharms = 99999;

      switch (ThreadLocalRandom.current().nextInt(1, 10 + 1)) {
        case 1 :
          fireCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);
          woodCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          break;
        case 2 :
          fireCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          waterCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          break;
        case 3 :
          fireCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          thunderCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          break;
        case 4 :
          fireCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          airCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          break;
        case 5 :
          woodCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          waterCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          break;
        case 6 :
          woodCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          thunderCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          break;
        case 7 :
          woodCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          airCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          break;
        case 8 :
          waterCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          thunderCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          break;
        case 9 :
          waterCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          airCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          break;
        case 10 :
          thunderCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          airCharms = ThreadLocalRandom.current().nextInt(3, 7 + 1);;
          break;
        default :
          break;
      }

      wildWistitiRepository.resetWildWistiti(eventDinozDto.getAppearanceCode(), randomLife, eventDinozDto.getFightCount(),
              fireCharms, woodCharms, waterCharms, thunderCharms, airCharms);
    }
  }

  @Scheduled(cron = "0 5 * * * *", zone = "Europe/Paris")
  public void publishWistitiChecksAt5MinutesAfterHour() {
    EventDinozDto eventDinozDto = wildWistitiRepository.getWildWistiti();
    if (eventDinozDto.getLife() > 0) {
        discordService.postMessage(DinoparcConstants.CHANNEL_CERBERE, "Players that checked the jungle : " + AccountService.wistitiCheckers);
        wistitiLogsNotYetPosted = true;

    } else if (wistitiLogsNotYetPosted) {
      sendDiscordMessageForWistitiLogs("Logs du Ouistiti : \n" + String.join(",", EventFightService.wistitiLogs));
      wistitiLogsNotYetPosted = false;
    }
  }

  @Scheduled(cron = "0 0 12 * * MON", zone = "Europe/Paris")
  public void resetRaidBossEverySundayNight() {
    computeRaidBoss(true);
  }

  @Scheduled(cron = "0 0 * * * ?", zone = "Europe/Paris")
  public void moveMerchantAroundMap() {
    accountService.changeMerchantLocation(getRandomLocation());
  }

  @Scheduled(cron = "0 0/5 * 1/1 * ?", zone = "Europe/Paris")
  public void refreshAllClansNumberOfDinozAndNumberOfWarriorDinoz() {
    //This auto-updater runs every 5 minutes :
    updateClansData();
  }

  @Scheduled(cron = "0 0/1 * 1/1 * ?", zone = "Europe/Paris")
  public void spawnPVEWaveAndRefreshClansOccupationSummary() {
    //This auto-updater runs every minute :
    if (DinoparcConstants.WAR_MODE) {
      List<Dinoz> newPVEWarriors = new ArrayList<>();
      if ((clanService.occupationSummary == null || clanService.occupationSummary.getAlivePVEWarEnnemies().isEmpty()) && ThreadLocalRandom.current().nextInt(1, 4 + 1) == 1) {
        for (int i = 1; i <= 200; i++) {
          Dinoz newPVEWarriorEnnemy = clanService.createRandomPVEWarriorDinoz();
          newPVEWarriors.add(newPVEWarriorEnnemy);
        }
      }
      clanService.computeOccupationSummary(newPVEWarriors);
    }
  }

  @Scheduled(cron = "0 0/1 * 1/1 * ?", zone = "Europe/Paris")
  public void giveTerritoryPointsToOccupyingClans() {
    if (DinoparcConstants.WAR_MODE) {
      //This auto-updater runs every minute :
      OccupationSummary occupationSummary = clanService.occupationSummary;
      if (Objects.nonNull(occupationSummary)) {
        for (OccupationData location : occupationSummary.getOccupationData()) {
          if (Objects.nonNull(location.getPercentage()) && Float.parseFloat(location.getPercentage()) > 50) {
            for (Clan clan : clanRepository.getAllClans().stream().filter(clan -> alliedClanDeserveTerritoryPoints(location, clan)).toList()) {
              clanRepository.addClanPointsOccupation(clan.getId(), 1);
            }
          }
        }
      }
    }
  }

//  @Scheduled(cron = "0 0 * * * ?", zone = "Europe/Paris")
//  public void moveHalloweenHordesAroundMap() {
//    accountService.changeHalloweenHordesLocation(getRandomLocationLight());
//  }

  private void sendDiscordMessageForWistitiLogs(String wistitiLogs) {
    EmbedCreateSpec embedDiscordMessage = EmbedCreateSpec
            .builder()
            .color(Color.VIVID_VIOLET)
            .thumbnail("https://i.ibb.co/3NbvRdr/zone17.gif")
            .title("Résultats de la chasse au Ouistiti")
            .description(wistitiLogs)
            .footer("Généré par le serveur Neoparc", "https://i.ibb.co/bB74rVG/hist-reset.gif")
            .build();

    discordService.postEmbedMessage(DinoparcConstants.CHANNEL_CERBERE, embedDiscordMessage);
  }

  public Long getEventCounter(String playerId) {
    return playerStatRepository.getCounter(UUID.fromString(playerId), PLAYER_STAT.EVENT_COUNTER.getName());
  }

  public Integer getRandomLocation() {
    return ThreadLocalRandom.current().nextInt(0, 37 + 1);
  }

  public Integer getRandomLocationLight() {
    if (ThreadLocalRandom.current().nextBoolean()) {
      return ThreadLocalRandom.current().nextInt(0, 37 + 1);
    }
    return ThreadLocalRandom.current().nextInt(0, 14 + 1);
  }

  private void regenerateWistiti() {
    String wildWistitiAppCode = "F" + DinozUtils.getRandomHexString();

    wildWistitiRepository.resetWildWistiti(
            wildWistitiAppCode,
            500,
            0,
            ThreadLocalRandom.current().nextInt(4, 9),
            ThreadLocalRandom.current().nextInt(4, 9),
            ThreadLocalRandom.current().nextInt(4, 9),
            ThreadLocalRandom.current().nextInt(4, 9),
            ThreadLocalRandom.current().nextInt(4, 9));
  }

  public void computeRaidBoss(boolean adminAction) {
    Calendar cal = Calendar.getInstance(Locale.FRANCE);
    var life = DinoparcConstants.RAID_BOSS_MAX_LIFE;
    var bonuses = 2500;

    if (adminAction || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
      RaidBossInfo raidInfos = getNextRaidBossInfos();
      raidBossRepository.resetRaidBoss(raidInfos.getPlaceNumber(), raidInfos.getDayOfWeek(), raidInfos.getHourOfDay(),
              "B" + DinozUtils.getRandomHexString() + "#", 0,
              110, 110, 110, 110, 110,
              life,
              bonuses, bonuses, bonuses, bonuses, bonuses, bonuses,
              raidInfos.getFirstElement(), raidInfos.getSecondElement(), raidInfos.getThirdElement());
    } else {
      raidBossRepository.reviveRaidBoss(life, bonuses);
    }
  }

  public Integer getRandomPrizeAtCasino(Player player, Integer jeton) {
    switch (jeton) {
      case 1 :
        //Code bronze :
        if (inventoryRepository.getQty(player.getId(), DinoparcConstants.JETON_CASINO_BRONZE) >= 1) {
          inventoryRepository.substractInventoryItem(player.getId(), DinoparcConstants.JETON_CASINO_BRONZE, 1);

          RandomCollection<Integer> randomElement = new RandomCollection<Integer>()
                  .add(60, 0)
                  .add(30, 2)
                  .add(9, 3)
                  .add(1, 10);
          Integer factorDrawn = randomElement.next();
          playerRepository.updateCash(player.getId(), (factorDrawn * 5000));

          return switch (factorDrawn) {
            case 2 -> 1;
            case 3 -> 2;
            case 10 -> 3;
            default -> 0;
          };
        } else {
          return -1;
        }

      case 2 :
        //Code argent :
        if (inventoryRepository.getQty(player.getId(), DinoparcConstants.JETON_CASINO_ARGENT) >= 1) {
          inventoryRepository.substractInventoryItem(player.getId(), DinoparcConstants.JETON_CASINO_ARGENT, 1);

          RandomCollection<Integer> randomElement = new RandomCollection<Integer>()
                  .add(60, 0)
                  .add(30, 2)
                  .add(9, 3)
                  .add(1, 10);
          Integer factorDrawn = randomElement.next();
          playerRepository.updateCash(player.getId(), (factorDrawn * 25000));

          return switch (factorDrawn) {
            case 2 -> 1;
            case 3 -> 2;
            case 10 -> 3;
            default -> 0;
          };
        } else {
          return -1;
        }

      case 3 :
        //Code or :
        if (inventoryRepository.getQty(player.getId(), DinoparcConstants.JETON_CASINO_OR) >= 1) {
          inventoryRepository.substractInventoryItem(player.getId(), DinoparcConstants.JETON_CASINO_OR, 1);

          RandomCollection<Integer> randomElement = new RandomCollection<Integer>()
                  .add(60, 0)
                  .add(30, 2)
                  .add(9, 3)
                  .add(1, 10);
          Integer factorDrawn = randomElement.next();
          playerRepository.updateCash(player.getId(), (factorDrawn * 500000));

          return switch (factorDrawn) {
            case 2 -> 1;
            case 3 -> 2;
            case 10 -> 3;
            default -> 0;
          };
        } else {
          return -1;
        }
    }
    return -1;
  }

  public CasinoTokenCountDto getCasinoTokenCountForPlayer(String playerId) {
    Integer bronzeCount = inventoryRepository.getQty(playerId, DinoparcConstants.JETON_CASINO_BRONZE);
    Integer silverCount = inventoryRepository.getQty(playerId, DinoparcConstants.JETON_CASINO_ARGENT);
    Integer goldCount = inventoryRepository.getQty(playerId, DinoparcConstants.JETON_CASINO_OR);

    return new CasinoTokenCountDto(bronzeCount, silverCount, goldCount);
  }

  @Scheduled(fixedRate = 300000)
  public void fetchGeneralRankingsEvery5Minutes() {
    this.generalRankingsList.clear();
    Iterable<Player> result = playerRepository.findAll();
    this.iterator = result.iterator();

    while (iterator.hasNext()) {
      Player next = iterator.next();
      var playerNbDinoz = playerRepository.countDinoz(next.getId());

      if (playerNbDinoz > 0) {
        this.refreshPoints(next);
      }

      PublicAccountDto dto = new PublicAccountDto();
      dto.setNbPoints(next.getNbPoints());
      dto.setId(next.getId());
      dto.setName(next.getName());
      dto.setNbDinoz(playerNbDinoz);

      if (Objects.nonNull(next.getLastLogin())) {
        dto.setActive(isPlayerActiveSince7Days(next.getLastLogin()));
      } else {
        dto.setActive(false);
      }
      this.generalRankingsList.add(dto);
    }
    this.generalRankingsList.sort(Comparator.comparingDouble(PublicAccountDto::getNbPoints).reversed());
  }

  public static Boolean isPlayerActiveSince7Days(Long timestamp) {
    return (ZonedDateTime.now(ZoneId.of("Europe/Paris")).toEpochSecond() - timestamp) < 604800;
  }

  public Boolean exchangeAllPrizesAtDinotownWheel(Player player) {
    Integer giftQty = inventoryRepository.getQty(player.getId(), DinoparcConstants.GIFT);
    if (giftQty > 0) {
      for (int i = giftQty; i > 0; i--) {
        getRandomChristmasPrize(player);
      }
      return true;
    }
    return false;
  }

  public WheelPrizeDto getRandomChristmasPrize(Player account) {
    RandomCollection<Object> randomElement = new RandomCollection<Object>()
            .add(10, DinoparcConstants.OEUF_SANTAZ)
            .add(3, DinoparcConstants.OEUF_COBALT)
            .add(10, DinoparcConstants.COINS)
            .add(10, DinoparcConstants.COULIS_CERISE)
            .add(40, DinoparcConstants.ETERNITY_PILL)
            .add(10, DinoparcConstants.COINS)
            .add(3, DinoparcConstants.OEUF_GLUON)
            .add(3, DinoparcConstants.OEUF_SERPANTIN)
            .add(1, DinoparcConstants.OEUF_FEROSS)
            .add(10, DinoparcConstants.COINS);

    String consumableDrawn = (String) randomElement.next();
    WheelPrizeDto prizeDrawn = mapPrizeDrawnToWheelNumber(consumableDrawn);

    if (consumableDrawn.equals(DinoparcConstants.COINS)) {
      prizeDrawn.setQtee(accountService.getRandomAmountOfGoldWonForEgg());
      playerRepository.updateCash(account.getId(), prizeDrawn.getQtee());
    } else {
      inventoryRepository.addInventoryItem(account.getId(), consumableDrawn, 1);
    }
    inventoryRepository.substractInventoryItem(account.getId(), DinoparcConstants.GIFT, 1);
    return prizeDrawn;
  }

  private WheelPrizeDto mapPrizeDrawnToWheelNumber(String consumableDrawn) {
    switch (consumableDrawn) {
      case DinoparcConstants.BIERE:
        return new WheelPrizeDto(0, 1);
      case DinoparcConstants.OEUF_COBALT:
        return new WheelPrizeDto(1, 1);
      case DinoparcConstants.COINS:
        return new WheelPrizeDto(getRandomIntegerForGoldPrizeId(), 1);
      case DinoparcConstants.COULIS_CERISE:
        return new WheelPrizeDto(3, 1);
      case DinoparcConstants.ETERNITY_PILL:
        return new WheelPrizeDto(4, 1);
      case DinoparcConstants.OEUF_GLUON:
        return new WheelPrizeDto(6, 1);
      case DinoparcConstants.OEUF_SERPANTIN:
        return new WheelPrizeDto(7, 1);
      case DinoparcConstants.OEUF_FEROSS:
        return new WheelPrizeDto(8, 1);

     default:
        return null;
    }
  }

  public int getRandomIntegerForGoldPrizeId() {
    List<Integer> list = new ArrayList<>(Arrays.asList(2, 5, 9));
    return list.get(new Random().nextInt(list.size()));
  }

  private void refreshPoints(Player account) {
    Integer sumOfPoints = 0;
    List<Dinoz> playerDinoz = this.getAllDinozOfAccount(account);
    int nbDinozs = playerDinoz.isEmpty() ? 1 : playerDinoz.size();

    for (Dinoz dinoz : playerDinoz) {
      sumOfPoints += dinoz.getLevel();
    }

    account.setNbPoints(sumOfPoints);
    account.setAverageLevelPoints(sumOfPoints / nbDinozs);
  }

  private void toggleSpecialActions(Dinoz dinoz) {
    switch (dinoz.getPlaceNumber()) {
      case 4:
        // Barrage
        dinoz.getActionsMap().put(DinoparcConstants.BARRAGE, true);
        break;

      case 7:
        // Porte de Granit
        dinoz.getActionsMap().put(DinoparcConstants.PASSAGEGRANIT, true);
        break;

      case 11:
        // Port de la Prune
        dinoz.getActionsMap().put(DinoparcConstants.ACT_ESCALADER, true);
        dinoz.getActionsMap().put(DinoparcConstants.ACT_NAVIGUER, true);
        break;

      case 16:
        // Marais Collant
        dinoz.getActionsMap().put(DinoparcConstants.ACT_ESCALADER, true);
        break;

      case 21:
        // Hutte du Vieux sage
        dinoz.getActionsMap().put(DinoparcConstants.ACT_ESCALADER, true);
        break;

      case 26:
        // Labyrinthe
        dinoz.getActionsMap().put(DinoparcConstants.PASSAGELABYRINTHE, true);
        break;

      case 33:
        // Lac Céleste :
        Player player = playerRepository.findPlayerByDinozId(dinoz.getId());
        if (!player.getHasClaimedTradeFromFishermanToday() && inventoryRepository.getQty(player.getId(), DinoparcConstants.RAMENS) > 0) {
          dinoz.getActionsMap().put(DinoparcConstants.INUIT, true);
        }
        break;

      case 36:
        // Cimetiere
        dinoz.getActionsMap().put(DinoparcConstants.COMBAT, false);
        break;

      default:
        break;
    }
  }

  private void resetDailyActions(Dinoz dinoz) {
    if (dinoz.getActionsMap().get(DinoparcConstants.POTION_IRMA) != null) {
      dinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, false);
    }

    dinoz.getActionsMap().put(DinoparcConstants.COMBAT, true);
    dinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.FOUILLER)
        && dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) != null
        && dinoz.getSkillsMap().get(DinoparcConstants.FOUILLER) > 0) {

      dinoz.getActionsMap().put(DinoparcConstants.FOUILLER, true);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.ROCK)
        && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) != null
        && dinoz.getSkillsMap().get(DinoparcConstants.ROCK) > 0) {

      dinoz.getActionsMap().put(DinoparcConstants.ROCK, true);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.BAIN_FLAMMES)
            && dinoz.getSkillsMap().get(DinoparcConstants.BAIN_FLAMMES) != null
            && dinoz.getSkillsMap().get(DinoparcConstants.BAIN_FLAMMES) > 0
            && dinoz.getLife() < 10) {

      dinoz.getActionsMap().put(DinoparcConstants.BAIN_FLAMMES, true);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.PÊCHE)
            && dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) != null
            && dinoz.getSkillsMap().get(DinoparcConstants.PÊCHE) > 0
            && DinoparcConstants.FISHING_LOCS.contains(dinoz.getPlaceNumber())) {

      dinoz.getActionsMap().put(DinoparcConstants.PÊCHE, true);
    }

    if (dinoz.getSkillsMap().containsKey(DinoparcConstants.CUEILLETTE)
            && dinoz.getSkillsMap().get(DinoparcConstants.CUEILLETTE) != null
            && dinoz.getSkillsMap().get(DinoparcConstants.CUEILLETTE) > 0
            && DinoparcConstants.PICKING_LOCS.contains(dinoz.getPlaceNumber())) {

      dinoz.getActionsMap().put(DinoparcConstants.CUEILLETTE, true);
    }

    if (dinoz.getMalusList().contains(DinoparcConstants.CHIKABUM) && dinoz.getLife() > 0) {
    	dinoz.setLife(dinoz.getLife() - 1);
    }

    if (dinoz.getMalusList().contains(DinoparcConstants.EMPOISONNE) && dinoz.getLife() > 4) {
      dinoz.setLife(dinoz.getLife() - 5);

    } else if (dinoz.getMalusList().contains(DinoparcConstants.EMPOISONNE) && dinoz.getLife() > 0 && dinoz.getLife() < 5) {
      dinoz.setLife(0);
    }

    if (dinoz.getLife() < 0) {
      dinoz.setLife(0);
    }

    if (dinoz.getRace().equalsIgnoreCase(DinoparcConstants.KORGON) && dinoz.getLife() > 0) {
        if (dinoz.getPassiveList().get(DinoparcConstants.CHARME_TERRE) == null || dinoz.getPassiveList().get(DinoparcConstants.CHARME_TERRE) == 0) {
            dinoz.getPassiveList().put(DinoparcConstants.CHARME_TERRE, 1);
            dinozRepository.setPassiveList(dinoz.getId(), dinoz.getPassiveList());
        }
    }

    dinoz.setLastFledEnnemy(null);
  }


  @Scheduled(fixedRate = 900000)
  public void fetchAverageRankingsEvery15Minutes() {
    this.averageRankingsList.clear();
    Iterable<Player> result = playerRepository.findAll();
    this.iteratorX = result.iterator();

    while (iteratorX.hasNext()) {
      Player next = iteratorX.next();
      var playerNbDinoz = playerRepository.countDinoz(next.getId());
      PublicAccountDto dto = new PublicAccountDto();

      if (playerNbDinoz > 0) {
        float totalPoints = Float.parseFloat(String.valueOf(next.getNbPoints()));
        float totalDinoz = Float.parseFloat(String.valueOf(playerNbDinoz));
        float m = totalPoints / totalDinoz;
        String moyenne = String.format("%.1f", m).replace(",", ".");
        dto.setAverageLevelPoints(Double.valueOf(moyenne));

      } else {
        dto.setAverageLevelPoints((double) 0);
      }

      dto.setId(next.getId());
      dto.setName(next.getName());
      this.averageRankingsList.add(dto);
    }

    this.averageRankingsList.sort(Comparator.comparingDouble(PublicAccountDto::getAverageLevelPoints).reversed());
  }

  public Integer getGeneralRankingsTotalPages() {
    if (this.generalRankingsList.size() % 30 == 0) {
      return this.generalRankingsList.size() / 30;

    } else {
      return (this.generalRankingsList.size() / 30) + 1;
    }
  }

  public List<PublicAccountDto> getGeneralRankingsAtGivenPage(Integer pageNumber) {
    Integer offset = pageNumber * 30;

    try {
      return this.generalRankingsList.subList(offset, offset + 30);
    } catch (IndexOutOfBoundsException e) {
      try {
        return this.generalRankingsList.subList(offset, this.generalRankingsList.size());

      } catch (Exception finalException) {
        return new ArrayList<PublicAccountDto>();
      }
    }
  }

  public Integer getUsernameRanking(String username) {
    Integer counter = 0;
    for (PublicAccountDto dto : this.generalRankingsList) {
      if (!dto.getName().equalsIgnoreCase(username)) {
        counter++;

      } else {
        break;
      }
    }

    if (counter == 0) {
      return 0;
    }

    if (counter == this.generalRankingsList.size()) {
      return 0;
    }

    return (counter / 30);
  }

  public Integer getUsernameRankingForProfile(String username) {
    Integer counter = 1;
    for (PublicAccountDto dto : this.generalRankingsList) {
      if (!dto.getName().equalsIgnoreCase(username)) {
        counter++;

      } else {
        break;
      }
    }

    return counter;
  }

  public void createANews(News news) {
    newsRepository.create(news);
  }

  public List<News> getLatestNews() {
    return newsRepository.findAll();
  }

  public List<PublicAccountDto> getAverageRankingsAtGivenPage(Integer pageNumber) {
    Integer offset = pageNumber * 30;

    try {
      return this.averageRankingsList.subList(offset, offset + 30);
    } catch (IndexOutOfBoundsException e) {
      try {
        return this.averageRankingsList.subList(offset, this.averageRankingsList.size());

      } catch (Exception finalException) {
        return new ArrayList<PublicAccountDto>();
      }
    }
  }

  public Integer getUsernameRankingX(String username) {
    Integer counter = 0;
    for (PublicAccountDto dto : this.averageRankingsList) {
      if (!dto.getName().equalsIgnoreCase(username)) {
        counter++;

      } else {
        break;
      }
    }

    if (counter == 0) {
      return 0;
    }

    if (counter == this.averageRankingsList.size()) {
      return 0;
    }

    return (counter / 30);
  }

  public PublicAccountDto getPublicAccountById(String userId, Integer pageNumber) {
    PublicAccountDto response = new PublicAccountDto();
    Optional<Player> account = playerRepository.findById(userId);

    if (account.isPresent()) {
      response.setId(userId);
      response.setName(account.get().getName());
      response.setNbDinoz(playerRepository.countDinoz(account.get().getId()));
      response.setNbPoints(account.get().getNbPoints());
      response.setPosition(getUsernameRankingForProfile(response.getName()));
      response.setActualHermitStage(account.get().getHermitStage());
      response.setCap(account.get().getAccountDinozLimit());
      if (clanRepository.getPlayerClan(userId).isPresent()) {
        response.setClanName(clanRepository.getPlayerClan(userId).get().getClanName());
      }
      if (account.get().getWistitiCaptured() != null) {
        response.setNbCaptures(account.get().getWistitiCaptured());
      } else {
        response.setNbCaptures(0);
      }
    } else {
      response = null;
    }

    return response;
  }

  public List<Dinoz> getAllDinozOfAccount(Player account) {
    return dinozRepository.findByPlayerId(account.getId());
  }

  public Integer getTotalPages(String userId) {
    var playerNbDinoz = playerRepository.countDinoz(userId);
    return playerNbDinoz % dinozPageSize == 0 ?
            (playerNbDinoz / dinozPageSize) :
            (playerNbDinoz / dinozPageSize) + 1;
  }

  public HashMap<String, String> getAllPlayerIds() {
      HashMap<String, String> playerIdentification = new HashMap<String, String>();
      playerRepository.findAll().forEach( account -> {
        playerIdentification.put(account.getId(), account.getName());
      });

      return playerIdentification;
  }

  public List<Integer> getBankValues() {
    List<Integer> bankValues = new ArrayList<Integer>();
    Rng rng = neoparcConfig.getRngFactory()
            .string("getBankValues")
            .string(String.valueOf(ZonedDateTime.now(ZoneId.of("Europe/Paris")).get(ChronoField.ALIGNED_WEEK_OF_YEAR)))
            .string(String.valueOf(ZonedDateTime.now(ZoneId.of("Europe/Paris")).get(ChronoField.YEAR)))
            .toRng();

    int randomBaseValue = rng.randIntBetween(5000, 6000);
    bankValues.add(rng.randIntBetween(randomBaseValue, randomBaseValue + 1000));
    bankValues.add(rng.randIntBetween(randomBaseValue - 1000, randomBaseValue));

    return bankValues;
  }

  public List<HOFDinozDto> getRaceRankings(String chapter) {
    return dinozRepository.getRanking(chapter);
  }

  private void splitPlayersIntoFiveGroups(List<List<Player>> groups, List<String> groups1Ids, List<String> groups2Ids, List<String> groups3Ids, List<String> groups4Ids, List<String> groups5Ids) {
    for (Player account : groups.get(0)) {
      playerRepository.updateHunterGroup(account.getId(), 1);
      groups1Ids.add(account.getId());
    }

    for (Player account : groups.get(1)) {
      playerRepository.updateHunterGroup(account.getId(), 1);
      groups1Ids.add(account.getId());
    }

    for (Player account : groups.get(2)) {
      playerRepository.updateHunterGroup(account.getId(), 1); //15% of players in G1
      groups2Ids.add(account.getId());
    }

    for (Player account : groups.get(3)) {
      playerRepository.updateHunterGroup(account.getId(), 2);
      groups2Ids.add(account.getId());
    }

    for (Player account : groups.get(4)) {
      playerRepository.updateHunterGroup(account.getId(), 2);
      groups3Ids.add(account.getId());
    }

    for (Player account : groups.get(5)) {
      playerRepository.updateHunterGroup(account.getId(), 2);
      groups3Ids.add(account.getId());
    }

    for (Player account : groups.get(6)) {
      playerRepository.updateHunterGroup(account.getId(), 2); //20% of players in G2
      groups4Ids.add(account.getId());
    }

    for (Player account : groups.get(7)) {
      playerRepository.updateHunterGroup(account.getId(), 3);
      groups4Ids.add(account.getId());
    }

    for (Player account : groups.get(8)) {
      playerRepository.updateHunterGroup(account.getId(), 3);
      groups5Ids.add(account.getId());
    }

    for (Player account : groups.get(9)) {
      playerRepository.updateHunterGroup(account.getId(), 3);
      groups5Ids.add(account.getId());
    }

    for (Player account : groups.get(10)) {
      playerRepository.updateHunterGroup(account.getId(), 3); //20% of players in G3
    }

    for (Player account : groups.get(11)) {
      playerRepository.updateHunterGroup(account.getId(), 4);
    }

    for (Player account : groups.get(12)) {
      playerRepository.updateHunterGroup(account.getId(), 4); //10% of players in G4
    }

    for (Player account : groups.get(13)) {
      playerRepository.updateHunterGroup(account.getId(), 5);
    }

    for (Player account : groups.get(14)) {
      playerRepository.updateHunterGroup(account.getId(), 5); //10% of players in G5
    }

    for (Player account : groups.get(15)) {
      playerRepository.updateHunterGroup(account.getId(), 6);
    }

    for (Player account : groups.get(16)) {
      playerRepository.updateHunterGroup(account.getId(), 6);
    }

    for (Player account : groups.get(17)) {
      playerRepository.updateHunterGroup(account.getId(), 6);
    }

    for (Player account : groups.get(18)) {
      playerRepository.updateHunterGroup(account.getId(), 6);
    }

    for (Player account : groups.get(19)) {
      playerRepository.updateHunterGroup(account.getId(), 6); //25% can't see
    }
  }

  private RaidBossInfo getNextRaidBossInfos() {
    RaidBossInfo raidBossInfo = new RaidBossInfo();
    ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
    Rng rng = neoparcConfig.getRngFactory()
            .string("getNextRaidBossInfos_V8")
            .string(getWeekOfYear())
            .string(String.valueOf(now.getYear()))
            .toRng();

    raidBossInfo.setDayOfWeek(rng.randIntBetween(5, 8));
    raidBossInfo.setHourOfDay(rng.randIntBetween(19, 23));
    raidBossInfo.setPlaceNumber(rng.randIntBetween(0, 23));
    List<String> possibleElements = new ArrayList(Arrays.asList(DinoparcConstants.FEU, DinoparcConstants.TERRE, DinoparcConstants.EAU, DinoparcConstants.FOUDRE, DinoparcConstants.AIR));

    int firstE = rng.randIntBetween(0, 5);
    raidBossInfo.setFirstElement(possibleElements.get(firstE));
    possibleElements.remove(firstE);

    int secondE = rng.randIntBetween(0, 4);
    raidBossInfo.setSecondElement(possibleElements.get(secondE));
    possibleElements.remove(secondE);

    int thirdE = rng.randIntBetween(0, 3);
    raidBossInfo.setThirdElement(possibleElements.get(thirdE));

    return raidBossInfo;
  }

  private String getWeekOfYear() {
    Calendar cal = Calendar.getInstance(Locale.FRANCE);
    return String.valueOf(cal.get(Calendar.WEEK_OF_YEAR));
  }

  private boolean alliedClanDeserveTerritoryPoints(OccupationData location, Clan clan) {
    Long warriorsAliveOnLocation = clanRepository.getAllWarriorDinozOfClan(clan.getId())
            .stream()
            .filter(warrior -> warrior.getLife() > 0)
            .filter(warrior -> new Integer(warrior.getPlaceNumber()).equals(Integer.parseInt(location.getLocation())))
            .count();
    return clan.getFaction().equalsIgnoreCase(location.getFaction()) && warriorsAliveOnLocation >= 1;
  }

  private void processCargouMilkRemoval(Dinoz dinoz) {
    if (dinoz.getMalusList().contains(DinoparcConstants.LAIT_DE_CARGOU)) {
      List<String> newMalusList = dinoz.getMalusList();
      newMalusList.remove(DinoparcConstants.LAIT_DE_CARGOU);
      dinozRepository.setMalusList(dinoz.getId(), newMalusList);
    }
  }
}