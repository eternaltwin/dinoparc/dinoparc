package com.dinoparc.api.services;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.RandomCollection;
import com.dinoparc.api.domain.misc.ChaudronCraftTryResult;
import com.dinoparc.api.repository.PgInventoryRepository;
import com.dinoparc.api.repository.PgPlayerRepository;
import org.springframework.stereotype.Component;

@Component
public class ChaudronService {

    private final PgPlayerRepository playerRepository;
    private final DiscordService discordService;

    public ChaudronService(PgPlayerRepository playerRepository, DiscordService discordService) {
        this.playerRepository = playerRepository;
        this.discordService = discordService;
    }

    public ChaudronCraftTryResult findMatchingRecipe(String playerId, PgInventoryRepository inventoryRepository, Integer recipeNumber) {
        Integer nbFish1 = inventoryRepository.getQty(playerId, DinoparcConstants.PERCHE_PERLEE);
        Integer nbFish2 = inventoryRepository.getQty(playerId, DinoparcConstants.GREMILLE_GRELOTTANTE);
        Integer nbFish3 = inventoryRepository.getQty(playerId, DinoparcConstants.CUBE_GLU);
        Integer nbPlant1 = inventoryRepository.getQty(playerId, DinoparcConstants.FEUILLE_PACIFIQUE);
        Integer nbPlant2 = inventoryRepository.getQty(playerId, DinoparcConstants.OREADE_BLANC);
        Integer nbPlant3 = inventoryRepository.getQty(playerId, DinoparcConstants.TIGE_RONCIVORE);
        Integer nbPlant4 = inventoryRepository.getQty(playerId, DinoparcConstants.ANÉMONE_SOLITAIRE);

        return processRecipe(inventoryRepository, playerId, recipeNumber, nbFish1, nbFish2, nbFish3, nbPlant1, nbPlant2, nbPlant3, nbPlant4);
    }

    private ChaudronCraftTryResult processRecipe(PgInventoryRepository inventoryRepository, String playerId, Integer recipeNumber,
                                             Integer nbFish1, Integer nbFish2, Integer nbFish3, Integer nbPlant1, Integer nbPlant2, Integer nbPlant3, Integer nbPlant4) {

        ChaudronCraftTryResult result = new ChaudronCraftTryResult();

        if (recipeNumber == 1 && nbFish1 >= 50 && nbFish2 >= 10 && nbPlant1 >= 50 && nbPlant2 >= 10) {
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.PERCHE_PERLEE, 50);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.GREMILLE_GRELOTTANTE, 10);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.FEUILLE_PACIFIQUE, 50);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.OREADE_BLANC, 10);

            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.NUAGE_BURGER, 10);
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.NUAGE_BURGER);
            result.setGeneratedObjectQty(10);

        } else if (recipeNumber == 2 && nbFish1 >= 75 && nbFish2 >= 15 && nbPlant1 >= 60 && nbPlant2 >= 20 && nbPlant3 >= 1) {
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.PERCHE_PERLEE, 75);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.GREMILLE_GRELOTTANTE, 15);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.FEUILLE_PACIFIQUE, 60);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.OREADE_BLANC, 20);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.TIGE_RONCIVORE, 1);

            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.TARTE_VIANDE, 10);
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.TARTE_VIANDE);
            result.setGeneratedObjectQty(10);

        } else if (recipeNumber == 3 && nbPlant1 >= 90 && nbPlant2 >= 60 && nbPlant3 >= 25) {
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.FEUILLE_PACIFIQUE, 90);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.OREADE_BLANC, 60);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.TIGE_RONCIVORE, 25);

            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.MEDAILLE_CHOCOLAT, 10);
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.MEDAILLE_CHOCOLAT);
            result.setGeneratedObjectQty(10);

        } else if (recipeNumber == 4 && nbPlant1 >= 20 && nbPlant2 >= 5 && nbPlant3 >= 3 && nbPlant4 >= 1) {
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.FEUILLE_PACIFIQUE, 20);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.OREADE_BLANC, 5);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.TIGE_RONCIVORE, 3);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.ANÉMONE_SOLITAIRE, 1);

            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.TISANE, 1);
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.TISANE);
            result.setGeneratedObjectQty(1);

        } else if (recipeNumber == 5 && nbFish2 >= 20 && nbFish3 >= 1 && nbPlant3 >= 20 && nbPlant4 >= 2) {
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.GREMILLE_GRELOTTANTE, 20);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.CUBE_GLU, 1);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.TIGE_RONCIVORE, 20);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.ANÉMONE_SOLITAIRE, 2);

            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.BIERE, 1);
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.BIERE);
            result.setGeneratedObjectQty(1);

        }

        else if (recipeNumber == 6 && nbFish1 >= 300 && nbFish2 >= 100 && nbPlant1 >= 300 && nbPlant2 >= 100 && nbPlant3 >= 20) {
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.PERCHE_PERLEE, 300);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.GREMILLE_GRELOTTANTE, 100);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.FEUILLE_PACIFIQUE, 300);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.OREADE_BLANC, 100);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.TIGE_RONCIVORE, 20);

            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.POTION_IRMA, 100);
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.POTION_IRMA);
            result.setGeneratedObjectQty(100);

        }

        else if (recipeNumber == 7 && nbFish1 >= 50 && nbFish2 >= 10 && nbPlant1 >= 50 && nbFish3 >= 1) {
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.PERCHE_PERLEE, 50);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.GREMILLE_GRELOTTANTE, 10);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.FEUILLE_PACIFIQUE, 50);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.CUBE_GLU, 1);

            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.ANTIDOTE, 10);
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.ANTIDOTE);
            result.setGeneratedObjectQty(10);

        } else if (recipeNumber == 8 && nbFish3 >= 5 && nbPlant4 >= 5) {
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.CUBE_GLU, 5);
            inventoryRepository.substractInventoryItem(playerId, DinoparcConstants.ANÉMONE_SOLITAIRE, 5);
            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.LAIT_DE_CARGOU, 1);
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.LAIT_DE_CARGOU);
            result.setGeneratedObjectQty(1);
        }

        return result;
    }

    private void getRandomRareItem(ChaudronCraftTryResult result, PgInventoryRepository inventoryRepository, String playerId) {
        RandomCollection<Object> randomElement = new RandomCollection<>()
                .add(1, DinoparcConstants.ETERNITY_PILL)
                .add(1, DinoparcConstants.OEUF_GLUON)
                .add(1, DinoparcConstants.OEUF_SERPANTIN)
                .add(1, DinoparcConstants.BIERE);

        String drawnElement = (String) randomElement.next();
         if (drawnElement.equalsIgnoreCase(DinoparcConstants.OEUF_GLUON)) {
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.OEUF_GLUON);
            result.setGeneratedObjectQty(1);
            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.OEUF_GLUON, 1);

        } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.BIERE)) {
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.BIERE);
            result.setGeneratedObjectQty(1);
            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.BIERE, 1);

        } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.OEUF_SERPANTIN)) {
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.OEUF_SERPANTIN);
            result.setGeneratedObjectQty(1);
            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.OEUF_SERPANTIN, 1);

        } else if (drawnElement.equalsIgnoreCase(DinoparcConstants.ETERNITY_PILL)) {
            result.setSuccess(true);
            result.setGeneratedObject(DinoparcConstants.ETERNITY_PILL);
            result.setGeneratedObjectQty(1);
            inventoryRepository.addInventoryItem(playerId, DinoparcConstants.ETERNITY_PILL, 1);
        }

        discordService.postMessage(DinoparcConstants.CHANNEL_CERBERE, "Recette 100 x 100 effectuée par "
                + playerRepository.findById(playerId).get().getName()
                + ". Résultat = " + result.getGeneratedObject());
    }
}