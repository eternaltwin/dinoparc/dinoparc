package com.dinoparc.api.services;

import com.dinoparc.api.domain.account.*;
import com.dinoparc.api.domain.bazar.BazarListing;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.dinoz.DinozUtils;
import com.dinoparc.api.domain.misc.Pagination;
import com.dinoparc.api.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Component
@EnableScheduling
public class BazarService {
    private final PgPlayerRepository playerRepository;
    private final PgPlayerStatRepository playerStatRepository;
    private final PgPlayerMissionRepository playerMissionRepository;
    private final PgBazarRepository bazarRepository;
    private final PgDinozRepository dinozRepository;
    private final AccountService accountService;
    private final PgHistoryRepository historyRepository;
    private final PgInventoryRepository inventoryRepository;

    @Autowired
    public BazarService(
            PgPlayerRepository playerRepository,
            PgPlayerStatRepository playerStatRepository, PgPlayerMissionRepository playerMissionRepository, PgBazarRepository bazarRepository,
            PgDinozRepository dinozRepository,
            AccountService accountService,
            PgHistoryRepository historyRepository,
            PgInventoryRepository inventoryRepository) {

        this.playerRepository = playerRepository;
        this.playerStatRepository = playerStatRepository;
        this.playerMissionRepository = playerMissionRepository;
        this.bazarRepository = bazarRepository;
        this.dinozRepository = dinozRepository;
        this.accountService = accountService;
        this.historyRepository = historyRepository;
        this.inventoryRepository = inventoryRepository;
    }

    @Scheduled(initialDelay = 118800000, fixedRate = 118800000)
    public void createBazarListingFromNPC() {
        for (int i = 0; i <= 2; i++) {
            BazarListing npcListing = new BazarListing();
            npcListing.setActive(true);
            npcListing.setSellerId("NPC_" + ThreadLocalRandom.current().nextInt(1, 4 + 1));
            npcListing.setSellerName(getNPCNameBySellerId(npcListing.getSellerId()));
            npcListing.setLastBiderId(npcListing.getSellerId());
            npcListing.setLastBiderName(npcListing.getSellerName());
            npcListing.setInitialDurationInDays(getRandomDuration(new int[]{3, 5, 7}));
            npcListing.setEndDateInEpochSeconds(ZonedDateTime.now(ZoneId.of("Europe/Paris")).plusDays(npcListing.getInitialDurationInDays()).plusMinutes(i * 5L).toEpochSecond());
            npcListing.setLastPrice(1);

            npcListing.getObjects().put(DinoparcConstants.CHAMPIFUZ, ThreadLocalRandom.current().nextInt(3, 30 + 1));
            npcListing.getObjects().put(DinoparcConstants.SPECIAL_CHAMPIFUZ, ThreadLocalRandom.current().nextInt(1, 3 + 1));
            npcListing.getObjects().put(DinoparcConstants.MONO_CHAMPIFUZ, ThreadLocalRandom.current().nextInt(1, 3 + 1));
            npcListing.getObjects().put(DinoparcConstants.RAMENS, ThreadLocalRandom.current().nextInt(1, 3 + 1));

            bazarRepository.save(npcListing);
        }

        //Special ultra-rare PNJ offer !
        if (ThreadLocalRandom.current().nextInt(1, 150 + 1) == 1) {
            BazarListing npcListing = new BazarListing();
            npcListing.setActive(true);
            npcListing.setSellerId("NPC_" + ThreadLocalRandom.current().nextInt(1, 4 + 1));
            npcListing.setSellerName(getNPCNameBySellerId(npcListing.getSellerId()));
            npcListing.setLastBiderId(npcListing.getSellerId());
            npcListing.setLastBiderName(npcListing.getSellerName());
            npcListing.setInitialDurationInDays(getRandomDuration(new int[]{3, 5, 7}));
            npcListing.setEndDateInEpochSeconds(ZonedDateTime.now(ZoneId.of("Europe/Paris")).plusDays(npcListing.getInitialDurationInDays()).plusMinutes(3 * 5L).toEpochSecond());
            npcListing.setLastPrice(1);

            npcListing.getObjects().put(DinoparcConstants.BIERE, 10);
            npcListing.getObjects().put(DinoparcConstants.RAMENS, 10);
            npcListing.getObjects().put(DinoparcConstants.ANTIDOTE, 50);
            npcListing.getObjects().put(DinoparcConstants.CHARME_PRISMATIK, 50);
            npcListing.getObjects().put(DinoparcConstants.LAIT_DE_CARGOU, 10);
            npcListing.getObjects().put(DinoparcConstants.COULIS_CERISE, 1);
            npcListing.getObjects().put(DinoparcConstants.OEUF_CHOCOLAT, 10);
            npcListing.getObjects().put(DinoparcConstants.OEUF_GLUON, 1);
            npcListing.getObjects().put(DinoparcConstants.OEUF_SANTAZ, 1);
            npcListing.getObjects().put(DinoparcConstants.OEUF_SERPANTIN, 1);
            npcListing.getObjects().put(DinoparcConstants.OEUF_FEROSS, 1);
            npcListing.getObjects().put(DinoparcConstants.OEUF_COBALT, 1);
            npcListing.getObjects().put(DinoparcConstants.JETON_CASINO_BRONZE, 3);
            npcListing.getObjects().put(DinoparcConstants.JETON_CASINO_ARGENT, 2);
            npcListing.getObjects().put(DinoparcConstants.JETON_CASINO_OR, 1);
            npcListing.getObjects().put(DinoparcConstants.MEDAILLE_CHOCOLAT, 10);
            npcListing.getObjects().put(DinoparcConstants.ETERNITY_PILL, 5);
            npcListing.getObjects().put(DinoparcConstants.POTION_SOMBRE, 1);
            npcListing.getObjects().put(DinoparcConstants.GIFT, 25);
            npcListing.getObjects().put(DinoparcConstants.CHAMPIFUZ, 50);
            npcListing.getObjects().put(DinoparcConstants.SPECIAL_CHAMPIFUZ, 10);
            npcListing.getObjects().put(DinoparcConstants.MONO_CHAMPIFUZ, 10);
            npcListing.getObjects().put(DinoparcConstants.MAGIK_CHAMPIFUZ, 1);

            bazarRepository.save(npcListing);
        }
    }

    @Deprecated
    public List<BazarListing> deprecatedGetAllBazarListings(Player account, String sort) {
        List<BazarListing> allListings = bazarRepository.findAllActive();
        switch (sort) {
            case "opt_oo":
                allListings.removeIf(listing -> listing.getDinozId() != null && listing.getDinozId().length() != 0);
                break;

            case "opt_50+":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozLevel(listing.getDinozId(), 50));
                break;

            case "opt_100+":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozLevel(listing.getDinozId(), 100));
                break;

            case "opt_dark":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozDark(listing.getDinozId()));
                break;

            case "opt_cherry":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozCherry(listing.getDinozId()));
                break;

            case "opt_0":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "0"));
                break;

            case "opt_1":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "1"));
                break;

            case "opt_2":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "2"));
                break;

            case "opt_3":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "3"));
                break;

            case "opt_4":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "4"));
                break;

            case "opt_5":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "5"));
                break;

            case "opt_6":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "6"));
                break;

            case "opt_7":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "7"));
                break;

            case "opt_8":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "8"));
                break;

            case "opt_9":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "9"));
                break;

            case "opt_A":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "A"));
                break;

            case "opt_B":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "B"));
                break;

            case "opt_C":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "C"));
                break;

            case "opt_D":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "D"));
                break;

            case "opt_E":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "E"));
                break;

            case "opt_F":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "F"));
                break;

            case "opt_G":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "G"));
                break;

            case "opt_H":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "H"));
                break;

            case "opt_I":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "I"));
                break;

            case "opt_J":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "J"));
                break;

            case "opt_K":
                allListings.removeIf(listing -> listing.getDinozId() == null || listing.getDinozId().length() == 0 || !isDinozFromRace(listing.getDinozId(), "K"));
                break;

            case "own":
                allListings.removeIf(listing -> !((listing.getLastBiderId() != null && listing.getLastBiderId().equalsIgnoreCase(account.getId())) || listing.getSellerId().equalsIgnoreCase(account.getId())));
                break;

            case "opt_his":
                allListings = bazarRepository.findLastCompleted();
                if (allListings.size() > 50) {
                    allListings = allListings.subList(allListings.size() - 50, allListings.size());
                    Collections.reverse(allListings);
                }
                return allListings;

            default:
                Collections.sort(allListings);
                if (!sort.equalsIgnoreCase("opt_his")) {
                    allListings.removeIf(listing -> !listing.isActive());
                }
                if (allListings.size() > 15) {
                    return allListings.subList(0, 15);
                } else {
                    return allListings;
                }
        }

        Collections.sort(allListings);
        return allListings;
    }

    public Pagination<BazarListing> listBazarListings(Player account, Integer limit, Integer offset, String category, boolean active) {
        return bazarRepository.list(account.getId(), limit, offset, category, active);
    }

    public Dinoz getBazarDinoz(String dinozId) {
        Optional<Dinoz> hypotheticalDinoz = dinozRepository.findById(dinozId);
        if (hypotheticalDinoz.isPresent()) {
            return hypotheticalDinoz.get();
        } else {
            return null;
        }
    }

    public boolean validateAndPublishBazarListing(Player account, Dinoz dinoz, BazarListing bazarListing) {
        String scenario = "";
        if (bazarListing.getLastPrice() == null || bazarListing.getLastPrice() < 1) {
            return false;
        }

        for (String objectInListing : bazarListing.getObjects().keySet()) {
            if (bazarListing.getObjects().get(objectInListing) != null && bazarListing.getObjects().get(objectInListing) > 0) {
                scenario += "objects";
                break;
            }
        }

        bazarListing.setActive(true);
        bazarListing.setSellerId(account.getId());
        bazarListing.setSellerName(account.getName());
        bazarListing.setEndDateInEpochSeconds(ZonedDateTime.now(ZoneId.of("Europe/Paris")).plusDays(bazarListing.getInitialDurationInDays()).toEpochSecond());

        if (bazarListing.getDinozId() != null && bazarListing.getDinozId() != "" && bazarListing.getDinozId().equalsIgnoreCase(dinoz.getId())) {
            scenario += "dinoz";
        }

        switch (scenario) {
            case "dinoz":
                return processDinozOnlyListing(account, dinoz, bazarListing);

            case "objects":
                return processObjectsOnlyListing(account, dinoz, bazarListing);

            case "objectsdinoz":
                return processFullListing(account, dinoz, bazarListing);

            default:
                return false;
        }
    }

    public List<Boolean> validateAndBidOnBazarListing(Player account, String listingId, Integer nbBons) {
        List<Boolean> response = new ArrayList<>();
        Integer totalBonsEngaged = accountService.getEngagedBonsAmountInBazarForUser(account);

        Optional<BazarListing> actualListing = bazarRepository.findById(listingId);
        var currentBonsQty = inventoryRepository.getQty(account.getId(), DinoparcConstants.BONS);

        if (isBidValid(account, nbBons, actualListing) && ((totalBonsEngaged + nbBons) > currentBonsQty)) {
            response.add(true);
            response.add(false);
            response.add(true);

            return response;

        } else if (isBidValid(account, nbBons, actualListing)) {
            BazarListing listing = actualListing.get();
            listing.setLastBiderId(account.getId());
            listing.setLastBiderName(account.getName());
            listing.setLastPrice(nbBons);
            ZonedDateTime nowTime = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
            ZonedDateTime endTime = ZonedDateTime.ofInstant(Instant.ofEpochSecond(actualListing.get().getEndDateInEpochSeconds()), ZoneId.of("Europe/Paris"));

            if (endTime.minusMinutes(5).isBefore(nowTime)) {
                listing.setEndDateInEpochSeconds(listing.getEndDateInEpochSeconds() + 300);
            }

            bazarRepository.update(listing);

            response.clear();
            response.add(true);
            response.add(true);
            response.add(true);
            return response;

        } else {
            response.add(false);
            response.add(true);
            response.add(true);

            return response;
        }
    }

    public void deleteBazarListing(String listingId) {
        bazarRepository.deleteById(listingId);
    }

    public void deleteAllNPCListingsThatDontHaveBids() {
        for (BazarListing listing : bazarRepository.findAllActive()) {
            if (listing.isActive() && listing.getSellerId().startsWith("NPC_") && listing.getLastBiderId().startsWith("NPC_")) {
                bazarRepository.deleteById(listing.getId());
            }
        }
    }

    private void createRandomDinozForBazar(BazarListing npcListing) {
        Dinoz listedDinoz = new Dinoz();
        listedDinoz.setId(UUID.randomUUID().toString());
        listedDinoz.setInBazar(true);
        listedDinoz.setMasterId("admin_bazar");
        listedDinoz.setMasterName("Bazar");
        listedDinoz.setName("???");
        listedDinoz.setBeginMessage(DinoparcConstants.BEGINMESSAGE1);
        listedDinoz.setEndMessage(DinoparcConstants.ENDMESSAGE1);
        listedDinoz.setLife(100);
        listedDinoz.setExperience(0);
        listedDinoz.setDanger(0);
        listedDinoz.setPlaceNumber(15);
        listedDinoz.setDark(false);
        listedDinoz.getActionsMap().put(DinoparcConstants.COMBAT, true);
        listedDinoz.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);
        listedDinoz.getActionsMap().put(DinoparcConstants.BAZAR, true);

        //Decide race & app Code :
        String generatedRace = getRandomRaceForBazarNPCListing();
        listedDinoz.setRace(Dinoz.getRaceFromAppearanceCode(generatedRace));
        listedDinoz.setAppearanceCode(generatedRace + DinozUtils.getRandomHexString());

        //Decide level + elements + skills :
        DinozUtils.initializeNPCDinoz(listedDinoz);
        if (npcListing.getObjects().get(DinoparcConstants.POTION_SOMBRE) == 0 && ThreadLocalRandom.current().nextInt(1, 50 + 1) == 5) {
            listedDinoz.setAppearanceCode(listedDinoz.getAppearanceCode() + "#");
            listedDinoz.setDark(true);
            listedDinoz.getSkillsMap().put(DinoparcConstants.APPRENTI_FEU, 1);
            listedDinoz.getElementsValues().put(DinoparcConstants.FEU, listedDinoz.getElementsValues().get(DinoparcConstants.FEU) + 1);
        }

        int[] randomCompetences = new Random().ints(1, 6).distinct().limit(3).toArray();
        listedDinoz.getSkillsMap().put(DinozUtils.getCompetenceNameFromNumber(randomCompetences[0]), 3);
        listedDinoz.getSkillsMap().put(DinozUtils.getCompetenceNameFromNumber(randomCompetences[1]), 2);
        listedDinoz.getSkillsMap().put(DinozUtils.getCompetenceNameFromNumber(randomCompetences[2]), 1);
        DinozUtils.concatElementWithCompetence(listedDinoz);

        listedDinoz.setLevel(1);
        Integer randomDrawnLevel = ThreadLocalRandom.current().nextInt(25, getRandomLevelLimit() + 1);
        npcListing.setLastPrice(Math.floorDiv(randomDrawnLevel, 5));

        for (int i = 1; i <= randomDrawnLevel; i++) {
            Map<String, Integer> availableLearnings = accountService.getAvailableLearnings(listedDinoz);
            List<String> availableUncompletedSkills = new ArrayList<String>();

            for (String existingSkill : listedDinoz.getSkillsMap().keySet()) {
                if (listedDinoz.getSkillsMap().get(existingSkill) > 0 && listedDinoz.getSkillsMap().get(existingSkill) < 5) {
                    availableUncompletedSkills.add(existingSkill);
                }
            }

            if (availableUncompletedSkills.size() > 0) {
                String skillToUp = DinozUtils.getActualSkillToUp(listedDinoz, availableUncompletedSkills);
                accountService.levelUp(listedDinoz, skillToUp, true);

            } else {
                List<String> newPossibleSkills = new ArrayList<String>(availableLearnings.keySet());
                String newLearning = DinozUtils.getNewLearning(listedDinoz, newPossibleSkills);
                accountService.learnNewSkill(listedDinoz, newLearning, true);
            }
        }

        npcListing.setDinozId(listedDinoz.getId());
        dinozRepository.create(listedDinoz);
    }

    private boolean isBidValid(Player account, Integer nbBons, Optional<BazarListing> actualListing) {
        var currentBonsQty = inventoryRepository.getQty(account.getId(), DinoparcConstants.BONS);

        return actualListing.isPresent()
                && actualListing.get().isActive()
                && nbBons > actualListing.get().getLastPrice()
                && currentBonsQty >= nbBons
                && !account.getId().equalsIgnoreCase(actualListing.get().getSellerId())
                && (ZonedDateTime.ofInstant(Instant.ofEpochSecond(actualListing.get().getEndDateInEpochSeconds()), ZoneId.of("Europe/Paris")).isAfter(ZonedDateTime.now(ZoneId.of("+2"))));
    }

    private boolean isDinozFromRace(String dinozId, String race) {
        Dinoz dinoz = dinozRepository.findById(dinozId).get();
        if (dinoz.getAppearanceCode().startsWith(race)) {
            return true;
        }
        return false;
    }

    private boolean isDinozLevel(String dinozId, Integer level) {
        Dinoz dinoz = dinozRepository.findById(dinozId).get();
        if (dinoz.getLevel() >= level) {
            return true;
        }
        return false;
    }

    private boolean isDinozDark(String dinozId) {
        Dinoz dinoz = dinozRepository.findById(dinozId).get();
        if (dinoz.getAppearanceCode().endsWith("%23") || dinoz.getAppearanceCode().endsWith("#") || dinoz.isDark()) {
            return true;
        }
        return false;
    }

    private boolean isDinozCherry(String dinozId) {
        Dinoz dinoz = dinozRepository.findById(dinozId).get();
        if (dinoz.getAppearanceCode().endsWith("$")) {
            return true;
        }
        return false;
    }

    private boolean processFullListing(Player account, Dinoz dinoz, BazarListing bazarListing) {
        addListingHistoryEvent(account);
        dinozRepository.setBazar(dinoz.getId(), true);

        List<String> malusList = dinoz.getMalusList();
        malusList.remove(DinoparcConstants.FACTION_1_GUERRIER);
        malusList.remove(DinoparcConstants.FACTION_2_GUERRIER);
        malusList.remove(DinoparcConstants.FACTION_3_GUERRIER);
        dinozRepository.setMalusList(dinoz.getId(), malusList);

        dinozRepository.setMasterInfos(dinoz.getId(), "admin_bazar", "Bazar");
        playerStatRepository.deleteDinozStats(dinoz.getId());
        playerMissionRepository.deletePlayerDinozMission(dinoz.getId());

        for (String objectKey : bazarListing.getObjects().keySet()) {
            var itemQty = inventoryRepository.getQty(account.getId(), objectKey);
            if (bazarListing.getObjects().get(objectKey) > 0 && itemQty >= bazarListing.getObjects().get(objectKey)) {
                inventoryRepository.substractInventoryItem(account.getId(), objectKey, bazarListing.getObjects().get(objectKey));
            }
        }

        playerRepository.deleteDinoz(account.getId(), dinoz.getId());
        bazarRepository.save(bazarListing);
        return true;
    }

    private boolean processObjectsOnlyListing(Player account, Dinoz dinoz, BazarListing bazarListing) {
        addListingHistoryEvent(account);
        for (String objectKey : bazarListing.getObjects().keySet()) {
            var itemQty = inventoryRepository.getQty(account.getId(), objectKey);
            if (bazarListing.getObjects().get(objectKey) > 0) {
                if (itemQty >= bazarListing.getObjects().get(objectKey)) {
                    inventoryRepository.substractInventoryItem(account.getId(), objectKey, bazarListing.getObjects().get(objectKey));
                } else {
                    return false;
                }
            }
        }

        bazarRepository.save(bazarListing);
        return true;
    }

    private boolean processDinozOnlyListing(Player account, Dinoz dinoz, BazarListing bazarListing) {
        addListingHistoryEvent(account);
        dinozRepository.setBazar(dinoz.getId(), true);
        List<String> malusList = dinoz.getMalusList();
        malusList.remove(DinoparcConstants.FACTION_1_GUERRIER);
        malusList.remove(DinoparcConstants.FACTION_2_GUERRIER);
        malusList.remove(DinoparcConstants.FACTION_3_GUERRIER);
        dinozRepository.setMalusList(dinoz.getId(), malusList);

        dinozRepository.setMasterInfos(dinoz.getId(), "admin_bazar", "Bazar");
        playerStatRepository.deleteDinozStats(dinoz.getId());
        playerMissionRepository.deletePlayerDinozMission(dinoz.getId());
        playerRepository.deleteDinoz(account.getId(), dinoz.getId());
        bazarRepository.save(bazarListing);

        return true;
    }

    private void addListingHistoryEvent(Player account) {
        History bazarEvent = new History();
        bazarEvent.setPlayerId(account.getId());
        bazarEvent.setType("bazarListing");
        bazarEvent.setIcon("hist_default.gif");
        historyRepository.save(bazarEvent);
    }

    private String getNPCNameBySellerId(String sellerId) {
        switch (sellerId) {
            case "NPC_1":
                return "Jean Bambois";
            case "NPC_2":
                return "Gérard Mandufrik";
            case "NPC_3":
                return "Gang Kabuki";
            case "NPC_4":
                return "Pr. Raymond";
            default:
                return "Jean Bambois";
        }
    }

    private String getRandomRaceForBazarNPCListing() {
        String possiblesRaces = "0123456789ACDGH";
        return String.valueOf(possiblesRaces.charAt(new Random().nextInt(possiblesRaces.length())));
    }

    private Integer getRandomLevelLimit() {
        RandomCollection<Object> randomElement = new RandomCollection<Object>()
                .add(20, 30)
                .add(20, 45)
                .add(20, 70)
                .add(10, 80)
                .add(5, 110)
                .add(5, 140);
        return (Integer) randomElement.next();
    }

    private int getRandomDuration(int[] array) {
        int rnd = new Random().nextInt(array.length);
        return array[rnd];
    }
}