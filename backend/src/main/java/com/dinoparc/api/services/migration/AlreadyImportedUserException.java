package com.dinoparc.api.services.migration;

import net.eternaltwin.dinoparc.DinoparcServer;
import net.eternaltwin.dinoparc.DinoparcUserId;
import net.eternaltwin.dinoparc.DinoparcUsername;
import net.eternaltwin.dinoparc.ShortDinoparcUser;
import net.eternaltwin.user.UserId;

/**
 * The remote dinoparc user was already imported
 */
public final class AlreadyImportedUserException extends ImportCheckException {
  public final DinoparcServer server;
  public final DinoparcUserId dparcUserId;
  public final DinoparcUsername username;

  public AlreadyImportedUserException(final DinoparcServer server, final DinoparcUserId dparcUserId, final DinoparcUsername username) {
    this.server = server;
    this.dparcUserId = dparcUserId;
    this.username = username;
  }

  public AlreadyImportedUserException(final ShortDinoparcUser dparcUser) {
    this(dparcUser.getServer(), dparcUser.getId(), dparcUser.getUsername());
  }
}
