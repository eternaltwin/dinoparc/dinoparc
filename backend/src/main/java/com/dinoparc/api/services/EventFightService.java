package com.dinoparc.api.services;

import com.dinoparc.api.configuration.NeoparcConfig;
import com.dinoparc.api.controllers.dto.FightSummaryDto;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.History;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.dinoz.*;
import com.dinoparc.api.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import kotlin.Pair;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import static com.dinoparc.api.domain.dinoz.Dinoz.isDinozMaxed;

@Component
public class EventFightService {
    public static Boolean wistitiWasCaughtToday = false;
    public static List<String> wistitiLogs = new ArrayList<>();
    public static List<String> raidBossLogs = new ArrayList<>();

    private FightService fightService;
    private PgPlayerRepository playerRepository;
    private PgPlayerStatRepository playerStatRepository;
    private PgDinozRepository dinozRepository;
    private PgHistoryRepository historyRepository;
    private PgCollectionRepository collectionRepository;
    private PgInventoryRepository inventoryRepository;
    private PgBossRepository bossRepository;
    private PgWildWistitiRepository wildWistitiRepository;
    private NeoparcConfig neoparcConfig;

    @Autowired
    public EventFightService(FightService fightService, PgPlayerRepository playerRepository,
                             PgPlayerStatRepository playerStatRepository, PgDinozRepository dinozRepository,
                             PgHistoryRepository historyRepository, PgCollectionRepository collectionRepository,
                             PgInventoryRepository inventoryRepository, PgBossRepository bossRepository,
                             PgWildWistitiRepository wildWistitiRepository, NeoparcConfig neoparcConfig) {
        this.fightService = fightService;
        this.playerRepository = playerRepository;
        this.playerStatRepository = playerStatRepository;
        this.dinozRepository = dinozRepository;
        this.historyRepository = historyRepository;
        this.collectionRepository = collectionRepository;
        this.inventoryRepository = inventoryRepository;
        this.bossRepository = bossRepository;
        this.wildWistitiRepository = wildWistitiRepository;
        this.neoparcConfig = neoparcConfig;
    }

    public FightSummaryDto fightBoss(Dinoz homeDinoz, EventDinozDto boss, boolean armyAttack) {
        computeRaidBossElementsBeforeFight(boss);
        int initialHomeDinozLife = homeDinoz.getLife();
        FightElements fightElements = new FightElements(homeDinoz, boss);
        FightSummaryDto summary = initFight(homeDinoz, boss);
        EventDinozUpdate eventDinozUpdate = new EventDinozUpdate();

        summary.setFinalLifeLoss(0);

        long dailyRaidFights = playerStatRepository.getCounter(UUID.fromString(homeDinoz.getMasterId()), "nb_daily_raid_fight");

        calculateFirstRoundEffects(summary, homeDinoz, boss, fightElements, eventDinozUpdate);
        calculateSecondRoundEffects(summary, homeDinoz, boss, fightElements, eventDinozUpdate);
        calculateThirdRoundEffects(summary, homeDinoz, boss, fightElements, eventDinozUpdate);
        calculateFinalInfos(summary, homeDinoz, dailyRaidFights, boss, eventDinozUpdate, 500, true, initialHomeDinozLife, armyAttack);

        if (armyAttack) {
            eventDinozUpdate.setLife(eventDinozUpdate.getLife() * DinoparcConstants.BOSS_ARMY_ATTACK_LIFE_FACTOR);
        }

        // Retrieve again raidBoss in case of life set to 0 by another player
        boss = bossRepository.getBoss(DinoparcConstants.RAID_BOSS_ID).get();

        if (boss.getLife() <= 0) {
            return null;
        }

        // Update raid boss and summary depending on fight result
        checkForRaidBossStatus(homeDinoz, dailyRaidFights, boss, eventDinozUpdate, summary);

        // Update home dinoz and player history
        homeDinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);
        homeDinoz.getActionsMap().put(DinoparcConstants.COMBAT, fightService.checkForCherryEffects(homeDinoz));

        dinozRepository.processFight(homeDinoz.getId(), homeDinoz.getActionsMap(), homeDinoz.getLife(), homeDinoz.getExperience(), homeDinoz.getDanger(), homeDinoz.getPassiveList(), homeDinoz.getMalusList());
        fightService.concatHomeHistoryEventToDeque(homeDinoz, summary, false);

        // Check focus / prisma / cherry
        fightService.checkForFocusVanishing(homeDinoz, summary);
        fightService.checkForPrismatikVanishing(homeDinoz, summary);

        List statsToUpdate;
        if (summary.isHaveWon()) {
            statsToUpdate = List.of(
                    new Pair(PgPlayerStatRepository.NB_FIGHT, 1),
                    new Pair(PgPlayerStatRepository.NB_RAID_FIGHT, 1),
                    new Pair(PgPlayerStatRepository.NB_DAILY_RAID_FIGHT, 1),
                    new Pair(PgPlayerStatRepository.SUM_GOLD_WON, summary.getMoneyWon()),
                    new Pair(PgPlayerStatRepository.NB_WON_FIGHT, 1),
                    new Pair(PgPlayerStatRepository.NB_RAID_FIGHT_WON, 1)
            );
        } else {
            statsToUpdate = List.of(
                    new Pair(PgPlayerStatRepository.NB_FIGHT, 1),
                    new Pair(PgPlayerStatRepository.NB_RAID_FIGHT, 1),
                    new Pair(PgPlayerStatRepository.NB_DAILY_RAID_FIGHT, 1),
                    new Pair(PgPlayerStatRepository.SUM_GOLD_WON, summary.getMoneyWon()),
                    new Pair(PgPlayerStatRepository.NB_LOST_FIGHT, 1)
            );
        }
        playerStatRepository.addStats(UUID.fromString(homeDinoz.getMasterId()), homeDinoz.getId(), statsToUpdate);
        return summary;
    }

    private FightSummaryDto initFight(Dinoz homeDinoz, EventDinozDto eventDinoz) {
        FightSummaryDto summary = new FightSummaryDto();
        summary.setSkillsEffectsFr(new ArrayList<String>());
        summary.setSkillsEffectsEs(new ArrayList<String>());
        summary.setSkillsEffectsEn(new ArrayList<String>());
        summary.setCharmsEffectsFr(new ArrayList<String>());
        summary.setCharmsEffectsEs(new ArrayList<String>());
        summary.setCharmsEffectsEn(new ArrayList<String>());
        summary.setLeftDinozName(homeDinoz.getName());
        summary.setRightDinozName(eventDinoz.getName());
        summary.setLeftDinozAppCode(homeDinoz.getAppearanceCode());
        summary.setRightDinozAppCode(eventDinoz.getAppearanceCode());
        summary.setLeftDinozBeginMessage(homeDinoz.getBeginMessage());
        summary.setRightDinozBeginMessage("");
        summary.setLeftDinozEndMessage(homeDinoz.getEndMessage());
        summary.setRightDinozEndMessage("");
        summary.setInitialHpLeft(homeDinoz.getLife());
        summary.setInitialHpRight(eventDinoz.getLife());
        summary.setScoreFinalHome(0);
        summary.setScoreFinalEnnemy(0);
        return summary;
    }

    public FightSummaryDto fightWildWistiti(Dinoz homeDinoz, EventDinozDto wildWistiti, Integer hunterGroup) {
        int elementAttacker = Dinoz.getHighestElementValue(homeDinoz.getElementsValues());
        decideWistitiElements(elementAttacker, wildWistiti);
        int initialHomeDinozLife = homeDinoz.getLife();

        FightElements fightElements = new FightElements(homeDinoz, wildWistiti);
        FightSummaryDto summary = initFight(homeDinoz, wildWistiti);
        EventDinozUpdate eventDinozUpdate = new EventDinozUpdate();
        summary.setFinalLifeLoss(0);
        calculateFirstRoundEffects(summary, homeDinoz, wildWistiti, fightElements, eventDinozUpdate);
        calculateSecondRoundEffects(summary, homeDinoz, wildWistiti, fightElements, eventDinozUpdate);
        calculateThirdRoundEffects(summary, homeDinoz, wildWistiti, fightElements, eventDinozUpdate);
        calculateFinalInfos(summary, homeDinoz, null, wildWistiti, eventDinozUpdate, homeDinoz.getLevel(), false, initialHomeDinozLife, false);

        // Retrieve again wildWistiti in case of life set to 0 by another player
        wildWistiti = wildWistitiRepository.getWildWistiti();

        if (wildWistiti.getLife() <= 0) {
            return null;
        }

        // Update wild wistiti and summary depending on fight result
        checkForW(homeDinoz, wildWistiti, eventDinozUpdate, summary);

        // Update home dinoz and player history
        homeDinoz.getActionsMap().put(DinoparcConstants.POTION_IRMA, true);
        homeDinoz.getActionsMap().put(DinoparcConstants.COMBAT, fightService.checkForCherryEffects(homeDinoz));

        dinozRepository.processFight(homeDinoz.getId(), homeDinoz.getActionsMap(), homeDinoz.getLife(), homeDinoz.getExperience(), homeDinoz.getDanger(),
                homeDinoz.getPassiveList(), homeDinoz.getMalusList());
        fightService.concatHomeHistoryEventToDeque(homeDinoz, summary, false);

        // Check focus / prisma / cherry
        fightService.checkForFocusVanishing(homeDinoz, summary);
        fightService.checkForPrismatikVanishing(homeDinoz, summary);

        playerStatRepository.resetAllPlayerStat(UUID.fromString(homeDinoz.getMasterId()), PgPlayerStatRepository.NB_WISTITI_SKIP);

        List statsToUpdate;
        if (summary.isHaveWon()) {
            statsToUpdate = List.of(
                    new Pair(PgPlayerStatRepository.NB_FIGHT, 1),
                    new Pair(PgPlayerStatRepository.NB_WISTITI_FIGHT, 1),
                    new Pair(PgPlayerStatRepository.SUM_GOLD_WON, summary.getMoneyWon()),
                    new Pair(PgPlayerStatRepository.NB_WON_FIGHT, 1)
            );
        } else {
            statsToUpdate = List.of(
                    new Pair(PgPlayerStatRepository.NB_FIGHT, 1),
                    new Pair(PgPlayerStatRepository.NB_WISTITI_FIGHT, 1),
                    new Pair(PgPlayerStatRepository.SUM_GOLD_WON, summary.getMoneyWon()),
                    new Pair(PgPlayerStatRepository.NB_LOST_FIGHT, 1)
            );
        }
        playerStatRepository.addStats(UUID.fromString(homeDinoz.getMasterId()), homeDinoz.getId(), statsToUpdate);

        return summary;
    }

    private void checkForRaidBossStatus(Dinoz homeDinoz, Long dailyBossFight, EventDinozDto raidBoss, EventDinozUpdate eventDinozUpdate, FightSummaryDto summary) {
        raidBoss = bossRepository.updateRaidBoss(eventDinozUpdate);

        if (raidBoss.getLife() <= 0) {
            // Player killed raidBoss
            summary.getSkillsEffectsFr().add("Raid : Vous avez mis <b>K.O</b> un Boss Unique !");
            summary.getSkillsEffectsEs().add("Raid : <b>¡Has noqueado a un Boss único !</b>");
            summary.getSkillsEffectsEn().add("Raid : You've <b>Knocked Out</b> a Unique Boss !");
        }

        if (dailyBossFight < DinoparcConstants.RAID_BOSS_LIMIT_NB_ATTACK
                && summary.getScoreFinalHome() >= summary.getScoreFinalEnnemy()) {
            summary.getSkillsEffectsFr().add("Raid : Vous avez récolté une <b>Goutte de Glu !</b>");
            summary.getSkillsEffectsEs().add("Raid : Te has ganado una <b>Gota de Limo !</b>");
            summary.getSkillsEffectsEn().add("Raid : You have earned a <b>Slime Drop !</b>");
            inventoryRepository.addInventoryItem(homeDinoz.getMasterId(), DinoparcConstants.GOUTTE_GLU, 1);
        }

        summary.setEventDinozLifeLoss(eventDinozUpdate.getLife());
    }

    private void checkForW(Dinoz homeDinoz, EventDinozDto wildWistiti, EventDinozUpdate eventDinozUpdate, FightSummaryDto summary) {
        wildWistiti = wildWistitiRepository.updateWildWistiti(eventDinozUpdate);

        if (wildWistiti.getLife() <= 0) {
            // Player killed wildWistiti
            summary.getSkillsEffectsFr().add("Votre adversaire est <b>K.O!</b> Vous avez capturé un <b>Ouistiti !</b>");
            summary.getSkillsEffectsEs().add("¡Tu oponente es <b>K.O!</b>  Capturaste un <b>Wiska !</b>");
            summary.getSkillsEffectsEn().add("Your opponent is <b>K.O!</b> You have just captured a <b>Wistiti !</b>");

            Player homeAccount = fightService.getAccountByDinozId(homeDinoz.getId());
            Dinoz newWis = DinozUtils.initNewWis(wildWistiti, homeAccount);

            wistitiLogs.add("Caught by " + homeAccount.getName() + " at " + ZonedDateTime.now(ZoneId.of("Europe/Paris")) + "\n");
            wistitiWasCaughtToday = true;
            dinozRepository.deleteById("admin-w-wild");
            dinozRepository.create(newWis);

            var newValWistitiCaptured = homeAccount.getWistitiCaptured() == null ? 1 : homeAccount.getWistitiCaptured() + 1;

            playerRepository.updateWistitiCaptured(homeAccount.getId(), newValWistitiCaptured);
            playerRepository.updateDinozLimit(homeAccount.getId(), 1);
            playerRepository.addDinoz(homeAccount.getId(), newWis.getId());
        } else {
            ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
            wistitiLogs.add("[" + now.getHour() + ":" + now.getMinute() + ":" + now.getSecond()
                    + "] : Fight against " + homeDinoz.getName() + " (" + homeDinoz.getMasterName()
                    + ") : Wistiti's Life After Fight = {" + wildWistiti.getLife() + " HPs}" + "\n");
        }
    }

    private void calculateFirstRoundEffects(FightSummaryDto summary, Dinoz homeDinoz, EventDinozDto eventDinoz, FightElements fightElements, EventDinozUpdate eventDinozUpdate) {
        Integer roundFactor = fightService.getRoundFactorByElements(fightElements.getHighestLeft(), fightElements.getHighestRight());

        boolean homeDinozHasTheCharm = false;
        if (homeDinoz.getPassiveList().get(fightService.getCharmNameFromElement(fightElements.getHighestRight())) != null
                && homeDinoz.getPassiveList().get(fightService.getCharmNameFromElement(fightElements.getHighestRight())) > 0) {
            homeDinozHasTheCharm = true;
        }

        boolean homeDinozHasThePrismatik = false;
        if (homeDinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) != null
                && homeDinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) > 0) {
            homeDinozHasThePrismatik = true;
        }

        boolean foreignDinozHasTheCharm = eventDinoz.hasCharm(fightElements.getHighestLeft());

        Integer firstRoundResult = 0;
        var homeDinozElementsValues = homeDinoz.getElementsValues();
        var eventDinozElementsValues = eventDinoz.getElementsValues();
        if (roundFactor < 0) {
            firstRoundResult = (roundFactor * eventDinozElementsValues.get(fightElements.getHighestRight())) + homeDinozElementsValues.get(fightElements.getHighestLeft());
        } else if (roundFactor > 0) {
            firstRoundResult = (roundFactor * homeDinozElementsValues.get(fightElements.getHighestLeft())) - (eventDinozElementsValues.get(fightElements.getHighestRight()));
        }

        Integer lifeLossByLoser = firstRoundResult / 5;
        Integer lifeLostPreventedLoser = 0;
        String charmUsed = "";

        // Life loss by the foreign dinoz :
        if (lifeLossByLoser > 0 && foreignDinozHasTheCharm) {
            eventDinozUpdate.addLostCharm(fightElements.getHighestLeft());
            lifeLossByLoser = 0;
        }

        // Life loss by the home dinoz :
        if ((lifeLossByLoser < 0 && homeDinozHasTheCharm) || (lifeLossByLoser < 0 && homeDinozHasThePrismatik)) {
            String charmName = "";
            if (homeDinozHasThePrismatik) {
                charmName = DinoparcConstants.CHARME_PRISMATIK;

            } else {
                charmName = fightService.getCharmNameFromElement(fightElements.getHighestRight());
                homeDinoz.getPassiveList().put(charmName, homeDinoz.getPassiveList().get(charmName) - 1);
            }

            summary.setCharmFirstRoundForLoser(1);
            lifeLostPreventedLoser = lifeLossByLoser * -1;
            charmUsed = charmName;
            lifeLossByLoser = 0;
        }

        // Life loss by the foreign dinoz (no charms) :
        if (lifeLossByLoser > 0 && !foreignDinozHasTheCharm) {
            if (homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) != null && homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) >= 1) {
                Integer critiqueLevel = homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE);
                if (ThreadLocalRandom.current().nextInt(1, 100 + 1) <= critiqueLevel) {
                    lifeLossByLoser = (lifeLossByLoser * 2);
                    summary.getSkillsEffectsFr().add("Vous avez infligé un <b>Coup Critique</b> à votre adversaire, <b>doublant</b> ainsi les dégâts générés au Round #1!");
                    summary.getSkillsEffectsEs().add("¡Has golpeado <b>Críticamente</b> a tu oponente, <b>duplicando</b> el daño generado desde la Ronda #1!");
                    summary.getSkillsEffectsEn().add("You have <b>Critically Hit</b> your opponent, <b>doubling</b> the damage generated on Round #1!");
                }
            }

            eventDinozUpdate.setLife(eventDinozUpdate.getLife() + lifeLossByLoser);
        }

        // Life loss by the home dinoz (no charms) :
        if (lifeLossByLoser < 0 && !homeDinozHasTheCharm) {
            homeDinoz.setLife(homeDinoz.getLife() + lifeLossByLoser);
        }

        summary.setFirstElementLeft(fightService.getElementNumberFromElementValue(fightElements.getHighestLeft()));
        summary.setFirstElementRight(fightService.getElementNumberFromElementValue(fightElements.getHighestRight()));
        summary.setRoundOneScore(firstRoundResult);
        summary.setLifeLossFirstRoundByLoser(Math.abs(lifeLossByLoser));
        summary.setLifeLostPreventedFirstRound(lifeLostPreventedLoser);
        summary.setCharmTypeFirstRound(charmUsed);

        if (lifeLossByLoser < 0) {
            summary.setFinalLifeLoss(summary.getFinalLifeLoss() + Math.abs(lifeLossByLoser));
        }

        fightService.computeFinalScoreStepByStep(summary, firstRoundResult);
    }

    private void calculateSecondRoundEffects(FightSummaryDto summary, Dinoz homeDinoz, EventDinozDto eventDinoz, FightElements fightElements, EventDinozUpdate eventDinozUpdate) {
        Integer roundFactor = fightService.getRoundFactorByElements(fightElements.getSecondHighestLeft(), fightElements.getSecondHighestRight());

        Boolean homeDinozIsAlive;
        if (homeDinoz.getLife() > 0) {
            homeDinozIsAlive = true;
        } else {
            homeDinozIsAlive = false;
        }

        boolean homeDinozHasTheCharm = false;
        if (homeDinoz.getPassiveList().get(fightService.getCharmNameFromElement(fightElements.getSecondHighestRight())) != null
                && homeDinoz.getPassiveList().get(fightService.getCharmNameFromElement(fightElements.getSecondHighestRight())) > 0) {
            homeDinozHasTheCharm = true;
        }

        boolean homeDinozHasThePrismatik = false;
        if (homeDinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) != null
                && homeDinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) > 0) {
            homeDinozHasThePrismatik = true;
        }

        boolean foreignDinozHasTheCharm = eventDinoz.hasCharm(fightElements.getSecondHighestLeft());

        Integer secondRoundResult = 0;
        var homeDinozElementsValues = homeDinoz.getElementsValues();
        var eventDinozElementsValues = eventDinoz.getElementsValues();
        if (roundFactor < 0) {
            secondRoundResult = (roundFactor * eventDinozElementsValues.get(fightElements.getSecondHighestRight())) + homeDinozElementsValues.get(fightElements.getSecondHighestLeft());
        } else if (roundFactor > 0) {
            secondRoundResult = (roundFactor * homeDinozElementsValues.get(fightElements.getSecondHighestLeft())) - (eventDinozElementsValues.get(fightElements.getSecondHighestRight()));
        }

        Integer lifeLossByLoser;
        if (secondRoundResult < 0 && eventDinoz.getLife() <= 0) {
            lifeLossByLoser = 0;
        } else if (secondRoundResult > 0 && !homeDinozIsAlive) {
            lifeLossByLoser = 0;
        } else {
            lifeLossByLoser = secondRoundResult / 5;
        }

        Integer lifeLostPreventedLoser = 0;
        String charmUsed = "";

        // Life loss by the foreign dinoz :
        if (lifeLossByLoser > 0 && foreignDinozHasTheCharm) {
            eventDinozUpdate.addLostCharm(fightElements.getSecondHighestLeft());
            lifeLossByLoser = 0;
        }

        // Life loss by the home dinoz :
        if ((lifeLossByLoser < 0 && homeDinozHasTheCharm) || (lifeLossByLoser < 0 && homeDinozHasThePrismatik)) {
            String charmName = "";
            if (homeDinozHasThePrismatik) {
                charmName = DinoparcConstants.CHARME_PRISMATIK;

            } else {
                charmName = fightService.getCharmNameFromElement(fightElements.getSecondHighestRight());
                homeDinoz.getPassiveList().put(charmName, homeDinoz.getPassiveList().get(charmName) - 1);
            }

            summary.setCharmSecondRoundForLoser(1);
            lifeLostPreventedLoser = lifeLossByLoser * -1;
            charmUsed = charmName;
            lifeLossByLoser = 0;
        }

        // Life loss by the foreign dinoz (no charms) :
        if (lifeLossByLoser > 0 && !foreignDinozHasTheCharm) {
            if (homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) != null && homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) >= 1) {
                Integer critiqueLevel = homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE);
                if (ThreadLocalRandom.current().nextInt(1, 100 + 1) <= critiqueLevel) {
                    lifeLossByLoser = (lifeLossByLoser * 2);
                    summary.getSkillsEffectsFr().add("Vous avez infligé un <b>Coup Critique</b> à votre adversaire, <b>doublant</b> ainsi les dégâts générés au Round #2!");
                    summary.getSkillsEffectsEs().add("¡Has golpeado <b>Críticamente</b> a tu oponente, <b>duplicando</b> el daño generado desde la Ronda #2!");
                    summary.getSkillsEffectsEn().add("You have <b>Critically hit</b> your opponent, <b>doubling</b> the damage generated on Round #2!");
                }
            }
            eventDinozUpdate.setLife(eventDinozUpdate.getLife() + lifeLossByLoser);
        }

        // Life loss by the home dinoz (no charms) :
        if (lifeLossByLoser < 0 && !homeDinozHasTheCharm) {
            homeDinoz.setLife(homeDinoz.getLife() + lifeLossByLoser);
        }

        summary.setSecondElementLeft(fightService.getElementNumberFromElementValue(fightElements.getSecondHighestLeft()));
        summary.setSecondElementRight(fightService.getElementNumberFromElementValue(fightElements.getSecondHighestRight()));
        summary.setRoundTwoScore(secondRoundResult);
        summary.setLifeLossSecondRoundByLoser(Math.abs(lifeLossByLoser));
        summary.setLifeLostPreventedSecondRound(lifeLostPreventedLoser);
        summary.setCharmTypeSecondRound(charmUsed);

        if (lifeLossByLoser < 0) {
            summary.setFinalLifeLoss(summary.getFinalLifeLoss() + Math.abs(lifeLossByLoser));
        }

        fightService.computeFinalScoreStepByStep(summary, secondRoundResult);
    }

    private void calculateThirdRoundEffects(FightSummaryDto summary, Dinoz homeDinoz, EventDinozDto eventDinoz, FightElements fightElements, EventDinozUpdate eventDinozUpdate) {
        Integer roundFactor = fightService.getRoundFactorByElements(fightElements.getThirdHighestLeft(), fightElements.getThirdHighestRight());

        Boolean homeDinozIsAlive;
        if (homeDinoz.getLife() > 0) {
            homeDinozIsAlive = true;
        } else {
            homeDinozIsAlive = false;
        }

        boolean homeDinozHasTheCharm = false;
        if (homeDinoz.getPassiveList().get(fightService.getCharmNameFromElement(fightElements.getThirdHighestRight())) != null
                && homeDinoz.getPassiveList().get(fightService.getCharmNameFromElement(fightElements.getThirdHighestRight())) > 0) {
            homeDinozHasTheCharm = true;
        }

        boolean homeDinozHasThePrismatik = false;
        if (homeDinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) != null
                && homeDinoz.getPassiveList().get(DinoparcConstants.CHARME_PRISMATIK) > 0) {
            homeDinozHasThePrismatik = true;
        }

        boolean foreignDinozHasTheCharm = eventDinoz.hasCharm(fightElements.getThirdHighestLeft());

        Integer thirdRoundResult = 0;
        if (roundFactor < 0) {
            thirdRoundResult = (roundFactor * eventDinoz.getElementsValues().get(fightElements.getThirdHighestRight()))
                    + homeDinoz.getElementsValues().get(fightElements.getThirdHighestLeft());
        } else if (roundFactor > 0) {
            thirdRoundResult = (roundFactor * homeDinoz.getElementsValues().get(fightElements.getThirdHighestLeft()))
                    - (eventDinoz.getElementsValues().get(fightElements.getThirdHighestRight()));
        }

        Integer lifeLossByLoser;
        if (thirdRoundResult < 0 && eventDinoz.getLife() <= 0) {
            lifeLossByLoser = 0;
        } else if (thirdRoundResult > 0 && !homeDinozIsAlive) {
            lifeLossByLoser = 0;
        } else {
            lifeLossByLoser = thirdRoundResult / 5;
        }

        Integer lifeLostPreventedLoser = 0;
        String charmUsed = "";

        // Life loss by the foreign dinoz :
        if (lifeLossByLoser > 0 && foreignDinozHasTheCharm) {
            eventDinozUpdate.addLostCharm(fightElements.getThirdHighestLeft());
            lifeLossByLoser = 0;
        }

        // Life loss by the home dinoz :
        if ((lifeLossByLoser < 0 && homeDinozHasTheCharm) || (lifeLossByLoser < 0 && homeDinozHasThePrismatik)) {
            String charmName = "";
            if (homeDinozHasThePrismatik) {
                charmName = DinoparcConstants.CHARME_PRISMATIK;

            } else {
                charmName = fightService.getCharmNameFromElement(fightElements.getThirdHighestRight());
                homeDinoz.getPassiveList().put(charmName, homeDinoz.getPassiveList().get(charmName) - 1);
            }

            summary.setCharmThirdRoundForLoser(1);
            lifeLostPreventedLoser = lifeLossByLoser * -1;
            charmUsed = charmName;
            lifeLossByLoser = 0;
        }

        // Life loss by the foreign dinoz (no charms) :
        if (lifeLossByLoser > 0 && !foreignDinozHasTheCharm) {
            if (homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) != null && homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE) >= 1) {
                Integer critiqueLevel = homeDinoz.getSkillsMap().get(DinoparcConstants.COUP_CRITIQUE);
                if (ThreadLocalRandom.current().nextInt(1, 100 + 1) <= critiqueLevel) {
                    lifeLossByLoser = (lifeLossByLoser * 2);
                    summary.getSkillsEffectsFr().add("Vous avez infligé un <b>Coup Critique</b> à votre adversaire, <b>doublant</b> ainsi les dégâts générés au Round #3!");
                    summary.getSkillsEffectsEs().add("¡Has golpeado <b>Críticamente</b> a tu oponente, <b>duplicando</b> el daño generado desde la Ronda #3!");
                    summary.getSkillsEffectsEn().add("You have <b>Critically hit</b> your opponent, <b>doubling</b> the damage generated on Round #3!");
                }
            }
            eventDinozUpdate.setLife(eventDinozUpdate.getLife() + lifeLossByLoser);
        }

        // Life loss by the home dinoz (no charms) :
        if (lifeLossByLoser < 0 && !homeDinozHasTheCharm) {
            homeDinoz.setLife(homeDinoz.getLife() + lifeLossByLoser);
        }

        summary.setThirdElementLeft(fightService.getElementNumberFromElementValue(fightElements.getThirdHighestLeft()));
        summary.setThirdElementRight(fightService.getElementNumberFromElementValue(fightElements.getThirdHighestRight()));
        summary.setRoundThreeScore(thirdRoundResult);
        summary.setLifeLossThirdRoundByLoser(Math.abs(lifeLossByLoser));
        summary.setLifeLostPreventedThirdRound(lifeLostPreventedLoser);
        summary.setCharmTypeThirdRound(charmUsed);

        if (lifeLossByLoser < 0) {
            summary.setFinalLifeLoss(summary.getFinalLifeLoss() + Math.abs(lifeLossByLoser));
        }

        fightService.computeFinalScoreStepByStep(summary, thirdRoundResult);
    }

    private void calculateFinalInfos(FightSummaryDto summary, Dinoz homeDinoz, Long dailyRaidFights, EventDinozDto eventDinoz, EventDinozUpdate eventDinozUpdate,
                                     int level, boolean isRaidBoss, int initialHomeDinozLife, boolean armyAttack) {
        Player account = fightService.getAccountByDinozId(homeDinoz.getId());
        setWinnerOrLoser(summary);
        calculateExperienceAndDangerWon(summary, homeDinoz, level);

        Integer moneyWon = 0;
        if (isRaidBoss) {
            moneyWon = overrideMoneyWonForRaidFights(dailyRaidFights, summary.getScoreFinalHome());
            if (fightIsNotFromCherryOrDailyPotions(homeDinoz.getMalusList(), inventoryRepository.getQty(account.getId(), DinoparcConstants.POTION_IRMA_SURPLUS))) {
                summary.setStolenObject(DinoparcConstants.POTION_IRMA);
                inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.POTION_IRMA, 1);
            }
        } else {
            moneyWon = ThreadLocalRandom.current().nextInt(400, 800 + 1);
        }

        moneyWon = fightService.applyLuckToMoneyWon(summary, homeDinoz, moneyWon);
        moneyWon = fightService.applyBeerEffectsToMoneyWon(summary, homeDinoz, moneyWon);
        summary.setMoneyWon(moneyWon);

        if (isRaidBoss) {
            playerRepository.updateCash(account.getId(), summary.getMoneyWon());
        } else {
            playerRepository.updateCashAndWistitiFight(account.getId(), summary.getMoneyWon());
        }

        applyForceToFightEffects(summary, homeDinoz, eventDinozUpdate);
        fightService.applyHealToFightEffects(summary, homeDinoz, initialHomeDinozLife);
        applyMartialArtsToFightEffects(summary, homeDinoz, eventDinozUpdate);
        applyPoisonTransmission(summary, homeDinoz, eventDinozUpdate, eventDinoz.getBonusClaw() > 0);
        fightService.applySurvieToFightEffects(summary, homeDinoz);

        if (homeDinoz.getLife() < 1) {
            homeDinoz.setLife(0);
            summary.getSkillsEffectsFr().add("Affaibli par ses blessures, " + homeDinoz.getName() + " s'écroule à la fin du combat !");
            summary.getSkillsEffectsEs().add("Debilitado por sus heridas, " + homeDinoz.getName() + " se derrumba al final de la pelea!");
            summary.getSkillsEffectsEn().add("Weakened by his wounds, " + homeDinoz.getName() + " collapses at the end of the fight!");

            History homeHistoryEvent = new History();
            homeHistoryEvent.setPlayerId(homeDinoz.getMasterId());
            homeHistoryEvent.setIcon("hist_death.gif");
            homeHistoryEvent.setType("death");
            historyRepository.save(homeHistoryEvent);
        }

        if (summary.getLifeLostPreventedFirstRound() != 0 && summary.getRoundOneScore() < 0) {
            summary.getCharmsEffectsFr().add("Votre <b>" + fightService.getCharmFromCode(summary.getCharmTypeFirstRound(),"fr") + "</b> a été activé et vous a permis d'éviter la perte de <b>" + summary.getLifeLostPreventedFirstRound() + "%</b> de vie! (Round #1)");
            summary.getCharmsEffectsEs().add("Tu <b>" + fightService.getCharmFromCode(summary.getCharmTypeFirstRound(),"es") + "</b> se ha activado y eso te ha hecho evitar la pérdida de <b>" + summary.getLifeLostPreventedFirstRound() + "%</b> de vida! (Round #1)");
            summary.getCharmsEffectsEn().add("Your <b>" + fightService.getCharmFromCode(summary.getCharmTypeFirstRound(),"en") + "</b> has been activated and that prevented you to lost <b>" + summary.getLifeLostPreventedFirstRound() + "%</b> of life! (Round #1)");
        }

        if (summary.getLifeLostPreventedSecondRound() != 0 && summary.getRoundTwoScore() < 0) {
            summary.getCharmsEffectsFr().add("Votre <b>" + fightService.getCharmFromCode(summary.getCharmTypeSecondRound(),"fr") + "</b> a été activé et vous a permis d'éviter la perte de <b>" + summary.getLifeLostPreventedSecondRound() + "%</b> de vie! (Round #2)");
            summary.getCharmsEffectsEs().add("Tu <b>" + fightService.getCharmFromCode(summary.getCharmTypeSecondRound(),"es") + "</b> se ha activado y eso te ha hecho evitar la pérdida de <b>" + summary.getLifeLostPreventedSecondRound() + "%</b> de vida! (Round #2)");
            summary.getCharmsEffectsEn().add("Your <b>" + fightService.getCharmFromCode(summary.getCharmTypeSecondRound(),"en") + "</b> has been activated and that prevented you to lost <b>" + summary.getLifeLostPreventedSecondRound() + "%</b> of life! (Round #2)");
        }

        if (summary.getLifeLostPreventedThirdRound() != 0 && summary.getRoundThreeScore() < 0) {
            summary.getCharmsEffectsFr().add("Votre <b>" + fightService.getCharmFromCode(summary.getCharmTypeThirdRound(),"fr") + "</b> a été activé et vous a permis d'éviter la perte de <b>" + summary.getLifeLostPreventedThirdRound() + "%</b> de vie! (Round #3)");
            summary.getCharmsEffectsEs().add("Tu <b>" + fightService.getCharmFromCode(summary.getCharmTypeThirdRound(),"es") + "</b> se ha activado y eso te ha hecho evitar la pérdida de <b>" + summary.getLifeLostPreventedThirdRound() + "%</b> de vida! (Round #3)");
            summary.getCharmsEffectsEn().add("Your <b>" + fightService.getCharmFromCode(summary.getCharmTypeThirdRound(),"en") + "</b> has been activated and that prevented you to lost <b>" + summary.getLifeLostPreventedThirdRound() + "%</b> of life! (Round #3)");
        }
    }

    private boolean fightIsNotFromCherryOrDailyPotions(List<String> effectsOnDinoz, Integer irmaSurplusQty) {
        if (effectsOnDinoz.contains(DinoparcConstants.COULIS_CERISE) || irmaSurplusQty > 0) {
            return false;
        }
        return true;
    }

    private void setWinnerOrLoser(FightSummaryDto summary) {
        summary.setEnnemyMasterDinozName("???");
        summary.setEnnemyMasterName("admin");

        Integer roundSum = summary.getRoundOneScore() + summary.getRoundTwoScore() + summary.getRoundThreeScore();
        if (roundSum > 0) {
            summary.setHaveWon(true);
        } else {
            summary.setHaveWon(false);
        }
    }

    private void calculateExperienceAndDangerWon(FightSummaryDto summary, Dinoz homeDinoz, int eventDinozLevel) {
        summary.setExperienceWon(fightService.getExperienceFromFight(isDinozMaxed(homeDinoz, !AccountService.getAvailableLearnings(homeDinoz).isEmpty()), homeDinoz.getSkillsMap().get(DinoparcConstants.INTELLIGENCE), homeDinoz.getLevel(), eventDinozLevel));
        if (homeDinoz.getExperience() + summary.getExperienceWon() >= 100) {
            homeDinoz.setExperience(100);
            homeDinoz.getActionsMap().put(DinoparcConstants.LEVELUP, true);
            summary.getSkillsEffectsFr().add("<b>Level Up</b> -> " + homeDinoz.getName() + " a accumulé assez d'expérience pour gagner un niveau supplémentaire!");
            summary.getSkillsEffectsEs().add("<b>Level Up</b> -> " + homeDinoz.getName() + " ¡ha acumulado suficiente experiencia para ganar un nivel adicional!");
            summary.getSkillsEffectsEn().add("<b>Level Up</b> -> " + homeDinoz.getName() + " has accumulated enough experience to gain an additional level!");

        } else if (summary.getExperienceWon() > 0) {
            homeDinoz.setExperience(homeDinoz.getExperience() + summary.getExperienceWon());
        } else {
            homeDinoz.setExperience(0);
        }

        fightService.setDangerWonByHomeDinoz(summary, homeDinoz);
    }

    private Integer overrideMoneyWonForRaidFights(Long dailyRaidFights, Integer scoreHome) {
        int factorScore = scoreHome / 100;
        Double bonusMoneyWon = ThreadLocalRandom.current().nextDouble(0, factorScore + 1);

        if (scoreHome >= 2000) {
            return dailyRaidFights > DinoparcConstants.RAID_BOSS_LIMIT_NB_ATTACK ?
                    DinoparcConstants.RAID_BOSS_GOLD_AFTER_LIMIT + bonusMoneyWon.intValue() :
                    800 + bonusMoneyWon.intValue();
        }
        if (scoreHome >= 1500) {
            return dailyRaidFights > DinoparcConstants.RAID_BOSS_LIMIT_NB_ATTACK ?
                    DinoparcConstants.RAID_BOSS_GOLD_AFTER_LIMIT + bonusMoneyWon.intValue() :
                    775 + bonusMoneyWon.intValue();
        }
        if (scoreHome >= 1000) {
            return dailyRaidFights > DinoparcConstants.RAID_BOSS_LIMIT_NB_ATTACK ?
                    DinoparcConstants.RAID_BOSS_GOLD_AFTER_LIMIT + bonusMoneyWon.intValue() :
                    750 + bonusMoneyWon.intValue();
        }
        if (scoreHome >= 500) {
            return dailyRaidFights > DinoparcConstants.RAID_BOSS_LIMIT_NB_ATTACK ?
                    DinoparcConstants.RAID_BOSS_GOLD_AFTER_LIMIT + bonusMoneyWon.intValue() :
                    700 + bonusMoneyWon.intValue();
        }
        if (scoreHome >= 300) {
            return dailyRaidFights > DinoparcConstants.RAID_BOSS_LIMIT_NB_ATTACK ?
                    DinoparcConstants.RAID_BOSS_GOLD_AFTER_LIMIT + bonusMoneyWon.intValue() :
                    625 + bonusMoneyWon.intValue();
        }
        if (scoreHome >= 100) {
            return 525 + bonusMoneyWon.intValue();
        }
        return 400 + bonusMoneyWon.intValue();
    }

    private void applyForceToFightEffects(FightSummaryDto summary, Dinoz homeDinoz, EventDinozUpdate eventDinozUpdate) {
        if (homeDinoz.getSkillsMap().get(DinoparcConstants.FORCE) != null) {
            Integer forcePoints = ThreadLocalRandom.current().nextInt(1, homeDinoz.getSkillsMap().get(DinoparcConstants.FORCE) + 3);

            eventDinozUpdate.setLife(eventDinozUpdate.getLife() + forcePoints);
            summary.getSkillsEffectsFr().add("Votre <b>Force</b> vous a permis de faire perdre <b>" + forcePoints + "%</b> de vie supplémentaire à votre adversaire!");
            summary.getSkillsEffectsEs().add("Tu <b>Fuerza</b> te ha permitido perder un <b>" + forcePoints + "%</b> más de vida de tu oponente!");
            summary.getSkillsEffectsEn().add("Your <b>Strength</b> made your opponent lose <b>" + forcePoints + "</b> more percent of life!");
        }
    }

    private void applyMartialArtsToFightEffects(FightSummaryDto summary, Dinoz homeDinoz, EventDinozUpdate eventDinozUpdate) {
        if (homeDinoz.getSkillsMap().get(DinoparcConstants.ARTS_MARTIAUX) != null) {
            Integer amPoints = ThreadLocalRandom.current().nextInt(1, homeDinoz.getSkillsMap().get(DinoparcConstants.ARTS_MARTIAUX) + 1);

            eventDinozUpdate.setLife(eventDinozUpdate.getLife() + amPoints);
            summary.getSkillsEffectsFr().add("Votre maîtrise des <b>Arts Martiaux</b> vous a permis de faire perdre <b>" + amPoints + "%</b> de vie supplémentaire à cette créature sauvage!");
            summary.getSkillsEffectsEs().add("¡Tu conocimiento de las <b>Artes Marciales</b> hizo que la bestia salvaje perdiera un " + amPoints + " más de vida!");
            summary.getSkillsEffectsEn().add("Your knowledge of <b>Martial Arts</b> made the wild beast <b>" + amPoints + "</b> more percent of life!");
        }
    }

    private void applyPoisonTransmission(FightSummaryDto summary, Dinoz homeDinoz, EventDinozUpdate eventDinozUpdate, boolean hasClaws) {
        if (hasClaws && !homeDinoz.getMalusList().contains(DinoparcConstants.EMPOISONNE)) {
                homeDinoz.getMalusList().add(DinoparcConstants.EMPOISONNE);
                homeDinoz.setLife(homeDinoz.getLife() - 5);
                eventDinozUpdate.setBonusClaw(1);
                summary.getSkillsEffectsFr().add("Vous avez été <b>empoisonné</b> par le Dinoz adverse avec ses griffes spécialement préparées! Vous perdez <b>5%</b> de vie supplémentaire...");
                summary.getSkillsEffectsEs().add("¡Has sido <b>envenenado</b> por el oponente Dinoz con sus garras especialmente preparadas! Pierdes un <b>5%</b> más de vida ...");
                summary.getSkillsEffectsEn().add("You have been <b>poisoned</b> by the opposing Dinoz with its specially prepared claws! You lose <b>5%</b> more percent of life ...");
                summary.setFinalLifeLoss(summary.getFinalLifeLoss() + 5);
        }

        if (homeDinoz.getPassiveList().get(DinoparcConstants.GRIFFES_EMPOISONNÉES) != null) {
            if (homeDinoz.getPassiveList().get(DinoparcConstants.GRIFFES_EMPOISONNÉES) > 0) {
                eventDinozUpdate.setLife(eventDinozUpdate.getLife() + 5);
                homeDinoz.getPassiveList().put(DinoparcConstants.GRIFFES_EMPOISONNÉES, homeDinoz.getPassiveList().get(DinoparcConstants.GRIFFES_EMPOISONNÉES) - 1);
                summary.getSkillsEffectsFr().add("<b>Vous avez empoisonné</b> le Dinoz adverse avec vos griffes spécialement préparées!");
                summary.getSkillsEffectsEs().add("¡<b>Has envenenado</b> al rival Dinoz con tus garras especialmente preparadas!");
                summary.getSkillsEffectsEn().add("<b>You have poisoned</b> the opposing Dinoz with your specially prepared claws!");
            }
        }
    }

    private void decideWistitiElements(int elementAttacker, EventDinozDto eventDinozDto) {
        eventDinozDto.setFire(ThreadLocalRandom.current().nextInt(1, 2 * elementAttacker));
        eventDinozDto.setWood(ThreadLocalRandom.current().nextInt(1, 2 * elementAttacker));
        eventDinozDto.setWater(ThreadLocalRandom.current().nextInt(1, 2 * elementAttacker));
        eventDinozDto.setThunder(ThreadLocalRandom.current().nextInt(1, 2 * elementAttacker));
        eventDinozDto.setAir(ThreadLocalRandom.current().nextInt(1, 2 * elementAttacker));
    }

    private void computeRaidBossElementsBeforeFight(EventDinozDto raidBoss) {
        buffRaidBossElements(raidBoss, raidBoss.getFirstElement(), 15);
        buffRaidBossElements(raidBoss, raidBoss.getSecondElement(), 10);
        buffRaidBossElements(raidBoss, raidBoss.getThirdElement(), 5);
    }

    private void buffRaidBossElements(EventDinozDto raidBoss, String element, Integer elementBuff) {
        switch (element) {
            case (DinoparcConstants.FEU) :
                raidBoss.setFire(raidBoss.getFire() + elementBuff);
                break;

            case (DinoparcConstants.TERRE) :
                raidBoss.setWood(raidBoss.getWood() + elementBuff);
                break;

            case (DinoparcConstants.EAU) :
                raidBoss.setWater(raidBoss.getWater() + elementBuff);
                break;

            case (DinoparcConstants.FOUDRE) :
                raidBoss.setThunder(raidBoss.getThunder() + elementBuff);
                break;

            case (DinoparcConstants.AIR) :
                raidBoss.setAir(raidBoss.getAir() + elementBuff);
                break;
        }
    }
}
