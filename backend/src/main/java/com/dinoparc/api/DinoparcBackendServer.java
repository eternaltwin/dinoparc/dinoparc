package com.dinoparc.api;

import java.net.URISyntaxException;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.autoconfigure.r2dbc.R2dbcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication(exclude = {R2dbcAutoConfiguration.class})
public class DinoparcBackendServer {
  public static void main(String[] args) throws URISyntaxException {
    System.setProperty("org.jooq.no-logo", "true");
    System.setProperty("org.jooq.no-tips", "true");
    new SpringApplicationBuilder(DinoparcBackendServer.class)
      .bannerMode(Banner.Mode.OFF)
      .run(args);
  }
}
