package com.dinoparc.api.schedulers;

import static com.dinoparc.api.domain.account.DinoparcConstants.RAID_BOSS_ID;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.dinoz.ArmyDinoz;
import com.dinoparc.api.domain.dinoz.EventDinozDto;
import com.dinoparc.api.repository.PgArmyDinozRepository;
import com.dinoparc.api.repository.PgBossRepository;
import com.dinoparc.api.repository.PgDinozRepository;
import com.dinoparc.api.repository.PgHistoryRepository;
import com.dinoparc.api.services.EventFightService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import java.util.UUID;

@Component
public class RaidBossArmyScheduler {

    private PgArmyDinozRepository armyDinozRepository;

    private PgBossRepository bossRepository;

    private PgDinozRepository dinozRepository;

    private EventFightService eventFightService;

    public RaidBossArmyScheduler(PgArmyDinozRepository armyDinozRepository,
                                 PgBossRepository bossRepository,
                                 PgDinozRepository dinozRepository,
                                 EventFightService eventFightService) {
        this.armyDinozRepository = armyDinozRepository;
        this.bossRepository = bossRepository;
        this.dinozRepository = dinozRepository;
        this.eventFightService = eventFightService;
    }

    @Scheduled(cron = "0 30 * * * ?", zone = "Europe/Paris")
    public void runArmy()
    {
        this.runArmy(false);
    }

    public void runArmy(boolean forceRun) {
        var raidBoss = bossRepository.getRaidBoss();

        if (forceRun || canStartArmy(raidBoss)) {
            bossRepository.setArmyAttacking(RAID_BOSS_ID, true);
            var armyFinished = false;

            while (!armyFinished
                    && raidBoss.getLife() > 0
                    && raidBoss.getRemainingArmyAttacks() > 0
                    && (forceRun || armyAllowed(raidBoss))) {
                var optArmyDinoz = armyDinozRepository.getNextArmyDinoz(RAID_BOSS_ID);

                if (optArmyDinoz.isEmpty()) {
                    armyFinished = true;
                } else {
                    raidBoss = runArmyDinozAttack(raidBoss, optArmyDinoz.get());
                    try {
                        Thread.sleep(DinoparcConstants.BOSS_ARMY_ATTACK_DELAY_MS);
                    } catch (InterruptedException e) {
                        // Nothing to do, an InterruptedException won't cause any issue here
                    }
                }
            }

            if (armyFinished) {
                bossRepository.setRemainingArmyAttacks(RAID_BOSS_ID, 0);
            }
        }
    }

    @Scheduled(cron = "0 55 * * * ?", zone = "Europe/Paris")
    public void cleanArmy()
    {
        cleanArmy(false);
    }

    public void cleanArmy(boolean forceRun) {
        var boss = bossRepository.getRaidBoss();

        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
        if (forceRun || (
                boss.getDayOfWeek() == now.getDayOfWeek().getValue()
                && boss.getHourOfDay() <     now.getHour()
                && (
                    boss.getRemainingArmyAttacks() <= 0 || boss.getLife() <=0
                )
        )) {
            armyDinozRepository.cleanAllForBoss(RAID_BOSS_ID);
        }
    }

    private boolean canStartArmy(EventDinozDto boss) {
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
        return boss.getDayOfWeek() == now.getDayOfWeek().getValue()
                && boss.getHourOfDay() == now.getHour()
                && boss.getMinutesDuration() <= now.getMinute()
                && boss.getLife() > 0
                && !boss.getArmyAttacking()
                && boss.getRemainingArmyAttacks() > 0;
    }

    private boolean armyAllowed(EventDinozDto boss) {
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));

        // Army allowed only same day and during one hour
        // with boss alive, and positive remaining army attacks
        return boss.getDayOfWeek() == now.getDayOfWeek().getValue()
            && (
                (boss.getHourOfDay() == now.getHour())
                ||
                (boss.getHourOfDay() + 1 == now.getHour() && now.getMinute() < 31)
            );
    }

    private EventDinozDto runArmyDinozAttack(EventDinozDto boss, ArmyDinoz armyDinoz) {
        var optAttackingDinoz = dinozRepository.findById(armyDinoz.getDinozId());

        // Check attacking dinoz still exists
        if (optAttackingDinoz.isEmpty()) {
            armyDinozRepository.deleteArmyDinozAndUpdateRanks(UUID.fromString(armyDinoz.getArmyDinozId()), UUID.fromString(boss.getBossId()), armyDinoz.getRank());
            return bossRepository.getRaidBoss();
        }

        // Retrieve attacking dinoz, if KO then update its maxNbAttacks to its current nbAttacks value
        var attackingDinoz = optAttackingDinoz.get();
        if (attackingDinoz.getLife() == 0) {
            armyDinozRepository.updateMaxNbAttacks(UUID.fromString(armyDinoz.getArmyDinozId()), armyDinoz.getNbAttacks());
            return bossRepository.getRaidBoss();
        }

        // Run boss fight
        var summary = eventFightService.fightBoss(attackingDinoz, boss, true);

        // Update army dinoz nbAttacks and bossLifeLost
        armyDinozRepository.addFightAndBossLifeLost(UUID.fromString(armyDinoz.getArmyDinozId()), summary.getEventDinozLifeLoss());

        // Decrement raid boss remaining army attacks and return updated raid boss
        return bossRepository.decrementBossRemainingArmyAttacks(RAID_BOSS_ID);
    }
}
