package com.dinoparc.api.schedulers;

import com.dinoparc.api.repository.PgHistoryRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CleanHistoryScheduler {
    private PgHistoryRepository historyRepository;

    public CleanHistoryScheduler(PgHistoryRepository historyRepository) {
        this.historyRepository = historyRepository;
    }

    @Scheduled(cron = "0 */5 * * * ?", zone = "Europe/Paris")
    public void cleanHistory() {
        historyRepository.cleanHistory();
    }
}
