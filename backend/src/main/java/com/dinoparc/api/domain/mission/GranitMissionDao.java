package com.dinoparc.api.domain.mission;

public class GranitMissionDao {

    private String id;
    private String playerId;
    private Boolean isComplete;
    private Boolean isCompleteAndValidated;
    private Integer moueffe;
    private Integer picori;
    private Integer castivore;
    private Integer sirain;
    private Integer winks;
    private Integer gorilloz;
    private Integer cargou;
    private Integer hippoclamp;
    private Integer rokky;
    private Integer pigmou;
    private Integer wanwan;
    private Integer goupignon;
    private Integer kump;
    private Integer pteroz;
    private Integer santaz;
    private Integer korgon;
    private Integer kabuki;
    private Integer serpantin;
    private Integer soufflet;
    private Integer feross;

    private Integer mannyLife;
    private Integer mannyLocation;

    public GranitMissionDao() {

    }

    public void validateStatus() {
        if (this.moueffe >= 10
                && this.picori >= 10
                && this.castivore >= 10
                && this.sirain >= 10
                && this.winks >= 10
                && this.gorilloz >= 10
                && this.cargou >= 10
                && this.hippoclamp >= 10
                && this.rokky >= 10
                && this.pigmou >= 10
                && this.wanwan >= 10
                && this.goupignon >= 3
                && this.kump >= 10
                && this.pteroz >= 10
                && this.santaz >= 3
                && this.korgon >= 10
                && this.kabuki >= 5
                && this.serpantin >= 3
                && this.soufflet >= 2
                && this.feross >= 1
        ) {
            this.isComplete = true;
        }
    }

    public Boolean isMannyKO() {
        return this.mannyLife <= 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public Boolean getComplete() {
        return isComplete;
    }

    public void setComplete(Boolean complete) {
        isComplete = complete;
    }

    public Boolean getCompleteAndValidated() {
        return isCompleteAndValidated;
    }

    public void setCompleteAndValidated(Boolean completeAndValidated) {
        isCompleteAndValidated = completeAndValidated;
    }

    public Integer getMoueffe() {
        return moueffe;
    }

    public void setMoueffe(Integer moueffe) {
        this.moueffe = moueffe;
    }

    public Integer getPicori() {
        return picori;
    }

    public void setPicori(Integer picori) {
        this.picori = picori;
    }

    public Integer getCastivore() {
        return castivore;
    }

    public void setCastivore(Integer castivore) {
        this.castivore = castivore;
    }

    public Integer getSirain() {
        return sirain;
    }

    public void setSirain(Integer sirain) {
        this.sirain = sirain;
    }

    public Integer getWinks() {
        return winks;
    }

    public void setWinks(Integer winks) {
        this.winks = winks;
    }

    public Integer getGorilloz() {
        return gorilloz;
    }

    public void setGorilloz(Integer gorilloz) {
        this.gorilloz = gorilloz;
    }

    public Integer getCargou() {
        return cargou;
    }

    public void setCargou(Integer cargou) {
        this.cargou = cargou;
    }

    public Integer getHippoclamp() {
        return hippoclamp;
    }

    public void setHippoclamp(Integer hippoclamp) {
        this.hippoclamp = hippoclamp;
    }

    public Integer getRokky() {
        return rokky;
    }

    public void setRokky(Integer rokky) {
        this.rokky = rokky;
    }

    public Integer getPigmou() {
        return pigmou;
    }

    public void setPigmou(Integer pigmou) {
        this.pigmou = pigmou;
    }

    public Integer getWanwan() {
        return wanwan;
    }

    public void setWanwan(Integer wanwan) {
        this.wanwan = wanwan;
    }

    public Integer getGoupignon() {
        return goupignon;
    }

    public void setGoupignon(Integer goupignon) {
        this.goupignon = goupignon;
    }

    public Integer getKump() {
        return kump;
    }

    public void setKump(Integer kump) {
        this.kump = kump;
    }

    public Integer getPteroz() {
        return pteroz;
    }

    public void setPteroz(Integer pteroz) {
        this.pteroz = pteroz;
    }

    public Integer getSantaz() {
        return santaz;
    }

    public void setSantaz(Integer santaz) {
        this.santaz = santaz;
    }

    public Integer getKorgon() {
        return korgon;
    }

    public void setKorgon(Integer korgon) {
        this.korgon = korgon;
    }

    public Integer getKabuki() {
        return kabuki;
    }

    public void setKabuki(Integer kabuki) {
        this.kabuki = kabuki;
    }

    public Integer getSerpantin() {
        return serpantin;
    }

    public void setSerpantin(Integer serpantin) {
        this.serpantin = serpantin;
    }

    public Integer getSoufflet() {
        return soufflet;
    }

    public void setSoufflet(Integer soufflet) {
        this.soufflet = soufflet;
    }

    public Integer getFeross() {
        return feross;
    }

    public void setFeross(Integer feross) {
        this.feross = feross;
    }

    public Integer getMannyLife() {
        return mannyLife;
    }

    public void setMannyLife(Integer mannyLife) {
        this.mannyLife = mannyLife;
    }

    public Integer getMannyLocation() {
        return mannyLocation;
    }

    public void setMannyLocation(Integer mannyLocation) {
        this.mannyLocation = mannyLocation;
    }
}
