package com.dinoparc.api.domain.account;

import discord4j.common.util.Snowflake;

import java.util.*;

public interface DinoparcConstants {

  //IMPORTANT : EDIT THESE FLAGS WHEN THE WAR CHANGES :
  public static final boolean WAR_MODE = false;
  public static final int ACTUAL_WAR_NUMBER = 4;
  public static final int CLAN_PLAYER_LIMIT = 5;

  public static final String PVE_FACTION = "Pve_Faction";
  public static final String NO_FACTION = "noFaction";
  public static final String FACTION_1 = "Faction_1_Ozonite";
  public static final String FACTION_1_GUERRIER = "Faction_1_Ozonite.warrior";
  public static final String FACTION_1_ACTION = "Faction_1_Ozonite_Portail";
  public static final String FACTION_2 = "Faction_2_Gorak";
  public static final String FACTION_2_GUERRIER = "Faction_2_Gorak.warrior";
  public static final String FACTION_2_ACTION = "Faction_2_Gorak_Portail";
  public static final String FACTION_3 = "Faction_3_Buldozor";
  public static final String FACTION_3_GUERRIER = "Faction_3_Buldozor.warrior";
  public static final String FACTION_3_ACTION = "Faction_3_Buldozor_Portail";

  public static final String CLAN_CREATOR = "admin-creator";
  public static final String CLAN_ADMIN = "admin";
  public static final String CLAN_MEMBER = "member";

  public static final String PERCEPTION = "Perception";
  public static final String INTELLIGENCE = "Intelligence";
  public static final String ENDURANCE = "Endurance";
  public static final String ARTS_MARTIAUX = "Arts Martiaux";
  public static final String CAMOUFLAGE = "Camouflage";
  public static final String MEDECINE = "Médecine";
  public static final String NATATION = "Natation";
  public static final String ESCALADE = "Escalade";
  public static final String SURVIE = "Survie";
  public static final String STRATEGIE = "Stratégie";
  public static final String COMMERCE = "Commerce";
  public static final String NAVIGATION = "Navigation";
  public static final String COURSE = "Course";
  public static final String CONTRE_ATTAQUE = "Contre Attaque";
  public static final String CHANCE = "Chance";
  public static final String MUSIQUE = "Musique";
  public static final String CUISINE = "Cuisine";
  public static final String SAUT = "Saut";
  public static final String FORCE = "Force";
  public static final String AGILITÉ = "Agilité";
  public static final String FOUILLER = "Fouiller";
  public static final String VOLER = "Voler";
  public static final String TAUNT = "Taunt";
  public static final String ROCK = "Rock";
  public static final String APPRENTI_FEU = "Apprenti Feu";
  public static final String APPRENTI_TERRE = "Apprenti Terre";
  public static final String APPRENTI_EAU = "Apprenti Eau";
  public static final String APPRENTI_FOUDRE = "Apprenti Foudre";
  public static final String POUVOIR_SOMBRE = "Pouvoir Sombre";
  public static final String PROTECTIONDUFEU = "Protection du Feu";
  public static final String FORTERESSE = "Forteresse";
  public static final String SOLUBILITE = "Solubilité";
  public static final String RESILIENCE = "Résilience";
  public static final String COUP_CRITIQUE = "Coup Critique";
  public static final String BAIN_FLAMMES = "Bain de Flammes";
  public static final String CUEILLETTE = "Cueillette";
  public static final String PÊCHE = "Pêche";
  public static final String INGENIEUR = "Ingénieur";
  public static final String CHASSEUR = "Chasseur";
  public static final String PIQURE = "Piqûre";

  public static final String FEU = "Feu";
  public static final String TERRE = "Terre";
  public static final String EAU = "Eau";
  public static final String FOUDRE = "Foudre";
  public static final String AIR = "Air";

  public static final String MOUEFFE = "Moueffe";
  public static final String PICORI = "Picori";
  public static final String CASTIVORE = "Castivore";
  public static final String SIRAIN = "Sirain";
  public static final String WINKS = "Winks";
  public static final String GORILLOZ = "Gorilloz";
  public static final String CARGOU = "Cargou";
  public static final String HIPPOCLAMP = "Hippoclamp";
  public static final String ROKKY = "Rokky";
  public static final String PIGMOU = "Pigmou";
  public static final String WANWAN = "Wanwan";
  public static final String GOUPIGNON = "Goupignon";
  public static final String KUMP = "Kump";
  public static final String PTEROZ = "Pteroz";
  public static final String SANTAZ = "Santaz";
  public static final String OUISTITI = "Ouistiti";
  public static final String KORGON = "Korgon";
  public static final String KABUKI = "Kabuki";
  public static final String SERPANTIN = "Serpantin";
  public static final String SOUFFLET = "Soufflet";
  public static final String FEROSS = "Feross";
  public static final String MAHAMUTI = "Mahamuti";

  public static final Integer FOCUS_LIMIT = 5;
  public static final String POTION_IRMA = "Potion Irma";
  public static final String POTION_IRMA_SURPLUS = "Potion Irma Surplus";
  public static final String POTION_ANGE = "Potion Ange";
  public static final String NUAGE_BURGER = "Nuage Burger";
  public static final String PAIN_CHAUD = "Pain Chaud";
  public static final String TARTE_VIANDE = "Tarte Viande";
  public static final String MEDAILLE_CHOCOLAT = "Médaille Chocolat";
  public static final String BAVE_LOUPI = "Bave Loupi";
  public static final String RAMENS = "Bol Ramens";
  public static final String POTION_SOMBRE = "Potion Sombre";
  public static final String COULIS_CERISE = "Coulis Cerise";
  public static final String ETERNITY_PILL = "Eternity Pill";
  public static final String OEUF_GLUON = "EGG_11";
  public static final String OEUF_SANTAZ = "EGG_14";
  public static final String OEUF_SERPANTIN = "EGG_18";
  public static final String OEUF_FEROSS = "EGG_20";
  public static final String OEUF_COBALT = "EGG_X";
  public static final String OEUF_CHOCOLAT = "EGG_CHOC";
  public static final String TISANE = "Tisane des bois";
  public static final String BIERE = "Bière de Dinojak";
  public static final String POTION_GUERRE = "Potion Guerre";
  public static final String LAIT_DE_CARGOU = "Lait de Cargou";

  public static final String CHARME_FEU = "bonus_fire";
  public static final String CHARME_TERRE = "bonus_wood";
  public static final String CHARME_EAU = "bonus_water";
  public static final String CHARME_FOUDRE = "bonus_thunder";
  public static final String CHARME_AIR = "bonus_air";
  public static final String CHARME_PRISMATIK = "bonus_prismatik";
  public static final String FOCUS_AGGRESIVITE = "bonus_boostaggro";
  public static final String FOCUS_NATURE = "bonus_boostnature";
  public static final String FOCUS_SIRAINS = "bonus_boostwater";
  public static final String FOCUS_MAHAMUTI = "bonus_mahamuti";
  public static final String IMMUNITÉ = "bonus_immunity";
  public static final String ANTIDOTE = "bonus_pill";
  public static final String OEIL_DU_TIGRE = "bonus_tigereye";
  public static final String GRIFFES_EMPOISONNÉES = "bonus_claw";
  public static final String EMPOISONNE = "effect_poison";
  public static final String CHIKABUM = "effect_virus";
  public static final String KABUKI_QUETE = "effect_kabuki";

  public static final String COMBAT = "Combat";
  public static final String DÉPLACERVERT = "Deplacer";
  public static final String POSTEMISSIONS = "PosteMissions";
  public static final String LEVELUP = "LevelUp";
  public static final String MADAME_IRMA = "MadameIrma";
  public static final String CATAPULTE = "Catapulte";
  public static final String BAZAR = "JazzBazar";
  public static final String CREDIT = "CreditBank";
  public static final String BARRAGE = "Barrage";
  public static final String PASSAGEGRANIT = "PassageGranit";
  public static final String GARDE_GRANIT = "GardeGranit";
  public static final String GARDE_GRANIT_MISSION_STATUS = "GardeGranitMissionStatus";
  public static final String PASSAGEBALEINE = "PassageBaleine";
  public static final String PASSAGELABYRINTHE = "PassageLabyrinthe";
  public static final String ACT_ESCALADER = "Escalader";
  public static final String ACT_DELTAPLANER = "PrendreDeltaplane";
  public static final String ACT_NAVIGUER = "PrendrePrunayer";

  public static final String ACT_ARMY_JOIN_ARMY = "Army_Join";
  public static final String ACT_ARMY_QUIT_ARMY = "Army_Quit";
  public static final String ACT_ARMY_SEE_ARMY = "Army_See";

  public static final String BEGINMESSAGE1 = "Go!";
  public static final String ENDMESSAGE1 = "Ouille ouille ouille...";
  public static final String BEGINMESSAGEBOT = "Grrr!";
  public static final String ENDMESSAGEBOT = "Gloups...";

  public static final String BUZE = "totemBuze";
  public static final String PLUME = "totemPlume";
  public static final String DENT = "totemDent";
  public static final String CRIN = "totemCrin";
  public static final String BOIS = "totemBois";

  public static final String FEUILLE_PACIFIQUE = "Feuille de Pacifique";
  public static final String OREADE_BLANC = "Oréade blanc";
  public static final String TIGE_RONCIVORE = "Tige de Roncivore";
  public static final String ANÉMONE_SOLITAIRE = "Anémone solitaire";
  public static final String PERCHE_PERLEE = "Perche perlée";
  public static final String GREMILLE_GRELOTTANTE = "Grémille grelottante";
  public static final String CUBE_GLU = "Cube de glu";
  public static final String ANGUILLE_CELESTE = "Anguille";

  public static final String JETON_CASINO_OR = "JetonCasinoOr";
  public static final String JETON_CASINO_ARGENT = "JetonCasinoArgent";
  public static final String JETON_CASINO_BRONZE = "JetonCasinoBronze";

  public static final List<Integer> FISHING_LOCS = new ArrayList<Integer>(){{
    add(3);
    add(5);
    add(11);
    add(24);
    add(33);
  }};

  public static final List<Integer> PICKING_LOCS = new ArrayList<Integer>(){{
    add(2);
    add(9);
    add(13);
    add(26);
    add(27);
  }};

  public static final String GLOBE_CHARBON = "globe_charbon";
  public static final String GLOBE_BRONZE = "globe_bronze";
  public static final String GLOBE_ARGENT = "globe_argent";
  public static final String GLOBE_OR = "globe_or";
  public static final String GLOBE_PLATINE = "globe_platine";
  public static final String GLOBE_DIAMANT = "globe_diamant";
  public static final String GIFT = "Ticket Cadeau";
  public static final String ACT_GIFT = "Gift";
  public static final String ENTER_DEMON = "Enter Demon";

  public static final HashMap<String, Integer> PRICES = new HashMap<String, Integer>() {{
    put(BUZE, 40);
    put(PLUME, 60);
    put(DENT, 100);
    put(CRIN, 200);
    put(BOIS, 500);

    put(PERCHE_PERLEE, 40);
    put(GREMILLE_GRELOTTANTE, 60);
    put(CUBE_GLU, 300);
    put(ANGUILLE_CELESTE, 1000);

    put(FEUILLE_PACIFIQUE, 40);
    put(OREADE_BLANC, 60);
    put(TIGE_RONCIVORE, 200);
    put(ANÉMONE_SOLITAIRE, 400);
  }};

  public static final String TournoiDinoville = "TournoideDinoville";
  public static final String TournoiDinoplage = "TournoideDinoplage";
  public static final String TournoiMontDino = "TournoiduMontDino";
  public static final String TournoiTemple = "TournoiduTempleBouddhiste";
  public static final String TournoiRuines = "TournoidesRuinesMayincas";
  public static final String PRUNE_SHOP = "PruneShop";
  public static final String PRUNIAC = "Pruniac";
  public static final String CRATER_SHOP = "CraterShop";
  public static final String ANOMALY_SHOP = "AnomalyShop";
  public static final String PLAINS_SHOP = "PlainsShop";
  public static final String PLAINS_RAID_EXCHANGE = "raidExchange";
  public static final String ARENA = "TheArena";
  public static final String HERMIT = "Hermit";
  public static final String COINS = "COINS";
  public static final String TAVERNE = "Taverne";
  public static final String PAMAGUERRE = "Pamaguerre";
  public static final String MARCHÉPIRATE = "MARCHÉPIRATE";
  public static final String INUIT = "INUIT";
  
  public static final String KABUKI_0_ID = "@JEUNE-KABUKI";
  public static final String KABUKI_1_ID = "@APPRENTI-KABUKI";
  public static final String KABUKI_2_ID = "@NOVICE-KABUKI";
  public static final String KABUKI_3_ID = "@GUERRIER-KABUKI";
  public static final String KABUKI_4_ID = "@SOLDAT-KABUKI";
  public static final String KABUKI_5_ID = "@COMMANDO-KABUKI";
  public static final String KABUKI_6_ID = "@MAGE-KABUKI";
  public static final String KABUKI_7_ID = "@SORCIER-KABUKI";
  public static final String KABUKI_8_ID = "@GENERAL-KABUKI";
  public static final String KABUKI_9_ID = "@BOSS-KABUKI";

  public static final String MANNY_ID = "@MANNY";
  public static final String MANNY_APPCODE = "L7BU9KGZ59PUJM1";
  
  public static final String KABUKI_0_APPCODE = "HOKMSTahkin0000";
  public static final String KABUKI_1_APPCODE = "Hrx249EPUcgo000";
  public static final String KABUKI_2_APPCODE = "Hv6CJMSXZty4000";
  public static final String KABUKI_3_APPCODE = "H58DFKNQYa6mqtu";
  public static final String KABUKI_4_APPCODE = "HU7EFXZbeo37qtu";
  public static final String KABUKI_5_APPCODE = "HBIPTYceiqs7qtu";
  public static final String KABUKI_6_APPCODE = "H3DLORZgiu7Btu0";
  public static final String KABUKI_7_APPCODE = "HDOabfkmqrv3Btu";
  public static final String KABUKI_8_APPCODE = "HKSUWbgsv6FNBtu";
  public static final String KABUKI_9_APPCODE = "HZjqy3DTXdsNBts";
  
  public static final String FUSION_CENTER = "CentreFusions";
  public static final String CHAMPIFUZ = "Ticket Champifuz";
  public static final String SPECIAL_CHAMPIFUZ = "Ticket Champifuz Special";
  public static final String MONO_CHAMPIFUZ = "Ticket Monochrome";
  public static final String MAGIK_CHAMPIFUZ = "Ticket Magik";
  public static final String wbm = "Ouh! Ouh! Ah!";
  public static final String wem = "Ouh ouh...";
  public static final String rbbm = "AAAARRRRGGHHHH!";
  public static final String rbem = "(Burp...)";
  public static final String BONS = "Bons du Trésor";
  public static final String NPCDinozBuyer = "NPCDinozBuyer";
  public static final String CASINO = "Casino";
  public static final String SACRIFICE = "AutelSacrifices";
  public static final String DINOBATH = "DinoBath";
  public static final String DINOFOUNTAIN = "DinoFountain";
  public static final Integer BONS_BUY = 6000;
  public static final Integer BONS_SELL = 4000;
  public static final String EGG_HATCHER = "EggHatcher";
  public static final String GOUTTE_GLU = "Goutte Glu";
  public static final Integer CHARMS_LIMIT = 10;
  public static final String CHAUDRON = "Chaudron";
  public static final String HORDES = "HordesHalloweenEvent";
  public static final String CANNEAPECHE = "CanneAPeche";

  public static final Integer INITIALMONEY = 99000;
  public static final String TOKEN_PREFIX = "Bearer ";
  public static final Integer JWT_EXPIRATION_TIME = 86400000;
  public static final String HEADER_STRING = "Authorization";

  public static final String[] UnrestrictedPathsArray = new String[] {
          "/api/utils/numberplayers",
          "/api/utils/numberdinozs",
          "/api/account/redirect",
          "/api/account/callback",
          "/api/admin/**",
          "/swagger-ui/**",
          "/v3/api-docs/**",
  };

  public static final String SUPERADMIN_ROLE = "superadmin";
  public static final String ADMIN_ROLE = "admin";
  public static final String GAME_MASTER = "game_master";
  public static final String PLAYER_ROLE = "player";
  public static final List<String> ROLES_SUPERADMIN = Arrays.asList(SUPERADMIN_ROLE);
  public static final List<String> ROLES_ADMIN = Arrays.asList(ADMIN_ROLE, SUPERADMIN_ROLE);
  public static final List<String> ROLES_ADMIN_OR_GAME_MASTER = Arrays.asList(ADMIN_ROLE, SUPERADMIN_ROLE, GAME_MASTER);
  public static final List<String> ROLES_PLAYER = Arrays.asList(PLAYER_ROLE);

  public static final List<Integer> TOURNAMENTS = Arrays.asList(0, 3, 6, 10, 13);
  public static final String TELESCOPE = "Telescope";

  public static final int PLAYER_MISSION_LIMIT_DAILY = 3;
  public static final int PLAYER_MISSION_LIMIT_WEEKLY = 3;
  public static final int PLAYER_MISSION_LIMIT_MONTHLY = 3;
  public static final int PLAYER_DINOZ_MISSION_LIMIT_DAILY = 3;
  public static final int PLAYER_DINOZ_MISSION_LIMIT_WEEKLY = 3;
  public static final int PLAYER_DINOZ_MISSION_LIMIT_MONTHLY = 3;
  public static final String HERO_SIBILIN = "HeroTalk";
  public static final String CERBERE = "CerbereTalk";
  public static final int MAX_VISIBLE_HISTORY_SIZE = 500;

  public static final int RAID_BOSS_MAX_LIFE = 30000000;
  public static final UUID RAID_BOSS_ID = UUID.fromString("00000000-0000-0000-0000-000000000000");
  public static final int RAID_BOSS_MAX_NB_ATTACK = 7500;
  public static final int RAID_BOSS_ARMY_DINOZ_MAX_NB_ATTACK = 100;
  public static final int RAID_BOSS_LIMIT_NB_ATTACK = 1500;
  public static final int RAID_BOSS_GOLD_AFTER_LIMIT = 550;
  public static final int BOSS_ARMY_DINOZ_MAX_NB_ATTACK = 100;
  public static final int BOSS_ARMY_ATTACK_DELAY_MS = 250;
  public static final int BOSS_ARMY_ATTACK_LIFE_FACTOR = 10;

  public static final Snowflake CHANNEL_UPDATE =  Snowflake.of("969699481832529941");
  public static final Snowflake CHANNEL_GENERAL =  Snowflake.of("693527322032209921");
  public static final Snowflake CHANNEL_REACTIONS =  Snowflake.of("969696996728406026");
  public static final Snowflake CHANNEL_TOURNOIS =  Snowflake.of("969698289966518373");
  public static final Snowflake CHANNEL_LEVELUP =  Snowflake.of("969697271212036096");
  public static final Snowflake CHANNEL_OUISTITI =  Snowflake.of("969698006158954596");
  public static final Snowflake CHANNEL_BAZAR =  Snowflake.of("969697596740370504");
  public static final Snowflake CHANNEL_CLANS =  Snowflake.of("969696879464046683");
  public static final Snowflake CHANNEL_EVENTS =  Snowflake.of("1180263805289234482");
  public static final Snowflake CHANNEL_BUGS =  Snowflake.of("969698424851165204");
  public static final Snowflake CHANNEL_CERBERE =  Snowflake.of("1206728633821954118");
}
