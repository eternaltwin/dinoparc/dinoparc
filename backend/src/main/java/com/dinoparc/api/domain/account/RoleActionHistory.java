package com.dinoparc.api.domain.account;

import java.time.OffsetDateTime;
import java.util.UUID;

public class RoleActionHistory {

    private UUID id;
    private String author;
    private String endpoint;
    private String content;
    private boolean success;
    private boolean authorized;
    private OffsetDateTime numericalDate;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isAuthorized() {
        return authorized;
    }

    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }

    public OffsetDateTime getNumericalDate() {
        return numericalDate;
    }

    public void setNumericalDate(OffsetDateTime numericalDate) {
        this.numericalDate = numericalDate;
    }
}
