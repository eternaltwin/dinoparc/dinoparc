package com.dinoparc.api.domain.dinoz;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.account.RandomCollection;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class DinozUtils {

    public static String getRandomHexString() {
        Random r = new Random();
        String alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        StringBuffer sb = new StringBuffer();

        while (sb.length() < 21) {
            sb.append(alphabet.charAt(r.nextInt(alphabet.length())));
        }

        return sb.toString().substring(0, 21);
    }

    public static String getRaceChar(String race) {
        switch (race) {
            case DinoparcConstants.MOUEFFE:
                return "0";
            case DinoparcConstants.PICORI:
                return "1";
            case DinoparcConstants.CASTIVORE:
                return "2";
            case DinoparcConstants.SIRAIN:
                return "3";
            case DinoparcConstants.WINKS:
                return "4";
            case DinoparcConstants.GORILLOZ:
                return "5";
            case DinoparcConstants.CARGOU:
                return "6";
            case DinoparcConstants.HIPPOCLAMP:
                return "7";
            case DinoparcConstants.ROKKY:
                return "8";
            case DinoparcConstants.PIGMOU:
                return "9";
            case DinoparcConstants.WANWAN:
                return "A";
            case DinoparcConstants.GOUPIGNON:
                return "B";
            case DinoparcConstants.KUMP:
                return "C";
            case DinoparcConstants.PTEROZ:
                return "D";
            case DinoparcConstants.SANTAZ:
                return "E";
            case DinoparcConstants.OUISTITI:
                return "F";
            case DinoparcConstants.KORGON:
                return "G";
            case DinoparcConstants.KABUKI:
                return "H";
            case DinoparcConstants.SERPANTIN:
                return "I";
            case DinoparcConstants.SOUFFLET:
                return "J";
            case DinoparcConstants.FEROSS:
                return "K";
            case DinoparcConstants.MAHAMUTI:
                return "L";
            default:
                return "0";
        }
    }

    public static Integer getRaceNumber(String race) {
        switch (race) {
            case DinoparcConstants.MOUEFFE:
                return 0;
            case DinoparcConstants.PICORI:
                return 1;
            case DinoparcConstants.CASTIVORE:
                return 2;
            case DinoparcConstants.SIRAIN:
                return 3;
            case DinoparcConstants.WINKS:
                return 4;
            case DinoparcConstants.GORILLOZ:
                return 5;
            case DinoparcConstants.CARGOU:
                return 6;
            case DinoparcConstants.HIPPOCLAMP:
                return 7;
            case DinoparcConstants.ROKKY:
                return 8;
            case DinoparcConstants.PIGMOU:
                return 9;
            case DinoparcConstants.WANWAN:
                return 10;
            case DinoparcConstants.GOUPIGNON:
                return 11;
            case DinoparcConstants.KUMP:
                return 12;
            case DinoparcConstants.PTEROZ:
                return 13;
            case DinoparcConstants.SANTAZ:
                return 14;
            case DinoparcConstants.OUISTITI:
                return 15;
            case DinoparcConstants.KORGON:
                return 16;
            case DinoparcConstants.KABUKI:
                return 17;
            case DinoparcConstants.SERPANTIN:
                return 18;
            case DinoparcConstants.SOUFFLET:
                return 19;
            case DinoparcConstants.FEROSS:
                return 20;
            case DinoparcConstants.MAHAMUTI:
                return 21;
            default:
                return -1;
        }
    }

    public static String getRaceFromRaceNumber(Integer raceNumber) {
        switch (raceNumber) {
            case 0:
                return DinoparcConstants.MOUEFFE;
            case 1:
                return DinoparcConstants.PICORI;
            case 2:
                return DinoparcConstants.CASTIVORE;
            case 3:
                return DinoparcConstants.SIRAIN;
            case 4:
                return DinoparcConstants.WINKS;
            case 5:
                return DinoparcConstants.GORILLOZ;
            case 6:
                return DinoparcConstants.CARGOU;
            case 7:
                return DinoparcConstants.HIPPOCLAMP;
            case 8:
                return DinoparcConstants.ROKKY;
            case 9:
                return DinoparcConstants.PIGMOU;
            case 10:
                return DinoparcConstants.WANWAN;
            case 11:
                return DinoparcConstants.GOUPIGNON;
            case 12:
                return DinoparcConstants.KUMP;
            case 13:
                return DinoparcConstants.PTEROZ;
            case 14:
                return DinoparcConstants.SANTAZ;
            case 15:
                return DinoparcConstants.OUISTITI;
            case 16:
                return DinoparcConstants.KORGON;
            case 17:
                return DinoparcConstants.KABUKI;
            case 18:
                return DinoparcConstants.SERPANTIN;
            case 19:
                return DinoparcConstants.SOUFFLET;
            case 20:
                return DinoparcConstants.FEROSS;
            case 21:
                return DinoparcConstants.MAHAMUTI;
            default:
                return DinoparcConstants.MOUEFFE;
        }
    }

    public static Dinoz initNewWis(EventDinozDto wildWistiti, Player account) {
        Dinoz newWis = new Dinoz();
        newWis.setId(UUID.randomUUID().toString());
        newWis.setMasterId(account.getId());
        newWis.setMasterName(account.getName());
        newWis.setName("???");
        newWis.setRace(DinoparcConstants.OUISTITI);
        newWis.setBeginMessage(DinoparcConstants.BEGINMESSAGE1);
        newWis.setEndMessage(DinoparcConstants.ENDMESSAGE1);
        newWis.setLife(100);
        newWis.setLevel(1);
        newWis.setExperience(0);
        newWis.setDanger(0);
        newWis.getElementsValues().put(DinoparcConstants.FEU, 0);
        newWis.getElementsValues().put(DinoparcConstants.TERRE, 1);
        newWis.getElementsValues().put(DinoparcConstants.EAU, 1);
        newWis.getElementsValues().put(DinoparcConstants.FOUDRE, 1);
        newWis.getElementsValues().put(DinoparcConstants.AIR, 0);
        newWis.getActionsMap().put(DinoparcConstants.COMBAT, true);
        newWis.getActionsMap().put(DinoparcConstants.DÉPLACERVERT, true);
        newWis.setPlaceNumber(17);
        newWis.setAppearanceCode(wildWistiti.getAppearanceCode());
        newWis.setDinozIsActive(true);

        return newWis;
    }

    public static Integer getRandomPlaceNumberThatIsNotAFactionBase() {
        RandomCollection<Integer> randomWarLocation = new RandomCollection<Integer>()
                .add(1, 0)
                .add(1, 1)
                .add(1, 2)
                .add(1, 3)
                .add(1, 4)
                .add(1, 5)
                .add(1, 7)
                .add(1, 8)
                .add(1, 9)
                .add(1, 10)
                .add(1, 11)
                .add(1, 13)
                .add(1, 14)
                .add(1, 15)
                .add(1, 16)
                .add(1, 17)
                .add(1, 19)
                .add(1, 20)
                .add(1, 21)
                .add(1, 22)
                .add(1, 37);
        return (Integer) randomWarLocation.next();
    }

    public static String getRandomRace() {
        RandomCollection<String> randomRace = new RandomCollection<String>()
                .add(1, "0")
                .add(1, "1")
                .add(1, "2")
                .add(1, "3")
                .add(1, "4")
                .add(1, "5")
                .add(1, "6")
                .add(1, "7")
                .add(1, "8")
                .add(1, "9")
                .add(1, "A")
                .add(1, "B")
                .add(1, "C")
                .add(1, "D")
                .add(1, "E")
                .add(1, "F")
                .add(1, "G")
                .add(1, "H")
                .add(1, "I")
                .add(1, "J");
        return (String) randomRace.next();
    }

    public static void initializeNPCDinoz(Dinoz listedDinoz) {
        switch (String.valueOf(listedDinoz.getAppearanceCode().charAt(0))) {
            case "0":
                listedDinoz.getElementsValues().put(DinoparcConstants.FEU, 2);
                break;

            case "1":
                listedDinoz.getElementsValues().put(DinoparcConstants.AIR, 2);
                break;

            case "2":
                listedDinoz.getElementsValues().put(DinoparcConstants.AIR, 1);
                listedDinoz.getElementsValues().put(DinoparcConstants.TERRE, 1);
                break;

            case "3":
                listedDinoz.getElementsValues().put(DinoparcConstants.EAU, 2);
                break;

            case "4":
                listedDinoz.getElementsValues().put(DinoparcConstants.EAU, 1);
                listedDinoz.getElementsValues().put(DinoparcConstants.FOUDRE, 1);
                break;

            case "5":
                listedDinoz.getElementsValues().put(DinoparcConstants.TERRE, 2);
                break;

            case "6":
                listedDinoz.getElementsValues().put(DinoparcConstants.TERRE, 1);
                listedDinoz.getElementsValues().put(DinoparcConstants.EAU, 1);
                break;

            case "7":
                listedDinoz.getElementsValues().put(DinoparcConstants.FEU, 1);
                listedDinoz.getElementsValues().put(DinoparcConstants.TERRE, 1);
                listedDinoz.getElementsValues().put(DinoparcConstants.EAU, 1);
                listedDinoz.getElementsValues().put(DinoparcConstants.FOUDRE, 1);
                listedDinoz.getElementsValues().put(DinoparcConstants.AIR, 1);
                break;

            case "8":
                listedDinoz.getElementsValues().put(DinoparcConstants.FOUDRE, 1);
                listedDinoz.getSkillsMap().put(DinoparcConstants.ROCK, 1);
                listedDinoz.getActionsMap().put(DinoparcConstants.ROCK, true);
                break;

            case "9":
                listedDinoz.getElementsValues().put(DinoparcConstants.FEU, 2);
                break;

            case "A":
                listedDinoz.getElementsValues().put(DinoparcConstants.FOUDRE, 2);
                break;

            case "C":
                listedDinoz.getElementsValues().put(DinoparcConstants.EAU, 2);
                break;

            case "D":
                listedDinoz.getElementsValues().put(DinoparcConstants.TERRE, 1);
                listedDinoz.getElementsValues().put(DinoparcConstants.AIR, 4);
                listedDinoz.getSkillsMap().put(DinoparcConstants.FOUILLER, 1);
                listedDinoz.getActionsMap().put(DinoparcConstants.FOUILLER, true);
                break;

            case "E":
                listedDinoz.getElementsValues().put(DinoparcConstants.FEU, 1);
                listedDinoz.getElementsValues().put(DinoparcConstants.EAU, 2);
                listedDinoz.getElementsValues().put(DinoparcConstants.AIR, 1);
                break;

            case "F":
                listedDinoz.getElementsValues().put(DinoparcConstants.TERRE, 1);
                listedDinoz.getElementsValues().put(DinoparcConstants.EAU, 1);
                listedDinoz.getElementsValues().put(DinoparcConstants.FOUDRE, 1);
                break;

            case "G":
                listedDinoz.getElementsValues().put(DinoparcConstants.TERRE, 2);
                listedDinoz.getElementsValues().put(DinoparcConstants.FOUDRE, 1);
                break;

            case "H":
                listedDinoz.getElementsValues().put(DinoparcConstants.EAU, 2);
                listedDinoz.getElementsValues().put(DinoparcConstants.AIR, 5);
                break;

            case "I":
                listedDinoz.getElementsValues().put(DinoparcConstants.FEU, 5);
                listedDinoz.getSkillsMap().put(DinoparcConstants.PROTECTIONDUFEU, 1);
                break;

            default:
                break;
        }

        listedDinoz.getElementsValues().putIfAbsent(DinoparcConstants.FEU, 0);
        listedDinoz.getElementsValues().putIfAbsent(DinoparcConstants.TERRE, 0);
        listedDinoz.getElementsValues().putIfAbsent(DinoparcConstants.EAU, 0);
        listedDinoz.getElementsValues().putIfAbsent(DinoparcConstants.FOUDRE, 0);
        listedDinoz.getElementsValues().putIfAbsent(DinoparcConstants.AIR, 0);
    }

    public static String getCompetenceNameFromNumber(int competenceNumber) {
        switch (competenceNumber) {
            case 1:
                return DinoparcConstants.FORCE;
            case 2:
                return DinoparcConstants.ENDURANCE;
            case 3:
                return DinoparcConstants.PERCEPTION;
            case 4:
                return DinoparcConstants.INTELLIGENCE;
            case 5:
                return DinoparcConstants.AGILITÉ;
            default:
                return null;
        }
    }

    public static void concatElementWithCompetence(Dinoz listedDinoz) {
        for (String competence : listedDinoz.getSkillsMap().keySet()) {
            if (listedDinoz.getSkillsMap().get(competence) != null && listedDinoz.getSkillsMap().get(competence) > 0) {
                adjustElementsFromCompetence(competence, listedDinoz.getSkillsMap().get(competence), listedDinoz);
            }
        }
    }

    public static void adjustElementsFromCompetence(String competence, Integer value, Dinoz listedDinoz) {
        switch (competence) {
            case DinoparcConstants.FORCE:
                listedDinoz.getElementsValues().replace(DinoparcConstants.FEU, listedDinoz.getElementsValues().get(DinoparcConstants.FEU) + value);
                break;
            case DinoparcConstants.ENDURANCE:
                listedDinoz.getElementsValues().replace(DinoparcConstants.TERRE, listedDinoz.getElementsValues().get(DinoparcConstants.TERRE) + value);
                break;
            case DinoparcConstants.PERCEPTION:
                listedDinoz.getElementsValues().replace(DinoparcConstants.EAU, listedDinoz.getElementsValues().get(DinoparcConstants.EAU) + value);
                break;
            case DinoparcConstants.INTELLIGENCE:
                listedDinoz.getElementsValues().replace(DinoparcConstants.FOUDRE, listedDinoz.getElementsValues().get(DinoparcConstants.FOUDRE) + value);
                break;
            case DinoparcConstants.AGILITÉ:
                listedDinoz.getElementsValues().replace(DinoparcConstants.AIR, listedDinoz.getElementsValues().get(DinoparcConstants.AIR) + value);
                break;
            default:
                break;
        }
    }

    public static String getActualSkillToUp(Dinoz listedDinoz, List<String> availableUncompletedSkills) {
        String highestElement = Dinoz.getHighestElementName(listedDinoz.getElementsValues());
        for (String actualSkill : availableUncompletedSkills) {
            if (Dinoz.getElementNameFromSkillName(actualSkill).equalsIgnoreCase(highestElement)) {
                return actualSkill;
            }
        }
        return availableUncompletedSkills.get(0);
    }

    public static String getNewLearning(Dinoz listedDinoz, List<String> newPossibleSkills) {
        Map<String, Integer> softCopy = new HashMap<>(listedDinoz.getElementsValues());
        String highestElement = Dinoz.getHighestElementName(softCopy);
        for (String newPossibleSkill : newPossibleSkills) {
            if (Dinoz.getElementNameFromSkillName(newPossibleSkill).equalsIgnoreCase(highestElement)) {
                return newPossibleSkill;
            }
        }

        for (int i = 0; i < 3; i++) {
            softCopy.replace(highestElement, 0);
            highestElement = Dinoz.getHighestElementName(softCopy);
            for (String newPossibleSkill : newPossibleSkills) {
                if (Dinoz.getElementNameFromSkillName(newPossibleSkill).equalsIgnoreCase(highestElement)) {
                    return newPossibleSkill;
                }
            }
        }

        return newPossibleSkills.get(new Random().nextInt(newPossibleSkills.size()));
    }

    public static Integer getRandomLevelForPVEWarrior() {
        Integer seed = ThreadLocalRandom.current().nextInt(0, 100 + 1);
        if (seed <= 25) {
            return ThreadLocalRandom.current().nextInt(25, 125);
        } else if (seed <= 50) {
            return ThreadLocalRandom.current().nextInt(125, 275);
        } else if (seed <= 99) {
            return ThreadLocalRandom.current().nextInt(125, 1000);
        } else {
            return ThreadLocalRandom.current().nextInt(1000, 5000);
        }
    }
}
