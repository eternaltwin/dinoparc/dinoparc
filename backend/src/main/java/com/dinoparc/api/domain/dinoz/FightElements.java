package com.dinoparc.api.domain.dinoz;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class FightElements {

  private String highestLeft;
  private String highestRight;
  private String secondHighestLeft;
  private String secondHighestRight;
  private String thirdHighestLeft;
  private String thirdHighestRight;
  
  public FightElements(Dinoz homeDinoz, Dinoz foreignDinoz) {
    Map<String, Integer> hCopy = new HashMap<String, Integer>();
    hCopy.putAll(homeDinoz.getElementsValues());
    Map<String, Integer> fCopy = new HashMap<String, Integer>();
    fCopy.putAll(foreignDinoz.getElementsValues());
    
    // Home Dinoz :
    for (String element : hCopy.keySet()) {
      if (hCopy.get(element).equals(Collections.max(hCopy.values()))) {
        if (highestLeft == null) {
          this.highestLeft = element;
        } else if (ThreadLocalRandom.current().nextInt(0, 3) == 2) {
          this.highestLeft = element;
        }
      }
    }
    hCopy.remove(highestLeft);

    for (String element : hCopy.keySet()) {
      if (hCopy.get(element).equals(Collections.max(hCopy.values()))) {
        if (secondHighestLeft == null) {
          this.secondHighestLeft = element;
        } else if (ThreadLocalRandom.current().nextInt(0, 3) == 2) {
          this.secondHighestLeft = element;
        }
      }
    }
    hCopy.remove(secondHighestLeft);

    for (String element : hCopy.keySet()) {
      if (hCopy.get(element).equals(Collections.max(hCopy.values()))) {
        if (thirdHighestLeft == null) {
          this.thirdHighestLeft = element;
        } else if (ThreadLocalRandom.current().nextInt(0, 3) == 2) {
          this.thirdHighestLeft = element;
        }
      }
    }
    hCopy.remove(thirdHighestLeft);

    // Foreign Dinoz :
    for (String element : fCopy.keySet()) {
      if (fCopy.get(element).equals(Collections.max(fCopy.values()))) {
        if (highestRight == null) {
          this.highestRight = element;
        } else if (ThreadLocalRandom.current().nextInt(0, 3) == 2) {
          this.highestRight = element;
        }
      }
    }
    fCopy.remove(highestRight);

    for (String element : fCopy.keySet()) {
      if (fCopy.get(element).equals(Collections.max(fCopy.values()))) {
        if (secondHighestRight == null) {
          this.secondHighestRight = element;
        } else if (ThreadLocalRandom.current().nextInt(0, 3) == 2) {
          this.secondHighestRight = element;
        }
      }
    }
    fCopy.remove(secondHighestRight);
    
    for (String element : fCopy.keySet()) {
      if (fCopy.get(element).equals(Collections.max(fCopy.values()))) {
        if (thirdHighestRight == null) {
          this.thirdHighestRight = element;
        } else if (ThreadLocalRandom.current().nextInt(0, 3) == 2) {
          this.thirdHighestRight = element;
        }
      }
    }
    fCopy.remove(thirdHighestRight);
  }

  public FightElements(Dinoz homeDinoz, EventDinozDto raidBoss) {
    Map<String, Integer> hCopy = new HashMap<String, Integer>();
    hCopy.putAll(homeDinoz.getElementsValues());
    Map<String, Integer> fCopy = new HashMap<String, Integer>();
    fCopy.putAll(raidBoss.getElementsValues());

    // Home Dinoz :
    for (String element : hCopy.keySet()) {
      if (hCopy.get(element).equals(Collections.max(hCopy.values()))) {
        if (highestLeft == null) {
          this.highestLeft = element;
        } else if (ThreadLocalRandom.current().nextInt(0, 3) == 2) {
          this.highestLeft = element;
        }
      }
    }
    hCopy.remove(highestLeft);

    for (String element : hCopy.keySet()) {
      if (hCopy.get(element).equals(Collections.max(hCopy.values()))) {
        if (secondHighestLeft == null) {
          this.secondHighestLeft = element;
        } else if (ThreadLocalRandom.current().nextInt(0, 3) == 2) {
          this.secondHighestLeft = element;
        }
      }
    }
    hCopy.remove(secondHighestLeft);

    for (String element : hCopy.keySet()) {
      if (hCopy.get(element).equals(Collections.max(hCopy.values()))) {
        if (thirdHighestLeft == null) {
          this.thirdHighestLeft = element;
        } else if (ThreadLocalRandom.current().nextInt(0, 3) == 2) {
          this.thirdHighestLeft = element;
        }
      }
    }
    hCopy.remove(thirdHighestLeft);

    // Foreign Dinoz :
    for (String element : fCopy.keySet()) {
      if (fCopy.get(element).equals(Collections.max(fCopy.values()))) {
        if (highestRight == null) {
          this.highestRight = element;
        } else if (ThreadLocalRandom.current().nextInt(0, 3) == 2) {
          this.highestRight = element;
        }
      }
    }
    fCopy.remove(highestRight);

    for (String element : fCopy.keySet()) {
      if (fCopy.get(element).equals(Collections.max(fCopy.values()))) {
        if (secondHighestRight == null) {
          this.secondHighestRight = element;
        } else if (ThreadLocalRandom.current().nextInt(0, 3) == 2) {
          this.secondHighestRight = element;
        }
      }
    }
    fCopy.remove(secondHighestRight);

    for (String element : fCopy.keySet()) {
      if (fCopy.get(element).equals(Collections.max(fCopy.values()))) {
        if (thirdHighestRight == null) {
          this.thirdHighestRight = element;
        } else if (ThreadLocalRandom.current().nextInt(0, 3) == 2) {
          this.thirdHighestRight = element;
        }
      }
    }
    fCopy.remove(thirdHighestRight);
  }

  public FightElements(Dinoz homeDinoz) {
    Map<String, Integer> hCopy = new HashMap<String, Integer>();
    hCopy.putAll(homeDinoz.getElementsValues());

    // Home Dinoz :
    for (String element : hCopy.keySet()) {
      if (hCopy.get(element).equals(Collections.max(hCopy.values()))) {
        if (highestLeft == null) {
          this.highestLeft = element;
        } else if (ThreadLocalRandom.current().nextInt(0, 3) == 2) {
          this.highestLeft = element;
        }
      }
    }
    hCopy.remove(highestLeft);

    for (String element : hCopy.keySet()) {
      if (hCopy.get(element).equals(Collections.max(hCopy.values()))) {
        if (secondHighestLeft == null) {
          this.secondHighestLeft = element;
        } else if (ThreadLocalRandom.current().nextInt(0, 3) == 2) {
          this.secondHighestLeft = element;
        }
      }
    }
    hCopy.remove(secondHighestLeft);

    for (String element : hCopy.keySet()) {
      if (hCopy.get(element).equals(Collections.max(hCopy.values()))) {
        if (thirdHighestLeft == null) {
          this.thirdHighestLeft = element;
        } else if (ThreadLocalRandom.current().nextInt(0, 3) == 2) {
          this.thirdHighestLeft = element;
        }
      }
    }
    hCopy.remove(thirdHighestLeft);
  }

  public String getHighestLeft() {
    return highestLeft;
  }

  public void setHighestLeft(String highestLeft) {
    this.highestLeft = highestLeft;
  }

  public String getHighestRight() {
    return highestRight;
  }

  public void setHighestRight(String highestRight) {
    this.highestRight = highestRight;
  }

  public String getSecondHighestLeft() {
    return secondHighestLeft;
  }

  public void setSecondHighestLeft(String secondHighestLeft) {
    this.secondHighestLeft = secondHighestLeft;
  }

  public String getSecondHighestRight() {
    return secondHighestRight;
  }

  public void setSecondHighestRight(String secondHighestRight) {
    this.secondHighestRight = secondHighestRight;
  }

  public String getThirdHighestLeft() {
    return thirdHighestLeft;
  }

  public void setThirdHighestLeft(String thirdHighestLeft) {
    this.thirdHighestLeft = thirdHighestLeft;
  }

  public String getThirdHighestRight() {
    return thirdHighestRight;
  }

  public void setThirdHighestRight(String thirdHighestRight) {
    this.thirdHighestRight = thirdHighestRight;
  }

}