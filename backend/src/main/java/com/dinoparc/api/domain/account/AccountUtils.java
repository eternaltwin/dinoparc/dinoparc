package com.dinoparc.api.domain.account;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;

public class AccountUtils {
    public static RoleActionHistory getRoleActionHistory(String author, String endpoint, String content, boolean authorized, boolean success) {
        ZonedDateTime neoparcTime = ZonedDateTime.now(ZoneId.of("Europe/Paris"));

        var ret = new RoleActionHistory();
        ret.setId(UUID.randomUUID());
        ret.setAuthor(author);
        ret.setContent(content);
        ret.setAuthorized(authorized);
        ret.setSuccess(success);
        ret.setEndpoint(endpoint);
        ret.setNumericalDate(neoparcTime.toOffsetDateTime());
        return ret;
    }
}
