package com.dinoparc.api.domain.account;

public enum PlayerActionModeEnum {
    STANDARD,
    ADVANCED
}
