package com.dinoparc.api.domain.misc;

import java.util.List;

public interface HermitConstants {
    public static final List<String> names = List.of("Diegorus", "Alaniqo", "Traquus", "Gozelle", "Slavion", "Heavinop", "Mordof", "Suniqo", "Tranpod", "Astrin", "Torghu", "Weestol", "Zuko", "Volstom", "Xorrus", "Eroth", "Termonto", "Leader", "Raalf", "Zurdan", "Pacabra");

    public static final List<String> ADJ1_FR = List.of("le Grand", "le Magnifique", "l'Ultime", "le Tricheur", "l'Exterminateur", "le Gentil", "le Timide", "le Catcheur", "l'Éternel", "le Petit", "le Boss", "le Farmeur", "le Serviteur", "le Peureux", "le Toreador", "le Géant", "le Gladiateur", "le Canon", "le Costaud", "le Magicien", "le Seigneur");

    public static final List<String> ADJ2_FR = List.of("d'Outre-Tombe", "de Dinoland", "de Jazz", "Vagabond", "Buveur de Pruniac", "Mercenaire", "Enchanté", "Rouge", "Vert", "Surdoué", "Gris", "Destructeur", "Nu", "Poilu", "Acéré", "Bundy", "Apocalyptique", "en Acier", "en Or", "Gluant", "Médaillé", "Chasseur de Ouistitis");

    public static final List<String> ADJ1_ES = List.of("el Grande", "el Magnífico", "el último", "el Tramposo", "el Exterminador", "el Bueno", "el Tímido", "el Luchador", "el Eterno", "el Pequeño", "el Jefe", "el Farmer", "el Sirviente", "el Temeroso", "el Toreador", "el Gigante", "el Gladiador", "el Canon", "el Fuerte", "el Mago", "el Señor");

    public static final List<String> ADJ2_ES = List.of("de Outra-Tomba", "de Dinoland", "de Jazz", "Vagabundo", "Bebedor de Pruniac", "Mercenario", "Encantada", "Rojo", "Verde", "Dotado", "Gris", "Destructivo", "Desnudo", "Peludo", "Afilado", "Bundy", "Apocalíptico", "en Acero", "en Oro", "Pegajoso", "Medalla", "Cazador de Wistitis");

    public static final List<String> ADJ1_EN = List.of("the Great", "the Magnificent", "the Ultimate", "the Cheater", "the Exterminator", "the Nice", "the Shy", "the Catcher", "the Eternal", "the Small", "the Boss", "the Farmer", "the Servant", "the Cowardly", "the Toreador", "the Giant", "the Gladiator", "the Canon", "the Swole", "the Wizard", "the Lord");

    public static final List<String> ADJ2_EN = List.of("from Far Away", "from Dinoland", "from Jazz", "Wanderer", "Pruniac Drinker", "Mercenary", "Delighted", "Red", "Green", "Gifted", "Grey", "Destructor", "Naked", "Hairy", "Sharp", "Bundy", "Apocalyptic", "made of Iron", "made of Gold", "Sticky", "Medallist", "Wistiti Hunter");
}
