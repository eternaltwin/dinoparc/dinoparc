package com.dinoparc.api.domain.clan;

import java.util.*;

public class Clan {

    private String id;
    private String creatorId;
    private String name;
    private Integer nbDinozTotal;
    private Integer nbDinozWarriors;
    private Integer nbPointsOccupation;
    private Integer nbPointsFights;
    private Integer nbPointsTotem;
    private Integer position;
    private String faction;
    private String creationDate;
    private Map<String, Page> pages;
    private Totem totem;
    private HashMap<String, Integer> chest;
    private List<String> members;
    private List<String> candidates;
    private List<String> allies;

    public Clan() {
        this.chest = new HashMap<>();
        this.pages = new HashMap<>();
        this.totem = new Totem();
        this.members = new ArrayList<>();
        this.candidates = new ArrayList<>();
        this.allies = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNbDinozTotal() {
        return nbDinozTotal;
    }

    public void setNbDinozTotal(Integer nbDinozTotal) {
        this.nbDinozTotal = nbDinozTotal;
    }

    public Integer getNbDinozWarriors() {
        return nbDinozWarriors;
    }

    public void setNbDinozWarriors(Integer nbDinozWarriors) {
        this.nbDinozWarriors = nbDinozWarriors;
    }

    public Integer getNbPointsOccupation() {
        return nbPointsOccupation;
    }

    public void setNbPointsOccupation(Integer nbPointsOccupation) {
        this.nbPointsOccupation = nbPointsOccupation;
    }

    public Integer getNbPointsFights() {
        return nbPointsFights;
    }

    public void setNbPointsFights(Integer nbPointsFights) {
        this.nbPointsFights = nbPointsFights;
    }

    public Integer getNbPointsTotem() {
        return nbPointsTotem;
    }

    public void setNbPointsTotem(Integer nbPointsTotem) {
        this.nbPointsTotem = nbPointsTotem;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getFaction() {
        return faction;
    }

    public void setFaction(String faction) {
        this.faction = faction;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public Map<String, Page> getPages() {
        return pages;
    }

    public void setPages(Map<String, Page> pages) {
        this.pages = pages;
    }

    public Totem getTotem() {
        return totem;
    }

    public void setTotem(Totem totem) {
        this.totem = totem;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    public List<String> getCandidates() {
        return candidates;
    }

    public void setCandidates(List<String> candidates) {
        this.candidates = candidates;
    }

    public HashMap<String, Integer> getChest() {
        return chest;
    }

    public void setChest(HashMap<String, Integer> chest) {
        this.chest = chest;
    }

    public List<String> getAllies() {
        return allies;
    }

    public void setAllies(List<String> allies) {
        this.allies = allies;
    }
}
