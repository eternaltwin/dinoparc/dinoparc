package com.dinoparc.api.domain.account;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Message {
    private String id;
    private boolean seen;
    private String accountDestination;
    private String accountSender;
    private String body;
    private String numericalDate;

    public Message() {
        this.seen = false;
    }

    public Message(boolean seen, String accountDestination, String body,
                   String numericalDate, String accountSender) {
        this.seen = seen;
        this.accountDestination = accountDestination;
        this.body = body;
        this.numericalDate = numericalDate;
        this.accountSender = accountSender;
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public String getAccountDestination() {
        return accountDestination;
    }

    public void setAccountDestination(String accountDestination) {
        this.accountDestination = accountDestination;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getNumericalDate() {
        return numericalDate;
    }

    public void setNumericalDate(OffsetDateTime numericalDate) {
        StringBuilder sb = new StringBuilder(numericalDate.atZoneSameInstant(ZoneId.of("Europe/Paris")).format(DateTimeFormatter.ISO_LOCAL_DATE));
        sb.append(" ");
        sb.append(numericalDate.format(DateTimeFormatter.ISO_LOCAL_TIME).substring(0, 8));
        this.numericalDate = sb.toString();
    }

    public String getAccountSender() { return accountSender; }

    public void setAccountSender(String accountSender) { this.accountSender = accountSender; }

}
