package com.dinoparc.api.domain.misc;

import com.dinoparc.api.repository.PgPlayerRepository;
import org.springframework.security.access.AccessDeniedException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.JWT;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.account.DinoparcConstants;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TokenAccountValidation {
    private final PgPlayerRepository playerRepository;

    public TokenAccountValidation(PgPlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public String getPlayerIdFromToken(String token) {
        DecodedJWT tokenDecoded = JWT.decode(token.replace(DinoparcConstants.TOKEN_PREFIX, ""));
        return tokenDecoded.getSubject();
    }

    public void validate(Player player, String token, List<String> roles) {
        if (roles != null && !roles.contains(player.getRole())) {
            throw new AccessDeniedException("403 returned");
        }
        String accountId = player.getId();
        if (!accountId.equals(getPlayerIdFromToken(token)) && !DinoparcConstants.ROLES_ADMIN.contains(player.getRole())) {
            throw new AccessDeniedException("403 returned");
        }
    }

    public void validateByToken(String token, List<String> roles) {
        Player account = this.playerRepository.findByName(getPlayerIdFromToken(token));

        if (roles != null && !roles.contains(account.getRole())) {
            throw new AccessDeniedException("403 returned");
        }
    }

    public boolean isElevatedUser(String token) {
        boolean validationStatus;
        try {
            this.validateByToken(token, DinoparcConstants.ROLES_ADMIN);
            validationStatus = true;
        } catch (AccessDeniedException e) {
            validationStatus = false;
        }
        return validationStatus;
    }

    public boolean isAdminOrGameMasterUser(String token) {
        boolean validationStatus;
        try {
            this.validateByToken(token, DinoparcConstants.ROLES_ADMIN_OR_GAME_MASTER);
            validationStatus = true;
        } catch (AccessDeniedException e) {
            validationStatus = false;
        }
        return validationStatus;
    }
}
