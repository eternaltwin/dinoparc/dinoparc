package com.dinoparc.api.domain.dinoz;

import java.util.Objects;

public class BotDinozDto {

  private Integer feu;
  private Integer terre;
  private Integer eau;
  private Integer foudre;
  private Integer air;
  
  private String appearanceCode;
  
  public BotDinozDto() {
    
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(feu, terre, eau, foudre, air, appearanceCode);
  }

  public Integer getFeu() {
    return feu;
  }

  public void setFeu(Integer feu) {
    this.feu = feu;
  }

  public Integer getTerre() {
    return terre;
  }

  public void setTerre(Integer terre) {
    this.terre = terre;
  }

  public Integer getEau() {
    return eau;
  }

  public void setEau(Integer eau) {
    this.eau = eau;
  }

  public Integer getFoudre() {
    return foudre;
  }

  public void setFoudre(Integer foudre) {
    this.foudre = foudre;
  }

  public Integer getAir() {
    return air;
  }

  public void setAir(Integer air) {
    this.air = air;
  }

  public String getAppearanceCode() {
    return appearanceCode;
  }

  public void setAppearanceCode(String appearanceCode) {
    this.appearanceCode = appearanceCode;
  }
  
}
