package com.dinoparc.api.domain.account;

import java.util.List;
import java.util.UUID;

public class Collection {
    private UUID id;
    private UUID playerId;
    private List<String> collection;
    private List<String> epicCollection;

    public UUID getId() {
        return id;
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public List<String> getCollection() {
        return collection;
    }

    public List<String> getEpicCollection() {
        return epicCollection;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setPlayerId(UUID playerId) {
        this.playerId = playerId;
    }

    public void setCollection(List<String> collection) {
        this.collection = collection;
    }

    public void setEpicCollection(List<String> epicCollection) {
        this.epicCollection = epicCollection;
    }
}
