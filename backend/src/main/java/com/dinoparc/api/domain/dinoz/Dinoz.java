package com.dinoparc.api.domain.dinoz;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.RandomCollection;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Dinoz implements Comparable<Dinoz> {
  private String id;
  private String masterId;
  private String masterName;
  private String name;
  private String race;
  private String beginMessage;
  private String endMessage;
  private int life;
  private int level;
  private int experience;
  private int danger;
  private Map<String, Integer> passiveList;
  private List<String> malusList;
  private Map<String, Integer> elementsValues;
  private Map<String, Integer> skillsMap;
  private Map<String, Boolean> actionsMap;
  private Map<String, Tournament> tournaments;
  private List<String> activeMissions;
  private int placeNumber;
  private String appearanceCode;
  private boolean isDark;
  private boolean isInTourney;
  private int kabukiProgression;
  private int lastValidBotHash;
  private int skips;
  private boolean isInBazar;
  private long epochSecondsEndOfCherryEffect;
  private long cherryEffectMinutesLeft;
  private String lastFledEnnemy;
  private boolean botOnly;
  private DinozBossInfosDto bossInfos;
  private Integer maxLife;
  private boolean taggedAsClanEnnemy;
  private boolean dinozIsActive;

  public Dinoz() {
    malusList = new ArrayList<>();
    activeMissions = new ArrayList<>();
    elementsValues = new HashMap<>();
    skillsMap = new HashMap<>();
    actionsMap = new HashMap<>();
    tournaments = new HashMap<>();
    initTournament();
    passiveList = new HashMap<>();
    this.isInBazar = false;
    this.botOnly = false;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getMasterName() {
    return masterName;
  }

  public void setMasterName(String masterName) {
    this.masterName = masterName;
  }

  public String getMasterId() {
    return masterId;
  }

  public void setMasterId(String masterId) {
    this.masterId = masterId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getRace() {
    return race;
  }

  public void setRace(String race) {
    this.race = race;
  }

  public String getBeginMessage() {
    return beginMessage;
  }

  public void setBeginMessage(String beginMessage) {
    this.beginMessage = beginMessage;
  }

  public String getEndMessage() {
    return endMessage;
  }

  public void setEndMessage(String endMessage) {
    this.endMessage = endMessage;
  }

  public int getLife() {
    return life;
  }

  public void setLife(int life) {
    this.life = life;
  }

  public int getLevel() {
    return level;
  }

  public void setLevel(int level) {
    this.level = level;
  }

  public int getExperience() {
    return experience;
  }

  public void setExperience(int experience) {
    this.experience = experience;
  }

  public int getDanger() {
    return danger;
  }

  public void setDanger(int danger) {
    this.danger = danger;
  }

  public Map<String, Integer> getPassiveList() {
    return passiveList;
  }

  public void setPassiveList(Map<String, Integer> passiveList) {
    this.passiveList = passiveList;
  }

  public List<String> getMalusList() {
    return malusList;
  }

  public void setMalusList(List<String> malusList) {
    this.malusList = malusList;
  }

  public Map<String, Integer> getElementsValues() {
    return elementsValues;
  }

  public void setElementsValues(Map<String, Integer> elementsValues) {
    this.elementsValues = elementsValues;
  }

  public Map<String, Integer> getSkillsMap() {
    return skillsMap;
  }

  public void setSkillsMap(Map<String, Integer> skillsMap) {
    this.skillsMap = skillsMap;
  }

  public Map<String, Boolean> getActionsMap() {
    return actionsMap;
  }

  public void setActionsMap(Map<String, Boolean> actionsMap) {
    this.actionsMap = actionsMap;
  }

  public Map<String, Tournament> getTournaments() {
    return tournaments;
  }

  public void setTournaments(Map<String, Tournament> tournaments) {
    this.tournaments = tournaments;
  }

  public List<String> getActiveMissions() {
    return activeMissions;
  }

  public void setActiveMissions(List<String> activeMissions) {
    this.activeMissions = activeMissions;
  }

  public int getPlaceNumber() {
    return placeNumber;
  }

  public void setPlaceNumber(int placeNumber) {
    this.placeNumber = placeNumber;
  }

  public String getAppearanceCode() {
    return appearanceCode;
  }

  public void setAppearanceCode(String appearanceCode) {
    this.appearanceCode = appearanceCode;
  }

  public boolean isDark() {
    return isDark;
  }

  public void setDark(boolean isDark) {
    this.isDark = isDark;
  }

  public boolean isInTourney() {
    return isInTourney;
  }

  public void setInTourney(boolean isInTourney) {
    this.isInTourney = isInTourney;
  }

  public int getKabukiProgression() {
    return kabukiProgression;
  }

  public void setKabukiProgression(int kabukiProgression) {
    this.kabukiProgression = kabukiProgression;
  }

  public int getLastValidBotHash() {
    return lastValidBotHash;
  }

  public void setLastValidBotHash(int lastValidBotHash) {
    this.lastValidBotHash = lastValidBotHash;
  }

  public int getSkips() {
    return skips;
  }

  public void setSkips(int skips) {
    this.skips = skips;
  }

  public String getLastFledEnnemy() {
    return lastFledEnnemy;
  }

  public void setLastFledEnnemy(String lastFledEnnemy) {
    this.lastFledEnnemy = lastFledEnnemy;
  }

  public boolean isBotOnly() { return botOnly; }

  public void setBotOnly(boolean botOnly) { this.botOnly = botOnly; }

  public Integer getMaxLife() {
    return maxLife;
  }

  public void setMaxLife(Integer maxLife) {
    this.maxLife = maxLife;
  }

  public boolean isTaggedAsClanEnnemy() {
    return taggedAsClanEnnemy;
  }

  public void setTaggedAsClanEnnemy(boolean taggedAsClanEnnemy) {
    this.taggedAsClanEnnemy = taggedAsClanEnnemy;
  }

  public boolean isDinozIsActive() {
    return dinozIsActive;
  }

  public void setDinozIsActive(boolean dinozIsActive) {
    this.dinozIsActive = dinozIsActive;
  }

  @Override
  public int compareTo(Dinoz other) {
    if (this.getDanger() < other.getDanger()) {
      return 1;
    } else if (this.getDanger() == other.getDanger()) {
      return 0;
    } else {
      return -1;
    }
  }

  public void initTournament() {
    this.tournaments.put(DinoparcConstants.TournoiDinoville, new Tournament());
    this.tournaments.put(DinoparcConstants.TournoiDinoplage, new Tournament());
    this.tournaments.put(DinoparcConstants.TournoiMontDino, new Tournament());
    this.tournaments.put(DinoparcConstants.TournoiTemple, new Tournament());
    this.tournaments.put(DinoparcConstants.TournoiRuines, new Tournament());
  }

  public static Dinoz generateARandomDinoz(
          Dinoz homeDinoz,
          Integer numberOfDinozOfAccount,
          Integer shinyOddsTotal,
          Boolean isBannedOrBannedShiny) {

    Dinoz dinoz = new Dinoz();
    //Total odds denominator : 1500.
    RandomCollection<Object> randomRace = new RandomCollection<Object>()
            .add(100, 0) // Moueffe
            .add(100, 1) // Picori
            .add(100, 2) // Castivore
            .add(100, 3) // Sirain
            .add(100, 4) // Winks
            .add(100, 5) // Gorilloz
            .add(100, 6) // Cargou
            .add(100, 7) // Hippoclamp
            .add(100, 8) // Rokky
            .add(100, 9) // Pigmou
            .add(100, 10) // Wanwan
            .add(20, 11) // Goupignon
            .add(100, 12) // Kump
            .add(100, 13) // Pteroz
            .add(20, 14) // Santaz
            .add(0, 15) // Ouistiti
            .add(75, 16) // Korgon
            .add(50, 17) // Kabuki
            .add(20, 18) // Serpantin
            .add(10, 19) // Soufflet
            .add(5, 20); // Feross
    int randomRaceNumber = getRaceOfBotDinozForLocation(homeDinoz, randomRace);
    initializeDinoz(dinoz, randomRaceNumber, homeDinoz.getElementsValues());

    //Shiny draw:
    if (ThreadLocalRandom.current().nextInt(0, shinyOddsTotal + 1) == shinyOddsTotal && !isBannedOrBannedShiny) {
      dinoz.setAppearanceCode(dinoz.getAppearanceCode() + "&");
    }

    if (homeDinoz.getPlaceNumber() == 23) {
      if (!dinoz.getAppearanceCode().endsWith("&")) {
        dinoz.setAppearanceCode(dinoz.getAppearanceCode() + "%23");
      }
    }

    return dinoz;
  }

  public static int getRaceOfBotDinozForLocation(Dinoz homeDinoz, RandomCollection<Object> randomRace) {
    int currentLocation = homeDinoz.getPlaceNumber();
    switch (currentLocation) {
      case 3 :
        return 4;
      case 5 :
        return 9;
      case 6 :
        return 13;
      case 8 :
        return 17;
      case 9 :
        return 16;
      case 10 :
        return 2;
      case 11 :
        return 3;
      case 12 :
        return 7;
      case 13 :
        return 6;
      case 16 :
        return 12;
      case 17 :
        return 5;
      case 18 :
        return 1;
      case 21 :
        return 10;
      case 22 :
        return 0;
      case 23 :
        return (Arrays.asList(0, 13, 20)).get(ThreadLocalRandom.current().nextInt(0, 3));
      case 24 :
        return (Arrays.asList(3, 11, 17)).get(ThreadLocalRandom.current().nextInt(0, 3));
      case 25 :
        return (Arrays.asList(1, 13, 19)).get(ThreadLocalRandom.current().nextInt(0, 3));
      case 26 :
        return (Arrays.asList(5, 6, 18)).get(ThreadLocalRandom.current().nextInt(0, 3));
      case 27 :
        return (Arrays.asList(4, 6, 12)).get(ThreadLocalRandom.current().nextInt(0, 3));
      case 28 :
        return (Arrays.asList(0, 9, 18)).get(ThreadLocalRandom.current().nextInt(0, 3));
      case 29 :
        return (Arrays.asList(2, 10, 16)).get(ThreadLocalRandom.current().nextInt(0, 3));
      case 32 :
        return 14;
      case 33 :
        return (Arrays.asList(3, 4, 12)).get(ThreadLocalRandom.current().nextInt(0, 3));
      case 34 :
        return getRandomRaceForRepaireMahamutiBot(randomRace);
      case 35 :
        return (Arrays.asList(7, 10, 17)).get(ThreadLocalRandom.current().nextInt(0, 3));
      case 37 :
        return 8;

      default :
        return (int) randomRace.next();
    }
  }

  private static int getRandomRaceForRepaireMahamutiBot(RandomCollection<Object> randomRace) {
    if (ThreadLocalRandom.current().nextInt(0, 500) == 21) {
      return 21;
    }
    return (int) randomRace.next();
  }

  public static Integer getRaceOfDinozForLocation(Integer dinozPlaceNumber) {
    switch (dinozPlaceNumber) {
      case 3 :
        return 4;
      case 5 :
        return 9;
      case 6 :
        return 13;
      case 9 :
        return 16;
      case 10 :
        return 2;
      case 11 :
        return 3;
      case 12 :
        return 7;
      case 13 :
        return 6;
      case 16 :
        return 12;
      case 17 :
        return 5;
      case 18 :
        return 1;
      case 21 :
        return 10;
      case 22 :
        return 0;
      case 37 :
        return 8;

      default :
        return null;
    }
  }

  public static Dinoz generateBoss(EventDinozDto eventDinozDto) {
    return generateEventDinoz(eventDinozDto, DinoparcConstants.GOUPIGNON, "admin-b-" + eventDinozDto.bossId, DinoparcConstants.rbbm, DinoparcConstants.rbem, 500, true, true);
  }

  public static Dinoz generateActualRaidBoss(EventDinozDto eventDinozDto) {
    return generateEventDinoz(eventDinozDto, DinoparcConstants.GOUPIGNON, "admin-b-raid", DinoparcConstants.rbbm, DinoparcConstants.rbem, 500, true, true);
  }

  public static Dinoz generateActualW(EventDinozDto eventDinozDto) {
    return generateEventDinoz(eventDinozDto, DinoparcConstants.OUISTITI, "admin-w-wild", DinoparcConstants.wbm, DinoparcConstants.wem, 0, false, false);
  }

  public static Dinoz generateEventDinoz(EventDinozDto eventDinozDto, String race, String dinozId,
                                         String startMsg, String endMsg,
                                         int lvl, boolean withElements, boolean withClaws) {
    Dinoz eventDinoz = new Dinoz();

    Map<String, Integer> elementsValues = new HashMap<>();
    if (withElements) {
      elementsValues.put(DinoparcConstants.FEU, eventDinozDto.getFire());
      elementsValues.put(DinoparcConstants.TERRE, eventDinozDto.getWood());
      elementsValues.put(DinoparcConstants.EAU, eventDinozDto.getWater());
      elementsValues.put(DinoparcConstants.FOUDRE, eventDinozDto.getThunder());
      elementsValues.put(DinoparcConstants.AIR, eventDinozDto.getAir());
      eventDinoz.setElementsValues(elementsValues);
    } else {
      elementsValues.put(DinoparcConstants.FEU, 1);
      elementsValues.put(DinoparcConstants.TERRE, 1);
      elementsValues.put(DinoparcConstants.EAU, 1);
      elementsValues.put(DinoparcConstants.FOUDRE, 1);
      elementsValues.put(DinoparcConstants.AIR, 1);
      eventDinoz.setElementsValues(elementsValues);
    }

    Map<String, Integer> passiveList = new HashMap<>();
    passiveList.put(DinoparcConstants.CHARME_FEU, eventDinozDto.getBonusFire());
    passiveList.put(DinoparcConstants.CHARME_TERRE, eventDinozDto.getBonusWood());
    passiveList.put(DinoparcConstants.CHARME_EAU, eventDinozDto.getBonusWater());
    passiveList.put(DinoparcConstants.CHARME_FOUDRE, eventDinozDto.getBonusThunder());
    passiveList.put(DinoparcConstants.CHARME_AIR, eventDinozDto.getBonusAir());
    passiveList.put(DinoparcConstants.GRIFFES_EMPOISONNÉES, withClaws ? eventDinozDto.getBonusClaw() : 0);
    eventDinoz.setPassiveList(passiveList);

    eventDinoz.setId(dinozId);
    eventDinoz.setMasterName("admin");
    eventDinoz.setName(eventDinozDto.getName());
    eventDinoz.setLife(eventDinozDto.getLife());
    eventDinoz.setPlaceNumber(eventDinozDto.getPlaceNumber());
    eventDinoz.setAppearanceCode(eventDinozDto.getAppearanceCode());

    eventDinoz.setExperience(0);
    eventDinoz.setDanger(0);
    eventDinoz.setLevel(lvl);
    eventDinoz.setRace(race);
    eventDinoz.setActionsMap(new HashMap<>());
    eventDinoz.setCherryEffectMinutesLeft(0);
    eventDinoz.setMalusList(new ArrayList<>());
    eventDinoz.setSkillsMap(new HashMap<>());
    eventDinoz.setActionsMap(new HashMap<>());
    eventDinoz.setTournaments(new HashMap<>());
    eventDinoz.setActiveMissions(new ArrayList<>());
    eventDinoz.setDark(false);
    eventDinoz.setInTourney(false);
    eventDinoz.setKabukiProgression(0);
    eventDinoz.setLastValidBotHash(0);
    eventDinoz.setSkips(0);
    eventDinoz.setInBazar(false);
    eventDinoz.setCherryEffectMinutesLeft(0);
    eventDinoz.setLastFledEnnemy(null);
    eventDinoz.setMasterId("admin");
    eventDinoz.setBeginMessage(startMsg);
    eventDinoz.setEndMessage(endMsg);

    return eventDinoz;
  }

  private static void initializeDinoz(Dinoz dinoz, int randomRace, Map<String, Integer> attackerElements) {
    var randHexString = DinozUtils.getRandomHexString();

    switch (randomRace) {
      case 0:
        dinoz.setRace(DinoparcConstants.MOUEFFE);
        dinoz.getElementsValues().replace(DinoparcConstants.FEU, 2);
        dinoz.setAppearanceCode("0" + randHexString);
        break;

      case 1:
        dinoz.setRace(DinoparcConstants.PICORI);
        dinoz.getElementsValues().replace(DinoparcConstants.AIR, 2);
        dinoz.setAppearanceCode("1" + randHexString);
        break;

      case 2:
        dinoz.setRace(DinoparcConstants.CASTIVORE);
        dinoz.getElementsValues().replace(DinoparcConstants.AIR, 1);
        dinoz.getElementsValues().replace(DinoparcConstants.TERRE, 1);
        dinoz.setAppearanceCode("2" + randHexString);
        break;

      case 3:
        dinoz.setRace(DinoparcConstants.SIRAIN);
        dinoz.getElementsValues().replace(DinoparcConstants.EAU, 2);
        dinoz.setAppearanceCode("3" + randHexString);
        break;

      case 4:
        dinoz.setRace(DinoparcConstants.WINKS);
        dinoz.getElementsValues().replace(DinoparcConstants.EAU, 1);
        dinoz.getElementsValues().replace(DinoparcConstants.FOUDRE, 1);
        dinoz.setAppearanceCode("4" + randHexString);
        break;

      case 5:
        dinoz.setRace(DinoparcConstants.GORILLOZ);
        dinoz.getElementsValues().replace(DinoparcConstants.TERRE, 2);
        dinoz.setAppearanceCode("5" + randHexString);
        break;

      case 6:
        dinoz.setRace(DinoparcConstants.CARGOU);
        dinoz.getElementsValues().replace(DinoparcConstants.TERRE, 1);
        dinoz.getElementsValues().replace(DinoparcConstants.EAU, 1);
        dinoz.setAppearanceCode("6" + randHexString);
        break;

      case 7:
        dinoz.setRace(DinoparcConstants.HIPPOCLAMP);
        dinoz.getElementsValues().replace(DinoparcConstants.FEU, 1);
        dinoz.getElementsValues().replace(DinoparcConstants.TERRE, 1);
        dinoz.getElementsValues().replace(DinoparcConstants.EAU, 1);
        dinoz.getElementsValues().replace(DinoparcConstants.FOUDRE, 1);
        dinoz.getElementsValues().replace(DinoparcConstants.AIR, 1);
        dinoz.setAppearanceCode("7" + randHexString);
        break;

      case 8:
        dinoz.setRace(DinoparcConstants.ROKKY);
        dinoz.getElementsValues().replace(DinoparcConstants.FOUDRE, 1);
        dinoz.setAppearanceCode("8" + randHexString);
        break;

      case 9:
        dinoz.setRace(DinoparcConstants.PIGMOU);
        dinoz.getElementsValues().replace(DinoparcConstants.FEU, 2);
        dinoz.setAppearanceCode("9" + randHexString);
        break;

      case 10:
        dinoz.setRace(DinoparcConstants.WANWAN);
        dinoz.getElementsValues().replace(DinoparcConstants.FOUDRE, 2);
        dinoz.setAppearanceCode("A" + randHexString);
        break;

      case 11:
        dinoz.setRace(DinoparcConstants.GOUPIGNON);
        dinoz.getElementsValues().replace(DinoparcConstants.FEU, 1);
        dinoz.getElementsValues().replace(DinoparcConstants.TERRE, 2);
        dinoz.getElementsValues().replace(DinoparcConstants.EAU, 2);
        dinoz.setAppearanceCode("B" + randHexString);
        break;

      case 12:
        dinoz.setRace(DinoparcConstants.KUMP);
        dinoz.getElementsValues().replace(DinoparcConstants.EAU, 2);
        dinoz.setAppearanceCode("C" + randHexString);
        break;

      case 13:
        dinoz.setRace(DinoparcConstants.PTEROZ);
        dinoz.getElementsValues().replace(DinoparcConstants.AIR, 4);
        dinoz.getElementsValues().replace(DinoparcConstants.TERRE, 1);
        dinoz.setAppearanceCode("D" + randHexString);
        break;

      case 14:
        dinoz.setRace(DinoparcConstants.SANTAZ);
        dinoz.getElementsValues().replace(DinoparcConstants.FEU, 1);
        dinoz.getElementsValues().replace(DinoparcConstants.EAU, 2);
        dinoz.getElementsValues().replace(DinoparcConstants.AIR, 1);
        dinoz.setAppearanceCode("E" + randHexString);
        break;

      case 16:
        dinoz.setRace(DinoparcConstants.KORGON);
        dinoz.getElementsValues().replace(DinoparcConstants.TERRE, 2);
        dinoz.getElementsValues().replace(DinoparcConstants.FOUDRE, 1);
        dinoz.setAppearanceCode("G" + randHexString);
        break;

      case 17:
        dinoz.setRace(DinoparcConstants.KABUKI);
        dinoz.getElementsValues().replace(DinoparcConstants.EAU, 2);
        dinoz.getElementsValues().replace(DinoparcConstants.AIR, 5);
        dinoz.setAppearanceCode("H" + randHexString);
        break;

      case 18:
        dinoz.setRace(DinoparcConstants.SERPANTIN);
        dinoz.getElementsValues().replace(DinoparcConstants.FEU, 4);
        dinoz.setAppearanceCode("I" + randHexString);
        break;

      case 19:
        dinoz.setRace(DinoparcConstants.SOUFFLET);
        dinoz.getElementsValues().replace(DinoparcConstants.FOUDRE, 2);
        dinoz.getElementsValues().replace(DinoparcConstants.AIR, 2);
        dinoz.setAppearanceCode("J" + randHexString);
        break;

      case 20:
        dinoz.setRace(DinoparcConstants.FEROSS);
        dinoz.getElementsValues().replace(DinoparcConstants.TERRE, 3);
        dinoz.getElementsValues().replace(DinoparcConstants.EAU, 2);
        dinoz.setAppearanceCode("K" + randHexString);
        break;

      case 21:
        dinoz.setRace(DinoparcConstants.MAHAMUTI);
        dinoz.getElementsValues().replace(DinoparcConstants.AIR, 2);
        dinoz.setAppearanceCode("L" + randHexString);
        break;

      default:
        dinoz.setRace(DinoparcConstants.MOUEFFE);
        dinoz.getElementsValues().replace(DinoparcConstants.FEU, 2);
        dinoz.setAppearanceCode("0" + randHexString);
        break;
    }

    // Decide Elements of the Wild Dinoz here :
    int elementAttacker = getHighestElementValue(attackerElements);
    dinoz.getElementsValues().put(DinoparcConstants.FEU, ThreadLocalRandom.current().nextInt(0, elementAttacker + 1));
    dinoz.getElementsValues().put(DinoparcConstants.TERRE, ThreadLocalRandom.current().nextInt(0, elementAttacker + 1));
    dinoz.getElementsValues().put(DinoparcConstants.EAU, ThreadLocalRandom.current().nextInt(0, elementAttacker + 1));
    dinoz.getElementsValues().put(DinoparcConstants.FOUDRE, ThreadLocalRandom.current().nextInt(0, elementAttacker + 1));
    dinoz.getElementsValues().put(DinoparcConstants.AIR, ThreadLocalRandom.current().nextInt(0, elementAttacker + 1));
  }

  public static int getHighestElementValue(Map<String, Integer> attackerElements) {
    int highest = 0;
    for (int elementValue : attackerElements.values()) {
      if (elementValue >= highest) {
        highest = elementValue;
      }
    }
    return highest;
  }

  public static String getHighestElementName(Map<String, Integer> attackerElements) {
    String highestName = "";
    int highest = 0;
    for (Map.Entry<String, Integer> element : attackerElements.entrySet()) {
      if (element.getValue() >= highest) {
        highest = element.getValue();
        highestName = element.getKey();
      }
    }
    return highestName;
  }

  public static String getRaceFromAppearanceCode(String appCode) {
    switch (appCode.charAt(0)) {
      case '0':
        return DinoparcConstants.MOUEFFE;
      case '1':
        return DinoparcConstants.PICORI;
      case '2':
        return DinoparcConstants.CASTIVORE;
      case '3':
        return DinoparcConstants.SIRAIN;
      case '4':
        return DinoparcConstants.WINKS;
      case '5':
        return DinoparcConstants.GORILLOZ;
      case '6':
        return DinoparcConstants.CARGOU;
      case '7':
        return DinoparcConstants.HIPPOCLAMP;
      case '8':
        return DinoparcConstants.ROKKY;
      case '9':
        return DinoparcConstants.PIGMOU;
      case 'A':
        return DinoparcConstants.WANWAN;
      case 'B':
        return DinoparcConstants.GOUPIGNON;
      case 'C':
        return DinoparcConstants.KUMP;
      case 'D':
        return DinoparcConstants.PTEROZ;
      case 'E':
        return DinoparcConstants.SANTAZ;
      case 'F':
        return DinoparcConstants.OUISTITI;
      case 'G':
        return DinoparcConstants.KORGON;
      case 'H':
        return DinoparcConstants.KABUKI;
      case 'I':
        return DinoparcConstants.SERPANTIN;
      case 'J':
        return DinoparcConstants.SOUFFLET;
      case 'K':
        return DinoparcConstants.FEROSS;
      default:
        return DinoparcConstants.MOUEFFE;

    }
  }

  public static String getRandomElement() {
    RandomCollection<Object> randomElement = new RandomCollection<Object>()
                .add(20, DinoparcConstants.FEU)
                .add(20, DinoparcConstants.TERRE)
                .add(20, DinoparcConstants.EAU)
                .add(20, DinoparcConstants.FOUDRE)
                .add(20, DinoparcConstants.AIR);
    return (String) randomElement.next();
  }

  public static String getRandomElementWithWeights(Map<String, Integer> rootElements) {
    RandomCollection<Object> randomElement = new RandomCollection<Object>()
                    .add(rootElements.get(DinoparcConstants.FEU), DinoparcConstants.FEU)
                    .add(rootElements.get(DinoparcConstants.TERRE), DinoparcConstants.TERRE)
                    .add(rootElements.get(DinoparcConstants.EAU), DinoparcConstants.EAU)
                    .add(rootElements.get(DinoparcConstants.FOUDRE), DinoparcConstants.FOUDRE)
                    .add(rootElements.get(DinoparcConstants.AIR), DinoparcConstants.AIR);

    return (String) randomElement.next();
  }

  public static String getNextElementOfCycle(String currentElement) {
    switch (currentElement) {
      case DinoparcConstants.FEU:
        return DinoparcConstants.TERRE;
      case DinoparcConstants.TERRE:
        return DinoparcConstants.EAU;
      case DinoparcConstants.EAU:
        return DinoparcConstants.FOUDRE;
      case DinoparcConstants.FOUDRE:
        return DinoparcConstants.AIR;
      case DinoparcConstants.AIR:
        return DinoparcConstants.FEU;

      default:
        return null;
    }
  }

  public boolean isInBazar() {
    return isInBazar;
  }

  public void setInBazar(boolean inBazar) {
    isInBazar = inBazar;
  }

  public Long getEpochSecondsEndOfCherryEffect() {
    return epochSecondsEndOfCherryEffect;
  }

  public void setEpochSecondsEndOfCherryEffect(Long epochSecondsEndOfCherryEffect) {
    this.epochSecondsEndOfCherryEffect = epochSecondsEndOfCherryEffect;
  }

  public long getCherryEffectMinutesLeft() {
    return cherryEffectMinutesLeft;
  }

  public void setCherryEffectMinutesLeft(long cherryEffectMinutesLeft) {
    this.cherryEffectMinutesLeft = cherryEffectMinutesLeft;
  }

  public DinozBossInfosDto getBossInfos() {
    return bossInfos;
  }

  public void setBossInfos(DinozBossInfosDto bossInfos) {
    this.bossInfos = bossInfos;
  }

  public static String getElementNameFromSkillName(String newPossibleSkill) {
    switch (newPossibleSkill) {
      case DinoparcConstants.FORCE:
      case DinoparcConstants.ARTS_MARTIAUX:
      case DinoparcConstants.SURVIE:
      case DinoparcConstants.CONTRE_ATTAQUE:
      case DinoparcConstants.PROTECTIONDUFEU:
      case DinoparcConstants.APPRENTI_FEU:
      case DinoparcConstants.BAIN_FLAMMES:
      case DinoparcConstants.RESILIENCE:
        return DinoparcConstants.FEU;

      case DinoparcConstants.ENDURANCE:
      case DinoparcConstants.ESCALADE:
      case DinoparcConstants.FOUILLER:
      case DinoparcConstants.COURSE:
      case DinoparcConstants.SAUT:
      case DinoparcConstants.APPRENTI_TERRE:
      case DinoparcConstants.CUEILLETTE:
      case DinoparcConstants.FORTERESSE:
        return DinoparcConstants.TERRE;

      case DinoparcConstants.PERCEPTION:
      case DinoparcConstants.NATATION:
      case DinoparcConstants.NAVIGATION:
      case DinoparcConstants.CUISINE:
      case DinoparcConstants.TAUNT:
      case DinoparcConstants.SOLUBILITE:
      case DinoparcConstants.APPRENTI_EAU:
      case DinoparcConstants.PÊCHE:
        return DinoparcConstants.EAU;

      case DinoparcConstants.INTELLIGENCE:
      case DinoparcConstants.MEDECINE:
      case DinoparcConstants.VOLER:
      case DinoparcConstants.COMMERCE:
      case DinoparcConstants.MUSIQUE:
      case DinoparcConstants.ROCK:
      case DinoparcConstants.APPRENTI_FOUDRE:
      case DinoparcConstants.INGENIEUR:
        return DinoparcConstants.FOUDRE;

      case DinoparcConstants.AGILITÉ:
      case DinoparcConstants.CAMOUFLAGE:
      case DinoparcConstants.STRATEGIE:
      case DinoparcConstants.CHANCE:
      case DinoparcConstants.POUVOIR_SOMBRE:
      case DinoparcConstants.COUP_CRITIQUE:
      case DinoparcConstants.CHASSEUR:
      case DinoparcConstants.PIQURE:
        return DinoparcConstants.AIR;

      default:
        return "";
    }
  }

  public static Boolean isDinozMaxed(Dinoz dinoz, Boolean isLearningAvailable) {
    for (Integer skillPoints : dinoz.getSkillsMap().values()) {
      if (skillPoints != null && skillPoints < 5) {
        return false;
      }
    }

    if (isLearningAvailable) {
      return false;
    }

    return true;
  }
}