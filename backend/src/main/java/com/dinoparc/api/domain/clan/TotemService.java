package com.dinoparc.api.domain.clan;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.rand.Rng;
import com.dinoparc.api.rand.RngFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.HashMap;

@Component
public class TotemService {

    @Autowired
    public TotemService() {}

    public Totem generateNewTotem(RngFactory rngFactory, Integer totemNumber) {
        if (DinoparcConstants.WAR_MODE) {
            Rng rng = rngFactory
                    .string("generateNewTotemV4-2024")
                    .string(String.valueOf(totemNumber))
                    .string(String.valueOf(DinoparcConstants.ACTUAL_WAR_NUMBER))
                    .toRng();

            Integer buzeNumber = rng.randIntBetween(totemNumber * 16, totemNumber * 32);
            Integer plumesNumber = rng.randIntBetween(totemNumber * 8, totemNumber * 16);
            Integer dentsNumber = rng.randIntBetween(totemNumber * 4, totemNumber * 8);
            Integer crinsNumber = rng.randIntBetween(totemNumber * 2, totemNumber * 5);
            Integer boisNumber = rng.randIntBetween(totemNumber * 1, totemNumber * 3);

            Totem totem =  new Totem(totemNumber, new HashMap<>(),
                    (buzeNumber * rng.randIntBetween(1,10)),
                    (plumesNumber * rng.randIntBetween(1,10)),
                    (dentsNumber * rng.randIntBetween(1,8)),
                    (crinsNumber * rng.randIntBetween(1,6)),
                    (boisNumber * rng.randIntBetween(1,5))
            );

            HashMap<String, Integer> totemRewards = new HashMap<>();
            Integer totalElementNumber = totem.getTotalBuzeNeeded() + totem.getTotalPlumesNeeded() + totem.getTotalDentsNeeded() + totem.getTotalCrinsNeeded() + totem.getTotalBoisNeeded();
            totemRewards.put(DinoparcConstants.POTION_IRMA, rng.randIntBetween((totalElementNumber / 15), (totalElementNumber / 10)));
            totemRewards.put(DinoparcConstants.CHARME_PRISMATIK, rng.randIntBetween(0, (totalElementNumber / 50)));
            totemRewards.put(DinoparcConstants.POTION_GUERRE, rng.randIntBetween(2,5));
            totem.setTotemRewards(totemRewards);
            return totem;
        }
        return new Totem();
    }
}
