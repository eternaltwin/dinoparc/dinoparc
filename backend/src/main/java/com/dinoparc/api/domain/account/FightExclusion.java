package com.dinoparc.api.domain.account;

import java.util.UUID;

public class FightExclusion {
    private UUID id;
    private UUID fromPlayerId;
    private UUID toPlayerId;
    private String reason;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getFromPlayerId() {
        return fromPlayerId;
    }

    public void setFromPlayerId(UUID fromPlayerId) {
        this.fromPlayerId = fromPlayerId;
    }

    public UUID getToPlayerId() {
        return toPlayerId;
    }

    public void setToPlayerId(UUID toPlayerId) {
        this.toPlayerId = toPlayerId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
