package com.dinoparc.api.domain.account;

import java.time.OffsetDateTime;
import java.util.UUID;

public class ClientInformationBis {
    private UUID playerId;
    private String dinozId;
    private byte[] first;
    private String fourth;
    private ClientInformationBisDetail skipInfo;
    private Integer rank;
    private OffsetDateTime retrievedDate;

    public UUID getPlayerId() {
        return playerId;
    }

    public void setPlayerId(UUID playerId) {
        this.playerId = playerId;
    }

    public String getDinozId() {
        return dinozId;
    }

    public void setDinozId(String dinozId) {
        this.dinozId = dinozId;
    }

    public byte[] getFirst() {
        return first;
    }

    public void setFirst(byte[] first) {
        this.first = first;
    }

    public String getFourth() {
        return fourth;
    }

    public void setFourth(String fourth) {
        this.fourth = fourth;
    }

    public ClientInformationBisDetail getSkipInfo() {
        return skipInfo;
    }

    public void setSkipInfo(ClientInformationBisDetail skipInfo) {
        this.skipInfo = skipInfo;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public OffsetDateTime getRetrievedDate() {
        return retrievedDate;
    }

    public void setRetrievedDate(OffsetDateTime retrievedDate) {
        this.retrievedDate = retrievedDate;
    }
}
