package com.dinoparc.api.domain.dinoz;

public class TournamentRanking implements Comparable<TournamentRanking> {

  private Integer position;
  private String dinozName;
  private String masterName;
  private Integer nbPoints;
  private Boolean isAlive;
  private Integer level;

  public TournamentRanking() {

  }

  public TournamentRanking(Integer position, String dinozName, String masterName, Integer nbPoints, Boolean isAlive, Integer level) {
    this.position = position;
    this.dinozName = dinozName;
    this.masterName = masterName;
    this.nbPoints = nbPoints;
    this.isAlive = isAlive;
    this.level = level;
  }

  public Integer getPosition() {
    return position;
  }

  public void setPosition(Integer position) {
    this.position = position;
  }

  public String getDinozName() {
    return dinozName;
  }

  public void setDinozName(String dinozName) {
    this.dinozName = dinozName;
  }

  public String getMasterName() {
    return masterName;
  }

  public void setMasterName(String masterName) {
    this.masterName = masterName;
  }

  public Integer getNbPoints() {
    return nbPoints;
  }

  public void setNbPoints(Integer nbPoints) {
    this.nbPoints = nbPoints;
  }

  public Boolean getAlive() {
    return isAlive;
  }

  public void setAlive(Boolean alive) {
    isAlive = alive;
  }

  public Integer getLevel() {
    return level;
  }

  public void setLevel(Integer level) {
    this.level = level;
  }

  @Override
  public int compareTo(TournamentRanking otherRanking) {
    return otherRanking.getNbPoints() - this.nbPoints;
  }

}
