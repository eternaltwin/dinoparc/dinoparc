package com.dinoparc.api.domain.dinoz;

import java.util.UUID;

public class ArmyDinozDto {
    private String armyDinozId;
    private String masterName;
    private String dinozName;
    private int rank;
    private int nbAttacks;
    private int maxNbAttacks;
    private int bossLifeLost;

    public String getArmyDinozId() {
        return armyDinozId;
    }

    public void setArmyDinozId(String armyDinozId) {
        this.armyDinozId = armyDinozId;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getDinozName() {
        return dinozName;
    }

    public void setDinozName(String dinozName) {
        this.dinozName = dinozName;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getNbAttacks() {
        return nbAttacks;
    }

    public void setNbAttacks(int nbAttacks) {
        this.nbAttacks = nbAttacks;
    }

    public int getMaxNbAttacks() {
        return maxNbAttacks;
    }

    public void setMaxNbAttacks(int maxNbAttacks) {
        this.maxNbAttacks = maxNbAttacks;
    }

    public int getBossLifeLost() {
        return bossLifeLost;
    }

    public void setBossLifeLost(int bossLifeLost) {
        this.bossLifeLost = bossLifeLost;
    }
}
