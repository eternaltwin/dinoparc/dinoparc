package com.dinoparc.api.configuration;

import discord4j.core.DiscordClientBuilder;
import discord4j.core.GatewayDiscordClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

@Configuration
public class DiscordWebhookConfig {

    @Value("${server.discord-token:#{null}}")
    public String discordToken;

    @Bean
    public GatewayDiscordClient gatewayDiscordClient() {
        if (Objects.isNull(discordToken)) {
            return null;
        }

        return DiscordClientBuilder.create(discordToken)
                .build()
                .login()
                .block();
    }
}