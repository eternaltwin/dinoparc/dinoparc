package com.dinoparc.api.controllers.dto;

public class RockDto {

  private String rockSummary;
  private Integer newDangerValue;

  public RockDto() {

  }

  public RockDto(String rockSummary, Integer newDangerValue) {
    super();
    this.rockSummary = rockSummary;
    this.newDangerValue = newDangerValue;
  }

  public String getRockSummary() {
    return rockSummary;
  }

  public void setRockSummary(String rockSummary) {
    this.rockSummary = rockSummary;
  }

  public Integer getNewDangerValue() {
    return newDangerValue;
  }

  public void setNewDangerValue(Integer newDangerValue) {
    this.newDangerValue = newDangerValue;
  }

}
