package com.dinoparc.api.controllers.dto;

import com.dinoparc.api.domain.account.DinoparcConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InventoryDto {

  private Map<String, Integer> inventoryItemsMap;
  private List<DinozId> dinozList;
  private Map<String, Integer> pricesList;

  public InventoryDto() {
    inventoryItemsMap = new HashMap<String, Integer>();
    dinozList = new ArrayList<DinozId>();
    pricesList = DinoparcConstants.PRICES;
  }

  public Map<String, Integer> getInventoryItemsMap() {
    return inventoryItemsMap;
  }

  public void setInventoryItemsMap(Map<String, Integer> inventoryItemsMap) {
    this.inventoryItemsMap = inventoryItemsMap;
  }

  public List<DinozId> getDinozList() {
    return dinozList;
  }

  public void setDinozList(List<DinozId> dinozList) {
    this.dinozList = dinozList;
  }

  public Map<String, Integer> getPricesList() {
    return pricesList;
  }

  public void setPricesList(Map<String, Integer> pricesList) {
    this.pricesList = pricesList;
  }
}
