package com.dinoparc.api.controllers;

import com.dinoparc.api.controllers.dto.mission.PlayerMissionRequest;
import com.dinoparc.api.domain.account.AccountUtils;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.misc.TokenAccountValidation;
import com.dinoparc.api.domain.mission.PlayerMission;
import com.dinoparc.api.domain.mission.RecMission;
import com.dinoparc.api.repository.*;
import com.dinoparc.api.services.AccountService;
import com.dinoparc.api.services.RecMissionsService;
import com.dinoparc.api.services.RewardService;
import kotlin.Pair;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/rec-missions")
public class RecMissionController {
    private final TokenAccountValidation tokenAccountValidation;

    private final AccountService accountService;
    private final RecMissionsService recMissionsService;
    private final RewardService rewardService;
    private final PgArchivePlayerMissionRepository archivePlayerMissionRepository;
    private final PgDinozRepository dinozRepository;
    private final PgPlayerMissionRepository playerMissionRepository;
    private final PgPlayerStatRepository playerStatRepository;
    private final PgRecMissionRepository recMissionRepository;
    private final PgRoleActionRepository roleActionRepository;

    public RecMissionController(TokenAccountValidation tokenAccountValidation, AccountService accountService,
                                RecMissionsService recMissionsService, RewardService rewardService,
                                PgArchivePlayerMissionRepository archivePlayerMissionRepository, PgDinozRepository dinozRepository,
                                PgPlayerMissionRepository playerMissionRepository,
                                PgPlayerStatRepository playerStatRepository, PgRecMissionRepository recMissionRepository,
                                PgRoleActionRepository roleActionRepository) {
        this.tokenAccountValidation = tokenAccountValidation;
        this.accountService = accountService;
        this.recMissionsService = recMissionsService;
        this.rewardService = rewardService;
        this.archivePlayerMissionRepository = archivePlayerMissionRepository;
        this.dinozRepository = dinozRepository;
        this.playerMissionRepository = playerMissionRepository;
        this.playerStatRepository = playerStatRepository;
        this.recMissionRepository = recMissionRepository;
        this.roleActionRepository = roleActionRepository;
    }

    // Role : ADMIN / GAME MASTER
    @PostMapping
    @Transactional
    public List<RecMission> createMissions(@RequestHeader("Authorization") String token, @RequestBody List<RecMission> recMissionsToCreate) {
        var playerId = this.tokenAccountValidation.getPlayerIdFromToken(token);
        var endpoint = "POST /api/rec-missions";
        var content = String.valueOf(recMissionsToCreate.size());

        if (!this.tokenAccountValidation.isAdminOrGameMasterUser(token)) {
            this.roleActionRepository.create(AccountUtils.getRoleActionHistory(playerId, endpoint, content, false, false));
            return Collections.emptyList();
        }

        var success = recMissionsToCreate.isEmpty() || !recMissionsToCreate.stream().anyMatch(this::isInvalidRecMission);

        if (!success) {
            this.roleActionRepository.create(AccountUtils.getRoleActionHistory(playerId, endpoint, content, true, false));
            return Collections.emptyList();
        }

        try {
            recMissionsToCreate.stream().forEach(this.recMissionRepository::create);
            this.roleActionRepository.create(AccountUtils.getRoleActionHistory(playerId, endpoint, content, true, true));
            return recMissionsToCreate;
        } catch (Exception e) {
            this.roleActionRepository.create(AccountUtils.getRoleActionHistory(playerId, endpoint, content, true, false));
            return Collections.emptyList();
        }
    }

    @GetMapping("/{accountId}/{dinozId}/availableMissions")
    public List<PlayerMission> getMissions(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
        if (checkPlayerDinoz(token, accountId, dinozId)) {
            var playerId = UUID.fromString(accountId);

            //Delete expired missions :
            ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
            for (PlayerMission expiredPlayerMission : playerMissionRepository.getAllMissions(playerId).stream().filter(activeMission -> activeMission.getExpirationDate() < (now.toEpochSecond() * 1000)).toList()) {
                playerMissionRepository.delete(expiredPlayerMission.getId());
            }

            List<PlayerMission> playerActiveMissions = playerMissionRepository.getAllMissions(playerId).stream().toList();

            //Choose a random new missions (1 - 3) :
            for (int i = 0; i < (3 - playerActiveMissions.size()); i++) {
                    PlayerMissionRequest mission = new PlayerMissionRequest();
                    mission.setPlayerId(UUID.fromString(accountId));

                    //Draw if we want a Dinoz specific mission or not in the current list :
                    if (ThreadLocalRandom.current().nextInt(1, 3 + 1) == 1) {
                        List<Dinoz> allDinozOfPlayer = accountService.getAllDinozOfAccount(accountId);
                        Collections.shuffle(allDinozOfPlayer);
                        mission.setDinozId(allDinozOfPlayer.get(0).getId());
                    }
                    mission.setMissionType(recMissionsService.getRandomMissionTypeWithWeights());
                    this.generatePlayerMissions(token, mission);
            }

            //Before returning, we must refresh each mission counters:
            for (PlayerMission activeMission : playerMissionRepository.getAllMissions(playerId)) {
                Boolean playerMission = activeMission.getDinozId() == null;
                Long currentCounter = playerMission ? playerStatRepository.getCounter(playerId, activeMission.getCounterName())
                        : playerStatRepository.getCounter(playerId, activeMission.getDinozId(), activeMission.getCounterName());

                if (currentCounter >= activeMission.getCounterTargetValue()) {
                    playerMissionRepository.setCounterCurrentValue(activeMission.getId(), activeMission.getCounterTargetValue());
                } else {
                    playerMissionRepository.setCounterCurrentValue(activeMission.getId(), currentCounter);
                }
            }
            List<PlayerMission> playerMissions = playerMissionRepository.getAllMissions(playerId);
            for (PlayerMission mission : playerMissions) {
                mission.setReward(rewardService.getReward(mission.getRewardId()));
                mission.getReward().generateDisplayableRewardsMap();
            }
            return playerMissions;
        }
        return null;
    }

    // Role : ALL
    @GetMapping("/{accountId}/{dinozId}")
    public List<PlayerMission> getPlayerDinozRecMissions(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
        if (checkPlayerDinoz(token, accountId, dinozId)) {
            var playerId = UUID.fromString(accountId);
            var playerMissions = new ArrayList<>(playerMissionRepository.getAllMissions(playerId));

            // Compute mission completed
            for (var playerMission : playerMissions) {
                if (playerMission.getCounterCurrentValue() < playerMission.getCounterTargetValue()
                    && playerMission.getExpirationDate() > ZonedDateTime.now(ZoneId.of("Europe/Paris")).toEpochSecond()) {

                    // Compute current counter
                    Long currentCounter;

                    if (playerMission.getDinozId() != null) {
                        // Dinoz mission
                        currentCounter = playerStatRepository.getCounter(playerId, dinozId, playerMission.getCounterName());
                    } else {
                        // Player mission
                        currentCounter = playerStatRepository.getCounter(playerId, playerMission.getCounterName());
                    }

                    if (currentCounter >= playerMission.getCounterTargetValue()) {
                        currentCounter = playerMission.getCounterTargetValue();
                    }
                    playerMission.setCounterCurrentValue(currentCounter);
                    playerMissionRepository.setCounterCurrentValue(playerMission.getId(), currentCounter);
                }
            }
            return playerMissions;
        }

        return Collections.emptyList();
    }

    @DeleteMapping("/{accountId}/{dinozId}/{playerMissionId}")
    public boolean archivePlayerRecMission(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId, @PathVariable String playerMissionId) {
        if (checkPlayerDinoz(token, accountId, dinozId)) {
            var playerId = UUID.fromString(accountId);
            var playerMission = playerMissionRepository.get(UUID.fromString(playerMissionId));
            if (isValidAndExpired(playerMission, playerId, dinozId)) {
                archivePlayerMissionRepository.create(playerMission);
                playerMissionRepository.delete(playerMission.getId());
                return true;
            }
        }

        return false;
    }

    @PostMapping("/claim/{accountId}/{dinozId}/{playerMissionId}")
    public boolean claimReward(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId, @PathVariable String playerMissionId) {
        if (checkPlayerDinoz(token, accountId, dinozId)) {
            var playerId = UUID.fromString(accountId);
            var playerMission = playerMissionRepository.get(UUID.fromString(playerMissionId));
            if (isValidAndNonExpired(playerMission, playerId, dinozId)
                    && playerMission.getCounterCurrentValue() >= playerMission.getCounterTargetValue()
                    && !playerMission.isRewardClaimed()) {
                rewardService.applyReward(playerId, playerMission.getRewardId());
                playerMissionRepository.rewardClaimed(UUID.fromString(playerMissionId));

                if (playerMission.isDaily()) {
                    playerStatRepository.addStats(UUID.fromString(accountId), dinozId, List.of(new Pair(PgPlayerStatRepository.NB_DAILY_REC_MISSION, 1)));
                } else if (playerMission.isWeekly()) {
                    playerStatRepository.addStats(UUID.fromString(accountId), dinozId, List.of(new Pair(PgPlayerStatRepository.NB_WEEKLY_REC_MISSION, 1)));
                } else {
                    playerStatRepository.addStats(UUID.fromString(accountId), dinozId, List.of(new Pair(PgPlayerStatRepository.NB_MONTHLY_REC_MISSION, 1)));
                }
                return true;
            }
        }

        return false;
    }

    @PostMapping("/generate")
    public List<PlayerMission> generatePlayerMissions(@RequestHeader("Authorization") String token, @RequestBody PlayerMissionRequest playerMissionRequest) {
        UUID playerId = playerMissionRequest.getPlayerId();
        Optional<Player> player = accountService.getAccountById(playerId.toString());

        if (player.isPresent()) {
            this.tokenAccountValidation.validate(player.get(), token, null);

            if (AccountController.isValidAndNotBlockedAccount(player)) {
                var playerMissions = playerMissionRepository.getAllMissions(playerId);

                int nbPlayerDailyMission = 0;
                int nbPlayerWeeklyMission = 0;
                int nbPlayerMonthlyMission = 0;
                int nbPlayerDinozDailyMission = 0;
                int nbPlayerDinozWeeklyMission = 0;
                int nbPlayerDinozMonthlyMission = 0;

                for (var playerMission : playerMissions) {
                    if (playerMission.getDinozId() == null) {
                        if (playerMission.isDaily()) {
                            nbPlayerDailyMission++;
                        } else if (playerMission.isWeekly()) {
                            nbPlayerWeeklyMission++;
                        } else {
                            nbPlayerMonthlyMission++;
                        }
                    } else {
                        if (playerMission.isDaily()) {
                            nbPlayerDinozDailyMission++;
                        } else if (playerMission.isWeekly()) {
                            nbPlayerDinozWeeklyMission++;
                        } else {
                            nbPlayerDinozMonthlyMission++;
                        }
                    }
                }

                var playerMission = playerMissionRequest.getDinozId() == null;
                var currentMissions = playerMissions.stream().map(pm -> pm.getId()).collect(Collectors.toList());
                int limit;
                int nbMissions;

                switch (playerMissionRequest.getMissionType()) {
                    case 1:
                        // Daily
                        limit = playerMission ? DinoparcConstants.PLAYER_MISSION_LIMIT_DAILY : DinoparcConstants.PLAYER_DINOZ_MISSION_LIMIT_DAILY;
                        nbMissions = playerMission ? nbPlayerDailyMission : nbPlayerDinozDailyMission;
                        generateOnePlayerMission(playerId, playerMissionRequest.getDinozId(), currentMissions, nbMissions, limit, playerMission, true, false, false);
                        break;
                    case 2:
                        // Weekly
                        limit = playerMission ? DinoparcConstants.PLAYER_MISSION_LIMIT_WEEKLY : DinoparcConstants.PLAYER_DINOZ_MISSION_LIMIT_WEEKLY;
                        nbMissions = playerMission ? nbPlayerWeeklyMission : nbPlayerDinozWeeklyMission;
                        generateOnePlayerMission(playerId, playerMissionRequest.getDinozId(), currentMissions, nbMissions, limit, playerMission, false, true, false);
                        break;
                    default:
                        // Monthly
                        limit = playerMission ? DinoparcConstants.PLAYER_MISSION_LIMIT_MONTHLY : DinoparcConstants.PLAYER_DINOZ_MISSION_LIMIT_MONTHLY;
                        nbMissions = playerMission ? nbPlayerMonthlyMission : nbPlayerDinozMonthlyMission;
                        generateOnePlayerMission(playerId, playerMissionRequest.getDinozId(), currentMissions, nbMissions, limit, playerMission, false, false, true);
                        break;
                }

                return playerMissionRepository.getAllMissions(playerId);
            }
        }
        return Collections.emptyList();
    }

    private boolean checkPlayerDinoz(String token, @PathVariable String accountId, @PathVariable String dinozId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            if (AccountController.isValidAndNotBlockedAccount(player)) {
                var dinoz = dinozRepository.findById(dinozId);

                // Check dinoz owned by player, dinoz is alive, and dinoz place number
                if (dinoz.isPresent()
                        && dinoz.get().getMasterId().equals(accountId)
                        && dinoz.get().getPlaceNumber() == 0
                        && dinoz.get().getLife() > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isValidAndNonExpired(PlayerMission playerMission, UUID playerId, String dinozId) {
        return playerMission.getPlayerId().equals(playerId) && (playerMission.getDinozId() == null || playerMission.getDinozId().equals(dinozId)) && playerMission.getExpirationDate() > ZonedDateTime.now(ZoneId.of("Europe/Paris")).toEpochSecond();
    }

    private boolean isValidAndExpired(PlayerMission playerMission, UUID playerId, String dinozId) {
        return playerMission.getPlayerId().equals(playerId) && (playerMission.getDinozId() == null || playerMission.getDinozId().equals(dinozId)) && playerMission.getExpirationDate() <= ZonedDateTime.now(ZoneId.of("Europe/Paris")).toEpochSecond();
    }

    private void generateOnePlayerMission(UUID playerId, String dinozId, List<UUID> currentMissions, int currentNbMission, int limitNbMission, boolean playerMission, boolean daily, boolean weekly, boolean monthly) {
        if (currentNbMission < limitNbMission) {
            Integer random = ThreadLocalRandom.current().nextInt(1, 100 + 1);
            int rarity = random < 40 ? 1 : random < 70 ? 2 : random < 90 ? 3 : random < 99 ? 4 : 5;
            var recMissions = recMissionRepository.getMissions(playerMission, daily, weekly, monthly, 1, rarity);
            Collections.shuffle(recMissions);
            var selectedMission = recMissions.get(0);
            var playerStat = playerMission ? playerStatRepository.getCounter(playerId, selectedMission.getCounterName()) : playerStatRepository.getCounter(playerId, dinozId, selectedMission.getCounterName());

            // Generate player stat / player dinoz stat if not exists
            if (playerStat == null) {
                playerStatRepository.create(playerId, dinozId);
                playerStat = playerMission ? playerStatRepository.getCounter(playerId, selectedMission.getCounterName()) : playerStatRepository.getCounter(playerId, dinozId, selectedMission.getCounterName());
            }

            var playerMissionToCreate = new PlayerMission();
            playerMissionToCreate.setId(UUID.randomUUID());
            playerMissionToCreate.setPlayerId(playerId);
            playerMissionToCreate.setDinozId(dinozId);
            if (dinozId != null && !dinozId.isEmpty()) {
                playerMissionToCreate.setDinozName(dinozRepository.findById(dinozId).get().getName());
            }
            playerMissionToCreate.setRecMissionId(selectedMission.getId());
            playerMissionToCreate.setDaily(daily);
            playerMissionToCreate.setWeekly(weekly);
            playerMissionToCreate.setMonthly(monthly);
            playerMissionToCreate.setRewardId(selectedMission.getRewardId());
            playerMissionToCreate.setDifficulty(selectedMission.getDifficulty());
            playerMissionToCreate.setRarity(selectedMission.getRarity());
            playerMissionToCreate.setCounterName(selectedMission.getCounterName());
            playerMissionToCreate.setCounterInitialValue(playerStat);
            playerMissionToCreate.setCounterCurrentValue(playerStat);
            playerMissionToCreate.setCounterTargetValue(playerStat + selectedMission.getCounterValue());

            playerMissionRepository.create(playerMissionToCreate, daily, weekly, monthly);
            currentNbMission++;
        }
    }

    private boolean isInvalidRecMission(RecMission recMission) {
        var givenTimeParams = List.of(recMission.isDaily(), recMission.isWeekly(), recMission.isMonthly()).stream().filter(b -> b).count();
        var invalidReward = false;

        if (recMission.getRewardId() != null) {
            try {
                rewardService.getReward(recMission.getRewardId());
            } catch(Exception e) {
                invalidReward = true;
            }
        }

        return givenTimeParams == 0
                || givenTimeParams > 1
                || invalidReward
                || recMission.getId() == null
                || recMission.getRarity() < 0
                || recMission.getRarity() > 5
                || recMission.getCounterName() == null
                || recMission.getCounterValue() < 1;
    }
}
