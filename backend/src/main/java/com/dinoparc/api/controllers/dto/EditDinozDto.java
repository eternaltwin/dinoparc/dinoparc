package com.dinoparc.api.controllers.dto;

public class EditDinozDto {

    private Integer feu;
    private Integer terre;
    private Integer eau;
    private Integer foudre;
    private Integer air;

    public EditDinozDto() {

    }

    public Integer getFeu() {
        return feu;
    }

    public void setFeu(Integer feu) {
        this.feu = feu;
    }

    public Integer getTerre() {
        return terre;
    }

    public void setTerre(Integer terre) {
        this.terre = terre;
    }

    public Integer getEau() {
        return eau;
    }

    public void setEau(Integer eau) {
        this.eau = eau;
    }

    public Integer getFoudre() {
        return foudre;
    }

    public void setFoudre(Integer foudre) {
        this.foudre = foudre;
    }

    public Integer getAir() {
        return air;
    }

    public void setAir(Integer air) {
        this.air = air;
    }
}
