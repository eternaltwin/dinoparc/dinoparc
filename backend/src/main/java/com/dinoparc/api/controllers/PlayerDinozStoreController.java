package com.dinoparc.api.controllers;

import com.dinoparc.api.domain.account.BuyNewDinozRequest;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.account.PlayerDinozStore;
import com.dinoparc.api.domain.misc.TokenAccountValidation;
import com.dinoparc.api.repository.*;
import com.dinoparc.api.services.*;
import net.eternaltwin.client.HttpEtwinClient;
import net.eternaltwin.oauth.client.RfcOauthClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/player-dinoz-store")
public class PlayerDinozStoreController {

    private final AccountService accountService;
    private final PlayerDinozStoreService playerDinozStoreService;
    private TokenAccountValidation tokenAccountValidation;

    @Autowired
    public PlayerDinozStoreController (
            AccountService accountService,
            PlayerDinozStoreService playerDinozStoreService,
            TokenAccountValidation tokenAccountValidation) {

        this.accountService = accountService;
        this.playerDinozStoreService = playerDinozStoreService;
        this.tokenAccountValidation = tokenAccountValidation;
    }

    @GetMapping("/{accountId}")
    public List<PlayerDinozStore> getOrGenerateStore(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
        Optional<Player> optAccount = accountService.getAccountById(accountId);
        if (AccountController.isValidAndNotBlockedAccount(optAccount)) {
            Player account = optAccount.get();
            this.tokenAccountValidation.validate(account, token, null);

            if (account.getLastGeneratedStore() == null) {
                return playerDinozStoreService.renewShopForUser(account.getId());
            }

            return playerDinozStoreService.getAllByPlayerId(UUID.fromString(account.getId()));
        }

        return Collections.emptyList();
    }

    @PostMapping
    public boolean buyNewDinozFromShop(@RequestHeader("Authorization") String token, @RequestBody BuyNewDinozRequest buyNewDinozRequest) {
        Optional<Player> optBuyerAccount = accountService.getAccountById(buyNewDinozRequest.getAccountId());
        if (AccountController.isValidAndNotBlockedAccount(optBuyerAccount)) {
            Player buyerAccount = optBuyerAccount.get();
            this.tokenAccountValidation.validate(buyerAccount, token, null);
            Optional<PlayerDinozStore> optPlayerDinozStore = playerDinozStoreService.getById(UUID.fromString(buyNewDinozRequest.getNewBornDinozId()));

            if (optPlayerDinozStore.isPresent()) {
                return playerDinozStoreService.buyDinoz(buyerAccount, optPlayerDinozStore.get(), buyNewDinozRequest.getDinozName());
            }
        }

        return true;
    }
}
