package com.dinoparc.api.controllers.dto;

public class TradeCollectionResponseDto {

    private Integer nbOfGiftTickets;
    private Integer nbOfGiftMagikTickets;
    private Boolean success;

    public TradeCollectionResponseDto() {

    }

    public TradeCollectionResponseDto(Integer nbOfGiftTickets, Integer nbOfGiftMagikTickets, Boolean success) {
        this.nbOfGiftTickets = nbOfGiftTickets;
        this.nbOfGiftMagikTickets = nbOfGiftMagikTickets;
        this.success = success;
    }

    public Integer getNbOfGiftTickets() {
        return nbOfGiftTickets;
    }

    public void setNbOfGiftTickets(Integer nbOfGiftTickets) {
        this.nbOfGiftTickets = nbOfGiftTickets;
    }

    public Integer getNbOfGiftMagikTickets() {
        return nbOfGiftMagikTickets;
    }

    public void setNbOfGiftMagikTickets(Integer nbOfGiftMagikTickets) {
        this.nbOfGiftMagikTickets = nbOfGiftMagikTickets;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
