package com.dinoparc.api.controllers.dto;

public class GiveObjectDto {

  private boolean inutileError;
  private boolean badNameError;
  private boolean success;
  private boolean noStockError;
  private boolean limitReached;

  private String successMessageFr;
  private String successMessageEs;
  private String successMessageEn;

  public GiveObjectDto() {
    this.inutileError = false;
    this.noStockError = false;
    this.badNameError = false;
    this.limitReached = false;
  }

  public boolean isLimitReached() {
    return limitReached;
  }

  public void setLimitReached(boolean limitReached) {
    this.limitReached = limitReached;
  }

  public boolean isInutileError() {
    return inutileError;
  }

  public void setInutileError(boolean inutileError) {
    this.inutileError = inutileError;
  }

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public String getSuccessMessageFr() {
    return successMessageFr;
  }

  public void setSuccessMessageFr(String successMessageFr) {
    this.successMessageFr = successMessageFr;
  }

  public String getSuccessMessageEs() {
    return successMessageEs;
  }

  public void setSuccessMessageEs(String successMessageEs) {
    this.successMessageEs = successMessageEs;
  }

  public String getSuccessMessageEn() {
    return successMessageEn;
  }

  public void setSuccessMessageEn(String successMessageEn) {
    this.successMessageEn = successMessageEn;
  }

  public boolean isNoStockError() {
    return noStockError;
  }

  public void setNoStockError(boolean noStockError) {
    this.noStockError = noStockError;
  }

  public boolean isBadNameError() {
    return badNameError;
  }

  public void setBadNameError(boolean badNameError) {
    this.badNameError = badNameError;
  }

}
