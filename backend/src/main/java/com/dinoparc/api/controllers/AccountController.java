package com.dinoparc.api.controllers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.dinoparc.api.controllers.dto.*;
import com.dinoparc.api.domain.account.Collection;
import com.dinoparc.api.domain.account.*;
import com.dinoparc.api.domain.bazar.BazarListing;
import com.dinoparc.api.domain.dinoz.*;
import com.dinoparc.api.domain.misc.ChaudronCraftTryResult;
import com.dinoparc.api.domain.misc.TokenAccountValidation;
import com.dinoparc.api.domain.mission.GranitMissionDao;
import com.dinoparc.api.repository.*;
import com.dinoparc.api.services.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.eternaltwin.auth.AccessTokenAuthContext;
import net.eternaltwin.auth.AuthContext;
import net.eternaltwin.client.Auth;
import net.eternaltwin.client.HttpEtwinClient;
import net.eternaltwin.oauth.client.AccessToken;
import net.eternaltwin.oauth.client.RfcOauthClient;
import okhttp3.HttpUrl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.HandlerMapping;

import java.io.IOException;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/account")
public class AccountController {

  @Value("${server.frontend}")
  public String frontendUri;

  @Value("${server.secret}")
  public String secret;

  public static List<String> connectedToday = new ArrayList<>();
  public static List<String> enrolledToday = new ArrayList<>();

  private final AccountService accountService;
  private final EventFightService eventFightService;
  private final FightService fightService;
  private final FusionService fusionService;
  private final TournamentService tournamentService;
  private final ArenaService arenaService;
  private final ClientInformationService clientInformationService;
  private final BossService bossService;
  private final PgPlayerRepository playerRepository;
  private final RfcOauthClient rfcOauthClient;
  private final HttpEtwinClient httpEtwinClient;
  private TokenAccountValidation tokenAccountValidation;
  private final BazarService bazarService;
  private final PgClientInformationTraceRepository clientInformationTraceRepository;
  private final PgHistoryRepository historyRepository;
  private final PgCollectionRepository collectionRepository;
  private final PgDinozRepository dinozRepository;
  private final PgInventoryRepository inventoryRepository;
  private final PgBossRepository raidBossRepository;
  private final PgWildWistitiRepository wildWistitiRepository;
  private final JooqPlayerStatRepository jooqPlayerStatRepository;

  @Autowired
  public AccountController(
          AccountService accountService,
          FightService fightService,
          EventFightService eventFightService,
          FusionService fusionService,
          TournamentService tournamentService,
          ArenaService arenaService,
          ClientInformationService clientInformationService,
          BossService bossService,
          PgPlayerRepository playerRepository,
          RfcOauthClient rfcOauthClient,
          HttpEtwinClient httpEtwinClient,
          TokenAccountValidation tokenAccountValidation,
          BazarService bazarService,
          PgClientInformationTraceRepository clientInformationTraceRepository,
          PgHistoryRepository historyRepository,
          PgCollectionRepository collectionRepository,
          PgDinozRepository dinozRepository,
          PgInventoryRepository inventoryRepository,
          PgBossRepository raidBossRepository,
          PgWildWistitiRepository wildWistitiRepository,
          JooqPlayerStatRepository jooqPlayerStatRepository) {

    this.accountService = accountService;
    this.fightService = fightService;
    this.eventFightService = eventFightService;
    this.fusionService = fusionService;
    this.tournamentService = tournamentService;
    this.arenaService = arenaService;
    this.clientInformationService = clientInformationService;
    this.bossService = bossService;
    this.playerRepository = playerRepository;
    this.rfcOauthClient = rfcOauthClient;
    this.httpEtwinClient = httpEtwinClient;
    this.tokenAccountValidation = tokenAccountValidation;
    this.bazarService = bazarService;
    this.clientInformationTraceRepository = clientInformationTraceRepository;
    this.historyRepository = historyRepository;
    this.collectionRepository = collectionRepository;
    this.dinozRepository = dinozRepository;
    this.inventoryRepository = inventoryRepository;
    this.raidBossRepository = raidBossRepository;
    this.wildWistitiRepository = wildWistitiRepository;
    this.jooqPlayerStatRepository = jooqPlayerStatRepository;
  }

  @PostMapping("/create")
  public Player createAccount(@RequestBody CreateAccountDto createDto) throws Exception {
    return accountService.createAndInitializeAccount(createDto, UUID.randomUUID().toString());
  }

  @PostMapping("/redirect")
  public void redirect(HttpServletResponse response) throws IOException {
    response.sendRedirect(rfcOauthClient.getAuthorizationUri("", "").toString());
  }

  @GetMapping("/callback")
  public void callback(@RequestParam String code, HttpServletRequest request, HttpServletResponse response) throws Exception {
    AccessToken accessToken = rfcOauthClient.getAccessTokenSync(code);
    AuthContext authContext = httpEtwinClient.getSelf(Auth.fromToken(accessToken.getAccessToken()));

    AccessTokenAuthContext acx = ((AuthContext.AccessToken) authContext).getInner();
    String userId = acx.getUser().getId().toUuidString();
    String username = acx.getUser().getDisplayName().getCurrent().getValue().getInner();

    Optional<Player> possibleAccount = playerRepository.findById(userId);

    String token = JWT.create()
            .withSubject(userId)
            .withExpiresAt(new Date(System.currentTimeMillis() + DinoparcConstants.JWT_EXPIRATION_TIME))
            .sign(Algorithm.HMAC512(secret.getBytes()));

    if (isValidAndNotBlockedAccount(possibleAccount)) {
      Player account = possibleAccount.get();
      if (account.getLastLogin() == null) {
        account.setLastLogin(ZonedDateTime.now(ZoneId.of("Europe/Paris")).minusDays(1).toEpochSecond());
      }

      ZonedDateTime currentParisDate = ZonedDateTime.now(ZoneId.of("Europe/Paris"));
      ZonedDateTime lastLogin = ZonedDateTime.ofInstant(Instant.ofEpochSecond(account.getLastLogin()), ZoneId.of("Europe/Paris"));

      if (lastLogin.getDayOfYear() != currentParisDate.getDayOfYear()) {
        addDailyGoldBonusEvent(account, currentParisDate);
      }
      account.setLastLogin(ZonedDateTime.now(ZoneId.of("Europe/Paris")).toEpochSecond());
      playerRepository.updateImgMode(account.getId(), PlayerImgModeEnum.BASE_64.name());
      if (!possibleAccount.get().getName().equals(username)) {
        account.setName(username);
        List<Dinoz> dinozList = accountService.getAllDinozOfAccount(account);
        for (Dinoz dinoz : dinozList) {
            dinozRepository.setMasterInfos(dinoz.getId(), account.getId(), username);
        }
      }
      if (!account.getUnlockedDinoz().contains(19)) {
        account.getUnlockedDinoz().add(19);
      }
      playerRepository.save(account);

    } else {
      accountService.createAndInitializeAccount(username, userId);
      enrolledToday.add(username);
    }
    clientInformationService.checkClientInfos(username, request);
    redirectToNeoparc(response, userId, username, token);
  }

  protected void addDailyGoldBonusEvent(Player account, ZonedDateTime currentDateTime) {
    Integer dailyBonus = 2000;

    //JUNE : NEOPARC ANNIVERSARY DAILY BONUS
    if (currentDateTime.getMonthValue() == 6 && currentDateTime.getDayOfMonth() == 22) {
      inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.GIFT, 30);
      dailyBonus = 100000;
    }

    //OCTOBER : HALLOWEEN DAILY BONUS
    if (currentDateTime.getMonthValue() == 10 && currentDateTime.getDayOfMonth() == 31) {
      inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.MEDAILLE_CHOCOLAT, 50);
      inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.POTION_SOMBRE, 1);
    }

    //DECEMBER : CHRISTMAS DAILY BONUS
    if (currentDateTime.getMonthValue() == 12) {
      inventoryRepository.addInventoryItem(account.getId(), DinoparcConstants.GIFT, 1);
      dailyBonus = (2000 * currentDateTime.getDayOfMonth());
    }

    account.setCash(account.getCash() + dailyBonus);
    History buyHistory = new History();
    buyHistory.setPlayerId(account.getId());
    buyHistory.setType("newDay");
    buyHistory.setIcon("hist_buy.gif");
    buyHistory.setBuyAmount(dailyBonus);
    historyRepository.save(buyHistory);
  }

  private void redirectToNeoparc(HttpServletResponse response, String userId, String username, String token) throws Exception {
    HttpUrl redirectUrl = HttpUrl.parse(frontendUri.toString())
            .newBuilder()
            .addQueryParameter("userId", userId)
            .addQueryParameter("userDisplayName", username)
            .addQueryParameter("ac", Base64.getEncoder().encodeToString((userId + username).getBytes()))
            .addQueryParameter("token", token)
            .build();

    response.sendRedirect(redirectUrl.toString());
  }

  @GetMapping("/{accountId}/shinies")
  public String getDailyShiniesCountOfAccount(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return desiredAccount.get().getDailyShiniesFought().toString();
    }
    return "0";
  }

  @GetMapping("/{accountId}/dailyRaidFights")
  public String getDailyRaidFightsCountOfAccount(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.getDailyRaidFightsCountOfAccount(desiredAccount.get().getId()).toString();
    }
    return "0";
  }

  @GetMapping("/{accountId}/bonsStocks")
  public List<Integer> getBonsStocksOfAccount(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      List<Integer> response = new ArrayList<>();
      response.add(inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.BONS));
      response.add(accountService.getEngagedBonsAmountInBazarForUser(desiredAccount.get()));
      return response;
    }

    return new ArrayList<>();
  }

  @GetMapping("/{accountId}/money")
  public Integer getMoneyOfAccount(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return desiredAccount.get().getCash();
    }

    return 0;
  }

  @GetMapping("/{accountId}/shop")
  public ShopDto getShopForAccount(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.getShop(desiredAccount.get());
    }

    return null;
  }

  @PostMapping("/{accountId}/shop/buy")
  public boolean buyObjects(@RequestHeader("Authorization") String token, @PathVariable String accountId, @RequestBody BuyRequestDto buyRequest) throws IllegalAccessException {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.buyObjects(desiredAccount.get(), buyRequest);
    }

    return false;
  }

  @PutMapping("/{accountId}/{dinozId}/botOnly/{botOnly}")
  public void setDinozBotOnly(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String dinozId,
          @PathVariable Boolean botOnly) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);

      if (dinoz.isPresent()) {
        dinozRepository.setBotOnly(dinozId, botOnly);
      }
    }
  }

  @PutMapping("/{accountId}/{dinozId}/moveInList/{direction}")
  public void moveDinozInList(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String dinozId,
          @PathVariable Integer direction) {
    this.moveDinozInList(token, accountId, dinozId, direction, 1);
  }

  @PutMapping("/{accountId}/{dinozId}/moveInList/{direction}/{count}")
  public void moveDinozInList(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String dinozId,
          @PathVariable Integer direction,
          @PathVariable Integer count) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);

      if (dinoz.isPresent() && count > 0) {
        accountService.moveDinozInList(desiredAccount.get(), dinoz.get().getId(), direction, count);
      }
    }
  }

  @GetMapping("/{accountId}/{dinozId}/shop/crater")
  public ShopDto getCraterShopForAccount(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent()) {
        return accountService.getCraterShop(desiredAccount.get(), dinoz.get());
      }
    }

    return null;
  }

  @PostMapping("/{accountId}/{dinozId}/shop/crater/buy")
  public boolean buyCraterObjects(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String dinozId,
          @RequestBody BuyRequestDto buyRequest) throws IllegalAccessException {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && dinoz.get().getPlaceNumber() == 22 && dinoz.get().getLife() > 0) {
        return accountService.buyObjectsFromCrater(desiredAccount.get(), dinoz.get(), buyRequest);
      }
    }

    return false;
  }

  @GetMapping("/{accountId}/{dinozId}/shop/anomaly")
  public ShopDto getAnomalyShopForAccount(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent()) {
        return accountService.getAnomalyShop(desiredAccount.get());
      }
    }

    return null;
  }

  @PostMapping("/{accountId}/{dinozId}/shop/anomaly/buy")
  public boolean buyAnomalyObjects(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String dinozId,
          @RequestBody BuyRequestDto buyRequest) throws IllegalAccessException {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && dinoz.get().getPlaceNumber() == 20 && dinoz.get().getLife() > 0) {
        return accountService.buyObjectsFromAnomaly(desiredAccount.get(), buyRequest);
      }
    }

    return false;
  }

  @GetMapping("/{accountId}/{dinozId}/shop/pirates")
  public ShopDto getPiratesGuildeShopForAccount(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent()) {
        return accountService.getPirateShop(desiredAccount.get());
      }
    }

    return null;
  }

  @PostMapping("/{accountId}/{dinozId}/shop/pirates/buy")
  public boolean buyPiratesGuildeObjects(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String dinozId,
          @RequestBody BuyRequestDto buyRequest) throws IllegalAccessException {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && dinoz.get().getPlaceNumber() == 24 && dinoz.get().getLife() > 0) {
        return accountService.buyObjectsFromPirates(desiredAccount.get(), buyRequest);
      }
    }

    return false;
  }

  @PostMapping("/{accountId}/{dinozId}/shop-secret/plains/buy")
  public boolean buySecretLairObjects(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String dinozId,
          @RequestBody BuyRequestDto buyRequest) throws IllegalAccessException {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      EventDinozDto raidBoss = raidBossRepository.getRaidBoss();
      if (dinoz.isPresent() && dinoz.get().getPlaceNumber() == 37 && dinoz.get().getLife() > 0 && raidBoss.getLife() <= 0) {
        return accountService.buyObjectsFromSecretLair(desiredAccount.get(), buyRequest);
      }
    }

    return false;
  }

  @GetMapping("/{accountId}/{dinozId}/shop/plains")
  public ShopDto getPlainsShopForAccount(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent()) {
        return accountService.getPlainsShop(desiredAccount.get());
      }
    }

    return null;
  }

  @GetMapping("/{accountId}/{dinozId}/shop-secret/plains")
  public ShopDto getPlainsSecretShopForAccount(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent()) {
        return accountService.getPlainsSecretShop(desiredAccount.get());
      }
    }

    return null;
  }

  @GetMapping("/{accountId}/{dinozId}/clues-raid")
  public String getRaidCluesPieces(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && dinoz.get().getPlaceNumber() == 37) {
        return accountService.getRaidCluesPieces(dinoz.get().getId());
      }
    }

    return null;
  }

  @PostMapping("/{accountId}/{dinozId}/shop/plains/buy")
  public boolean buyFromPlainsShop(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String dinozId,
          @RequestBody BuyRequestDto buyRequest) throws IllegalAccessException {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && dinoz.get().getPlaceNumber() == 37 && dinoz.get().getLife() > 0) {
        return accountService.buyObjectsFromPlains(desiredAccount.get(), buyRequest);
      }
    }

    return false;
  }

  @PostMapping("/{accountId}/buyFromIrmaDirectly")
  public boolean buyFromIrmaDirectly(@RequestHeader("Authorization") String token, @PathVariable String accountId,
      @RequestBody BuyRequestDto buyRequest) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    Integer potionNumber = Integer.parseInt(buyRequest.getIrmaBuy());
    if (isValidAndNotBlockedAccount(desiredAccount) && potionNumber > 0) {
      return accountService.buyFromIrmaDirectly(potionNumber, desiredAccount.get());
    }
    return false;
  }

  @PostMapping("/{accountId}/buyPruniacDirectly")
  public boolean buyPruniacDirectly(@RequestHeader("Authorization") String token, @PathVariable String accountId, @RequestBody BuyRequestDto buyRequest) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    Integer pruniacNumber = Integer.parseInt(buyRequest.getPruniacBuy());

    if (isValidAndNotBlockedAccount(desiredAccount) && pruniacNumber > 0) {
      return accountService.buyPruniacDirectly(pruniacNumber, desiredAccount.get());
    }

    return false;
  }

  @GetMapping("/{accountId}")
  public AccountResponse getAccount(@RequestHeader("Authorization") String token, @PathVariable String accountId) throws Exception {
    Optional<Player> desiredAccount = playerRepository.findById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return new AccountResponse(HttpStatus.OK, desiredAccount.get(), playerRepository.countDinoz(accountId));
    } else {
      return new AccountResponse(HttpStatus.OK, null, 0);
    }
  }

  @GetMapping("/admin/{accountId}")
  public AccountResponse getAccountByAdmin(@RequestHeader("Authorization") String token, @PathVariable String accountId) throws Exception {
    Optional<Player> desiredAccount = playerRepository.findById(accountId);
    this.tokenAccountValidation.validateByToken(token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return new AccountResponse(HttpStatus.OK, desiredAccount.get(), playerRepository.countDinoz(accountId));

    } else {
      return new AccountResponse(HttpStatus.OK, null, 0);
    }
  }

  @GetMapping("/{accountId}/history/unseen")
  public List<History> getAccountHistoryUnseen(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = playerRepository.findById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      // FIXME : change getFirstUnseenPlayerHistories to getUnseenPlayerHistories (without limit 1 in jooq query)
      Optional<History> optHistory = historyRepository.getFirstUnseenPlayerHistories(desiredAccount.get().getId());
      return optHistory.isPresent() ? Collections.singletonList(optHistory.get()) : new ArrayList<>();
    }
    return new ArrayList<>();
  }

  @GetMapping("/{accountId}/alldinoz")
  public List<Dinoz> getDinozListByAccountId(@RequestHeader("Authorization") String token, @PathVariable String accountId) throws Exception {
    List<Dinoz> allDinoz = new ArrayList<Dinoz>();
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      allDinoz = accountService.getAllDinozOfAccount(desiredAccount.get());
    }

    return allDinoz;
  }

  @GetMapping("/{accountId}/granit-mission")
  public GranitMissionDao getGranitMissionData(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = playerRepository.findById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.getGranitMissionData(UUID.fromString(accountId));
    }
    return null;
  }

  @GetMapping("/{accountId}/dinozList/{pageNumber}")
  public List<RestrictedDinozDto> getDinozListByAccountId(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable Integer pageNumber) {
    List<RestrictedDinozDto> allDinozFromGivenPage = new ArrayList<>();
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount)) {
      allDinozFromGivenPage = accountService.getAllRestrictedDinozOfAccountFromGivenPage(desiredAccount.get(), pageNumber, true);
    }

    return allDinozFromGivenPage;
  }

  @GetMapping("/{accountId}/foreignDinozList/{pageNumber}")
  public List<RestrictedDinozDto> getForeignDinozListByAccountId(@PathVariable String accountId, @PathVariable Integer pageNumber) {

    List<Dinoz> allDinozFromGivenPage = new ArrayList<Dinoz>();
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    return accountService.getAllRestrictedDinozOfAccountFromGivenPage(desiredAccount.get(), pageNumber, false);
  }

  @GetMapping("/{accountId}/{dinozId}/{tourney}/access")
  public Boolean hasAccessToTournament(@RequestHeader("Authorization") String token, @PathVariable String accountId,
      @PathVariable String dinozId, @PathVariable String tourney) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount) && !desiredAccount.get().isBannedPvp()) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent()) {
        Collection collection = collectionRepository.getPlayerCollection(UUID.fromString(desiredAccount.get().getId()));
        return accountService.hasAccessToTournamentForDinoz(tourney, dinoz.get(), collection.getCollection());
      }
    }

    return false;
  }

  @GetMapping("/{accountId}/{dinozId}/tournament/{tournamentName}")
  public Tournament getTourney(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId, @PathVariable String tournamentName) {
    Tournament tournament = null;
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount) && !desiredAccount.get().isBannedPvp()) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && DinoparcConstants.TOURNAMENTS.contains(dinoz.get().getPlaceNumber())) {
        Collection collection = collectionRepository.getPlayerCollection(UUID.fromString(desiredAccount.get().getId()));
        tournament = accountService.getTournamentInfoForDinoz(tournamentName, dinoz.get(), collection.getCollection());
        tournamentService.addGlobalStatsToTournamentInstance(tournament, dinoz.get());
      }
    }

    return tournament;
  }

  @GetMapping("/{accountId}/{dinozId}/fight/{tournamentName}")
  public FightSummaryDto fightInTournament(@RequestHeader("Authorization") String token, @PathVariable String accountId,
      @PathVariable String dinozId, @PathVariable String tournamentName) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount) && !desiredAccount.get().isBannedPvp()) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);

      if (dinoz.isPresent()) {
        Dinoz foreignDinoz = tournamentService.findTournamentOpponent(dinoz.get(), tournamentName);

        if (foreignDinoz != null) {
          return fightService.fightDinozInTournament(dinoz.get(), foreignDinoz);
        }
      }
    }

    return null;
  }

  @GetMapping("/{accountId}/{dinozId}/tournament/{tournament}/quit")
  public Boolean quitTourney(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId,
      @PathVariable String tournament) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent()) {
        return accountService.attemptToQuitTourney(dinoz.get(), tournament);
      }
    }

    return false;
  }

  @GetMapping("/{accountId}/{dinozId}")
  public Dinoz getDinozById(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId).get();
    }

    return null;
  }

  @GetMapping("/{accountId}/{dinozId}/next")
  public Dinoz getNextDinozInList(@RequestHeader("Authorization") String token,
                                  @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.getNextDinozInList(desiredAccount.get(), dinozId).get();
    }

    return null;
  }

  @GetMapping("/{accountId}/{dinozId}/previous")
  public Dinoz getPreviousDinozInList(@RequestHeader("Authorization") String token,
                                      @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.getPreviousDinozInList(desiredAccount.get(), dinozId).get();
    }

    return null;
  }

  @GetMapping("/{accountId}/gift-tickets")
  public Integer getNumberOfGiftTickets(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.GIFT);
    }

    return null;
  }

  @GetMapping("/{accountId}/collection")
  public List<String> getCollection(@PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return collectionRepository.getPlayerCollection(UUID.fromString(accountId)).getCollection();
    }
    return null;
  }

  @GetMapping("/{accountId}/collectionEpic")
  public EpicCollectionDto getCollectionEpic(@PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);

    if (!isValidAndNotBlockedAccount(desiredAccount)) {
      return null;
    }

    var epicCollection = collectionRepository.getPlayerCollection(UUID.fromString(accountId)).getEpicCollection();
    EpicCollectionDto dto = new EpicCollectionDto();
    dto.setNumberOfLines(new ArrayList<String>());
    List<String> rawEpicList = new ArrayList<>(new HashSet<>(epicCollection));
    repositionSpecificEpicsInList(rawEpicList);

    if (desiredAccount.get().getWistitiCaptured() != null && desiredAccount.get().getWistitiCaptured() > 0) {
      Integer numberOfCaptures = desiredAccount.get().getWistitiCaptured();
      rawEpicList.remove("15");
      rawEpicList.remove("16");
      rawEpicList.remove("17");
      rawEpicList.remove("18");
      rawEpicList.remove("19");
      rawEpicList.remove("20");

      if (numberOfCaptures > 0 && numberOfCaptures <= 5) {
        rawEpicList.add("15");
      } else if (numberOfCaptures > 5 && numberOfCaptures <= 15) {
        rawEpicList.add("16");
      } else if (numberOfCaptures > 15 && numberOfCaptures <= 30) {
        rawEpicList.add("17");
      } else if (numberOfCaptures > 30 && numberOfCaptures <= 50) {
        rawEpicList.add("18");
      } else if (numberOfCaptures > 50 && numberOfCaptures <= 99) {
        rawEpicList.add("19");
      } else if (numberOfCaptures >= 100) {
        rawEpicList.add("20");
      }
    }

    if (desiredAccount.isPresent()) {
      dto.setCollectionEpic(rawEpicList);

      if (dto.getCollectionEpic().size() == 0) {
        dto.getNumberOfLines().add("0");

      } else if (dto.getCollectionEpic().size() >= 1 && dto.getCollectionEpic().size() <= 7) {
        dto.getNumberOfLines().addAll(new ArrayList<String>(Arrays.asList("0")));

      } else if (dto.getCollectionEpic().size() >= 8 && dto.getCollectionEpic().size() <= 14) {
        dto.getNumberOfLines().addAll(new ArrayList<String>(Arrays.asList("0", "1")));

      } else if (dto.getCollectionEpic().size() >= 15 && dto.getCollectionEpic().size() <= 21) {
        dto.getNumberOfLines().addAll(new ArrayList<String>(Arrays.asList("0", "1", "2")));

      } else if (dto.getCollectionEpic().size() >= 22 && dto.getCollectionEpic().size() <= 28) {
        dto.getNumberOfLines().addAll(new ArrayList<String>(Arrays.asList("0", "1", "2", "3")));

      } else if (dto.getCollectionEpic().size() >= 29 && dto.getCollectionEpic().size() <= 35) {
        dto.getNumberOfLines().addAll(new ArrayList<String>(Arrays.asList("0", "1", "2", "3", "4")));

      } else if (dto.getCollectionEpic().size() >= 36 && dto.getCollectionEpic().size() <= 42) {dto.getNumberOfLines().addAll(new ArrayList<String>(Arrays.asList("0", "1", "2", "3", "4", "5")));
        //For future use
      } else if (dto.getCollectionEpic().size() >= 43 && dto.getCollectionEpic().size() <= 49) {dto.getNumberOfLines().addAll(new ArrayList<String>(Arrays.asList("0", "1", "2", "3", "4", "5", "6")));
        //For future use
      } else if (dto.getCollectionEpic().size() >= 50 && dto.getCollectionEpic().size() <= 56) {dto.getNumberOfLines().addAll(new ArrayList<String>(Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7")));
        //For future use
      } else if (dto.getCollectionEpic().size() >= 57 && dto.getCollectionEpic().size() <= 63) {dto.getNumberOfLines().addAll(new ArrayList<String>(Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8")));
        //For future use
      } else {
        dto.getNumberOfLines().addAll(new ArrayList<String>(Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9")));
      }
    }
    return dto;
  }

  private void repositionSpecificEpicsInList(List<String> rawEpicList) {
    if (rawEpicList.contains("14")) {
      Collections.swap(rawEpicList, 0, rawEpicList.indexOf("14"));
    }

    if (rawEpicList.contains("1")) {
      rawEpicList.remove("1");
      rawEpicList.add("1");
    }

    if (rawEpicList.contains("3")) {
      rawEpicList.remove("3");
      rawEpicList.add("3");
    }

    if (rawEpicList.contains("4")) {
      rawEpicList.remove("4");
      rawEpicList.add("4");
    }

    if (rawEpicList.contains("5")) {
      rawEpicList.remove("5");
      rawEpicList.add("5");
    }

    if (rawEpicList.contains("6")) {
      rawEpicList.remove("6");
      rawEpicList.add("6");
    }

    if (rawEpicList.contains("7")) {
      rawEpicList.remove("7");
      rawEpicList.add("7");
    }

    if (rawEpicList.contains("8")) {
      rawEpicList.remove("8");
      rawEpicList.add("8");
    }

    if (rawEpicList.contains("9")) {
      rawEpicList.remove("9");
      rawEpicList.add("9");
    }

    if (rawEpicList.contains("10")) {
      rawEpicList.remove("10");
      rawEpicList.add("10");
    }

    if (rawEpicList.contains("11")) {
      rawEpicList.remove("11");
      rawEpicList.add("11");
    }

    if (rawEpicList.contains("12")) {
      rawEpicList.remove("12");
      rawEpicList.add("12");
    }

    if (rawEpicList.contains("13")) {
      rawEpicList.remove("13");
      rawEpicList.add("13");
    }

    if (rawEpicList.contains("21")) {
      rawEpicList.remove("21");
      rawEpicList.add("21");
    }

    if (rawEpicList.contains("22")) {
      rawEpicList.remove("22");
      rawEpicList.add("22");
    }

    if (rawEpicList.contains("23")) {
      rawEpicList.remove("23");
      rawEpicList.add("23");
    }

    if (rawEpicList.contains("24")) {
      rawEpicList.remove("24");
      rawEpicList.add("24");
    }

    if (rawEpicList.contains("25")) {
      rawEpicList.remove("25");
      rawEpicList.add("25");
    }

    if (rawEpicList.contains("26")) {
      rawEpicList.remove("26");
      rawEpicList.add("26");
    }

    if (rawEpicList.contains("27")) {
      rawEpicList.remove("27");
      rawEpicList.add("27");
    }

    if (rawEpicList.contains("28")) {
      rawEpicList.remove("28");
      rawEpicList.add("28");
    }

    if (rawEpicList.contains("29")) {
      rawEpicList.remove("29");
      rawEpicList.add("29");
    }

    if (rawEpicList.contains("30")) {
      rawEpicList.remove("30");
      rawEpicList.add("30");
    }

    if (rawEpicList.contains("31")) {
      rawEpicList.remove("31");
      rawEpicList.add("31");
    }

    if (rawEpicList.contains("32")) {
      rawEpicList.remove("32");
      rawEpicList.add("32");
    }

    if (rawEpicList.contains("33")) {
      rawEpicList.remove("33");
      rawEpicList.add("33");
    }

    if (rawEpicList.contains("34")) {
      rawEpicList.remove("34");
      rawEpicList.add("34");
    }

    if (rawEpicList.contains("35")) {
      rawEpicList.remove("35");
      rawEpicList.add("35");
    }

    if (rawEpicList.contains("36")) {
      rawEpicList.remove("36");
      rawEpicList.add("36");
    }
  }

  @GetMapping("/{accountId}/{dinozId}/dig")
  public DigResultDto getDiggingResult(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    DigResultDto result = new DigResultDto();
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent()
              && dinoz.get().getActionsMap().get(DinoparcConstants.FOUILLER) != null
              && dinoz.get().getActionsMap().get(DinoparcConstants.FOUILLER)) {

        return accountService.processDiggingForDinoz(desiredAccount.get(), dinoz.get());
      }
    }

    result.setDigCode(3);
    result.setDigObject(0);
    return result;
  }

  @GetMapping("/{accountId}/{dinozId}/fish")
  public Integer getFishingResult(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent()
              && dinoz.get().getActionsMap().get(DinoparcConstants.PÊCHE) != null
              && dinoz.get().getActionsMap().get(DinoparcConstants.PÊCHE)
              && DinoparcConstants.FISHING_LOCS.contains(dinoz.get().getPlaceNumber())) {

        return accountService.processFishingForDinoz(desiredAccount.get(), dinoz.get());
      }
    }
    return 0;
  }

  @GetMapping("/{accountId}/{dinozId}/pick")
  public Integer getPickingResult(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent()
              && dinoz.get().getActionsMap().get(DinoparcConstants.CUEILLETTE) != null
              && dinoz.get().getActionsMap().get(DinoparcConstants.CUEILLETTE)
              && DinoparcConstants.PICKING_LOCS.contains(dinoz.get().getPlaceNumber())) {

        return accountService.processPickingForDinoz(desiredAccount.get(), dinoz.get());
      }
    }
    return 0;
  }

  @PostMapping("/{accountId}/{dinozId}/{requestPlaceNumber}")
  public Dinoz moveDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId, @PathVariable String requestPlaceNumber) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Map<Integer, String> availableLocations = getAvailableLocations(token, desiredAccount.get().getId(), dinozId);
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);

      if (availableLocations != null
          && dinoz.isPresent()
          && dinoz.get().getActionsMap().get(DinoparcConstants.DÉPLACERVERT) != null
          && dinoz.get().getActionsMap().get(DinoparcConstants.DÉPLACERVERT)
          && !bossService.isInArmy(dinoz.get())) {

        return accountService.moveDinozToRequestedLocation(dinoz.get(), availableLocations, requestPlaceNumber);
      }
    }

    return null;
  }

  @GetMapping("/{accountId}/{dinozId}/specialMove/{specialMove}")
  public Boolean trySpecialMoveWithDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId,
      @PathVariable String dinozId, @PathVariable String specialMove) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent()
              && !bossService.isInArmy(dinoz.get())) {
        return accountService.trySpecialMoveWithDinoz(dinoz.get(), specialMove);
      }
    }

    return false;
  }

  @GetMapping("/{accountId}/{dinozId}/availableLocations")
  public Map<Integer, String> getAvailableLocations(@RequestHeader("Authorization") String token, @PathVariable String accountId,
      @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Map<Integer, String> locations = accountService.getAvailableLocations(desiredAccount.get(), dinozId);
      return locations;
    }

    return null;
  }

  @GetMapping("/{accountId}/inventory")
  public InventoryDto getInventory(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.getInventory(desiredAccount.get());
    }

    return null;
  }

  @GetMapping("/{accountId}/inventory/irma")
  public Integer getInventoryIrmaQty(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.getInventoryIrmaQty(desiredAccount.get());
    }

    return null;
  }

  @GetMapping("/{accountId}/{dinozId}/rock/{lang}")
  public RockDto getRockSummary(@RequestHeader("Authorization") String token,
    @PathVariable String accountId, @PathVariable String dinozId, @PathVariable String lang) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && dinoz.get().getRace().equalsIgnoreCase(DinoparcConstants.ROKKY)) {
        Dinoz rokky = dinoz.get();
        if (rokky.getActionsMap().get(DinoparcConstants.ROCK) == true) {
          return accountService.processRockAction(desiredAccount.get(), rokky, lang);
        }
      }
    }

    return null;
  }

  @GetMapping("/{accountId}/{dinozId}/availableEnnemies")
  public List<Dinoz> getAvailableEnnemies(HttpServletRequest request,
                                          @RequestHeader("Authorization") String token,
                                          @RequestHeader(value = "life", required = false) Integer life,
                                          @RequestHeader(value = "xp", required = false) Integer xp,
                                          @RequestHeader(value = "client", required = false) String client,
                                          @PathVariable String accountId,
                                          @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      if (desiredAccount.get().isHistorySkip()) {
        clientInformationService.manageClientInformationBis(request, accountId, dinozId, life, xp, client);
      }
      List<Dinoz> ennemies = accountService.getAvailableEnnemies(desiredAccount.get(), dinozId, life, xp);
      return ennemies;
    }

    return null;
  }

  @GetMapping("/{accountId}/{dinozId}/availableEnnemiesFromTelescope/{location}")
  public ResponseEntity availableEnnemiesFromTelescope(@RequestHeader("Authorization") String token,
                                          @PathVariable String accountId,
                                          @PathVariable String dinozId,
                                          @PathVariable Integer location) {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    Optional<Dinoz> presentDinozAtBordeciel = dinozRepository.findById(dinozId);

    try {
      if (isValidAndNotBlockedAccount(desiredAccount) && presentDinozAtBordeciel.isPresent() && presentDinozAtBordeciel.get().getPlaceNumber() == 18) {
        List<Dinoz> ennemies = accountService.availableEnnemiesFromTelescope(desiredAccount.get(), location, presentDinozAtBordeciel.get());
        return ResponseEntity.status(HttpStatus.OK).body(ennemies);
      }
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getStackTrace());
    }

    return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Not Authenticated");
  }

  @GetMapping("/{accountId}/{dinozId}/lvlup")
  public Map<String, Integer> getPossibleNewSkills(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    if (desiredAccount.isPresent()) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);

      if (isValidAndNotBlockedAccount(desiredAccount)) {
        return accountService.getAvailableLearnings(desiredDinoz.get());
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/{dinozId}/lvlup/{skill}")
  public void levelUpSkill(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String dinozId,
          @PathVariable String skill) {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);

      if (desiredDinoz.isPresent()) {
    	Dinoz dinoz = desiredDinoz.get();
        if (dinoz.getExperience() >= 100) {
			accountService.levelUp(desiredDinoz.get(), skill, false);
            int playerNbDinoz = playerRepository.countDinoz(accountId);
            playerRepository.updatePoints(desiredAccount.get().getId(), desiredAccount.get().getNbPoints() + 1, (desiredAccount.get().getNbPoints() + 1) / playerNbDinoz);
		}
      }
    }
  }

  @PutMapping("/{accountId}/{dinozId}/learn/{skill}")
  public void learnNewSkill(@RequestHeader("Authorization") String token,
      @PathVariable String accountId, @PathVariable String dinozId, @PathVariable String skill) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);

      if (desiredDinoz.isPresent()) {
      	Dinoz dinoz = desiredDinoz.get();
        if (dinoz.getExperience() >= 100) {
			accountService.learnNewSkill(desiredDinoz.get(), skill, false);
            int playerNbDinoz = playerRepository.countDinoz(accountId);
            playerRepository.updatePoints(desiredAccount.get().getId(), desiredAccount.get().getNbPoints() + 1, (desiredAccount.get().getNbPoints() + 1) / playerNbDinoz);
		}
      }
    }
  }

  @PutMapping("/{accountId}/firebath/{dinozId}")
  public Integer applyFireBath(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (desiredDinoz.isPresent()) {
        Dinoz dinoz = desiredDinoz.get();
        if (dinoz.getActionsMap().get(DinoparcConstants.BAIN_FLAMMES) != null
                && dinoz.getActionsMap().get(DinoparcConstants.BAIN_FLAMMES)
                && dinoz.getSkillsMap().get(DinoparcConstants.BAIN_FLAMMES) != null
                && dinoz.getSkillsMap().get(DinoparcConstants.BAIN_FLAMMES) >= 1
                && dinoz.getLife() > 0) {
          return accountService.applyFireBathToDinoz(dinoz);
        }
      }
    }
    return 0;
  }

  @PutMapping("/{accountId}/start-granit-mission")
  public void startGranitMission(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      accountService.startGranitMission(desiredAccount.get());
    }
  }

  @PutMapping("/{accountId}/granit-mission-claim")
  public void claimGranitMission(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      accountService.claimGranitMission(desiredAccount.get());
    }
  }

  @GetMapping("/{accountId}/{homeDinozId}/{foreignDinozId}")
  public FightSummaryDto applyFightAndGetSummary (
      @RequestHeader("Authorization") String token,
      @PathVariable String accountId,
      @PathVariable String homeDinozId,
      @PathVariable String foreignDinozId) {

    Optional<Player> optDesiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(optDesiredAccount.get(), token, null);

    // Account must be valid and not banned
    if (!isValidAndNotBannedAccount(optDesiredAccount)) {
      return null;
    }

    // Home dinoz must exist
    var desiredAccount = optDesiredAccount.get();
    Optional<Dinoz> homeDinoz = accountService.refreshStateAndGetDinoz(desiredAccount, homeDinozId);
    if (homeDinoz.isEmpty()) {
      return null;
    }

    // Home dinoz must not be in any army
    if (bossService.isInArmy(homeDinoz.get())) {
      return null;
    }

    // Fight raid boss
    if ("admin-b-raid".equals(foreignDinozId)) {
      // Try to fight raid boss
      EventDinozDto raidBoss = raidBossRepository.getRaidBoss();
      if (accountService.raidBossIsVisible(optDesiredAccount.get(), homeDinoz.get(), raidBoss)) {
        return eventFightService.fightBoss(homeDinoz.get(), raidBoss, false);
      }
      return null;
    }

    // Fight wild wistiti
    if ("admin-w-wild".equals(foreignDinozId)) {
      // Try to fight wild wistiti
      EventDinozDto wildWistiti = wildWistitiRepository.getWildWistiti();
      if (accountService.wildWistitiIsVisible(desiredAccount, wildWistiti, true)) {
        return eventFightService.fightWildWistiti(homeDinoz.get(), wildWistiti, desiredAccount.getHunterGroup());
      }
      return null;
    }

    // Must not be any boss (excepted raid boss declared above)
    if (foreignDinozId.startsWith("admin-b-")) {
      return null;
    }

    // Fight another dinoz
    Dinoz foreignDinoz = accountService.getEnnemyDinozByIds(foreignDinozId, homeDinoz.get());

    // Foreign dinoz must exist
    // Home dinoz must be alive with fight in actionsMap
    // Home dinoz must be in same place than foreign dinoz
    // Home dinoz and foreign dinoz must not have same master
    // Foreign dinoz must be alive
    // Foreign dinoz must have danger > 0
    if (foreignDinoz == null
            || homeDinoz.get().getActionsMap().get(DinoparcConstants.COMBAT) == null
            || !homeDinoz.get().getActionsMap().get(DinoparcConstants.COMBAT)
            || homeDinoz.get().getLife() <= 0
            || homeDinoz.get().getPlaceNumber() != foreignDinoz.getPlaceNumber()
            || homeDinoz.get().getMasterId().equalsIgnoreCase(foreignDinoz.getMasterId())
            || foreignDinoz.getLife() <= 0
            || (foreignDinoz.getDanger() <= 0 && Objects.nonNull(foreignDinoz.getId()))) {
      return null;
    }

    return fightService.fightDinoz(homeDinoz.get(), foreignDinoz, false);
  }

  @GetMapping("/{accountId}/{homeDinozId}/fightInArena")
  public FightSummaryDto applyFightInArenaAndGetSummary(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String homeDinozId) {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> homeDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), homeDinozId);
      if (homeDinoz.isPresent() && homeDinoz.get().getActionsMap().get(DinoparcConstants.COMBAT) && homeDinoz.get().getPlaceNumber() == 5 && homeDinoz.get().getLife() > 0) {
        Dinoz arenaBot = arenaService.rebuildArenaBot(desiredAccount.get(), homeDinoz.get());
        String actualFighterId = arenaService.getActualStage(desiredAccount.get(), homeDinoz.get()).getActualFighterId();

        if (actualFighterId != null && !actualFighterId.equalsIgnoreCase(homeDinoz.get().getId())) {
          return null;

        } else {
          FightSummaryDto fightSummaryDto = fightService.fightDinoz(homeDinoz.get(), arenaBot, false);
          desiredAccount = accountService.getAccountById(accountId);

          if (fightSummaryDto.isHaveWon()) {
            arenaService.setFighterForTheDay(desiredAccount.get(), homeDinoz.get().getId());
            arenaService.setIncrementWin(desiredAccount.get());

          } else {
            arenaService.resetWins(desiredAccount.get());
          }

          return fightSummaryDto;
        }
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/{homeDinozId}/acceptRewards")
  public void acceptHermitStageRewards(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String homeDinozId) {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> homeDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), homeDinozId);
      if (homeDinoz.isPresent() && homeDinoz.get().getLife() > 0 && homeDinoz.get().getPlaceNumber() == 5) {
        arenaService.validateRewards(desiredAccount.get(), homeDinoz.get());
      }
    }
  }

  @PutMapping("/{accountId}/{dinozId}/respawn")
  public void respawnDinoz(@RequestHeader("Authorization") String token,
                           @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);

      if (desiredDinoz.isPresent()
              && desiredDinoz.get().getLife() < 1
              && !bossService.isInArmy(desiredDinoz.get())) {
        accountService.respawnAtDinotown(desiredDinoz.get());
      }
    }
  }

  @PutMapping("/{accountId}/respawnAll")
  public void respawnAllDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
        accountService.respawnAllDinozAtDinotown(desiredAccount.get(), bossService.getPlayerArmyDinozs(accountId).stream().map(ArmyDinoz::getDinozId).toList());
    }
  }

  @PutMapping("/{accountId}/{dinozId}/catapult")
  public boolean catapultDinoz(@RequestHeader("Authorization") String token,
                               @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);

      if (desiredDinoz.isPresent()) {
        return accountService.catapultDinozToJazzIsland(desiredDinoz.get(), desiredAccount.get());
      }
    }

    return false;
  }

  @PutMapping("/{accountId}/{dinozId}/passagegranit")
  public boolean passageGranitDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);

      if (desiredDinoz.isPresent()) {
        return accountService.passTheGranitDoorDinoz(desiredDinoz.get(), desiredAccount.get());
      }
    }

    return false;
  }

  @PutMapping("/{accountId}/{dinozId}/passagebaleine")
  public boolean passageBaleineDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);

      if (desiredDinoz.isPresent()) {
        return accountService.passTheBaleineDinoz(desiredDinoz.get(), desiredAccount.get());
      }
    }

    return false;
  }

  @PutMapping("/{accountId}/{dinozId}/passagelabyrinthe")
  public boolean passageLabyrintheDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (desiredDinoz.isPresent()) {
        return accountService.passTheLabyrintheDinoz(desiredDinoz.get());
      }
    }
    return false;
  }

  @PutMapping("/{accountId}/{dinozId}/returnboat")
  public boolean takeReturnBoatToMainLand(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (desiredDinoz.isPresent()) {
        return accountService.takeReturnBoatToMainLand(desiredDinoz.get());
      }
    }
    return false;
  }

  @PutMapping("/{accountId}/{dinozId}/control")
  public boolean controlDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (desiredDinoz.isPresent() && desiredDinoz.get().getPlaceNumber() == 4) {
        return accountService.controlDinoz(desiredDinoz.get());
      }
    }

    return false;
  }

  @PutMapping("/{accountId}/irma/{dinozId}")
  public GiveObjectDto giveIrmaToDinoz(@RequestHeader("Authorization") String token,
                                       @PathVariable String accountId,
                                       @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      List<Integer> irmasQtys = accountService.getInventoryBothIrmasTypesQty(desiredAccount.get());
      if (desiredDinoz.isPresent() && (irmasQtys.get(0) > 0 || irmasQtys.get(1) > 0)) {
        return accountService.giveIrmaToDinoz(desiredDinoz.get(), desiredAccount.get(), irmasQtys);

      } else {
        GiveObjectDto noStock = new GiveObjectDto();
        noStock.setNoStockError(true);
        return noStock;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/ange/{dinozId}")
  public GiveObjectDto giveAngelPotionToDinoz(@RequestHeader("Authorization") String token,
    @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.POTION_ANGE);

      if (desiredDinoz.isPresent()
          && itemQty > 0
          && desiredDinoz.get().getLife() < 1) {
        return accountService.giveAngeToDinoz(desiredDinoz.get(), desiredAccount.get());

      } else if (desiredDinoz.isPresent()
          && itemQty == 0) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;

      } else if (desiredDinoz.isPresent() && desiredDinoz.get().getLife() > 0) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/nuage/{dinozId}/{quantity}")
  public GiveObjectDto giveBurgerToDinoz(final HttpServletRequest request,
      @RequestHeader("Authorization") String token, @PathVariable String accountId,
      @PathVariable String dinozId, @PathVariable Optional<Integer> quantity) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount) && validQty(request, accountId, dinozId, quantity)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.NUAGE_BURGER);

      if (desiredDinoz.isPresent()
          && itemQty >= 1
          && desiredDinoz.get().getLife() > 0) {
        return accountService.giveBurgerToDinoz(desiredDinoz.get(), desiredAccount.get(), 1);

      } else if (desiredDinoz.isPresent() && itemQty < 1) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;

      } else if (desiredDinoz.isPresent() && desiredDinoz.get().getLife() == 0) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/pain/{dinozId}")
  public GiveObjectDto givePainChaudToDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId,
      @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.PAIN_CHAUD);

      if (desiredDinoz.isPresent()
          && itemQty > 0
          && desiredDinoz.get().getLife() > 0) {
        return accountService.givePainChaudToDinoz(desiredDinoz.get(), desiredAccount.get());

      } else if (desiredDinoz.isPresent() && itemQty == 0) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;

      } else {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/tarte/{dinozId}")
  public GiveObjectDto giveTarteToDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.TARTE_VIANDE);

      if (desiredDinoz.isPresent()
          && itemQty >= 1
          && desiredDinoz.get().getLife() > 0) {
        return accountService.giveTarteToDinoz(desiredDinoz.get(), desiredAccount.get(), 1);

      } else if (desiredDinoz.isPresent() && itemQty == 0) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;

      } else if (desiredDinoz.isPresent() && desiredDinoz.get().getLife() == 0 || desiredDinoz.isPresent() && desiredDinoz.get().getLife() >= 100) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/medaille/{dinozId}")
  public GiveObjectDto giveMedalToDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.MEDAILLE_CHOCOLAT);

      if (desiredDinoz.isPresent()
              && itemQty >= 1
              && desiredDinoz.get().getExperience() < 100
              && !(desiredDinoz.get().getRace().equalsIgnoreCase(DinoparcConstants.OUISTITI) && desiredDinoz.get().getLevel() < 20)) {
        return accountService.giveMedalToDinoz(desiredDinoz.get(), desiredAccount.get(), 1);

      } else if (desiredDinoz.isPresent() && itemQty < 1) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;

      } else if (desiredDinoz.isPresent() && desiredDinoz.get().getExperience() >= 100) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;

      } else if (desiredDinoz.get().getRace().equalsIgnoreCase(DinoparcConstants.OUISTITI) && desiredDinoz.get().getLevel() < 20) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/lait-cargou/{dinozId}")
  public GiveObjectDto giveCargouMilkToDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.LAIT_DE_CARGOU);

      if (desiredDinoz.isPresent() && itemQty >= 1 && !desiredDinoz.get().getMalusList().contains(DinoparcConstants.LAIT_DE_CARGOU)) {
        return accountService.giveCargouMilkToDinoz(desiredDinoz.get(), desiredAccount.get());

      } else if (desiredDinoz.isPresent() && itemQty < 1) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;

      } else if (desiredDinoz.isPresent() && desiredDinoz.get().getMalusList().contains(DinoparcConstants.LAIT_DE_CARGOU)) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/fishing-rod/{dinozId}")
  public GiveObjectDto giveFishingRodToDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.CANNEAPECHE);

      if (desiredDinoz.isPresent()
              && itemQty >= 1
              && !desiredDinoz.get().getMalusList().contains(DinoparcConstants.CANNEAPECHE)
              && desiredDinoz.get().getSkillsMap().containsKey(DinoparcConstants.PÊCHE)
              && desiredDinoz.get().getSkillsMap().get(DinoparcConstants.PÊCHE) >= 1) {
        return accountService.giveFishingRodToDinoz(desiredDinoz.get(), desiredAccount.get());

      } else if (desiredDinoz.isPresent() && itemQty < 1) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;

      } else {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/darkpotion/{dinozId}/{quantity}")
  public GiveObjectDto giveDarkPotionToDinoz(final HttpServletRequest request,
                                             @RequestHeader("Authorization") String token, @PathVariable String accountId,
                                             @PathVariable String dinozId, @PathVariable Optional<Integer> quantity) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    Integer qty = getAndValidateQty(request, accountId, dinozId, quantity);

    if (isValidAndNotBlockedAccount(desiredAccount) && qty != null) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.POTION_SOMBRE);

      if (desiredDinoz.isPresent() && itemQty >= qty && !desiredDinoz.get().isDark()) {
        return accountService.giveDarkPotionToDinoz(desiredDinoz.get(), desiredAccount.get(), qty);

      } else if (desiredDinoz.isPresent() && itemQty == 0) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;

      } else if (desiredDinoz.get().isDark()) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/cherry/{dinozId}/{quantity}")
  public GiveObjectDto giveCherryLiquorToDinoz(
          final HttpServletRequest request,
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String dinozId,
          @PathVariable Optional<Integer> quantity) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    Integer qty = getAndValidateQty(request, accountId, dinozId, quantity);

    if (isValidAndNotBlockedAccount(desiredAccount) && qty != null) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.COULIS_CERISE);

      if (desiredDinoz.isPresent()
              && !desiredDinoz.get().getMalusList().contains(DinoparcConstants.COULIS_CERISE)
              && itemQty >= qty) {

        return accountService.giveCherryLiquorToDinoz(desiredDinoz.get(), desiredAccount.get(), qty);

      } else if (desiredDinoz.isPresent() && itemQty == 0) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;

      } else if (desiredDinoz.get().getMalusList().contains(DinoparcConstants.COULIS_CERISE)) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/lvlUpWithPill/{dinozId}/{chosenElement}/{pillQty}")
  public GiveObjectDto giveEternityPillToDinoz(
          final HttpServletRequest request,
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String dinozId,
          @PathVariable String chosenElement,
          @PathVariable Integer pillQty) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    Integer qty = getAndValidateQty(request, accountId, dinozId, Optional.of(pillQty));
    if (isValidAndNotBlockedAccount(desiredAccount) && Objects.nonNull(qty)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.ETERNITY_PILL);

      if (desiredDinoz.isPresent() && itemQty >= qty) {
        //Can't use Eternity Pills beyons lvl 9000. Pills will be discontinued anyways.
        if (desiredDinoz.get().getLevel() + qty > 9000) {
          GiveObjectDto error = new GiveObjectDto();
          error.setInutileError(true);
          return error;
        } else {
          return accountService.giveLvlUpPillToDinoz(desiredAccount.get(), desiredDinoz.get(), chosenElement, qty);
        }

      } else if (desiredDinoz.isPresent() && itemQty < qty) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;
      }
    }

    return null;
  }


  @PutMapping("/{accountId}/charme-feu/{dinozId}/{quantity}")
  public GiveObjectDto giveFireCharmToDinoz(final HttpServletRequest request,
       @RequestHeader("Authorization") String token, @PathVariable String accountId,
       @PathVariable String dinozId, @PathVariable Optional<Integer> quantity) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    Integer qty = getAndValidateQty(request, accountId, dinozId, quantity);

    if (isValidAndNotBlockedAccount(desiredAccount) && qty != null) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.CHARME_FEU);

      if (desiredDinoz.isPresent() && itemQty >= 1) {
        return accountService.giveFireCharmToDinoz(desiredDinoz.get(), desiredAccount.get(), 1);

      } else if (desiredDinoz.isPresent() && itemQty < 1) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/charme-terre/{dinozId}/{quantity}")
  public GiveObjectDto giveWoodCharmToDinoz(final HttpServletRequest request,
                                            @RequestHeader("Authorization") String token, @PathVariable String accountId,
                                            @PathVariable String dinozId, @PathVariable Optional<Integer> quantity) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    Integer qty = getAndValidateQty(request, accountId, dinozId, quantity);

    if (isValidAndNotBlockedAccount(desiredAccount) && qty != null) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.CHARME_TERRE);

      if (desiredDinoz.isPresent() && itemQty >= 1) {
        return accountService.giveWoodCharmToDinoz(desiredDinoz.get(), desiredAccount.get(), 1);

      } else if (desiredDinoz.isPresent() && itemQty < 1) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/charme-eau/{dinozId}/{quantity}")
  public GiveObjectDto giveWaterCharmToDinoz(final HttpServletRequest request,
                                             @RequestHeader("Authorization") String token, @PathVariable String accountId,
                                             @PathVariable String dinozId, @PathVariable Optional<Integer> quantity) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    Integer qty = getAndValidateQty(request, accountId, dinozId, quantity);

    if (isValidAndNotBlockedAccount(desiredAccount) && qty != null) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.CHARME_EAU);

      if (desiredDinoz.isPresent() && itemQty >= 1) {
        return accountService.giveWaterCharmToDinoz(desiredDinoz.get(), desiredAccount.get(), 1);

      } else if (desiredDinoz.isPresent() && itemQty < 1) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/charme-foudre/{dinozId}/{quantity}")
  public GiveObjectDto giveThunderCharmToDinoz(final HttpServletRequest request,
                                               @RequestHeader("Authorization") String token, @PathVariable String accountId,
                                               @PathVariable String dinozId, @PathVariable Optional<Integer> quantity) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    Integer qty = getAndValidateQty(request, accountId, dinozId, quantity);

    if (isValidAndNotBlockedAccount(desiredAccount) && qty != null) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.CHARME_FOUDRE);

      if (desiredDinoz.isPresent() && itemQty >= 1) {
        return accountService.giveThunderCharmToDinoz(desiredDinoz.get(), desiredAccount.get(), 1);

      } else if (desiredDinoz.isPresent() && itemQty < 1) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/charme-air/{dinozId}/{quantity}")
  public GiveObjectDto giveAirCharmToDinoz(final HttpServletRequest request,
                                           @RequestHeader("Authorization") String token, @PathVariable String accountId,
                                           @PathVariable String dinozId, @PathVariable Optional<Integer> quantity) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    Integer qty = getAndValidateQty(request, accountId, dinozId, quantity);

    if (isValidAndNotBlockedAccount(desiredAccount) && qty != null) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.CHARME_AIR);

      if (desiredDinoz.isPresent() && itemQty >= 1) {
        return accountService.giveAirCharmToDinoz(desiredDinoz.get(), desiredAccount.get(), 1);

      } else if (desiredDinoz.isPresent() && itemQty < 1) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/charme-prismatik/{dinozId}/{quantity}")
  public GiveObjectDto givePrismatikCharmToDinoz(
          final HttpServletRequest request,
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String dinozId,
          @PathVariable Optional<Integer> quantity) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    Integer qty = getAndValidateQty(request, accountId, dinozId, quantity);

    if (isValidAndNotBlockedAccount(desiredAccount) && qty != null) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.CHARME_PRISMATIK);

      if (desiredDinoz.isPresent() && itemQty >= 1) {
        return accountService.givePrismatikCharmToDinoz(desiredDinoz.get(), desiredAccount.get(), 1);

      } else if (desiredDinoz.isPresent() && itemQty < 1) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/griffes/{dinozId}")
  public GiveObjectDto giveGriffesEmpoisonneesToDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId,
      @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.GRIFFES_EMPOISONNÉES);

      if (desiredDinoz.isPresent() && itemQty > 0) {
        return accountService.giveGriffesToDinoz(desiredDinoz.get(), desiredAccount.get());

      } else if (desiredDinoz.isPresent() && itemQty == 0) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/bave/{dinozId}")
  public GiveObjectDto giveBaveLoupiToDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId,
      @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.BAVE_LOUPI);

      if (desiredDinoz.isPresent()
          && itemQty > 0) {
        return accountService.giveSpitToDinoz(desiredDinoz.get(), desiredAccount.get());

      } else if (desiredDinoz.isPresent()
          && itemQty == 0) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/ramens/{dinozId}/{quantity}")
  public GiveObjectDto giveRamensToDinoz(final HttpServletRequest request,
       @RequestHeader("Authorization") String token, @PathVariable String accountId,
       @PathVariable String dinozId, @PathVariable Optional<Integer> quantity) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    Integer qty = getAndValidateQty(request, accountId, dinozId, quantity);

    if (isValidAndNotBlockedAccount(desiredAccount) && qty != null) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.RAMENS);

      if (desiredDinoz.isPresent()
          && itemQty >= qty) {
        return accountService.giveRamensToDinoz(desiredDinoz.get(), desiredAccount.get(), qty);

      } else if (desiredDinoz.isPresent()
          && itemQty < qty) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/tea/{dinozId}/{quantity}")
  public GiveObjectDto giveHerbalTeaToDinoz(final HttpServletRequest request,
                                         @RequestHeader("Authorization") String token, @PathVariable String accountId,
                                         @PathVariable String dinozId, @PathVariable Optional<Integer> quantity) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    Integer qty = getAndValidateQty(request, accountId, dinozId, quantity);

    if (isValidAndNotBlockedAccount(desiredAccount) && qty != null) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.TISANE);

      if (desiredDinoz.isPresent()
              && itemQty >= qty
              && !(desiredDinoz.get().getRace().equalsIgnoreCase(DinoparcConstants.OUISTITI) && desiredDinoz.get().getLevel() < 20)) {
        return accountService.giveHerbalTeaToDinoz(desiredDinoz.get(), desiredAccount.get(), qty);

      } else if (desiredDinoz.isPresent() && itemQty < qty) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;

      } else if (desiredDinoz.get().getRace().equalsIgnoreCase(DinoparcConstants.OUISTITI) && desiredDinoz.get().getLevel() < 20) {
        GiveObjectDto error = new GiveObjectDto();
        error.setInutileError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/beer/{dinozId}/{quantity}")
  public GiveObjectDto giveDinojakBeerToDinoz(final HttpServletRequest request,
                                            @RequestHeader("Authorization") String token, @PathVariable String accountId,
                                            @PathVariable String dinozId, @PathVariable Optional<Integer> quantity) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    Integer qty = getAndValidateQty(request, accountId, dinozId, quantity);

    if (isValidAndNotBlockedAccount(desiredAccount) && qty != null) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.BIERE);

      if (desiredDinoz.isPresent() && itemQty >= qty) {
        return accountService.giveDinojakBeerToDinoz(desiredDinoz.get(), desiredAccount.get(), qty);

      } else if (desiredDinoz.isPresent() && itemQty < qty) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/eye/{dinozId}")
  public GiveObjectDto giveTigerEyesToDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId,
      @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.OEIL_DU_TIGRE);

      if (desiredDinoz.isPresent()
          && itemQty > 0) {
        return accountService.giveTigerEyesToDinoz(desiredDinoz.get(), desiredAccount.get());

      } else if (desiredDinoz.isPresent() && itemQty == 0) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/pill/{dinozId}")
  public GiveObjectDto giveChikabumPillToDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId,
      @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.ANTIDOTE);

      if (desiredDinoz.isPresent()
          && itemQty > 0) {
        return accountService.giveChikabumPillToDinoz(desiredDinoz.get(), desiredAccount.get());

      } else if (desiredDinoz.isPresent()
          && itemQty == 0) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/pruniac/{dinozId}/{newName}")
  public GiveObjectDto givePruniacToDinoz(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId, @PathVariable String newName) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      var itemQty = inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.PRUNIAC);

      if (desiredDinoz.isPresent() && itemQty > 0) {
        return accountService.givePruniacToDinoz(desiredDinoz.get(), desiredAccount.get(), newName);

      } else if (desiredDinoz.isPresent() && itemQty == 0) {
        GiveObjectDto error = new GiveObjectDto();
        error.setNoStockError(true);
        return error;
      }
    }

    return null;
  }

  @PutMapping("/{accountId}/{focusName}/{dinozId}/{quantity}")
  public GiveObjectDto giveFocusToDinoz(
      final HttpServletRequest request,
      @RequestHeader("Authorization") String token,
      @PathVariable String accountId,
      @PathVariable String focusName,
      @PathVariable String dinozId  ,
      @PathVariable Optional<Integer> quantity) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    Integer qty = getAndValidateQty(request, accountId, dinozId, quantity);

    if (isValidAndNotBlockedAccount(desiredAccount) && qty != null) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);

      if (desiredDinoz.isPresent() && focusName.equalsIgnoreCase("focus-aggro") && inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.FOCUS_AGGRESIVITE) >= qty) {
        return accountService.giveFocusAggroToDinoz(desiredDinoz.get(), desiredAccount.get(), qty);
      }

      if (desiredDinoz.isPresent() && focusName.equalsIgnoreCase("focus-nature") && inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.FOCUS_NATURE) >= qty) {
        return accountService.giveFocusNatureToDinoz(desiredDinoz.get(), desiredAccount.get(), qty);
      }

      if (desiredDinoz.isPresent() && focusName.equalsIgnoreCase("focus-water") && inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.FOCUS_SIRAINS) >= qty) {
        return accountService.giveFocusSirainsToDinoz(desiredDinoz.get(), desiredAccount.get(), qty);
      }

      if (desiredDinoz.isPresent() && focusName.equalsIgnoreCase("focus-mahamuti") && inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.FOCUS_MAHAMUTI) >= qty) {
        return accountService.giveFocusMahamutiToDinoz(desiredDinoz.get(), desiredAccount.get(), qty);
      }

      GiveObjectDto error = new GiveObjectDto();
      error.setNoStockError(true);
      return error;
    }

    return null;
  }

  @GetMapping("/{accountId}/{dinozId}/fusionable/init")
  public List<Dinoz> getFusionableDinozForOne(@RequestHeader("Authorization") String token,
    @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && dinoz.get().getPlaceNumber() == 20 && dinoz.get().getLife() > 0) {
        return accountService.fetchFusionableDinozForOne(desiredAccount.get(), dinoz.get());
      }
    }

    return new ArrayList<Dinoz>();
  }

  @GetMapping("/{accountId}/eggs")
  public List<Integer> getEggsCount(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      List<Integer> allEggs = new ArrayList<>();
      allEggs.add(inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.OEUF_GLUON));
      allEggs.add(inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.OEUF_SANTAZ));
      allEggs.add(inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.OEUF_SERPANTIN));
      allEggs.add(inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.OEUF_FEROSS));
      allEggs.add(inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.OEUF_COBALT));
      return allEggs;
    }
    return null;
  }

  @PostMapping("/{accountId}/hatch/{eggType}/{name}")
  public String hatchAnEgg(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String eggType,
          @PathVariable String name) {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount) && inventoryRepository.getQty(desiredAccount.get().getId(), eggType) > 0) {
        return accountService.hatchEgg(desiredAccount.get(), eggType, name);
    }
    return StringUtils.EMPTY;
  }

  @PutMapping("/{accountId}/chocolate-egg")
  public String spendOneChocolateEgg(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount) && inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.OEUF_CHOCOLAT) > 0) {
      return accountService.spendOneChocolateEgg(desiredAccount.get());
    }
    return StringUtils.EMPTY;
  }

  @PutMapping("/{accountId}/champifuz")
  public List<PlayerDinozStore> spendOneChampifuzTicket(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount) && inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.CHAMPIFUZ) > 0) {
      return accountService.spendOneChampifuzTicket(desiredAccount.get());
    }
    return Collections.emptyList();
  }

  @PutMapping("/{accountId}/champifuz-special/{chosenRace}")
  public List<PlayerDinozStore> spendOneSpecialChampifuzTicket(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String chosenRace) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount) && inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.SPECIAL_CHAMPIFUZ) > 0) {
      return accountService.spendOneSpecialChampifuzTicket(desiredAccount.get(), chosenRace);
    }
    return Collections.emptyList();
  }

  @PutMapping("/{accountId}/champifuz-monochrome")
  public List<PlayerDinozStore> spendOneChampifuzMonochromeTicket(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount) && inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.MONO_CHAMPIFUZ) > 0) {
      return accountService.spendOneChampifuzMonochromeTicket(desiredAccount.get());
    }
    return Collections.emptyList();
  }

  @PutMapping("/{accountId}/champifuz-magik")
  public List<PlayerDinozStore> spendOneMagikTicket(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount) && inventoryRepository.getQty(desiredAccount.get().getId(), DinoparcConstants.MAGIK_CHAMPIFUZ) > 0) {
      return accountService.spendOneMagikTicket(desiredAccount.get());
    }
    return Collections.emptyList();
  }

  @PutMapping("/{accountId}/{dinozId}/giveaway/jeanbambois")
  public Boolean giveawayDinozToJeanBambois(@RequestHeader("Authorization") String token,
    @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && dinoz.get().getPlaceNumber() == 9 && dinoz.get().getLife() > 0 && !bossService.isInArmy(dinoz.get())) {
        return accountService.sellDinozToJeanBambois(desiredAccount.get(), dinoz.get());
      }
    }
    return false;
  }

  @PutMapping("/{accountId}/{dinozId}/bath")
  public Integer takeBath(@RequestHeader("Authorization") String token,
                          @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && dinoz.get().getPlaceNumber() == 3 && dinoz.get().getLife() > 0 && dinoz.get().getLife() < 100) {
        return accountService.takeBath(desiredAccount.get(), dinoz.get());
      }
    }

    return -1;
  }

  @PutMapping("/{accountId}/{dinozId}/drink-fountain")
  public void drinkFromFountain(@RequestHeader("Authorization") String token,
                                @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && dinoz.get().getPlaceNumber() == 19 && dinoz.get().getLife() > 0) {
        accountService.drinkFromFountain(dinoz.get());
      }
    }
  }

  @PutMapping("/{accountId}/{dinozId}/giveaway/sacrifice")
  public SacrificeResultDto giveawayDinozForSacrifice(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent()
              && dinoz.get().getPlaceNumber() == 12
              && dinoz.get().getLife() > 0
              && dinoz.get().getLevel() >= 25
              && dinoz.get().getLevel() <= 117
              && accountService.racesValidesPourSacrifice.contains(dinoz.get().getRace())
              && inventoryRepository.getQty(dinoz.get().getMasterId(), DinoparcConstants.POTION_SOMBRE) == 0
              && !accountService.playerHasAtLeastOneActiveListingWithDarkPotion(accountId)
              && !bossService.isInArmy(dinoz.get())) {

        return accountService.sacrificeDinoz(desiredAccount.get(), dinoz.get());
      }
    }

    return null;
  }

  @GetMapping("/{accountId}/sacrifice-progress")
  public Double getSacrificeProgress(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.getSacrificeProgress(desiredAccount.get());
    }

    return null;
  }

  @GetMapping("/{accountId}/help-flag")
  public Boolean getHelpFlagValueForPlayer(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.getHelpFlagValueForPlayer(desiredAccount.get().getId());
    }
    return false;
  }

  @PutMapping("/{accountId}/help-flag/switch/{newValue}")
  public void switchHelpFlagValueForPlayer(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable Boolean newValue) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount)) {
      accountService.switchHelpFlagValueForPlayer(desiredAccount.get().getId(), newValue);
    }
  }

  @GetMapping("/{accountId}/fusion/{dinozIdOne}/{dinozIdTwo}/{fusionPrice}/{resultLevel}")
  public FusionSummaryDto makeFusionAndGetSummary(
      @RequestHeader("Authorization") String token,
      @PathVariable String accountId,
      @PathVariable String dinozIdOne,
      @PathVariable String dinozIdTwo,
      @PathVariable Integer fusionPrice,
      @PathVariable Integer resultLevel) {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinozOne = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozIdOne);
      Optional<Dinoz> dinozTwo = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozIdTwo);

      if (dinozOne.isPresent()
          && dinozTwo.isPresent()
          && dinozOne.get().getMasterId().equals(desiredAccount.get().getId())
          && dinozTwo.get().getMasterId().equals(desiredAccount.get().getId())
          && dinozOne.get().getRace().equals(dinozTwo.get().getRace())
          && fusionPrice > 1000
          && fusionPrice <= desiredAccount.get().getCash()
          && resultLevel > 1) {

        return fusionService.fusionTwoDinozTogether(dinozOne.get(), dinozTwo.get(), fusionPrice, resultLevel);
      }
    }

    return null;
  }

  @GetMapping("/{accountId}/fusion-simulation/{dinozIdOne}/{dinozIdTwo}")
  public List<String> makeFusionSimulationAndGetNineAppCodes(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String dinozIdOne,
          @PathVariable String dinozIdTwo) {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinozOne = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozIdOne);
      Optional<Dinoz> dinozTwo = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozIdTwo);

      if (dinozOne.isPresent() && dinozTwo.isPresent() && dinozOne.get().getRace().equals(dinozTwo.get().getRace()) && desiredAccount.get().getCash() >= 4900) {
        return fusionService.simulateTwoDinozTogether(dinozOne.get(), dinozTwo.get());
      }
    }

    return null;
  }

  @GetMapping("/{accountId}/import/check/{server}")
  public boolean hasAlreadyImported(@RequestHeader("Authorization") String token,
                                    @PathVariable String accountId, @PathVariable String server) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    return accountService.hasAlreadyImported(accountId, server);
  }

  @PostMapping("/{accountId}/import/{server}")
  public boolean tryToImportAccounts(@RequestHeader("Authorization") String token,
                                     @PathVariable String accountId,  @PathVariable String server) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount) && !accountService.hasAlreadyImported(accountId, server)) {
      return accountService.tryToImportAccount(desiredAccount.get(), server);
    }

    return false;
  }

  @PutMapping("/{accountId}/markMessages")
  public boolean markMessagesAsViewed(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.markMessagesAsViewed(desiredAccount.get());
    }
    return false;
  }

  @GetMapping("/{accountId}/blackList")
  public HashMap<String, String> getBlackList(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.getBlackList(desiredAccount.get());
    }

    return null;
  }

  @PostMapping("/{accountId}/blackList/{accountToAddId}")
  public boolean addToBlackList(@RequestHeader("Authorization") String token,
                                @PathVariable String accountId, @PathVariable String accountToAddId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.addToBlackList(desiredAccount.get(), accountToAddId);
    }

    return false;
  }

  @DeleteMapping("/{accountId}/blackList/{accountToRemoveId}")
  public boolean removeFromBlackList(@RequestHeader("Authorization") String token,
                                     @PathVariable String accountId, @PathVariable String accountToRemoveId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.removeFromBlackList(desiredAccount.get(), accountToRemoveId);
    }

    return false;
  }

  @PostMapping("/{accountId}/{dinozId}/merchant")
  public boolean sellToMerchant(@RequestHeader("Authorization") String token,
                                @PathVariable String accountId,
                                @PathVariable String dinozId,
                                @RequestBody HashMap<String, Integer> ingredientsMap) {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.sellToMerchant(desiredAccount.get(), dinozId, ingredientsMap);
    }
    return false;
  }

  @PostMapping("/{accountId}/{dinozId}/merchant/all")
  public boolean sellAllIngredientsToMerchant(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (isValidAndNotBlockedAccount(desiredAccount)) {
      return accountService.sellAllIngredientsToMerchant(desiredAccount.get(), dinozId);
    }
    return false;
  }

  @GetMapping("/{accountId}/arena/{dinozId}")
  public HermitStageDto getCurrentStage(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && dinoz.get().getPlaceNumber() == 5) {
        return arenaService.getActualStage(desiredAccount.get(), dinoz.get());
      }
    }

    return null;
  }

  @GetMapping("/{accountId}/{dinozId}/bazar/sellables")
  public Sellables getAccountSellables(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && dinoz.get().getPlaceNumber() == 15 && dinoz.get().getLife() > 0) {
        return accountService.getAllSellables(desiredAccount.get(), dinoz.get());
      }
    }

    return null;
  }

  @PostMapping("/{accountId}/{dinozId}/postUpForSale")
  public boolean postUpForSale(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String dinozId,
          @RequestBody BazarListing bazarListing) {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAndNotBannedBazarAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && dinoz.get().getPlaceNumber() == 15 && dinoz.get().getLife() > 0 && !bossService.isInArmy(dinoz.get())) {
          return bazarService.validateAndPublishBazarListing(desiredAccount.get(), dinoz.get(), bazarListing);
      }
    }

    return false;
  }

  @PutMapping("/{accountId}/bazar/bid/{listingId}/{nbBons}")
  public List<Boolean> bidOnBazarListing(
          @RequestHeader("Authorization") String token,
          @PathVariable String accountId,
          @PathVariable String listingId,
          @PathVariable Integer nbBons) {

    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAndNotBannedBazarAccount(desiredAccount)) {
      return bazarService.validateAndBidOnBazarListing(desiredAccount.get(), listingId, nbBons);
    }

    return new ArrayList<>();
  }

  @PostMapping("/{accountId}/{dinozId}/chaudron/postUpForCraft/{recipeNumber}")
  public ChaudronCraftTryResult postUpForCraft(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId, @PathVariable Integer recipeNumber) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && dinoz.get().getPlaceNumber() == 13 && dinoz.get().getLife() > 0) {
        return accountService.tryChaudronCraft(desiredAccount.get(), recipeNumber);
      }
    }
    return null;
  }

  @PostMapping("/{accountId}/{dinozId}/trade-ramen")
  public ChaudronCraftTryResult tradeRamenWithFisherman(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> dinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (dinoz.isPresent() && dinoz.get().getPlaceNumber() == 33 && dinoz.get().getLife() > 0) {
        return accountService.tradeRamenWithFisherman(desiredAccount.get(), dinoz.get());
      }
    }
    return null;
  }

  @GetMapping("/{accountId}/download")
  public ResponseEntity<Resource> getDinozDataAsCSV(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
    if (AccountController.isValidAndNotBlockedAccount(desiredAccount)) {
      String filename = "DinozDataExport.csv";
      InputStreamResource file = new InputStreamResource(accountService.downloadDinozData(accountId));

      return ResponseEntity.ok()
              .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
              .contentType(MediaType.parseMediaType("application/csv"))
              .body(file);
    }
    return null;
  }

  @PutMapping("/{accountId}/{dinozId}/enterDemon")
  public Boolean enterDemonHideout(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (desiredDinoz.isPresent()) {
        Dinoz dinoz = desiredDinoz.get();
        if (dinoz.getPlaceNumber() == 12 && dinoz.getLife() >= 1) {
            accountService.enterAtDemonsHideout(dinoz);
        }
      }
    }
    return true;
  }

  @PutMapping("/{accountId}/{dinozId}/givePruniacToDemons")
  public Integer givePruniacToDemons(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

    if (isValidAndNotBlockedAccount(desiredAccount)) {
      Optional<Dinoz> desiredDinoz = accountService.refreshStateAndGetDinoz(desiredAccount.get(), dinozId);
      if (desiredDinoz.isPresent()) {
        Dinoz dinoz = desiredDinoz.get();
        if (dinoz.getPlaceNumber() == 12 && dinoz.getLife() >= 1 && inventoryRepository.getQty(accountId, DinoparcConstants.PRUNIAC) > 0) {
          return accountService.exchangePruniacWithDemons(desiredAccount.get());
        }
      }
    }
    return 0;
  }

  @GetMapping("/{accountId}/pastillesvertes/quatrecentretrois")
  public Integer getNumberOfActivePlayers() {
    return accountService.getGeneralRankingsNumberOfActives();
  }

  @GetMapping("/{accountId}/event-stats/self")
  public HalloweenEventDto getPlayerEventStats(@RequestHeader("Authorization") String token, @PathVariable String accountId) {
    Optional<Player> desiredPlayer = accountService.getAccountById(accountId);
    this.tokenAccountValidation.validate(desiredPlayer.get(), token, null);
    HalloweenEventDto halloweenEventDto = new HalloweenEventDto();
    if (isValidAndNotBlockedAccount(desiredPlayer)) {
      halloweenEventDto.setEventCounter(accountService.getPlayerEventStats(desiredPlayer.get().getId()));
      halloweenEventDto.setEventPlayers(jooqPlayerStatRepository.getAllActiveHalloweenEventCounters());
    }

    return halloweenEventDto;
  }

//  @PutMapping("/{accountId}/{dinozId}/merchant/tradeCollection")
//  public TradeCollectionResponseDto tradeCollection(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String dinozId) {
//    Optional<Player> desiredAccount = accountService.getAccountById(accountId);
//    this.tokenAccountValidation.validate(desiredAccount.get(), token, null);
//    if (isValidAndNotBlockedAccount(desiredAccount)) {
//      return accountService.tradeCollection(desiredAccount.get(), dinozId);
//    }
//    return new TradeCollectionResponseDto(0, 0, false);
//  }

  public static boolean isValidAndNotBlockedAccount(Optional<Player> optAccount) {
    return optAccount.isPresent() && !optAccount.get().isBlocked();
  }

  public static boolean isValidAndNotBlockedAndNotBannedBazarAccount(Optional<Player> optAccount) {
    return optAccount.isPresent() && !optAccount.get().isBlocked() && !optAccount.get().isBannedBazar();
  }

  public static boolean isValidAndNotBannedAccount(Optional<Player> optAccount) {
    return isValidAndNotBlockedAccount(optAccount) && !optAccount.get().isBanned();
  }

  public void setTokenAccountValidation(TokenAccountValidation tokenAccountValidation) {
    this.tokenAccountValidation = tokenAccountValidation;
  }

  private Integer getAndValidateQty(HttpServletRequest request, String accountId, String dinozId, Optional<Integer> quantity) {
    if (quantity.isEmpty()) {
      return 1;
    }
    if (quantity.get() < 1) {
      String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
      ClientInformationTrace clientInformationTrace = new ClientInformationTrace();
      clientInformationTrace.setPlayerId(accountId);
      clientInformationTrace.setDinozId(dinozId);
      clientInformationTrace.setStatus(1);
      clientInformationTrace.setDetails(path);
      clientInformationTrace.setRetrievedDate(OffsetDateTime.now());
      this.clientInformationTraceRepository.addClientInformationTrace(clientInformationTrace);
      // TODO : return null soon
    }
    return quantity.get();
  }

  private boolean validQty(HttpServletRequest request, String accountId, String dinozId, Optional<Integer> quantity) {
    if (quantity.isPresent() && quantity.get() < 1) {
      String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
      ClientInformationTrace clientInformationTrace = new ClientInformationTrace();
      clientInformationTrace.setPlayerId(accountId);
      clientInformationTrace.setDinozId(dinozId);
      clientInformationTrace.setStatus(1);
      clientInformationTrace.setDetails(path);
      clientInformationTrace.setRetrievedDate(OffsetDateTime.now());
      this.clientInformationTraceRepository.addClientInformationTrace(clientInformationTrace);
      // TODO : return false soon
    }
    return true;
  }
}
