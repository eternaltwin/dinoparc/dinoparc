package com.dinoparc.api.controllers.dto;

public class CasinoTokenCountDto {

    private Integer bronze;
    private Integer silver;
    private Integer gold;

    public CasinoTokenCountDto() {

    }

    public CasinoTokenCountDto(Integer bronze, Integer silver, Integer gold) {
        this.bronze = bronze;
        this.silver = silver;
        this.gold = gold;
    }

    public Integer getBronze() {
        return bronze;
    }

    public void setBronze(Integer bronze) {
        this.bronze = bronze;
    }

    public Integer getSilver() {
        return silver;
    }

    public void setSilver(Integer silver) {
        this.silver = silver;
    }

    public Integer getGold() {
        return gold;
    }

    public void setGold(Integer gold) {
        this.gold = gold;
    }
}
