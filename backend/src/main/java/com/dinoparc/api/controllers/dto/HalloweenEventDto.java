package com.dinoparc.api.controllers.dto;

import java.util.ArrayList;
import java.util.List;

public class HalloweenEventDto {

    private Long eventCounter;
    private List<EventPlayerDto> eventPlayers;

    public HalloweenEventDto() {
        this.eventPlayers = new ArrayList<>();
    }

    public Long getEventCounter() {
        return eventCounter;
    }

    public void setEventCounter(Long eventCounter) {
        this.eventCounter = eventCounter;
    }

    public List<EventPlayerDto> getEventPlayers() {
        return eventPlayers;
    }

    public void setEventPlayers(List<EventPlayerDto> eventPlayers) {
        this.eventPlayers = eventPlayers;
    }
}
