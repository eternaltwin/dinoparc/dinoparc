package com.dinoparc.api.controllers.dto;

public class DinozId {

  private String dinozId;
  private String dinozName;
  private Integer dinozLife;

  public DinozId() {

  }

  public String getDinozId() {
    return dinozId;
  }

  public void setDinozId(String dinozId) {
    this.dinozId = dinozId;
  }

  public String getDinozName() {
    return dinozName;
  }

  public void setDinozName(String dinozName) {
    this.dinozName = dinozName;
  }

  public Integer getDinozLife() {
    return dinozLife;
  }

  public void setDinozLife(Integer dinozLife) {
    this.dinozLife = dinozLife;
  }

}

