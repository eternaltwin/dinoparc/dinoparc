package com.dinoparc.api.controllers.dto;

public class CreateClanRequest {

    private String accountId;
    private String clanName;
    private String clanDescription;
    private String pageId;
    private Boolean isPrivate;

    public CreateClanRequest() {

    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getClanName() {
        return clanName;
    }

    public void setClanName(String clanName) {
        this.clanName = clanName;
    }

    public String getClanDescription() {
        return clanDescription;
    }

    public void setClanDescription(String clanDescription) {
        this.clanDescription = clanDescription;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public Boolean getPrivate() {
        return isPrivate;
    }

    public void setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }
}
