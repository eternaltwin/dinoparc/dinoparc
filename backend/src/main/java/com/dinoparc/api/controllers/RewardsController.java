package com.dinoparc.api.controllers;

import com.dinoparc.api.domain.account.AccountUtils;
import com.dinoparc.api.domain.account.Collection;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.misc.TokenAccountValidation;
import com.dinoparc.api.domain.mission.PlayerMission;
import com.dinoparc.api.domain.mission.Reward;
import com.dinoparc.api.repository.*;
import com.dinoparc.api.services.AccountService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/rewards")
public class RewardsController {
    private final TokenAccountValidation tokenAccountValidation;

    private final AccountService accountService;
    private final PgRewardRepository rewardRepository;
    private final PgRoleActionRepository roleActionRepository;
    private final PgDinozRepository dinozRepository;
    private final PgPlayerMissionRepository playerMissionRepository;
    private final PgCollectionRepository collectionRepository;
    private final PgInventoryRepository inventoryRepository;
    private final PgPlayerRepository playerRepository;

    public RewardsController(TokenAccountValidation tokenAccountValidation, AccountService accountService,
                             PgRewardRepository rewardRepository, PgRoleActionRepository roleActionRepository,
                             PgDinozRepository dinozRepository, PgPlayerMissionRepository playerMissionRepository,
                             PgCollectionRepository collectionRepository, PgInventoryRepository inventoryRepository,
                             PgPlayerRepository playerRepository) {
        this.tokenAccountValidation = tokenAccountValidation;
        this.accountService = accountService;
        this.rewardRepository = rewardRepository;
        this.roleActionRepository = roleActionRepository;
        this.dinozRepository = dinozRepository;
        this.playerMissionRepository = playerMissionRepository;
        this.collectionRepository = collectionRepository;
        this.inventoryRepository = inventoryRepository;
        this.playerRepository = playerRepository;
    }

    // Role : ADMIN / GAME MASTER
    @PostMapping
    @Transactional
    public List<Reward> createRewards(@RequestHeader("Authorization") String token, @RequestBody List<Reward> rewardsToCreate) {
        var playerName = this.tokenAccountValidation.getPlayerIdFromToken(token);
        var endpoint = "POST /api/rewards";
        var content = String.valueOf(rewardsToCreate.size());

        if (!this.tokenAccountValidation.isAdminOrGameMasterUser(token)) {
            this.roleActionRepository.create(AccountUtils.getRoleActionHistory(playerName, endpoint, content, false, false));
            return Collections.emptyList();
        }

        var success = rewardsToCreate.isEmpty() || !rewardsToCreate.stream().anyMatch(reward -> {
            // Reward not valid if :
            // Id or description is null
            // Not any valid given element
            return reward.getId() == null
               || reward.getDescription() == null
               || (
                   (reward.getGoldAmount() == null || reward.getGoldAmount() < 0)
                   && (reward.getCollectionContent() == null || reward.getCollectionContent().isEmpty())
                   && (reward.getInventoryContent() == null || reward.getInventoryContent().isEmpty() && reward.getInventoryContent().values().stream().anyMatch(qty -> qty < 0))
           );
        });

        if (!success) {
            this.roleActionRepository.create(AccountUtils.getRoleActionHistory(playerName, endpoint, content, true, false));
            return Collections.emptyList();
        }

        try {
            rewardsToCreate.stream().forEach(this.rewardRepository::create);
            this.roleActionRepository.create(AccountUtils.getRoleActionHistory(playerName, endpoint, content, true, true));
            return rewardsToCreate;
        } catch (Exception e) {
            this.roleActionRepository.create(AccountUtils.getRoleActionHistory(playerName, endpoint, content, true, false));
            return Collections.emptyList();
        }
    }

    @PostMapping("/{accountId}/{dinozId}/{missionId}/submit-mission")
    public Reward submitCompletedMission(
            @RequestHeader("Authorization") String token,
            @PathVariable String accountId,
            @PathVariable String dinozId,
            @PathVariable String missionId) {

        if (checkPlayerDinoz(token, accountId, dinozId)) {
            var playerId = UUID.fromString(accountId);
            PlayerMission missionToValidate = playerMissionRepository.get(UUID.fromString(missionId));
            if (missionToValidate != null
                    && missionToValidate.getCounterCurrentValue() >= missionToValidate.getCounterTargetValue()
                    && !missionToValidate.isRewardClaimed()) {

                Reward reward = rewardRepository.get(missionToValidate.getRewardId());
                if (reward != null) {
                    if (reward.getCollectionContent() != null && !reward.getCollectionContent().isEmpty()) {
                        Collection playerCollection = collectionRepository.getPlayerCollection(playerId);
                        playerCollection.getCollection().addAll(reward.getCollectionContent());
                        collectionRepository.updateCollection(playerCollection.getId(), playerCollection.getCollection());
                    }

                    if (reward.getInventoryContent() != null && !reward.getInventoryContent().isEmpty()) {
                        for (Map.Entry<String, Integer> inventoryEntry : reward.getInventoryContent().entrySet()) {
                            inventoryRepository.addInventoryItem(playerId.toString(), inventoryEntry.getKey(), inventoryEntry.getValue());
                        }
                    }

                    if (reward.getGoldAmount() != null && reward.getGoldAmount() > 0) {
                        playerRepository.updateCash(playerId.toString(), reward.getGoldAmount());
                    }

                    playerMissionRepository.rewardClaimed(missionToValidate.getId());
                    reward.generateDisplayableRewardsMap();
                    return reward;
                }
            }
        }
        return null;
    }

    // Role : ALL
    @GetMapping("/{accountId}/{rewardId}")
    public Reward getRewardFromId(@RequestHeader("Authorization") String token, @PathVariable String accountId, @PathVariable String rewardId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent()) {
            this.tokenAccountValidation.validate(player.get(), token, null);

            if (AccountController.isValidAndNotBlockedAccount(player)) {
                return rewardRepository.get(UUID.fromString(rewardId));
            }
        }
        return null;
    }

    private boolean checkPlayerDinoz(String token, @PathVariable String accountId, @PathVariable String dinozId) {
        Optional<Player> player = accountService.getAccountById(accountId);
        if (player.isPresent()) {
            this.tokenAccountValidation.validate(player.get(), token, null);
            if (AccountController.isValidAndNotBlockedAccount(player)) {
                var dinoz = dinozRepository.findById(dinozId);

                // Check dinoz owned by player, dinoz is alive, and dinoz place number
                if (dinoz.isPresent()
                        && dinoz.get().getMasterId().equals(accountId)
                        && dinoz.get().getPlaceNumber() == 0
                        && dinoz.get().getLife() > 0) {
                    return true;
                }
            }
        }
        return false;
    }
}
