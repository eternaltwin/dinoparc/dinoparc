package com.dinoparc.api.controllers;

import com.dinoparc.api.controllers.dto.HOFDinozDto;
import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.History;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.misc.Pagination;
import com.dinoparc.api.domain.misc.TokenAccountValidation;
import com.dinoparc.api.repository.*;
import com.dinoparc.api.services.*;
import net.eternaltwin.client.HttpEtwinClient;
import net.eternaltwin.oauth.client.RfcOauthClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/history")
public class HistoryController {
    private final PgPlayerRepository playerRepository;
    private TokenAccountValidation tokenAccountValidation;
    private final PgHistoryRepository historyRepository;

    @Autowired
    public HistoryController(
            PgPlayerRepository playerRepository,
            TokenAccountValidation tokenAccountValidation,
            PgHistoryRepository historyRepository) {

        this.playerRepository = playerRepository;
        this.tokenAccountValidation = tokenAccountValidation;
        this.historyRepository = historyRepository;
    }

    @GetMapping("/{accountId}")
    public List<History> getAccountHistory(@RequestHeader("Authorization") String token,
                                           @PathVariable String accountId,
                                           @RequestParam(required = false) Integer offset,
                                           @RequestParam(required = false) Integer limit,
                                           HttpServletResponse response) {
        Optional<Player> desiredAccount = playerRepository.findById(accountId);
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        if (AccountController.isValidAndNotBlockedAccount(desiredAccount) && offset + limit <= DinoparcConstants.MAX_VISIBLE_HISTORY_SIZE) {
            Pagination<History> pagination = historyRepository.getPlayerHistories(desiredAccount.get().getId(), offset, limit);
            historyRepository.setPlayerSeen(desiredAccount.get().getId(), offset, limit);
            response.setHeader(Pagination.TOTAL_COUNT_HEADER, pagination.getTotalCount().toString());
            if (pagination.getOffset() != null) {
                response.setHeader(Pagination.OFFSET_HEADER, pagination.getOffset().toString());
                response.setHeader("Access-Control-Expose-Headers", Pagination.TOTAL_COUNT_HEADER + ", " + Pagination.OFFSET_HEADER);
            } else {
                response.setHeader("Access-Control-Expose-Headers", Pagination.TOTAL_COUNT_HEADER);
            }
            return pagination.getElements();
        }
        return new ArrayList<>();
    }
}
