package com.dinoparc.api.controllers.dto;

import java.util.HashMap;
import java.util.Map;

public class ShopDto {

  private Map<String, Double> pricesMap;
  private Map<String, Integer> quantityMap;
  private Integer accountCashAmount;

  public ShopDto() {
    this.pricesMap = new HashMap<String, Double>();
    this.quantityMap = new HashMap<String, Integer>();
    this.accountCashAmount = 0;
  }

  public Map<String, Double> getPricesMap() {
    return pricesMap;
  }

  public void setPricesMap(Map<String, Double> pricesMap) {
    this.pricesMap = pricesMap;
  }

  public Integer getAccountCashAmount() {
    return accountCashAmount;
  }

  public void setAccountCashAmount(Integer accountCashAmount) {
    this.accountCashAmount = accountCashAmount;
  }

  public Map<String, Integer> getQuantityMap() {
    return quantityMap;
  }

  public void setQuantityMap(Map<String, Integer> quantityMap) {
    this.quantityMap = quantityMap;
  }

}
