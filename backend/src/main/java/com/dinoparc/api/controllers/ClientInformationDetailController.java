package com.dinoparc.api.controllers;

import com.dinoparc.api.controllers.dto.ClientInformationDetailRequestDto;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.dinoz.Dinoz;
import com.dinoparc.api.domain.misc.TokenAccountValidation;
import com.dinoparc.api.services.AccountService;
import com.dinoparc.api.services.ClientInformationService;
import org.springframework.web.bind.annotation.*;

import java.time.OffsetDateTime;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/client-information-details")
public class ClientInformationDetailController {
    private final static String REQUEST_DETAIL_TYPE_FOREIGN = "1";
    private final static String DETAIL_TYPE_FOREIGN = "FOREIGN";

    private final AccountService accountService;
    private final ClientInformationService clientInformationService;
    private final TokenAccountValidation tokenAccountValidation;

    public ClientInformationDetailController(AccountService accountService, ClientInformationService clientInformationService, TokenAccountValidation tokenAccountValidation) {
        this.accountService = accountService;
        this.clientInformationService = clientInformationService;
        this.tokenAccountValidation = tokenAccountValidation;
    }

    @PostMapping("/{informationType}")
    public void addClientInformationDetail(@RequestHeader("Authorization") String token, @PathVariable String informationType, @RequestBody ClientInformationDetailRequestDto clientInformationDetailRequest) {
        Optional<Player> desiredAccount = accountService.getAccountById(clientInformationDetailRequest.getAccountId());
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        switch (informationType) {
            case REQUEST_DETAIL_TYPE_FOREIGN:
                clientInformationService.addClientInformationDetail(clientInformationDetailRequest, DETAIL_TYPE_FOREIGN, OffsetDateTime.now());
                break;
            default:
                break;
        }
    }
}
