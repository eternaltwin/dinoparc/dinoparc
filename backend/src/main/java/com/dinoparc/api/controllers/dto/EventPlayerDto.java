package com.dinoparc.api.controllers.dto;

public class EventPlayerDto implements Comparable<EventPlayerDto> {

    private String accountId;
    private String accountName;
    private Long objectsRetrievedInEvent;

    public EventPlayerDto(String accountId, String accountName, Long objectsRetrievedInEvent) {
        this.accountId = accountId;
        this.accountName = accountName;
        this.objectsRetrievedInEvent = objectsRetrievedInEvent;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Long getObjectsRetrievedInEvent() {
        return objectsRetrievedInEvent;
    }

    public void setObjectsRetrievedInEvent(Long objectsRetrievedInEvent) {
        this.objectsRetrievedInEvent = objectsRetrievedInEvent;
    }

    @Override
    public int compareTo(EventPlayerDto other) {
        if (this.getObjectsRetrievedInEvent() < other.getObjectsRetrievedInEvent()) {
            return 1;
        } else if (this.getObjectsRetrievedInEvent() == other.getObjectsRetrievedInEvent()) {
            return 0;
        } else {
            return -1;
        }
    }
}
