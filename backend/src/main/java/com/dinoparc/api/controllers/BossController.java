package com.dinoparc.api.controllers;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.bazar.BazarListing;
import com.dinoparc.api.domain.dinoz.*;
import com.dinoparc.api.domain.misc.TokenAccountValidation;
import com.dinoparc.api.repository.PgArmyDinozRepository;
import com.dinoparc.api.repository.PgBossRepository;
import com.dinoparc.api.services.AccountService;
import com.dinoparc.api.services.BossService;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/boss")
public class BossController {
    private final BossService bossService;
    private final AccountService accountService;

    private final PgArmyDinozRepository armyDinozRepository;

    private TokenAccountValidation tokenAccountValidation;

    public BossController(BossService bossService, AccountService accountService,  PgArmyDinozRepository armyDinozRepository, TokenAccountValidation tokenAccountValidation) {
        this.bossService = bossService;
        this.accountService = accountService;
        this.armyDinozRepository = armyDinozRepository;
        this.tokenAccountValidation = tokenAccountValidation;
    }

    @GetMapping("/armyDinoz/{bossId}/{dinozId}/overview")
    public ArmyDinozOverview getArmyDinozOverview(
            @RequestHeader("Authorization") String token,
            @PathVariable String bossId,
            @PathVariable String dinozId) {

        Dinoz dinoz = accountService.getDinozById(dinozId);

        Optional<Player> desiredAccount = accountService.getAccountById(dinoz.getMasterId().toString());
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        var isRaidBossCall = DinoparcConstants.RAID_BOSS_ID.equals(UUID.fromString(bossId));
        var optBoss = bossService.getBossById(UUID.fromString(bossId));

        if (optBoss.isEmpty()) {
            return null;
        }

        var isAllowedCall = !isRaidBossCall || (
                isRaidBossCall
                && !desiredAccount.get().isBannedRaid()
                && dinoz.getPlaceNumber() == optBoss.get().getPlaceNumber());

        if (AccountController.isValidAndNotBlockedAccount(desiredAccount) && isAllowedCall) {
            return bossService.getOverview(bossId).orElse(null);
        }

        return null;
    }

    @GetMapping("/armyDinoz/{bossId}/{dinozId}")
    public ArmyDinoz getArmyDinoz(
            @RequestHeader("Authorization") String token,
            @PathVariable String bossId,
            @PathVariable String dinozId) {

        Dinoz dinoz = accountService.getDinozById(dinozId);

        Optional<Player> desiredAccount = accountService.getAccountById(dinoz.getMasterId().toString());
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        var isRaidBossCall = DinoparcConstants.RAID_BOSS_ID.equals(UUID.fromString(bossId));
        var isAllowedCall = !isRaidBossCall || (isRaidBossCall && !desiredAccount.get().isBannedRaid() && dinoz.getPlaceNumber() == 37);

        if (AccountController.isValidAndNotBlockedAccount(desiredAccount) && isAllowedCall) {
            return bossService.getArmyDinoz(bossId, dinozId).orElse(null);
        }

        return null;
    }

    @PostMapping("/armyDinoz")
    public ArmyDinoz addArmyDinoz(
            @RequestHeader("Authorization") String token,
            @RequestBody ArmyDinozInputDTO armyDinozInputDTO) {

        Optional<Player> desiredAccount = accountService.getAccountById(armyDinozInputDTO.getMasterId().toString());
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        if (AccountController.isValidAndNotBlockedAccount(desiredAccount) && !desiredAccount.get().isBannedRaid()) {
            return bossService.addArmyDinoz(desiredAccount.get(), armyDinozInputDTO);
        }

        return null;
    }

    @DeleteMapping("/armyDinoz/{armyDinozId}")
    public boolean deleteArmyDinoz(
            @RequestHeader("Authorization") String token,
            @PathVariable String armyDinozId) {

        Optional<ArmyDinoz> optArmyDinoz = armyDinozRepository.findById(UUID.fromString(armyDinozId));

        if (optArmyDinoz.isEmpty()) {
            return false;
        }

        Optional<Player> desiredAccount = accountService.getAccountById(optArmyDinoz.get().getMasterId().toString());
        this.tokenAccountValidation.validate(desiredAccount.get(), token, null);

        if (AccountController.isValidAndNotBlockedAccount(desiredAccount) && !desiredAccount.get().isBannedRaid()) {
            return bossService.deleteArmyDinozAndUpdateRanks(optArmyDinoz.get());
        }

        return false;
    }
}
