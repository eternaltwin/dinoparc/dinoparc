package com.dinoparc.api.controllers.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class RankingClanDto {

    private String clanId;
    private String clanName;
    private String creatorName;
    private String creationDate;
    private String faction;

    private Integer nbPlayers;
    private Integer position;
    private Integer nbDinoz;
    private Integer nbDinozWarrior;
    private Integer pointsTotal;

    public RankingClanDto() {

    }

    public String getClanId() {
        return clanId;
    }

    public void setClanId(String clanId) {
        this.clanId = clanId;
    }

    public String getClanName() {
        return clanName;
    }

    public void setClanName(String clanName) {
        this.clanName = clanName;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getNbPlayers() {
        return nbPlayers;
    }

    public void setNbPlayers(Integer nbPlayers) {
        this.nbPlayers = nbPlayers;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getNbDinoz() {
        return nbDinoz;
    }

    public void setNbDinoz(Integer nbDinoz) {
        this.nbDinoz = nbDinoz;
    }

    public Integer getNbDinozWarrior() {
        return nbDinozWarrior;
    }

    public void setNbDinozWarrior(Integer nbDinozWarrior) {
        this.nbDinozWarrior = nbDinozWarrior;
    }

    public Integer getPointsTotal() {
        return pointsTotal;
    }

    public void setPointsTotal(Integer pointsTotal) {
        this.pointsTotal = pointsTotal;
    }

    public String getFaction() {
        return faction;
    }

    public void setFaction(String faction) {
        this.faction = faction;
    }

    @JsonIgnore
    public long getCreationDateAsLongEpoch() {
        return LocalDate.parse(this.getCreationDate(), DateTimeFormatter.ofPattern("yyyy/MM/dd")).toEpochDay();
    }
}
