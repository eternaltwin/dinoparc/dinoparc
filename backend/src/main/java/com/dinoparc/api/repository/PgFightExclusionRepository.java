package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.FightExclusion;

import java.util.Optional;
import java.util.UUID;

public interface PgFightExclusionRepository {
    Optional<FightExclusion> getFightExclusion(UUID fromPlayerId, UUID toPlayerId);
    void createFightExclusion(FightExclusion fightExclusion);
    void deleteFightExclusion(UUID fightExclusionId);
}
