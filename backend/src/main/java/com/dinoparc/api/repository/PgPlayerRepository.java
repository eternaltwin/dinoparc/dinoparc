package com.dinoparc.api.repository;

import com.dinoparc.api.controllers.dto.PlayerClanInfoDto;
import com.dinoparc.api.domain.account.Player;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public interface PgPlayerRepository {
    Optional<Player> findById(String userId);
    Player findPlayerByDinozId(String dinozId);
    Player findByName(String tokenName);
    List<Player> findAll();
    Integer findPlayerDinozRank(String id, String dinozId);
    List<Player> getTopPlayers(int i);
    void create(Player newAccount);
    void updateHunterGroup(String id, int hunterGroup);
    void deleteById(String accountId);
    void dailyReset(Player player);
    void updateCash(String accountId, int valueToAdd);
    void updateCashAll(Integer valueToAdd);
    void updatePoints(String id, int nbPts, int average);
    void addDinoz(String accountId, String dinozId);
    void addAllDinoz(String accountId, List<String> dinozIds);
    void deleteDinoz(String accountId, String dinozId);
    void updateSacrificePoints(String accountId, Integer sacrificePoints);
    void moveDinoz(String accountId, String dinozId, boolean previous, int count);
    void updateMessages(String accountId, boolean newVal);
    void updateBlackList(String accountId, HashMap<String, String> newBlackList);
    void updateHermitStageFightingDinoz(String accountId, String dinozId);
    void updateHermitStage(String accountId, int newHermitStage);
    void updateHermitStageCurrentWins(String accountId, int newHermitStageCurrentWins);
    void updateRole(String accountId, String superadminRole);
    void setWistitiCaptured(String accountId, Integer qty);
    void save(Player player);
    void updateWistitiCaptured(String id, int valueToSet);
    void updateDinozLimit(String id, int valueToAdd);
    Integer countDinoz(String id);
    void updateDailyShiniesFought(String id, int valueToAdd);
    void updateUnlockedDinoz(String id, List<Integer> unlockedDinoz);
    PlayerClanInfoDto checkIfPlayerIsInAClan(String playerId);
    void updateHistorySkip(String accountId, Boolean historySkip);
    void updateBanInfos(String accountId, boolean banned, boolean blocked, boolean bannedPvp, boolean bannedRaid, boolean bannedShiny, boolean bannedWistiti, boolean bannedBazar);
    void updateCashAndWistitiFight(String accountId, Integer moneyWon);
    void updateLastGeneratedStore(String accountId, OffsetDateTime now);
    Player updateImgMode(String playerId, String imgMode);
    Player updateActionMode(String playerId, String actionMode);
    void updateHelpToggleFlag(String playerId, Boolean helpToggleFlagValue);
    void updateHasClaimedTradeFromFishermanToday(String playerId, Boolean hasClaimedTradeFromFishermanToday);
    Boolean getPlayerHelpToggleValue(String playerId);
    void updateLastPirateBuy(String playerId, String lastPirateBuy);
}
