package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.bazar.BazarListing;
import com.dinoparc.api.domain.misc.Pagination;
import com.dinoparc.tables.records.BazarRecord;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jooq.*;
import org.jooq.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.dinoparc.tables.Bazar.BAZAR;
import static com.dinoparc.tables.Dinoz.DINOZ;

@Repository
public class JooqBazarRepository implements PgBazarRepository {

    private final static TableOnConditionStep<Record> BAZAR_LJ_DINOZ = BAZAR.leftJoin(DINOZ).on(BAZAR.DINOZ_ID.eq(DINOZ.ID));
    private DSLContext jooqContext;
    private ObjectMapper objectMapper;

    @Autowired
    public JooqBazarRepository(DSLContext jooqContext, ObjectMapper objectMapper) {
        this.jooqContext = jooqContext;
        this.objectMapper = objectMapper;
    }

    @Transactional
    @Override
    public Optional<BazarListing> update(BazarListing todoEntry) {
        var objects = todoEntry.getObjects() == null ? Collections.emptyMap() : todoEntry.getObjects();
        JSONB listingObjects = null;
        try {
            listingObjects = JSONB.valueOf(objectMapper.writeValueAsString(objects));
        } catch(Exception e) {
            listingObjects = JSONB.valueOf("{}");
        }
        jooqContext.update(BAZAR)
                .set(BAZAR.ID, UUID.fromString(todoEntry.getId()))
                .set(BAZAR.ACTIVE, todoEntry.isActive())
                .set(BAZAR.END_DATE_IN_EPOCH_SECONDS, todoEntry.getEndDateInEpochSeconds())
                .set(BAZAR.LAST_BIDER_ID, todoEntry.getLastBiderId())
                .set(BAZAR.DINOZ_ID, todoEntry.getDinozId())
                .set(BAZAR.INITIAL_DURATION_IN_DAYS, todoEntry.getInitialDurationInDays())
                .set(BAZAR.LAST_BIDER_NAME, todoEntry.getLastBiderName())
                .set(BAZAR.LAST_PRICE, todoEntry.getLastPrice())
                .set(BAZAR.OBJECTS, listingObjects)
                .set(BAZAR.SELLER_ID, todoEntry.getSellerId())
                .set(BAZAR.SELLER_NAME, todoEntry.getSellerName())
                .where(BAZAR.ID.eq(UUID.fromString(todoEntry.getId())))
                .execute();

        return findById(todoEntry.getId());
    }

    @Override
    public void save(BazarListing bazarListing) {
        jooqContext.insertInto(BAZAR).set(toRecord(bazarListing)).execute();
    }

    @Override
    public Pagination<BazarListing> list(String accountId, Integer limit, Integer offset, String sort, boolean active) {
        Pagination<BazarListing> ret = new Pagination<>();

        Condition whereCondition = BAZAR.ACTIVE.eq(active);
        var orderByClause = active ? BAZAR.END_DATE_IN_EPOCH_SECONDS : BAZAR.END_DATE_IN_EPOCH_SECONDS.desc();

        switch (sort) {
            case "opt_oo":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNull().or(BAZAR.DINOZ_ID.eq("")));
                break;

            case "opt_50":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.LEVEL.greaterOrEqual(50)));
                break;

            case "opt_100":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.LEVEL.greaterOrEqual(100)));
                break;

            case "opt_dark":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.endsWith("#")));
                break;

            case "opt_cherry":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.endsWith("$")));
                break;

            case "opt_0":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("0")));
                break;

            case "opt_1":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("1")));
                break;

            case "opt_2":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("2")));
                break;

            case "opt_3":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("3")));
                break;

            case "opt_4":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("4")));
                break;

            case "opt_5":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("5")));
                break;

            case "opt_6":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("6")));
                break;

            case "opt_7":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("7")));
                break;

            case "opt_8":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("8")));
                break;

            case "opt_9":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("9")));
                break;

            case "opt_A":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("A")));
                break;

            case "opt_B":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("B")));
                break;

            case "opt_C":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("C")));
                break;

            case "opt_D":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("D")));
                break;

            case "opt_E":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("E")));
                break;

            case "opt_F":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("F")));
                break;

            case "opt_G":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("G")));
                break;

            case "opt_H":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("H")));
                break;

            case "opt_I":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("I")));
                break;

            case "opt_J":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("J")));
                break;

            case "opt_K":
                whereCondition = whereCondition.and(BAZAR.DINOZ_ID.isNotNull().and(DINOZ.APPEARANCE_CODE.startsWith("K")));
                break;

            case "own":
                whereCondition = whereCondition.and(BAZAR.SELLER_ID.eq(accountId));
                break;
        }

        Integer totalCount = jooqContext.selectCount().from(BAZAR_LJ_DINOZ).where(whereCondition).fetchOneInto(Integer.class);
        ret.setTotalCount(totalCount);

        ret.setElements(jooqContext.select(BAZAR.fields())
                .from(BAZAR_LJ_DINOZ)
                .where(whereCondition)
                .orderBy(orderByClause)
                .limit(limit)
                .offset(offset)
                .fetch()
                .map(this::fromGenericRecord));

        Integer nextOffset = offset == null ? 0 : offset;
        nextOffset = (nextOffset + limit) < totalCount ? nextOffset + limit : null;
        ret.setOffset(nextOffset);

        return ret;
    }

    @Override
    public List<BazarListing> findActiveByLastBiderId(String id) {
        return jooqContext.selectFrom(BAZAR).where(BAZAR.ACTIVE.eq(true).and(BAZAR.LAST_BIDER_ID.eq(id))).fetch().map(this::fromRecord);
    }

    @Override
    public List<BazarListing> findAllActive() {
        return jooqContext.selectFrom(BAZAR).where(BAZAR.ACTIVE.eq(true)).fetch().map(this::fromRecord);
    }

    @Override
    public List<BazarListing> findLastCompleted() {
        return jooqContext.selectFrom(BAZAR).where(BAZAR.ACTIVE.eq(false)).orderBy(BAZAR.END_DATE_IN_EPOCH_SECONDS.desc()).limit(50).fetch().map(this::fromRecord);
    }

    @Override
    public Optional<BazarListing> findById(String listingId) {
        return jooqContext.selectFrom(BAZAR).where(BAZAR.ID.eq(UUID.fromString(listingId))).fetchOptional().map(this::fromRecord);
    }

    @Override
    public void deleteById(String listingId) {
        jooqContext.deleteFrom(BAZAR).where(BAZAR.ID.eq(UUID.fromString(listingId))).execute();
    }

    @Override
    public Optional<BazarListing> getNextActive() {
        return jooqContext.selectFrom(BAZAR).where(BAZAR.ACTIVE.eq(true)).orderBy(BAZAR.END_DATE_IN_EPOCH_SECONDS).limit(1).fetchOptional().map(this::fromRecord);
    }

    @Override
    public void unactive(BazarListing bazarListing) {
        jooqContext.update(BAZAR).set(BAZAR.ACTIVE, false).where(BAZAR.ID.eq(UUID.fromString(bazarListing.getId()))).execute();
    }

    @Override
    public Boolean playerHasAtLeastOneActiveListingWithDarkPotion(String playerId) {
        List<BazarListing> allActiveListingsOfPlayer = jooqContext.selectFrom(BAZAR)
                .where(BAZAR.ACTIVE.eq(true).and(BAZAR.SELLER_ID.eq(playerId)))
                .fetch()
                .map(this::fromRecord);

        for (BazarListing bazarListing : allActiveListingsOfPlayer) {
            if (Objects.nonNull(bazarListing.getObjects())
                    && bazarListing.getObjects().containsKey(DinoparcConstants.POTION_SOMBRE)
                    && bazarListing.getObjects().get(DinoparcConstants.POTION_SOMBRE) >= 1) {
                return true;
            }
        }
        return false;
    }

    private BazarRecord toRecord(BazarListing listing) {
        var ret = jooqContext.newRecord(BAZAR);

        ret.setId(UUID.fromString(listing.getId()));
        ret.setActive(listing.isActive());
        ret.setDinozId(listing.getDinozId() == null ? null : listing.getDinozId());
        ret.setEndDateInEpochSeconds(listing.getEndDateInEpochSeconds());
        ret.setSellerId(listing.getSellerId());
        ret.setSellerName(listing.getSellerName());
        ret.setInitialDurationInDays(listing.getInitialDurationInDays());
        ret.setEndDateInEpochSeconds(listing.getEndDateInEpochSeconds());
        ret.setLastBiderId(listing.getLastBiderId() == null ? null : listing.getLastBiderId());
        ret.setLastBiderName(listing.getLastBiderName());
        ret.setLastPrice(listing.getLastPrice());

        var objects = listing.getObjects() == null ? Collections.emptyMap() : listing.getObjects();
        try {
            ret.setObjects(JSONB.valueOf(objectMapper.writeValueAsString(objects)));
        } catch(Exception e) {
            ret.setObjects(JSONB.valueOf("{}"));
        }

        return ret;
    }

    private BazarListing fromRecord(BazarRecord record) {
        BazarListing ret = new BazarListing();

        ret.setId(record.getId().toString());
        ret.setActive(record.getActive());
        ret.setDinozId(record.getDinozId() == null ? null : record.getDinozId().toString());
        ret.setSellerId(record.getSellerId().toString());
        ret.setSellerName(record.getSellerName());
        ret.setInitialDurationInDays(record.getInitialDurationInDays());
        ret.setEndDateInEpochSeconds(record.getEndDateInEpochSeconds());
        ret.setLastBiderId(record.getLastBiderId() == null ? null : record.getLastBiderId().toString());
        ret.setLastBiderName(record.getLastBiderName());
        ret.setLastPrice(record.getLastPrice());
        try {
            ret.setObjects(record.getObjects() == null ? null : objectMapper.readValue(record.getObjects().data(), Map.class));
        } catch(Exception e) {
            ret.setObjects(null);
        }

        return ret;
    }

    private BazarListing fromGenericRecord(Record record) {
        BazarListing ret = new BazarListing();

        ret.setId(record.get(BAZAR.ID).toString());
        ret.setActive(record.get(BAZAR.ACTIVE));
        ret.setDinozId(record.get(BAZAR.DINOZ_ID) == null ? null : record.get(BAZAR.DINOZ_ID));
        ret.setSellerId(record.get(BAZAR.SELLER_ID));
        ret.setSellerName(record.get(BAZAR.SELLER_NAME));
        ret.setInitialDurationInDays(record.get(BAZAR.INITIAL_DURATION_IN_DAYS));
        ret.setEndDateInEpochSeconds(record.get(BAZAR.END_DATE_IN_EPOCH_SECONDS));
        ret.setLastBiderId(record.get(BAZAR.LAST_BIDER_ID) == null ? null : record.get(BAZAR.LAST_BIDER_ID));
        ret.setLastBiderName(record.get(BAZAR.LAST_BIDER_NAME));
        ret.setLastPrice(record.get(BAZAR.LAST_PRICE));
        try {
            ret.setObjects(record.get(BAZAR.OBJECTS) == null ? null : objectMapper.readValue(record.get(BAZAR.OBJECTS).data(), Map.class));
        } catch(Exception e) {
            ret.setObjects(null);
        }

        return ret;
    }
}
