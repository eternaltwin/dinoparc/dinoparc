package com.dinoparc.api.repository;

import com.dinoparc.api.domain.mission.GranitMissionDao;

import java.util.Optional;
import java.util.UUID;

public interface PgGranitMissionRepository {

    void create(UUID playerId);
    Optional<GranitMissionDao> getMissionData(UUID playerId);

    void setCompleted(UUID playerId);
    void setCompletedAndValidated(UUID playerId);

    void incrementMoueffe(UUID playerId);
    void incrementPicori(UUID playerId);
    void incrementCastivore(UUID playerId);
    void incrementSirain(UUID playerId);
    void incrementWinks(UUID playerId);
    void incrementGorilloz(UUID playerId);
    void incrementCargou(UUID playerId);
    void incrementHippoclamp(UUID playerId);
    void incrementRokky(UUID playerId);
    void incrementPigmou(UUID playerId);
    void incrementWanwan(UUID playerId);
    void incrementGoupignon(UUID playerId);
    void incrementKump(UUID playerId);
    void incrementPteroz(UUID playerId);
    void incrementSantaz(UUID playerId);
    void incrementKorgon(UUID playerId);
    void incrementKabuki(UUID playerId);
    void incrementSerpantin(UUID playerId);
    void incrementSoufflet(UUID playerId);
    void incrementFeross(UUID playerId);

    void updateMannysData(UUID playerId, Integer life, Integer location);
}
