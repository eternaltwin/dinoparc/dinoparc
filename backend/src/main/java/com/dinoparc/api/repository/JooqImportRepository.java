package com.dinoparc.api.repository;

import static com.dinoparc.tables.Import.IMPORT;
import com.dinoparc.api.controllers.dto.ImportTraceDto;
import com.dinoparc.tables.records.ImportRecord;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Repository
public class JooqImportRepository implements PgImportRepository {

    private DSLContext jooqContext;

    @Autowired
    public JooqImportRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }

    @Override
    public List<ImportTraceDto> findAll() {
        return jooqContext.selectFrom(IMPORT).fetch().map(this::fromRecord);
    }

    @Override
    public void deleteAll() {
        jooqContext.deleteFrom(IMPORT).execute();
    }

    @Override
    public void delete(ImportTraceDto it) {
        jooqContext.deleteFrom(IMPORT).where(IMPORT.ID.eq(UUID.fromString(it.getId()))).execute();
    }

    @Override
    public void save(ImportTraceDto it) {
        jooqContext.insertInto(IMPORT).set(toRecord(it)).execute();
    }

    private ImportRecord toRecord(ImportTraceDto it) {
        var ret = jooqContext.newRecord(IMPORT, it);
        ret.set(IMPORT.IMPORTATION_DATE, OffsetDateTime.now(ZoneId.of("Europe/Paris")));
        String[] allDinozIds = new String[it.getImportedDinoz().size()];
        allDinozIds = it.getImportedDinoz().toArray(allDinozIds);
        ret.setImportedDinozs(allDinozIds);
        return ret;
    }

    private ImportTraceDto fromRecord(ImportRecord record) {
        ImportTraceDto ret = new ImportTraceDto();

        ret.setId(record.getId().toString());
        ret.setImportationDate(record.getImportationDate());
        ret.setImportedAccount(record.getImportedAccount());
        ret.setOriginServer(record.getOriginServer());
        ret.setNewHostAccountId(record.getNewHostAccountId().toString());
        ret.setImportedDinoz(Arrays.asList(record.getImportedDinozs()));

        return ret;
    }
}
