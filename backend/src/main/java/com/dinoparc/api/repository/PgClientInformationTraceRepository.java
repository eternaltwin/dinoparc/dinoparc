package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.ClientInformationTrace;

public interface PgClientInformationTraceRepository {
    void addClientInformationTrace(ClientInformationTrace toInsert);

    void cleanUp();
}
