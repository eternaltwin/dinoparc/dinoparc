package com.dinoparc.api.repository;

import com.dinoparc.api.domain.dinoz.EventDinozDto;
import com.dinoparc.api.domain.dinoz.EventDinozUpdate;
import com.dinoparc.tables.records.WildWistitiRecord;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import static com.dinoparc.tables.WildWistiti.WILD_WISTITI;

@Repository
public class JooqWildWistitiRepository implements PgWildWistitiRepository {

    private DSLContext jooqContext;

    @Autowired
    public JooqWildWistitiRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }

    @Override
    public EventDinozDto getWildWistiti() {
        return jooqContext.selectFrom(WILD_WISTITI).fetch().map(this::fromRecord).get(0);
    }

    @Override
    public EventDinozDto updateWildWistiti(EventDinozUpdate eventDinozUpdate) {
        EventDinozDto raidBoss = jooqContext.update(WILD_WISTITI)
                .set(WILD_WISTITI.FIGHT_COUNT, WILD_WISTITI.FIGHT_COUNT.add(1))
                .set(WILD_WISTITI.LIFE, WILD_WISTITI.LIFE.sub(eventDinozUpdate.getLife()))
                .set(WILD_WISTITI.BONUS_FIRE, WILD_WISTITI.BONUS_FIRE.sub(eventDinozUpdate.getBonusFire()))
                .set(WILD_WISTITI.BONUS_WOOD, WILD_WISTITI.BONUS_WOOD.sub(eventDinozUpdate.getBonusWood()))
                .set(WILD_WISTITI.BONUS_WATER, WILD_WISTITI.BONUS_WATER.sub(eventDinozUpdate.getBonusWater()))
                .set(WILD_WISTITI.BONUS_THUNDER, WILD_WISTITI.BONUS_THUNDER.sub(eventDinozUpdate.getBonusThunder()))
                .set(WILD_WISTITI.BONUS_AIR, WILD_WISTITI.BONUS_AIR.sub(eventDinozUpdate.getBonusAir()))
                .returning()
                .fetch()
                .map(this::fromRecord)
                .get(0);

        if (raidBoss.getLife() < 0) {
            raidBoss = jooqContext.update(WILD_WISTITI).set(WILD_WISTITI.LIFE, 0).returning().fetch().map(this::fromRecord).get(0);
        }
        if (raidBoss.getBonusFire() < 0) {
            raidBoss = jooqContext.update(WILD_WISTITI).set(WILD_WISTITI.BONUS_FIRE, 0).returning().fetch().map(this::fromRecord).get(0);
        }
        if (raidBoss.getBonusWood() < 0) {
            raidBoss = jooqContext.update(WILD_WISTITI).set(WILD_WISTITI.BONUS_WOOD, 0).returning().fetch().map(this::fromRecord).get(0);
        }
        if (raidBoss.getBonusWater() < 0) {
            raidBoss = jooqContext.update(WILD_WISTITI).set(WILD_WISTITI.BONUS_WATER, 0).returning().fetch().map(this::fromRecord).get(0);
        }
        if (raidBoss.getBonusThunder() < 0) {
            raidBoss = jooqContext.update(WILD_WISTITI).set(WILD_WISTITI.BONUS_THUNDER, 0).returning().fetch().map(this::fromRecord).get(0);
        }
        if (raidBoss.getBonusAir() < 0) {
            raidBoss = jooqContext.update(WILD_WISTITI).set(WILD_WISTITI.BONUS_AIR, 0).returning().fetch().map(this::fromRecord).get(0);
        }

        return raidBoss;
    }

    @Override
    public void resetWildWistiti(String appearanceCode, int life, int fightCount, int bonusFire, int bonusWood, int bonusWater,
                                 int bonusThunder, int bonusAir) {
        jooqContext.update(WILD_WISTITI)
                .set(WILD_WISTITI.APPEARANCE_CODE, appearanceCode)
                .set(WILD_WISTITI.FIGHT_COUNT, fightCount)
                .set(WILD_WISTITI.LIFE, life)
                .set(WILD_WISTITI.BONUS_FIRE, bonusFire)
                .set(WILD_WISTITI.BONUS_WOOD, bonusWood)
                .set(WILD_WISTITI.BONUS_WATER, bonusWater)
                .set(WILD_WISTITI.BONUS_THUNDER, bonusThunder)
                .set(WILD_WISTITI.BONUS_AIR, bonusAir)
                .execute();
    }

    private EventDinozDto fromRecord(WildWistitiRecord record) {
        EventDinozDto ret = new EventDinozDto();
        ret.setAppearanceCode(record.getAppearanceCode());
        ret.setName(record.getName());
        ret.setPlaceNumber(record.getPlaceNumber());
        ret.setLife(record.getLife());
        ret.setBonusFire(record.getBonusFire());
        ret.setBonusWood(record.getBonusWood());
        ret.setBonusWater(record.getBonusWater());
        ret.setBonusThunder(record.getBonusThunder());
        ret.setBonusAir(record.getBonusAir());
        ret.setFightCount(record.getFightCount());
        ret.setBonusClaw(0);

        return ret;
    }
}
