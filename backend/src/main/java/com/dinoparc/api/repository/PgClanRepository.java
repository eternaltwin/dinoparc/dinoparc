package com.dinoparc.api.repository;

import com.dinoparc.api.domain.clan.Clan;
import com.dinoparc.api.domain.clan.ClanMessage;
import com.dinoparc.api.domain.clan.PlayerClan;
import com.dinoparc.api.domain.clan.Totem;
import com.dinoparc.api.domain.dinoz.Dinoz;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface PgClanRepository {
    Optional<Clan> getClan(String clanId);
    List<Clan> getAllClans();
    List<Clan> getLatest50ClansCreated();
    Clan getClanOfPlayer(String playerId);
    Clan modifyClanPage(Clan clan);
    ClanMessage postMessageToClanForum(String accountId, String clanId, String message);
    ClanMessage getClanMessages(String clanId);
    Optional<PlayerClan> getPlayerClan(String clanId, String playerId);
    Optional<PlayerClan> getPlayerClan(String playerId);

    void createClan(Clan newClan);
    void updateApplicationsToClan(String clandId, List<String> candidate);
    void addPlayerToClan(Clan clan, String playerId);
    void expellPlayerFromClan(Clan clan, String playerId);
    void refusePlayerApplication(Clan clan, String playerId);
    void updateClanNbOfDinoz(Clan clan, Integer nbDinozTotal);
    void updateClanNbOfWarriorDinoz(Clan clan, Integer nbDinozTotal);
    void adminWipeClanPagesContent(String clanId);
    void deleteGhostClan(String clanId);
    void quitClan(String playerId);
    void updatePlayerClan(PlayerClan playerClan);
    void updateTotem(String clanId, Totem totem);
    void updateChest(String clanId, Map<String, Integer> chest);
    void resetClanPoints(String clanId);
    void addClanPointsFights(String clanId, Integer nbPtsFights);
    void addClanPointsOccupation(String clanId, Integer nbPtsOccupation);
    void addClanPointsFouilles(String clanId, Integer nbPtsFouilles);
    void updatePosition(String id, Integer position);
    void updateFaction(String clanId, String faction);
    void updateChef(String clanId, String chefId);

    List<Dinoz> getAllWarriorDinozOfClan(String clanId);
    List<Dinoz> getAllDinozWarriorAliveInThatLocation(Integer placeNumber);
}
