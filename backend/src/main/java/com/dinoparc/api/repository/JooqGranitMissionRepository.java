package com.dinoparc.api.repository;

import com.dinoparc.api.domain.mission.GranitMissionDao;
import com.dinoparc.tables.records.GranitMissionQuestRecord;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

import static com.dinoparc.tables.GranitMissionQuest.GRANIT_MISSION_QUEST;

@Repository
public class JooqGranitMissionRepository implements PgGranitMissionRepository {

    private DSLContext jooqContext;

    @Autowired
    public JooqGranitMissionRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }

    @Override
    public void create(UUID playerId) {
        GranitMissionDao granitMissionDao = new GranitMissionDao();

        granitMissionDao.setId(UUID.randomUUID().toString());
        granitMissionDao.setPlayerId(playerId.toString());
        granitMissionDao.setComplete(false);
        granitMissionDao.setCompleteAndValidated(false);
        granitMissionDao.setMoueffe(0);
        granitMissionDao.setPicori(0);
        granitMissionDao.setCastivore(0);
        granitMissionDao.setSirain(0);
        granitMissionDao.setWinks(0);
        granitMissionDao.setGorilloz(0);
        granitMissionDao.setCargou(0);
        granitMissionDao.setHippoclamp(0);
        granitMissionDao.setRokky(0);
        granitMissionDao.setPigmou(0);
        granitMissionDao.setWanwan(0);
        granitMissionDao.setGoupignon(0);
        granitMissionDao.setKump(0);
        granitMissionDao.setPteroz(0);
        granitMissionDao.setSantaz(0);
        granitMissionDao.setKorgon(0);
        granitMissionDao.setKabuki(0);
        granitMissionDao.setSerpantin(0);
        granitMissionDao.setSoufflet(0);
        granitMissionDao.setFeross(0);

        granitMissionDao.setMannyLocation(6);
        granitMissionDao.setMannyLife(1000);

        jooqContext.insertInto(GRANIT_MISSION_QUEST)
                .set(toRecord(granitMissionDao))
                .execute();
    }

    @Override
    public Optional<GranitMissionDao> getMissionData(UUID playerId) {
        return jooqContext.selectFrom(GRANIT_MISSION_QUEST)
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .fetchOptional()
                .map(this::fromRecord);
    }

    @Override
    public void setCompleted(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.ISCOMPLETE, true)
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void setCompletedAndValidated(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.ISCOMPLETEANDVALIDATED, true)
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementMoueffe(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.MOUEFFE, Long.valueOf((this.getMissionData(playerId).get().getMoueffe() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementPicori(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.PICORI, Long.valueOf((this.getMissionData(playerId).get().getPicori() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementCastivore(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.CASTIVORE, Long.valueOf((this.getMissionData(playerId).get().getCastivore() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementSirain(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.SIRAIN, Long.valueOf((this.getMissionData(playerId).get().getSirain() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementWinks(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.WINKS, Long.valueOf((this.getMissionData(playerId).get().getWinks() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementGorilloz(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.GORILLOZ, Long.valueOf((this.getMissionData(playerId).get().getGorilloz() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementCargou(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.CARGOU, Long.valueOf((this.getMissionData(playerId).get().getCargou() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementHippoclamp(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.HIPPOCLAMP, Long.valueOf((this.getMissionData(playerId).get().getHippoclamp() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementRokky(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.ROKKY, Long.valueOf((this.getMissionData(playerId).get().getRokky() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementPigmou(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.PIGMOU, Long.valueOf((this.getMissionData(playerId).get().getPigmou() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementWanwan(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.WANWAN, Long.valueOf((this.getMissionData(playerId).get().getWanwan() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementGoupignon(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.GOUPIGNON, Long.valueOf((this.getMissionData(playerId).get().getGoupignon() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementKump(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.KUMP, Long.valueOf((this.getMissionData(playerId).get().getKump() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementPteroz(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.PTEROZ, Long.valueOf((this.getMissionData(playerId).get().getPteroz() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementSantaz(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.SANTAZ, Long.valueOf((this.getMissionData(playerId).get().getSantaz() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementKorgon(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.KORGON, Long.valueOf((this.getMissionData(playerId).get().getKorgon() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementKabuki(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.KABUKI, Long.valueOf((this.getMissionData(playerId).get().getKabuki() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementSerpantin(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.SERPANTIN, Long.valueOf((this.getMissionData(playerId).get().getSerpantin() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementSoufflet(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.SOUFFLET, Long.valueOf((this.getMissionData(playerId).get().getSoufflet() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void incrementFeross(UUID playerId) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.FEROSS, Long.valueOf((this.getMissionData(playerId).get().getFeross() + 1)))
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    @Override
    public void updateMannysData(UUID playerId, Integer life, Integer location) {
        jooqContext.update(GRANIT_MISSION_QUEST)
                .set(GRANIT_MISSION_QUEST.MANNYLIFE, life)
                .set(GRANIT_MISSION_QUEST.MANNYLOCATION, location)
                .where(GRANIT_MISSION_QUEST.PLAYER_ID.eq(playerId))
                .execute();
    }

    private Record toRecord(GranitMissionDao granitMissionDao) {
        GranitMissionQuestRecord record = jooqContext.newRecord(GRANIT_MISSION_QUEST);

        record.setId(UUID.fromString(granitMissionDao.getId()));
        record.setPlayerId(UUID.fromString(granitMissionDao.getPlayerId()));
        record.setIscomplete(granitMissionDao.getComplete());
        record.setIscompleteandvalidated(granitMissionDao.getCompleteAndValidated());

        record.setMoueffe(Long.valueOf(granitMissionDao.getMoueffe()));
        record.setPicori(Long.valueOf(granitMissionDao.getPicori()));
        record.setCastivore(Long.valueOf(granitMissionDao.getCastivore()));
        record.setSirain(Long.valueOf(granitMissionDao.getSirain()));
        record.setWinks(Long.valueOf(granitMissionDao.getWinks()));
        record.setGorilloz(Long.valueOf(granitMissionDao.getGorilloz()));
        record.setCargou(Long.valueOf(granitMissionDao.getCargou()));
        record.setHippoclamp(Long.valueOf(granitMissionDao.getHippoclamp()));
        record.setRokky(Long.valueOf(granitMissionDao.getRokky()));
        record.setPigmou(Long.valueOf(granitMissionDao.getPigmou()));
        record.setWanwan(Long.valueOf(granitMissionDao.getWanwan()));
        record.setGoupignon(Long.valueOf(granitMissionDao.getGoupignon()));
        record.setKump(Long.valueOf(granitMissionDao.getKump()));
        record.setPteroz(Long.valueOf(granitMissionDao.getPteroz()));
        record.setSantaz(Long.valueOf(granitMissionDao.getSantaz()));
        record.setKorgon(Long.valueOf(granitMissionDao.getKorgon()));
        record.setKabuki(Long.valueOf(granitMissionDao.getKabuki()));
        record.setSerpantin(Long.valueOf(granitMissionDao.getSerpantin()));
        record.setSoufflet(Long.valueOf(granitMissionDao.getSoufflet()));
        record.setFeross(Long.valueOf(granitMissionDao.getFeross()));

        record.setMannylife(granitMissionDao.getMannyLife());
        record.setMannylocation(granitMissionDao.getMannyLocation());

        return record;
    }

    private GranitMissionDao fromRecord(GranitMissionQuestRecord granitMissionQuestRecord) {
        GranitMissionDao granitMissionDao = new GranitMissionDao();

        granitMissionDao.setId(granitMissionQuestRecord.getId().toString());
        granitMissionDao.setPlayerId(granitMissionQuestRecord.getPlayerId().toString());
        granitMissionDao.setComplete(granitMissionQuestRecord.getIscomplete());
        granitMissionDao.setCompleteAndValidated(granitMissionQuestRecord.getIscompleteandvalidated());

        granitMissionDao.setMoueffe(granitMissionQuestRecord.getMoueffe().intValue());
        granitMissionDao.setPicori(granitMissionQuestRecord.getPicori().intValue());
        granitMissionDao.setCastivore(granitMissionQuestRecord.getCastivore().intValue());
        granitMissionDao.setSirain(granitMissionQuestRecord.getSirain().intValue());
        granitMissionDao.setWinks(granitMissionQuestRecord.getWinks().intValue());
        granitMissionDao.setGorilloz(granitMissionQuestRecord.getGorilloz().intValue());
        granitMissionDao.setCargou(granitMissionQuestRecord.getCargou().intValue());
        granitMissionDao.setHippoclamp(granitMissionQuestRecord.getHippoclamp().intValue());
        granitMissionDao.setRokky(granitMissionQuestRecord.getRokky().intValue());
        granitMissionDao.setPigmou(granitMissionQuestRecord.getPigmou().intValue());
        granitMissionDao.setWanwan(granitMissionQuestRecord.getWanwan().intValue());
        granitMissionDao.setGoupignon(granitMissionQuestRecord.getGoupignon().intValue());
        granitMissionDao.setKump(granitMissionQuestRecord.getKump().intValue());
        granitMissionDao.setPteroz(granitMissionQuestRecord.getPteroz().intValue());
        granitMissionDao.setSantaz(granitMissionQuestRecord.getSantaz().intValue());
        granitMissionDao.setKorgon(granitMissionQuestRecord.getKorgon().intValue());
        granitMissionDao.setKabuki(granitMissionQuestRecord.getKabuki().intValue());
        granitMissionDao.setSerpantin(granitMissionQuestRecord.getSerpantin().intValue());
        granitMissionDao.setSoufflet(granitMissionQuestRecord.getSoufflet().intValue());
        granitMissionDao.setFeross(granitMissionQuestRecord.getFeross().intValue());

        granitMissionDao.setMannyLife(granitMissionQuestRecord.getMannylife());
        granitMissionDao.setMannyLocation(granitMissionQuestRecord.getMannylocation());

        return granitMissionDao;
    }
}
