package com.dinoparc.api.repository;

import com.dinoparc.api.controllers.dto.PlayerClanInfoDto;
import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.account.PlayerActionModeEnum;
import com.dinoparc.api.domain.account.PlayerImgModeEnum;
import com.dinoparc.tables.records.PlayerClanRecord;
import com.dinoparc.tables.records.PlayerDinozRecord;
import com.dinoparc.tables.records.PlayerRecord;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jooq.DSLContext;
import org.jooq.JSONB;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.*;

import static com.dinoparc.tables.Player.PLAYER;
import static com.dinoparc.tables.PlayerClan.PLAYER_CLAN;
import static com.dinoparc.tables.PlayerDinoz.PLAYER_DINOZ;

@Repository
public class JooqPlayerRepository implements PgPlayerRepository {

    private DSLContext jooqContext;
    private ObjectMapper objectMapper;

    @Autowired
    public JooqPlayerRepository(DSLContext jooqContext, ObjectMapper objectMapper) {
        this.jooqContext = jooqContext;
        this.objectMapper = objectMapper;
    }

    @Override
    public Optional<Player> findById(String userId) {
        return this.findById(UUID.fromString(userId));
    }

    @Override
    public Player findPlayerByDinozId(String dinozId) {
       var optPlayerId = jooqContext.select(PLAYER_DINOZ.PLAYER_ID).from(PLAYER_DINOZ).where(PLAYER_DINOZ.DINOZ_ID.eq(dinozId)).fetchOptionalInto(UUID.class);
       if (optPlayerId.isEmpty()) {
           return null;
       }
       return findById(optPlayerId.get()).get();
    }

    private Optional<Player> findById(UUID userId) {
       return jooqContext.selectFrom(PLAYER).where(PLAYER.PLAYER_ID.eq(userId)).fetchOptional().map(this::fromRecord);
    }

    @Override
    public Player findByName(String tokenName) {
        var ret = jooqContext.selectFrom(PLAYER).where(PLAYER.NAME.eq(tokenName)).fetchOptional().map(this::fromRecord);
        return ret.isPresent() ? ret.get() : null;
    }

    @Override
    public List<Player> findAll(){
        return jooqContext.selectFrom(PLAYER).orderBy(PLAYER.NB_POINTS.desc()).fetch().map(this::fromRecord);
    }

    @Override
    public Integer findPlayerDinozRank(String playerId, String dinozId) {
        return jooqContext.select(PLAYER_DINOZ.RANK)
                .from(PLAYER_DINOZ)
                .where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(playerId)))
                .and(PLAYER_DINOZ.DINOZ_ID.eq(dinozId))
                .fetchOneInto(Integer.class);
    }

    @Override
    public List<Player> getTopPlayers(int nbPlayers) {
        return jooqContext.selectFrom(PLAYER).orderBy(PLAYER.NB_POINTS.desc()).limit(nbPlayers).fetch().map(this::fromRecord);
    }

    @Override
    public void create(Player newAccount) {
        jooqContext.insertInto(PLAYER).set(toRecord(newAccount, true)).execute();
    }

    @Override
    public void save(Player player) {
        jooqContext.update(PLAYER).set(toRecord(player, false)).where(PLAYER.PLAYER_ID.eq(UUID.fromString(player.getId()))).execute();
    }

    @Override
    public void updateHunterGroup(String id, int hunterGroup) {
        jooqContext.update(PLAYER).set(PLAYER.HUNTER_GROUP, hunterGroup).where(PLAYER.PLAYER_ID.eq(UUID.fromString(id))).execute();
    }

    @Override
    public void deleteById(String accountId) {
        jooqContext.deleteFrom(PLAYER_DINOZ).where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
        jooqContext.deleteFrom(PLAYER).where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public void dailyReset(Player player) {
        jooqContext.update(PLAYER)
                .set(PLAYER.DAILY_SHINIES_FOUGHT, 0)
                .set(PLAYER.HERMIT_STAGE_FIGHTING_DINOZ, DSL.val(null, PLAYER.HERMIT_STAGE_FIGHTING_DINOZ))
                .set(PLAYER.HUNTER_GROUP, DSL.val(null, PLAYER.HUNTER_GROUP))
                .set(PLAYER.NB_POINTS, player.getNbPoints())
                .set(PLAYER.AVERAGE_LEVEL_POINTS, player.getAverageLevelPoints())
                .set(PLAYER.LAST_WISTITI_FIGHT, DSL.val(null, PLAYER.LAST_WISTITI_FIGHT))
                .set(PLAYER.LAST_GENERATED_STORE, DSL.val(null, PLAYER.LAST_GENERATED_STORE))
                .set(PLAYER.HASCLAIMEDTRADEFROMFISHERMANTODAY, false)
                .where(PLAYER.PLAYER_ID.eq(UUID.fromString(player.getId())))
                .execute();
    }

    @Override
    public void updateCash(String accountId, int valueToAdd) {
        jooqContext.update(PLAYER).set(PLAYER.CASH, PLAYER.CASH.add(valueToAdd)).where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public void updateCashAll(Integer valueToAdd) {
        jooqContext.update(PLAYER).set(PLAYER.CASH, PLAYER.CASH.add(valueToAdd)).execute();
    }

    @Override
    public void updatePoints(String accountId, int nbPts, int average) {
        jooqContext.update(PLAYER)
                .set(PLAYER.NB_POINTS, nbPts)
                .set(PLAYER.AVERAGE_LEVEL_POINTS, average)
                .where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId)))
                .execute();
    }

    @Override
    public void addDinoz(String accountId, String dinozId) {
        Optional<Integer> maxRank = jooqContext.select(DSL.max(PLAYER_DINOZ.RANK)).from(PLAYER_DINOZ).where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(accountId))).fetchOptionalInto(Integer.class);
        var rank = maxRank.isPresent() ? maxRank.get() + 1 : 1;
        jooqContext.insertInto(PLAYER_DINOZ).set(getPlayerDinozRecord(accountId, dinozId, rank)).execute();
    }

    @Override
    public void addAllDinoz(String accountId, List<String> dinozIds) {
        for (String dinozId : dinozIds) {
            this.addDinoz(accountId, dinozId);
        }
    }

    @Override
    public void deleteDinoz(String accountId, String dinozId) {
        Optional<PlayerDinozRecord> toDelete = jooqContext.selectFrom(PLAYER_DINOZ).where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(accountId))).and(PLAYER_DINOZ.DINOZ_ID.eq(dinozId)).fetchOptional();
        if (toDelete.isPresent()) {
            // Delete dinoz from player dinoz list
            var deletedRank = toDelete.get().getRank();
            jooqContext.deleteFrom(PLAYER_DINOZ).where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(accountId))).and(PLAYER_DINOZ.DINOZ_ID.eq(dinozId)).execute();

            // Refresh player dinoz ranks
            var playerDinozs = jooqContext.selectFrom(PLAYER_DINOZ).where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(accountId))).orderBy(PLAYER_DINOZ.RANK).fetch();
            int rank = 1;
            for (var playerDinoz : playerDinozs) {
                jooqContext.update(PLAYER_DINOZ).set(PLAYER_DINOZ.RANK, rank).where(PLAYER_DINOZ.PLAYER_ID.eq(playerDinoz.getPlayerId())).and(PLAYER_DINOZ.DINOZ_ID.eq(playerDinoz.getDinozId())).execute();
                rank++;
            }
        }
    }

    @Override
    public void moveDinoz(String accountId, String dinozId, boolean previous, int count) {
        Optional<PlayerDinozRecord> toMove = jooqContext.selectFrom(PLAYER_DINOZ).where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(accountId))).and(PLAYER_DINOZ.DINOZ_ID.eq(dinozId)).fetchOptional();
        if (toMove.isPresent()) {
            var oldRank = toMove.get().getRank();
            var newRank = previous ? oldRank - count : oldRank + count;

            // Min rank is one, if less than this value, set it to min
            if (newRank < 1) {
                newRank = 1;
            }

            var maxRank = jooqContext.select(DSL.max(PLAYER_DINOZ.RANK)).from(PLAYER_DINOZ).where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(accountId))).fetchOneInto(Integer.class);

            // If newRank greater than max, set it to max
            if (newRank > maxRank) {
                newRank = maxRank;
            }

            // Old rank and new rank has same value, nothing to do
            if (newRank == oldRank) {
                return;
            }

            // Update dinozs rank of this player depending on previous
            if (previous) {
                jooqContext.update(PLAYER_DINOZ)
                        .set(PLAYER_DINOZ.RANK, PLAYER_DINOZ.RANK.add(1))
                        .where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(accountId)))
                        .and(PLAYER_DINOZ.RANK.ge(newRank))
                        .and(PLAYER_DINOZ.RANK.le(oldRank))
                        .execute();
            } else {
                jooqContext.update(PLAYER_DINOZ)
                        .set(PLAYER_DINOZ.RANK, PLAYER_DINOZ.RANK.sub(1))
                        .where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(accountId)))
                        .and(PLAYER_DINOZ.RANK.ge(oldRank))
                        .and(PLAYER_DINOZ.RANK.le(newRank))
                        .execute();
            }

            // Update target dinoz rank
            jooqContext.update(PLAYER_DINOZ).set(PLAYER_DINOZ.RANK, newRank).where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(accountId))).and(PLAYER_DINOZ.DINOZ_ID.eq(dinozId)).execute();
        }
    }

    @Override
    public void updateSacrificePoints(String accountId, Integer sacrificePoints) {
        jooqContext.update(PLAYER).set(PLAYER.SACRIFICE_POINTS, sacrificePoints).where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public void updateMessages(String accountId, boolean newVal) {
        jooqContext.update(PLAYER).set(PLAYER.MESSAGES, newVal).where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public void updateBlackList(String accountId, HashMap<String, String> newBlackList) {
        try {
            var blackList = JSONB.valueOf(objectMapper.writeValueAsString(newBlackList));
            jooqContext.update(PLAYER).set(PLAYER.BLACK_LIST, blackList).where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
        } catch(Exception e) {
            // Blacklist not updated
        }
    }

    @Override
    public void updateHermitStageFightingDinoz(String accountId, String dinozId) {
        jooqContext.update(PLAYER).set(PLAYER.HERMIT_STAGE_FIGHTING_DINOZ, dinozId).where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public void updateHermitStage(String accountId, int newHermitStage) {
        jooqContext.update(PLAYER).set(PLAYER.HERMIT_STAGE, newHermitStage).where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public void updateHermitStageCurrentWins(String accountId, int newHermitStageCurrentWins) {
        jooqContext.update(PLAYER).set(PLAYER.HERMIT_STAGE_CURRENT_WINS, newHermitStageCurrentWins).where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public void updateRole(String accountId, String role) {
        jooqContext.update(PLAYER).set(PLAYER.ROLE, role).where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public void setWistitiCaptured(String accountId, Integer qty) {
        jooqContext.update(PLAYER).set(PLAYER.NB_WISTITI_CAPTURED, qty).where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public void updateWistitiCaptured(String accountId, int valueToSet) {
        jooqContext.update(PLAYER).set(PLAYER.NB_WISTITI_CAPTURED, valueToSet).where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public void updateDinozLimit(String accountId, int valueToAdd) {
        jooqContext.update(PLAYER).set(PLAYER.ACCOUNT_DINOZ_LIMIT, PLAYER.ACCOUNT_DINOZ_LIMIT.add(valueToAdd)).where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public Integer countDinoz(String accountId) {
        return jooqContext.selectCount().from(PLAYER_DINOZ).where(PLAYER_DINOZ.PLAYER_ID.eq(UUID.fromString(accountId))).fetchOneInto(Integer.class);
    }

    @Override
    public void updateDailyShiniesFought(String accountId, int valueToAdd) {
        jooqContext.update(PLAYER).set(PLAYER.DAILY_SHINIES_FOUGHT, PLAYER.DAILY_SHINIES_FOUGHT.add(valueToAdd)).where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public void updateUnlockedDinoz(String accountId, List<Integer> unlockedDinoz) {
        try {
            var newUnlockedDinozList = JSONB.valueOf(objectMapper.writeValueAsString(unlockedDinoz));
            jooqContext.update(PLAYER).set(PLAYER.UNLOCKED_DINOZ, newUnlockedDinozList).where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
        } catch(Exception e) {
            // Unlocked dinoz list not updated
        }
    }

    @Override
    public PlayerClanInfoDto checkIfPlayerIsInAClan(String playerId) {
        PlayerClanInfoDto playerClanInfo = new PlayerClanInfoDto();
        @NotNull Optional<PlayerClanRecord> clanForPlayer = jooqContext.selectFrom(PLAYER_CLAN)
                .where(PLAYER_CLAN.PLAYER_ID.eq(UUID.fromString(playerId))).fetchOptional();

        if (clanForPlayer.isEmpty()) {
            playerClanInfo.setInAClan(false);
            playerClanInfo.setClanName("");
        } else {
            playerClanInfo.setInAClan(true);
            playerClanInfo.setClanName(clanForPlayer.get().getClanName());
        }

        return playerClanInfo;
    }

    @Override
    public void updateHistorySkip(String accountId, Boolean historySkip) {
        jooqContext.update(PLAYER)
                .set(PLAYER.HISTORY_SKIP, historySkip)
                .where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public void updateBanInfos(String accountId, boolean banned, boolean blocked, boolean bannedPvp, boolean bannedRaid, boolean bannedShiny, boolean bannedWistiti, boolean bannedBazar) {
        jooqContext.update(PLAYER)
                .set(PLAYER.BANNED, banned)
                .set(PLAYER.BLOCKED, blocked)
                .set(PLAYER.BANNED_PVP, bannedPvp)
                .set(PLAYER.BANNED_RAID, bannedRaid)
                .set(PLAYER.BANNED_SHINY, bannedShiny)
                .set(PLAYER.BANNED_WISTITI, bannedWistiti)
                .set(PLAYER.BANNED_BAZAR, bannedBazar)
                .where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public void updateCashAndWistitiFight(String accountId, Integer moneyWon) {
        jooqContext.update(PLAYER)
                .set(PLAYER.CASH, PLAYER.CASH.add(moneyWon))
                .set(PLAYER.LAST_WISTITI_FIGHT, OffsetDateTime.now(ZoneId.of("Europe/Paris")))
                .where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public void updateLastGeneratedStore(String accountId, OffsetDateTime value) {
        jooqContext.update(PLAYER)
                .set(PLAYER.LAST_GENERATED_STORE, value)
                .where(PLAYER.PLAYER_ID.eq(UUID.fromString(accountId)))
                .execute();
    }

    @Override
    public Player updateImgMode(String playerId, String imgMode) {
        return jooqContext.update(PLAYER)
            .set(PLAYER.IMG_MODE, imgMode)
            .where(PLAYER.PLAYER_ID.eq(UUID.fromString(playerId)))
            .returning()
            .fetchOne()
            .map(record -> fromRecord((PlayerRecord) record));
    }

    @Override
    public Player updateActionMode(String playerId, String actionMode) {
        return jooqContext.update(PLAYER)
                .set(PLAYER.ACTION_MODE, actionMode)
                .where(PLAYER.PLAYER_ID.eq(UUID.fromString(playerId)))
                .returning()
                .fetchOne()
                .map(record -> fromRecord((PlayerRecord) record));
    }

    @Override
    public void updateHelpToggleFlag(String playerId, Boolean helpToggleFlagValue) {
        jooqContext.update(PLAYER)
                .set(PLAYER.HELPTOGGLE, helpToggleFlagValue)
                .where(PLAYER.PLAYER_ID.eq(UUID.fromString(playerId)))
                .execute();
    }

    @Override
    public void updateHasClaimedTradeFromFishermanToday(String playerId, Boolean hasClaimedTradeFromFishermanToday) {
        jooqContext.update(PLAYER)
                .set(PLAYER.HASCLAIMEDTRADEFROMFISHERMANTODAY, hasClaimedTradeFromFishermanToday)
                .where(PLAYER.PLAYER_ID.eq(UUID.fromString(playerId)))
                .execute();
    }

    @Override
    public void updateLastPirateBuy(String playerId, String lastPirateBuy) {
        jooqContext.update(PLAYER)
                .set(PLAYER.LASTPIRATEBUY, lastPirateBuy)
                .where(PLAYER.PLAYER_ID.eq(UUID.fromString(playerId)))
                .execute();
    }

    @Override
    public Boolean getPlayerHelpToggleValue(String playerId) {
        return jooqContext.select(PLAYER.HELPTOGGLE)
                .from(PLAYER)
                .where(PLAYER.PLAYER_ID.eq(UUID.fromString(playerId)))
                .fetchOneInto(Boolean.class);
    }

    private PlayerRecord toRecord(Player player, boolean isCreation) {
        PlayerRecord ret = jooqContext.newRecord(PLAYER);

        if (isCreation) {
            ret.setPlayerId(UUID.fromString(player.getId()));
        }
        ret.setName(player.getName());
        ret.setCash(player.getCash());
        ret.setNbPoints(player.getNbPoints());
        ret.setAverageLevelPoints(player.getAverageLevelPoints());
        ret.setMessages(player.isMessages());
        ret.setLastLogin(player.getLastLogin());
        ret.setSacrificePoints(player.getSacrificePoints());
        ret.setDailyShiniesFought(player.getDailyShiniesFought());
        ret.setRole(player.getRole());
        ret.setHermitStage(player.getHermitStage());
        ret.setHermitStageCurrentWins(player.getHermitStageCurrentWins());
        ret.setHermitStageFightingDinoz(player.getHermitStageFightingDinoz());
        ret.setAccountDinozLimit(player.getAccountDinozLimit());
        ret.setNbWistitiCaptured(player.getWistitiCaptured());
        ret.setHunterGroup(player.getHunterGroup());
        ret.setBanned(player.isBanned());
        ret.setBlocked(player.isBlocked());
        ret.setBannedPvp(player.isBannedPvp());
        ret.setBannedRaid(player.isBannedRaid());
        ret.setBannedShiny(player.isBannedShiny());
        ret.setBannedWistiti(player.isBannedWistiti());
        ret.setLastGeneratedStore(player.getLastGeneratedStore());
        ret.setHelptoggle(player.getHelpToggle());
        ret.setHasclaimedtradefromfishermantoday(player.getHasClaimedTradeFromFishermanToday());
        ret.setLastpiratebuy(player.getLastPirateBuy());

        try {
            var newUnlockedDinozList = JSONB.valueOf(objectMapper.writeValueAsString(player.getUnlockedDinoz()));
            ret.setUnlockedDinoz(newUnlockedDinozList);
        } catch(Exception e) {
            // Error in unlockedDinoz create/update
        }

        try {
            var newBlackList = JSONB.valueOf(objectMapper.writeValueAsString(player.getBlackList()));
            ret.setBlackList(newBlackList);
        } catch(Exception e) {
            // Error in black list create/update
        }

        return ret;
    }

    private Player fromRecord(PlayerRecord playerRecord) {
        Player ret = new Player();
        ret.setId(playerRecord.getPlayerId().toString());
        ret.setName(playerRecord.getName());
        ret.setCash(playerRecord.getCash());
        ret.setNbPoints(playerRecord.getNbPoints());
        ret.setAverageLevelPoints(playerRecord.getAverageLevelPoints());
        ret.setMessages(playerRecord.getMessages());
        ret.setLastLogin(playerRecord.getLastLogin());
        ret.setSacrificePoints(playerRecord.getSacrificePoints());
        ret.setDailyShiniesFought(playerRecord.getDailyShiniesFought());
        ret.setRole(playerRecord.getRole());
        ret.setHermitStage(playerRecord.getHermitStage());
        ret.setHermitStageCurrentWins(playerRecord.getHermitStageCurrentWins());
        ret.setHermitStageFightingDinoz(playerRecord.getHermitStageFightingDinoz());
        ret.setAccountDinozLimit(playerRecord.getAccountDinozLimit());
        ret.setWistitiCaptured(playerRecord.getNbWistitiCaptured());
        ret.setHunterGroup(playerRecord.getHunterGroup());
        ret.setBanned(playerRecord.getBanned());
        ret.setBlocked(playerRecord.getBlocked());
        ret.setBannedPvp(playerRecord.getBannedPvp());
        ret.setBannedRaid(playerRecord.getBannedRaid());
        ret.setBannedShiny(playerRecord.getBannedShiny());
        ret.setBannedWistiti(playerRecord.getBannedWistiti());
        ret.setBannedBazar(playerRecord.getBannedBazar());
        ret.setHistorySkip(playerRecord.getHistorySkip());
        ret.setLastWistitiFight(playerRecord.getLastWistitiFight());
        ret.setLastGeneratedStore(playerRecord.getLastGeneratedStore());
        ret.setImgMode(PlayerImgModeEnum.valueOf(playerRecord.getImgMode()));
        ret.setActionMode(PlayerActionModeEnum.valueOf(playerRecord.getActionMode()));
        ret.setHelpToggle(playerRecord.getHelptoggle());
        ret.setHasClaimedTradeFromFishermanToday(playerRecord.getHasclaimedtradefromfishermantoday());
        ret.setLastPirateBuy(playerRecord.getLastpiratebuy());

        try {
            ret.setUnlockedDinoz(playerRecord.getUnlockedDinoz() == null ? null : objectMapper.readValue(playerRecord.getUnlockedDinoz().data(), List.class));
        } catch(Exception e) {
            ret.setUnlockedDinoz(null);
        }
        try {
            ret.setBlackList(playerRecord.getBlackList() == null ? null : objectMapper.readValue(playerRecord.getBlackList().data(), HashMap.class));
        } catch(Exception e) {
            ret.setBlackList(null);
        }

        return ret;
    }

    private PlayerDinozRecord getPlayerDinozRecord(String accountId, String dinozId, int rank) {
        PlayerDinozRecord ret = jooqContext.newRecord(PLAYER_DINOZ);
        ret.setPlayerId(UUID.fromString(accountId));
        ret.setDinozId(dinozId);
        ret.setRank(rank);
        return ret;
    }
}