package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.PlayerDinozStore;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PgPlayerDinozStoreRepository {
    Optional<PlayerDinozStore> findById(UUID id);

    List<PlayerDinozStore> findAllByPlayerId(UUID playerId);

    void create(PlayerDinozStore playerDinozStore);

    void deleteByPlayerId(UUID playerId);
}
