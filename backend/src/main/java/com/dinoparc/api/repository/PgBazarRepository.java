package com.dinoparc.api.repository;

import com.dinoparc.api.domain.bazar.BazarListing;
import com.dinoparc.api.domain.misc.Pagination;

import java.util.List;
import java.util.Optional;

public interface PgBazarRepository {

    Optional<BazarListing> update(BazarListing todoEntry);

    void save(BazarListing bazarListing);

    Pagination<BazarListing> list(String accountId, Integer limit, Integer offset, String sort, boolean active);

    List<BazarListing> findActiveByLastBiderId(String id);

    List<BazarListing> findAllActive();

    List<BazarListing> findLastCompleted();

    Optional<BazarListing> findById(String listingId);

    void deleteById(String listingId);

    Optional<BazarListing> getNextActive();

    void unactive(BazarListing bazarListing);

    Boolean playerHasAtLeastOneActiveListingWithDarkPotion(String playerId);
}
