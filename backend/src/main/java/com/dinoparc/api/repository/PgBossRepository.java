package com.dinoparc.api.repository;

import com.dinoparc.api.domain.dinoz.EventDinozDto;
import com.dinoparc.api.domain.dinoz.EventDinozUpdate;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PgBossRepository {
    public EventDinozDto getRaidBoss();

    EventDinozDto updateRaidBoss(EventDinozUpdate eventDinozUpdate);

    void resetRaidBoss(Integer placeNumber, Integer dayOfWeek, Integer hourOfDay,
           String appearanceCode, int fightCount, int fire, int wood, int water, int thunder, int air,
           int life, int bonusFire, int bonusWood, int bonusWater,
           int bonusThunder, int bonusAir, int bonusClaws, String firstElement, String secondElement, String thirdElement);

    void reviveRaidBoss(Integer hp, Integer charms);

    void setArmyAttacking(UUID raidBossId, boolean armyAttacking);

    EventDinozDto decrementBossRemainingArmyAttacks(UUID raidBossId);

    Optional<EventDinozDto> getBoss(UUID bossId);

    void setRemainingArmyAttacks(UUID raidBossId, int remainingNbAttacks);

    List<EventDinozDto> findByPlaceNumberAndDayOfWeek(int placeNumber, int dayOfWeek);
}
