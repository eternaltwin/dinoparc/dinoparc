package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.PlayerDinozStore;
import com.dinoparc.tables.records.PlayerDinozStoreRecord;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jooq.DSLContext;
import org.jooq.JSONB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

import static com.dinoparc.tables.PlayerDinozStore.PLAYER_DINOZ_STORE;

@Repository
public class JooqPlayerDinozStoreRepository implements PgPlayerDinozStoreRepository {

    private DSLContext jooqContext;
    private ObjectMapper objectMapper;

    @Autowired
    public JooqPlayerDinozStoreRepository(DSLContext jooqContext, ObjectMapper objectMapper) {
        this.jooqContext = jooqContext;
        this.objectMapper = objectMapper;
    }

    @Override
    public Optional<PlayerDinozStore> findById(UUID id) {
        return jooqContext.selectFrom(PLAYER_DINOZ_STORE).where(PLAYER_DINOZ_STORE.ID.eq(id)).fetchOptional().map(this::fromRecord);
    }

    @Override
    public List<PlayerDinozStore> findAllByPlayerId(UUID playerId) {
        return jooqContext.selectFrom(PLAYER_DINOZ_STORE).where(PLAYER_DINOZ_STORE.PLAYER_ID.eq(playerId)).orderBy(PLAYER_DINOZ_STORE.RANK).fetch().map(this::fromRecord);
    }

    @Override
    public void create(PlayerDinozStore playerDinozStore) {
        jooqContext.insertInto(PLAYER_DINOZ_STORE).set(toRecord(playerDinozStore)).execute();
    }

    @Override
    public void deleteByPlayerId(UUID playerId) {
        jooqContext.deleteFrom(PLAYER_DINOZ_STORE).where(PLAYER_DINOZ_STORE.PLAYER_ID.eq(playerId)).execute();
    }

    private PlayerDinozStoreRecord toRecord(PlayerDinozStore playerDinozStore) {
        PlayerDinozStoreRecord ret = jooqContext.newRecord(PLAYER_DINOZ_STORE);
        ret.setId(playerDinozStore.getId());
        ret.setPlayerId(playerDinozStore.getPlayerId());
        ret.setRace(playerDinozStore.getRace());
        ret.setAppearanceCode(playerDinozStore.getAppearanceCode());
        ret.setPrice(playerDinozStore.getPrice());
        ret.setRank(playerDinozStore.getRank());

        try {
            var newSkillsMap = JSONB.valueOf(objectMapper.writeValueAsString(playerDinozStore.getInitialCompList()));
            ret.setSkillsMap(newSkillsMap);
        } catch(Exception e) {
            // Error in skillsMap create/update
        }

        try {
            var newElementValues = JSONB.valueOf(objectMapper.writeValueAsString(playerDinozStore.getInitialElementsValues()));
            ret.setElementsValues(newElementValues);
        } catch(Exception e) {
            // Error in black list create/update
        }

        return ret;
    }

    private PlayerDinozStore fromRecord(PlayerDinozStoreRecord playerDinozStoreRecord) {
        PlayerDinozStore ret = new PlayerDinozStore();
        ret.setId(playerDinozStoreRecord.getId());
        ret.setPlayerId(playerDinozStoreRecord.getPlayerId());
        ret.setRace(playerDinozStoreRecord.getRace());
        ret.setAppearanceCode(playerDinozStoreRecord.getAppearanceCode());
        ret.setPrice(playerDinozStoreRecord.getPrice());
        ret.setRank(playerDinozStoreRecord.getRank());

        try {
            ret.setInitialCompList(playerDinozStoreRecord.getSkillsMap() == null ? null : objectMapper.readValue(playerDinozStoreRecord.getSkillsMap().data(), Map.class));
        } catch(Exception e) {
            ret.setInitialCompList(null);
        }
        try {
            ret.setInitialElementsValues(playerDinozStoreRecord.getElementsValues() == null ? null : objectMapper.readValue(playerDinozStoreRecord.getElementsValues().data(), HashMap.class));
        } catch(Exception e) {
            ret.setInitialElementsValues(null);
        }

        return ret;
    }
}
