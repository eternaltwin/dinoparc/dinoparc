package com.dinoparc.api.repository;

import static com.dinoparc.tables.History.HISTORY;
import static com.dinoparc.tables.News.NEWS;
import com.dinoparc.tables.records.NewsRecord;
import com.dinoparc.api.domain.account.News;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public class JooqNewsRepository implements PgNewsRepository {

    private DSLContext jooqContext;

    @Autowired
    public JooqNewsRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }

    @Override
    public News create(News news) {
        return jooqContext.insertInto(NEWS).set(toRecord(news)).returning().fetchOneInto(News.class);
    }

    @Override
    public void updateNewsLabels(String newsId, News news) {
        jooqContext.update(NEWS)
                .set(NEWS.IMAGE, news.getImage())
                .set(NEWS.TITLE_FR, news.getTitleFr())
                .set(NEWS.DATE_FR, news.getDateFr())
                .set(NEWS.TEXT_FR, news.getTextFr())
                .set(NEWS.TITLE_EN, news.getTitleEn())
                .set(NEWS.DATE_EN, news.getDateEn())
                .set(NEWS.TEXT_EN, news.getTextEn())
                .set(NEWS.TITLE_ES, news.getTitleEs())
                .set(NEWS.DATE_ES, news.getDateEs())
                .set(NEWS.TEXT_ES, news.getTextEs())
                .where(NEWS.ID.eq(UUID.fromString(newsId))).execute();
    }

    @Override
    public List<News> findAll() {
        return jooqContext.selectFrom(NEWS).orderBy(NEWS.DATE_NEWS.desc()).fetchInto(News.class);
    }

    private NewsRecord toRecord(News news) {
        var ret = jooqContext.newRecord(NEWS, news);
        ret.set(NEWS.ID, UUID.randomUUID());
        return ret;
    }
}
