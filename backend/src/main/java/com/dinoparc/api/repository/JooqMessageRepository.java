package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.Message;
import com.dinoparc.tables.records.MessageRecord;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.UUID;

import static com.dinoparc.tables.Message.MESSAGE;

@Repository
public class JooqMessageRepository implements PgMessageRepository {

    private DSLContext jooqContext;

    @Autowired
    public JooqMessageRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }

    @Override
    public List<Message> findByAccountDestinationOrAccountSender(String accountId) {
        var uuid = UUID.fromString(accountId);
        return jooqContext.selectFrom(MESSAGE).where(MESSAGE.ACCOUNT_DESTINATION.eq(uuid).or(MESSAGE.ACCOUNT_SENDER.eq(uuid)))
                .fetch().map(this::fromRecord);
    }

    @Override
    public List<Message> findByAccountDestinationAndSeenAndAccountSender(String accountId, boolean seen, String senderAccountId) {
        var uuidDest = UUID.fromString(accountId);
        var uuidSender = UUID.fromString(senderAccountId);
        return jooqContext.selectFrom(MESSAGE).where(MESSAGE.ACCOUNT_DESTINATION.eq(uuidDest).and(MESSAGE.ACCOUNT_SENDER.eq(uuidSender)).and(MESSAGE.SEEN.eq(seen)))
                .fetch().map(this::fromRecord);
    }

    @Override
    public void deleteByAccountDestination(String accountId) {
        jooqContext.deleteFrom(MESSAGE).where(MESSAGE.ACCOUNT_DESTINATION.eq(UUID.fromString(accountId))).execute();
    }

    @Override
    public List<Message> findByAccountDestinationAndAccountSender(String accountId, String destinationId) {
        var uuidDest = UUID.fromString(destinationId);
        var uuidSender = UUID.fromString(accountId);
        return jooqContext.selectFrom(MESSAGE).where(MESSAGE.ACCOUNT_DESTINATION.eq(uuidDest).and(MESSAGE.ACCOUNT_SENDER.eq(uuidSender))).fetch().map(this::fromRecord);
    }

    @Override
    public void save(Message message) {
        jooqContext.insertInto(MESSAGE).set(toRecord(message)).execute();
    }

    @Override
    public void setPlayerSeen(Message message) {
        jooqContext.update(MESSAGE).set(MESSAGE.SEEN, true).where(MESSAGE.ID.eq(UUID.fromString(message.getId()))).execute();
    }

    private MessageRecord toRecord(Message message) {
        var ret = jooqContext.newRecord(MESSAGE, message);
        ret.set(MESSAGE.ID, UUID.randomUUID());
        ret.set(MESSAGE.NUMERICAL_DATE, OffsetDateTime.now(ZoneId.of("Europe/Paris")));
        return ret;
    }

    private Message fromRecord(MessageRecord record) {
        Message ret = new Message();

        ret.setId(record.getId().toString());
        ret.setSeen(record.getSeen());
        ret.setAccountDestination(record.getAccountDestination().toString());
        ret.setAccountSender(record.getAccountSender().toString());
        ret.setBody(record.getBody());
        ret.setNumericalDate(record.getNumericalDate());

        return ret;
    }
}
