package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.ClientInformationBis;
import com.dinoparc.api.domain.account.ClientInformationBisDetail;
import com.dinoparc.tables.records.ClientInformationBisRecord;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jooq.DSLContext;
import org.jooq.JSONB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.UUID;

import static com.dinoparc.tables.ClientInformationBis.CLIENT_INFORMATION_BIS;

@Repository
public class JooqClientInformationBisRepository implements PgClientInformationBisRepository {

    private DSLContext jooqContext;
    private ObjectMapper objectMapper;

    @Autowired
    public JooqClientInformationBisRepository(DSLContext jooqContext, ObjectMapper objectMapper) {
        this.jooqContext = jooqContext;
        this.objectMapper = objectMapper;
    }

    @Override
    public void incrementRank(String accountId) {
        jooqContext.update(CLIENT_INFORMATION_BIS)
                .set(CLIENT_INFORMATION_BIS.RANK, CLIENT_INFORMATION_BIS.RANK.add(1))
                .where(CLIENT_INFORMATION_BIS.PLAYER_ID.eq(UUID.fromString(accountId)))
                .execute();
    }

    @Override
    public void cleanUpAccount(String accountId) {
        jooqContext.deleteFrom(CLIENT_INFORMATION_BIS)
                .where(CLIENT_INFORMATION_BIS.PLAYER_ID.eq(UUID.fromString(accountId)))
                .execute();
    }

    @Override
    public void cleanUp() {
        jooqContext.deleteFrom(CLIENT_INFORMATION_BIS)
                .where(CLIENT_INFORMATION_BIS.RANK.ge(2000))
                .or(CLIENT_INFORMATION_BIS.RETRIEVED_DATE.le(OffsetDateTime.now().minusDays(3)))
                .execute();
    }

    @Override
    public void addClientInformationBis(ClientInformationBis toInsert) {
        jooqContext.insertInto(CLIENT_INFORMATION_BIS).set(toRecord(toInsert)).execute();
    }

    private ClientInformationBisRecord toRecord(ClientInformationBis toInsert) {
        ClientInformationBisRecord ret = new ClientInformationBisRecord();
        ret.setPlayerId(toInsert.getPlayerId());
        ret.setDinozId(toInsert.getDinozId());
        ret.setRank(toInsert.getRank());
        ret.setFirst(toInsert.getFirst());
        ret.setFourth(toInsert.getFourth());
        ret.setRetrievedDate(toInsert.getRetrievedDate());

        try {
            ret.setSkipInfo(this.getJsonbSkipInfos(toInsert.getSkipInfo()));
        } catch(Exception e) {
            return null;
        }

        return ret;
    }

    private JSONB getJsonbSkipInfos(ClientInformationBisDetail skipInfo) {
        try {
            return JSONB.valueOf(objectMapper.writeValueAsString(skipInfo == null ? new HashMap<String, Object>() : skipInfo));
        } catch(Exception e) {
            return null;
        }
    }
}
