package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.DinoparcConstants;
import com.dinoparc.api.domain.dinoz.ArmyDinoz;
import com.dinoparc.api.domain.dinoz.ArmyDinozDto;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.dinoparc.tables.ArmyDinoz.ARMY_DINOZ;
import static com.dinoparc.tables.Dinoz.DINOZ;

@Repository
public class JooqArmyDinozRepository implements PgArmyDinozRepository {

    private DSLContext jooqContext;

    @Autowired
    public JooqArmyDinozRepository(DSLContext jooqContext) {
        this.jooqContext = jooqContext;
    }

    @Override
    public Optional<ArmyDinoz> getNextArmyDinoz(UUID bossId) {
        return jooqContext.selectFrom(ARMY_DINOZ)
            .where(ARMY_DINOZ.BOSS_ID.eq(bossId))
            .and(ARMY_DINOZ.NB_ATTACKS.lessThan(ARMY_DINOZ.MAX_NB_ATTACKS))
            .orderBy(ARMY_DINOZ.NB_ATTACKS, ARMY_DINOZ.RANK)
            .limit(1)
            .fetchOptionalInto(ArmyDinoz.class);
    }

    @Override
    public Optional<ArmyDinoz> findById(UUID armyDinozId) {
        return jooqContext.selectFrom(ARMY_DINOZ)
                .where(ARMY_DINOZ.ARMY_DINOZ_ID.eq(armyDinozId))
                .fetchOptionalInto(ArmyDinoz.class);
    }

    @Override
    public void deleteArmyDinozAndUpdateRanks(UUID armyDinozId, UUID bossId, int removedRank) {
        jooqContext.deleteFrom(ARMY_DINOZ)
                .where(ARMY_DINOZ.ARMY_DINOZ_ID.eq(armyDinozId))
                .execute();

        jooqContext.update(ARMY_DINOZ)
                .set(ARMY_DINOZ.RANK, ARMY_DINOZ.RANK.sub(1))
                .where(ARMY_DINOZ.BOSS_ID.eq(bossId))
                .and(ARMY_DINOZ.RANK.ge(removedRank))
                .execute();
    }

    @Override
    public void updateMaxNbAttacks(UUID armyDinozId, int maxNbAttacks) {
        jooqContext.update(ARMY_DINOZ).set(ARMY_DINOZ.MAX_NB_ATTACKS, ARMY_DINOZ.NB_ATTACKS).where(ARMY_DINOZ.ARMY_DINOZ_ID.eq(armyDinozId)).execute();
    }

    @Override
    public void addFightAndBossLifeLost(UUID armyDinozId, Integer eventDinozLifeLoss) {
        jooqContext.update(ARMY_DINOZ)
                .set(ARMY_DINOZ.NB_ATTACKS, ARMY_DINOZ.NB_ATTACKS.add(1))
                .set(ARMY_DINOZ.BOSS_LIFE_LOST, ARMY_DINOZ.BOSS_LIFE_LOST.add(eventDinozLifeLoss))
                .where(ARMY_DINOZ.ARMY_DINOZ_ID.eq(armyDinozId)).execute();
    }

    @Override
    public Optional<ArmyDinoz> getArmyDinozFromMaster(UUID bossId, UUID masterId) {
        return jooqContext.selectFrom(ARMY_DINOZ)
                .where(ARMY_DINOZ.BOSS_ID.eq(bossId))
                .and(ARMY_DINOZ.MASTER_ID.eq(masterId))
                .limit(1)
                .fetchOptionalInto(ArmyDinoz.class);
    }

    @Override
    public List<ArmyDinoz> getArmyDinozsFromMaster(UUID masterId) {
        return jooqContext.selectFrom(ARMY_DINOZ)
                .where(ARMY_DINOZ.MASTER_ID.eq(masterId))
                .fetchInto(ArmyDinoz.class);
    }

    @Override
    public Optional<ArmyDinoz> getArmyDinoz(String dinozId) {
        return jooqContext.selectFrom(ARMY_DINOZ)
                .where(ARMY_DINOZ.DINOZ_ID.eq(dinozId))
                .limit(1)
                .fetchOptionalInto(ArmyDinoz.class);
    }

    @Override
    public Optional<ArmyDinoz> getArmyDinoz(UUID bossId, String dinozId) {
        return jooqContext.selectFrom(ARMY_DINOZ)
            .where(ARMY_DINOZ.BOSS_ID.eq(bossId))
            .and(ARMY_DINOZ.DINOZ_ID.eq(dinozId))
            .limit(1)
            .fetchOptionalInto(ArmyDinoz.class);
    }

    @Override
    public int getNextRank(UUID bossId) {
        Optional<Integer> optMaxRank = jooqContext.select(DSL.max(ARMY_DINOZ.RANK))
                .from(ARMY_DINOZ)
                .where(ARMY_DINOZ.BOSS_ID.eq(bossId))
                .fetchOptionalInto(Integer.class);

        if (optMaxRank.isEmpty()) {
            return 1;
        }

        return optMaxRank.get().intValue() + 1;
    }

    @Override
    public ArmyDinoz createArmyDinoz(ArmyDinoz armyDinoz) {
        return jooqContext.insertInto(ARMY_DINOZ).set(jooqContext.newRecord(ARMY_DINOZ, armyDinoz)).returning().fetchOneInto(ArmyDinoz.class);
    }

    @Override
    public List<ArmyDinozDto> getArmyDinozsDto(UUID bossId) {
        return jooqContext.select(ARMY_DINOZ.ARMY_DINOZ_ID, DINOZ.MASTER_NAME.as("master_name"), DINOZ.NAME.as("dinoz_name"),
                        ARMY_DINOZ.RANK, ARMY_DINOZ.NB_ATTACKS, ARMY_DINOZ.MAX_NB_ATTACKS, ARMY_DINOZ.BOSS_LIFE_LOST)
                .from(ARMY_DINOZ)
                .join(DINOZ).on(ARMY_DINOZ.DINOZ_ID.eq(DINOZ.ID))
                .where(ARMY_DINOZ.BOSS_ID.eq(bossId))
                .orderBy(ARMY_DINOZ.BOSS_LIFE_LOST.desc(), ARMY_DINOZ.RANK)
                .fetchInto(ArmyDinozDto.class);
    }

    @Override
    public void cleanAll() {
        jooqContext.deleteFrom(ARMY_DINOZ).execute();
    }

    @Override
    public void cleanAllForBoss(UUID bossId) {
        jooqContext.deleteFrom(ARMY_DINOZ).where(ARMY_DINOZ.BOSS_ID.eq(bossId)).execute();
    }

    @Override
    public boolean isInArmy(String dinozId) {
        return jooqContext.selectCount()
                .from(ARMY_DINOZ)
                .where(ARMY_DINOZ.DINOZ_ID.eq(dinozId))
                .fetchOneInto(Integer.class)
                > 0;
    }
}
