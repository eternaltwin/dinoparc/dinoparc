package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.Message;

import java.util.List;

public interface PgMessageRepository {

    List<Message> findByAccountDestinationOrAccountSender(String accountId);

    List<Message> findByAccountDestinationAndSeenAndAccountSender(String accountId, boolean seen, String senderAccountId);

    void deleteByAccountDestination(String accountId);

    List<Message> findByAccountDestinationAndAccountSender(String accountId, String threadId);

    void save(Message message);

    void setPlayerSeen(Message message);
}
