package com.dinoparc.api.repository;

import com.dinoparc.api.domain.account.RoleActionHistory;

public interface PgRoleActionRepository {
    public void create(RoleActionHistory newRoleActionHistory);
}
