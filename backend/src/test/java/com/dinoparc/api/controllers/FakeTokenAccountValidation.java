package com.dinoparc.api.controllers;

import com.dinoparc.api.domain.account.Player;
import com.dinoparc.api.domain.misc.TokenAccountValidation;
import com.dinoparc.api.repository.PgPlayerRepository;

import java.util.List;

public class FakeTokenAccountValidation extends TokenAccountValidation {

    public FakeTokenAccountValidation(PgPlayerRepository playerRepository) {
        super(playerRepository);
    }

    @Override
    public void validate(Player player, String token, List<String> roles) {
        return;
    }
}
