# Neoparc Backend

This is the backend server for the Neoparc project.

## API Documentation

The API documentation can be accessed at:
- `/swagger-ui/index.html` for the human-readable version
- `/v3/api-docs` for the JSON version

This documentation is handled by [Springdoc](https://springdoc.org/).

## Requirements

- Java 17 or later
- Postgres 15 or later

## Gradle tasks

This project uses Gradle tasks. Use `./gradlew <task>` to execute a task.

### `run`

Compiles and runs the project.

### `flywayMigrate`

Update the database schema to the latest version

### `flywayClean`

Drop everything in the database!

## Gradle

Neoparc use the "Gradle Wrapper" setup for Gradle. It can be updated with the following
command. Replace `NEW_VERSION` with [the version you want](https://gradle.org/releases/).

```
./gradlew wrapper --gradle-version=NEW_VERSION --distribution-type=bin
```
